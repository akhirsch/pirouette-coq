Require Export LocalLang.
Require Export Locations.
Require Import LocationMap.
Require Export SyncLabels.

Require Import Coq.Arith.Arith.
Require Import Coq.Lists.List.
Require Import Permutation.
Require Import Coq.Classes.RelationClasses.
Require Import Coq.Program.Equality.
Require Import Coq.Lists.SetoidList.

Import ListNotations.


  (* 
     The control language. I use capital `E` for control-language expressions, and 
     lower-case `e` for local expressions.
   *)
Module CtrlLang (Import LL : LocalLang) (L : Locations) (LM : LocationMap L) (Import SL : SyncLabels).

  Module LMF := (LocationMapFacts L LM).

  Definition Loc := L.t.

  Inductive CtrlExpr : Set :=
  | Var (n : nat) : CtrlExpr
  | Unit : CtrlExpr
  | Ret (e : Expr) : CtrlExpr
  | If (e : Expr) (E1 E2 : CtrlExpr) : CtrlExpr
  | Send (ℓ : Loc) (e : Expr) (E : CtrlExpr) : CtrlExpr
  | Recv (ℓ : Loc) (E : CtrlExpr) : CtrlExpr
  | Choose (ℓ : Loc) (s : SyncLabel) (E : CtrlExpr) : CtrlExpr
  | AllowChoiceL (ℓ : Loc) (E : CtrlExpr) : CtrlExpr
  | AllowChoiceR (ℓ : Loc) (E : CtrlExpr) : CtrlExpr
  | AllowChoiceLR (ℓ : Loc) (E1 E2 : CtrlExpr) : CtrlExpr
  | LetRet (E1 E2 : CtrlExpr) : CtrlExpr
  | FunLocal (E : CtrlExpr) : CtrlExpr (* E is the body *)
  | AppLocal (E :CtrlExpr) (e : Expr) : CtrlExpr
  | FunGlobal (E : CtrlExpr) : CtrlExpr
  | AppGlobal (E1 E2 : CtrlExpr) : CtrlExpr.
  Global Hint Constructors CtrlExpr : CtrlExpr.

  Definition CtrlExprEqDec : forall E1 E2 : CtrlExpr, {E1 = E2} + {E1 <> E2}.
  Proof using.
    decide equality;
      try (first [apply Nat.eq_dec | apply ExprEqDec | apply ChorEqDec | apply L.eq_dec]).
    destruct s; destruct s0; try (left; auto; fail); try (right; intro H'; inversion H').
  Defined.

  Inductive CtrlExprVal : CtrlExpr -> Prop :=
    RetVal : forall v, ExprVal v -> CtrlExprVal (Ret v)
  | UnitVal : CtrlExprVal Unit
  | FunLocalVal : forall B, CtrlExprVal (FunLocal B)
  | FunGlobalVal : forall B, CtrlExprVal (FunGlobal B).
  Global Hint Constructors CtrlExprVal : CtrlExpr.

  (* CLOSEDNESS (ABOVE) *)

  (*
    Like with Pirouette programs, there are two types of variable to worry about. Unlike
    with Pirouette programs, we do not have to worry about different owners of local 
    variables, since programs are written from the point-of-view of one owner.

    n - the global bound
    m - the local bound
   *)
  Fixpoint CtrlExprClosedAbove (E : CtrlExpr) (n m : nat) : Prop :=
    match E with
    | Var x => x < n
    | Unit => True
    | Ret e => ExprClosedAbove m e
    | If e E1 E2 => ExprClosedAbove m e /\
                   CtrlExprClosedAbove E1 n m /\
                   CtrlExprClosedAbove E2 n m
    | Send l e E => ExprClosedAbove m e /\
                   CtrlExprClosedAbove E n m
    | Recv l E => CtrlExprClosedAbove E n (S m)
    | Choose l d E => CtrlExprClosedAbove E n m
    | AllowChoiceL l E => CtrlExprClosedAbove E n m
    | AllowChoiceR l E => CtrlExprClosedAbove E n m
    | AllowChoiceLR l E1 E2 =>
      CtrlExprClosedAbove E1 n m
      /\ CtrlExprClosedAbove E2 n m
    | LetRet E1 E2 =>
      CtrlExprClosedAbove E1 n m
      /\ CtrlExprClosedAbove E2 n (S m)
    | FunLocal E => CtrlExprClosedAbove E (S n) (S m)
    | AppLocal E1 e => CtrlExprClosedAbove E1 n m /\ ExprClosedAbove m e
    | FunGlobal E => CtrlExprClosedAbove E (S (S n)) m
    | AppGlobal E1 E2 => CtrlExprClosedAbove E1 n m /\
                        CtrlExprClosedAbove E2 n m
    end.
  
  Lemma CtrlExprClosedAboveMono : forall E a b c d,
      a <= c -> b <= d -> CtrlExprClosedAbove E a b -> CtrlExprClosedAbove E c d.
  Proof using.
    intro E; induction E; intros a b c d a_le_c b_le_d clsd; cbn in *;
      repeat match goal with
             | [ H : _ /\ _ |- _ ] => destruct H
             | [|- _ /\_ ] => split
             | [H : ?a <= ?b, H' : ExprClosedAbove ?a ?e |- ExprClosedAbove ?b ?e ] =>
               apply Nat.lt_eq_cases in H; destruct H; subst; auto;
                 apply ExprClosedAboveMono with (m := a); auto
             | [IH : forall a b c d, a <= c -> b <= d ->
                                CtrlExprClosedAbove ?E a b -> CtrlExprClosedAbove ?E c d,
                  H : ?a <= ?c, H' : ?b <= ?d, H'' : CtrlExprClosedAbove ?E ?a ?b
                                             |- CtrlExprClosedAbove ?E ?c ?d ] =>
               apply (IH a b c d H H' H'')
             | [H : ?a <= ?b |- context[S ?b]] =>
               lazymatch goal with
               | [_ : S a <= S b |- _ ] => fail
               | _ => assert (S a <= S b) by (apply le_n_S in H; exact H)
               end
             end; auto.
    apply lt_le_trans with (m := a); auto.
  Qed.

  (* RENAMING *)
  
  Definition UpRename : (nat -> nat) -> nat -> nat := fun ξ n => match n with
                                                  | 0 => 0
                                                  | S n => S (ξ n)
                                                  end.

  Definition IdRenaming := fun n : nat => n.

  Lemma UpIdRenaming : forall n, IdRenaming n = UpRename IdRenaming n.
  Proof using.
    intro n; destruct n; unfold IdRenaming; cbn; reflexivity.
  Qed.
  Lemma UpRenameExt : forall ξ1 ξ2, (forall n, ξ1 n = ξ2 n) -> forall n, UpRename ξ1 n = UpRename ξ2 n.
  Proof using.
    intros ξ1 ξ2 ext_eq n; destruct n; cbn; auto.
  Qed.

  Lemma UpMono : forall ξ, (forall k l, ξ k < ξ l -> k < l) ->
                       forall k l, UpRename ξ k < UpRename ξ l -> k < l.
  Proof using.
    intros ξ mono' k l k_lt_l; unfold UpRename in k_lt_l.
    destruct k; destruct l.
    1,3: inversion k_lt_l.
    apply Nat.lt_0_succ.
    apply lt_n_S; apply lt_S_n in k_lt_l; apply mono'; auto.
  Qed.

  
  Fixpoint CtrlExprLocalRename (E : CtrlExpr) (ξ : nat -> nat) :=
    match E with
    | Var x => Var x
    | Unit => Unit
    | Ret e => Ret (e ⟨e| ξ⟩)
    | If e E1 E2 => If (e ⟨e| ξ⟩) (CtrlExprLocalRename E1 ξ) (CtrlExprLocalRename E2 ξ)
    | Send l e E => Send l (e ⟨e| ξ⟩) (CtrlExprLocalRename E ξ)
    | Recv l E => Recv l (CtrlExprLocalRename E (UpRename ξ))
    | Choose l LR E => Choose l LR (CtrlExprLocalRename E ξ)
    | AllowChoiceL l E => AllowChoiceL l (CtrlExprLocalRename E ξ)
    | AllowChoiceR l E => AllowChoiceR l (CtrlExprLocalRename E ξ)
    | AllowChoiceLR l E1 E2 =>
      AllowChoiceLR l (CtrlExprLocalRename E1 ξ) (CtrlExprLocalRename E2 ξ)
    | LetRet E1 E2 => LetRet (CtrlExprLocalRename E1 ξ)(CtrlExprLocalRename E2 (UpRename ξ))
    | FunLocal E => FunLocal (CtrlExprLocalRename E (UpRename ξ))
    | AppLocal E e => AppLocal (CtrlExprLocalRename E ξ) (e ⟨e| ξ⟩)
    | FunGlobal E => FunGlobal (CtrlExprLocalRename E ξ)
    | AppGlobal E1 E2 => AppGlobal (CtrlExprLocalRename E1 ξ) (CtrlExprLocalRename E2 ξ)
    end.
  Notation "E ⟨cel| x ⟩" := (CtrlExprLocalRename E x) (at level 20).

  Lemma CtrlExprLocalRenameExt : forall E ξ1 ξ2, (forall n, ξ1 n = ξ2 n) -> E ⟨cel| ξ1⟩ = E ⟨cel| ξ2⟩.
  Proof using.
    intro E; induction E; intros ξ1 ξ2 ext_eq; cbn; auto.
    all: (repeat rewrite ExprRenameExt with (ξ2 := ξ2); auto).
    all: try (erewrite IHE1; eauto; erewrite IHE2; eauto; fail).
    all: try (erewrite IHE; eauto; fail).
    - erewrite IHE; [reflexivity|]. apply UpRenameExt; auto.
    - erewrite IHE1; [|auto]. erewrite IHE2; [reflexivity|]. apply UpRenameExt; auto.
    - erewrite IHE; [reflexivity|]. apply UpRenameExt; auto.
  Qed.

  Lemma CtrlExprLocalIdRename : forall E, E ⟨cel| IdRenaming⟩ = E.
  Proof using.
    intro E; induction E; cbn; auto.
    all: try rewrite ExprIdRenamingSpec; auto.
    all: try rewrite IHE1; try rewrite IHE2; try rewrite IHE; auto.
    1,3: rewrite CtrlExprLocalRenameExt with (ξ2 := IdRenaming); [rewrite IHE; reflexivity|];
      intro n; symmetry; apply UpIdRenaming.
    rewrite CtrlExprLocalRenameExt with (ξ2 := IdRenaming); [rewrite IHE2; reflexivity|].
    intro n; symmetry; apply UpIdRenaming.
  Qed.

  Lemma CtrlExprRenameFusion : forall E ξ1 ξ2, (E ⟨cel| ξ1⟩) ⟨cel| ξ2⟩ = E ⟨cel| fun n => ξ2 (ξ1 n)⟩.
  Proof using.
    intro E; induction E; intros ξ1 ξ2; cbn; auto.
    all: try rewrite ExprRenameFusion; try rewrite IHE;
      try rewrite IHE1; try rewrite IHE2; auto.
    1,3: erewrite CtrlExprLocalRenameExt; eauto.
    3: erewrite CtrlExprLocalRenameExt with (ξ1 := fun n => UpRename ξ2 (UpRename ξ1 n)); eauto.
    all: intro n; destruct n; cbn; auto.
  Qed.

  Fixpoint CtrlExprGlobalRename (E : CtrlExpr) (ξ : nat -> nat) : CtrlExpr :=
    match E with
    | Var x => Var (ξ x)
    | Unit => Unit
    | Ret e => Ret e
    | If e E1 E2 => If e (CtrlExprGlobalRename E1 ξ) (CtrlExprGlobalRename E2 ξ)
    | Send l e E => Send l e (CtrlExprGlobalRename E ξ)
    | Recv l E => Recv l (CtrlExprGlobalRename E ξ)
    | Choose l LR E => Choose l LR (CtrlExprGlobalRename E ξ)
    | AllowChoiceL l E => AllowChoiceL l (CtrlExprGlobalRename E ξ)
    | AllowChoiceR l E => AllowChoiceR l (CtrlExprGlobalRename E ξ)
    | AllowChoiceLR l E1 E2 =>
      AllowChoiceLR l (CtrlExprGlobalRename E1 ξ) (CtrlExprGlobalRename E2 ξ)
    | LetRet E1 E2 =>
      LetRet (CtrlExprGlobalRename E1 ξ) (CtrlExprGlobalRename E2 ξ)
    | FunLocal E => FunLocal (CtrlExprGlobalRename E (UpRename ξ)) (* f is a global var *)
    | AppLocal E1 e => AppLocal (CtrlExprGlobalRename E1 ξ) e
    | FunGlobal E => FunGlobal (CtrlExprGlobalRename E (UpRename (UpRename ξ)))
                                                   (* f and X are both global *) 
    | AppGlobal E1 E2 => AppGlobal (CtrlExprGlobalRename E1 ξ) (CtrlExprGlobalRename E2 ξ)
    end.
  Notation "E ⟨ceg| x ⟩" := (CtrlExprGlobalRename E x) (at level 20).
  

  Theorem CtrlExprGlobalRenameExt : forall E ξ1 ξ2,
      (forall n, ξ1 n = ξ2 n) -> E ⟨ceg| ξ1⟩ = E ⟨ceg| ξ2⟩.
  Proof using.
    intro E; induction E; intros ξ1 ξ2 ext_eq; cbn; auto.
    all: try (erewrite IHE1; eauto; erewrite IHE2; eauto; fail).
    all: try (erewrite IHE; eauto; fail).
    - erewrite IHE; [reflexivity|]; apply UpRenameExt; auto.
    - erewrite IHE; [reflexivity|]. apply UpRenameExt; apply UpRenameExt; auto.
  Qed.  

  Theorem CtrlExprGlobalIdRename : forall E, E ⟨ceg| IdRenaming⟩ = E.
  Proof using.
    intros E; induction E; cbn; auto.
    all: try rewrite IHE; try rewrite IHE1; try rewrite IHE2; auto.
    all: erewrite CtrlExprGlobalRenameExt; [rewrite IHE; reflexivity|].
    all: intro n; symmetry.
    2: rewrite UpRenameExt with (ξ2 := IdRenaming); [| intro m; symmetry].
    all: apply UpIdRenaming.
  Qed.

  Theorem CtrlExprGlobalRenameFusion :
    forall E ξ1 ξ2, (E ⟨ceg| ξ1⟩) ⟨ceg| ξ2⟩ = E ⟨ceg| fun n => ξ2 (ξ1 n)⟩.
  Proof using.
    intro E; induction E; intros ξ1 ξ2; cbn; auto.
    all: try rewrite IHE; try rewrite IHE1; try rewrite IHE2; auto.
    all: erewrite CtrlExprGlobalRenameExt; [reflexivity|].
    all: intro n; destruct n; cbn; auto; destruct n; cbn; auto.
  Qed.

  Lemma CtrlExprClosedAboveGlobalRenaming : forall E ξ n m,
      CtrlExprClosedAbove E n m -> (forall k l, k < l -> ξ k < ξ l) ->
      CtrlExprClosedAbove (E ⟨ceg| ξ⟩) (ξ n) m.
  Proof using.
    intro E; induction E; try rename n into x; intros ξ n m clsd mono; cbn in *;
      repeat match goal with
             | [ H : _ /\ _ |- _] => destruct H
             | [|- _ /\ _ ] => split
             end; auto.
    - apply IHE with (ξ := UpRename ξ) in clsd; auto.
      intros k l k_lt_l; unfold UpRename.
      destruct k. all: destruct l; [inversion k_lt_l|].
      apply Nat.lt_0_succ. apply lt_S_n in k_lt_l. apply lt_n_S. apply mono; auto.
    - apply IHE with (ξ := UpRename (UpRename ξ)) in clsd; auto.
      unfold UpRename.
      intros k l k_lt_l; destruct k.
      all: destruct l; [inversion k_lt_l|].
      apply Nat.lt_0_succ.
      destruct k. apply lt_n_S. destruct l.
      apply lt_S_n in k_lt_l; inversion k_lt_l. apply Nat.lt_0_succ.
      destruct l. apply lt_S_n in k_lt_l; inversion k_lt_l.
      repeat apply lt_S_n in k_lt_l; repeat apply lt_n_S; auto.
  Qed.

    Lemma CtrlExprClosedAboveGlobalRenaming' : forall E ξ n m,
      CtrlExprClosedAbove (E ⟨ceg| ξ⟩) (ξ n) m -> (forall k l, ξ k < ξ l -> k < l) ->
      CtrlExprClosedAbove E n m.
  Proof using.
    intro E; induction E; try rename n into x; intros ξ n m clsd mono; cbn in *;
      repeat match goal with
             | [ H : _ /\ _ |- _] => destruct H
             | [|- _ /\ _ ] => split
             | [ IH : forall ξ n m, CtrlExprClosedAbove (?E1 ⟨ceg|ξ⟩) (ξ n) m ->
                                    (forall k l, ξ k < ξ l -> k < l) -> CtrlExprClosedAbove ?E1 n m,
                   H : CtrlExprClosedAbove (?E1 ⟨ceg|?ξ⟩) (?ξ ?n) ?m,
                   H' : forall k l, ?ξ k < ?ξ l -> k < l
                                    |- CtrlExprClosedAbove ?E1 ?n ?m ] =>
               apply (IH ξ n m H H')
             end; auto.
    
    - apply IHE with (ξ := UpRename ξ); cbn; auto.
      apply UpMono; auto.
    - apply IHE with (ξ := UpRename (UpRename ξ)); cbn; auto.
      apply UpMono. apply UpMono. auto.
  Qed.

  (* SUBSTITUTION *)

  Fixpoint CtrlExprLocalSubst (E : CtrlExpr) (σ : nat -> Expr) :=
    match E with
    | Var x => Var x
    | Unit => Unit
    | Ret e => Ret (e [e| σ])
    | If e E1 E2 => If (e [e| σ]) (CtrlExprLocalSubst E1 σ) (CtrlExprLocalSubst E2 σ)
    | Send l e E => Send l (e [e| σ]) (CtrlExprLocalSubst E σ)
    | Recv l E => Recv l (CtrlExprLocalSubst E (ExprUpSubst σ))
    | Choose l LR E => Choose l LR (CtrlExprLocalSubst E σ)
    | AllowChoiceL l E => AllowChoiceL l (CtrlExprLocalSubst E σ)
    | AllowChoiceR l E => AllowChoiceR l (CtrlExprLocalSubst E σ)
    | AllowChoiceLR l E1 E2 =>
      AllowChoiceLR l (CtrlExprLocalSubst E1 σ) (CtrlExprLocalSubst E2 σ)
    | LetRet E1 E2 =>
      LetRet (CtrlExprLocalSubst E1 σ) (CtrlExprLocalSubst E2 (ExprUpSubst σ))
    | FunLocal E => FunLocal (CtrlExprLocalSubst E (ExprUpSubst σ))
    | AppLocal E e => AppLocal (CtrlExprLocalSubst E σ) (e [e| σ])
    | FunGlobal E => FunGlobal (CtrlExprLocalSubst E σ)
    | AppGlobal E1 E2 => AppGlobal (CtrlExprLocalSubst E1 σ) (CtrlExprLocalSubst E2 σ)
    end.
  Notation "E [cel| s ]" := (CtrlExprLocalSubst E s) (at level 20).

  Lemma CtrlExprLocalSubstExt : forall E σ1 σ2, (forall n, σ1 n = σ2 n) -> E [cel| σ1] = E [cel| σ2].
  Proof using.
    intro E; induction E; cbn; intros σ1 σ2 ext_eq; auto.
    all: try erewrite ExprSubstExt; eauto.
    all: try erewrite IHE1; eauto; try erewrite IHE2; eauto; try erewrite IHE; eauto.
    all: intro n; destruct n; cbn; auto; rewrite ext_eq; reflexivity.
  Qed.

  Lemma CtrlExprLocalIdSubst : forall E, E [cel| ExprIdSubst] = E.
  Proof using.
    intro E; induction E; cbn; auto.
    all: try rewrite ExprIdentitySubstSpec; try rewrite IHE;
      try rewrite IHE1; try rewrite IHE2; auto.
    1,3: erewrite CtrlExprLocalSubstExt; [rewrite IHE; reflexivity|].
    3: erewrite CtrlExprLocalSubstExt; [rewrite IHE2; reflexivity|].
    all: intro n; symmetry; apply ExprUpSubstId.
  Qed.

  Theorem CtrlExprLocalRenameSpec : forall E ξ, E ⟨cel| ξ⟩ = E [cel| (fun n => ExprVar (ξ  n))].
  Proof using.
    intro E; induction E; cbn; intro ξ; auto.
    all: try rewrite ExprRenameSpec; try rewrite IHE;
      try rewrite IHE1; try rewrite IHE2; auto.
    1,3: erewrite CtrlExprLocalSubstExt; [reflexivity|].
    3: erewrite CtrlExprLocalSubstExt with (σ1 := fun n => ExprVar (UpRename ξ n));
      [reflexivity|].
    all: intro n; destruct n; cbn; auto; rewrite ExprRenameVar; reflexivity.
  Qed.

  Lemma CtrlExprClosedAbove_LocalSubst : forall E σ n m,
      CtrlExprClosedAbove E n m ->
      (forall k, k < m -> σ k = ExprVar k) ->
      E [cel| σ] = E.
  Proof using.
    intro E; induction E; try rename n into x; intros σ n m cb σ_var; cbn in *; auto;
      repeat match goal with
             | [ H : _ /\ _ |- _ ] => destruct H
             end.
    1,2,3,11: rewrite ExprClosedAboveSubst with (n := m); auto.
    all: try (try erewrite IHE; eauto; try erewrite IHE1; eauto; try erewrite IHE2; eauto;
              fail).
    1: rewrite (IHE (ExprUpSubst σ) n (S m)); auto.
    2: rewrite (IHE1 σ n m); auto; rewrite (IHE2 (ExprUpSubst σ) n (S m)); auto.
    3: rewrite (IHE (ExprUpSubst σ) (S n) (S m)); auto.
    all: unfold ExprUpSubst; intros k lt_k_Sm; destruct k; auto;
      apply lt_S_n in lt_k_Sm; rewrite σ_var; cbn; auto;
        rewrite ExprRenameVar; reflexivity.
  Qed.                                          

  Lemma CtrlExprValLocalSubst : forall E σ, CtrlExprVal E -> CtrlExprVal (E [cel| σ]).
  Proof using.
    intros E σ val; inversion val; subst; cbn; constructor.
    rewrite ExprClosedSubst; auto. apply ExprValuesClosed; auto.
  Qed.
  
  Definition GlobalUpSubst : (nat -> CtrlExpr) -> nat -> CtrlExpr :=
    fun f n => match n with
            | 0 => Var 0
            | S n => (f n) ⟨ceg| S⟩
            end.

  Definition GlobalIdSubst : nat -> CtrlExpr := Var.

  Lemma GlobalUpIdSubst : forall n, GlobalUpSubst GlobalIdSubst n = GlobalIdSubst n.
  Proof using.
    intro n; destruct n; cbn; unfold GlobalIdSubst; reflexivity.
  Qed.

  Lemma GlobalUpSubstExt : forall σ1 σ2, (forall n, σ1 n = σ2 n) ->
                                    forall n, GlobalUpSubst σ1 n = GlobalUpSubst σ2 n.
  Proof using.
    intros σ1 σ2 ext_eq n; destruct n; cbn; auto; rewrite ext_eq; auto.
  Qed.

  Fixpoint CtrlExprGlobalSubst (E : CtrlExpr) (σ : nat -> CtrlExpr) :=
    match E with
    | Var x => σ x
    | Ret e => Ret e
    | Unit => Unit
    | If e E1 E2 => If e (CtrlExprGlobalSubst E1 σ) (CtrlExprGlobalSubst E2 σ)
    | Send l e E => Send l e (CtrlExprGlobalSubst E σ)
    | Recv l E => Recv l (CtrlExprGlobalSubst E (fun n => σ n ⟨cel| S⟩))
    | Choose l LR E => Choose l LR (CtrlExprGlobalSubst E σ)
    | AllowChoiceL l E => AllowChoiceL l (CtrlExprGlobalSubst E σ)
    | AllowChoiceR l E => AllowChoiceR l (CtrlExprGlobalSubst E σ)
    | AllowChoiceLR l E1 E2 =>
      AllowChoiceLR l (CtrlExprGlobalSubst E1 σ) (CtrlExprGlobalSubst E2 σ)
    | LetRet E1 E2 => LetRet (CtrlExprGlobalSubst E1 σ) (CtrlExprGlobalSubst E2 (fun n => σ n ⟨cel| S⟩))
    | FunLocal E => FunLocal (CtrlExprGlobalSubst E (GlobalUpSubst (fun n => σ n ⟨cel| S⟩))) (* f is global *)
    | AppLocal E e => AppLocal (CtrlExprGlobalSubst E σ) e
    | FunGlobal E => FunGlobal (CtrlExprGlobalSubst E (GlobalUpSubst (GlobalUpSubst σ)))
                              (* f, X are both global *)
    | AppGlobal E1 E2 => AppGlobal (CtrlExprGlobalSubst E1 σ) (CtrlExprGlobalSubst E2 σ)
    end.
  Notation "E [ceg| s ]" := (CtrlExprGlobalSubst E s) (at level 20).

  Lemma CtrlExprGlobalSubstExt : forall E σ1 σ2, (forall n, σ1 n = σ2 n) -> E [ceg| σ1] = E [ceg| σ2].
  Proof using.
    intro E; induction E; cbn; intros σ1 σ2 ext_eq; auto.
    all: try erewrite IHE1; eauto; try erewrite IHE2; eauto.
    all: try erewrite IHE; eauto.
    all: destruct n; cbn; auto; try rewrite ext_eq; auto. destruct n; cbn; auto.
    rewrite ext_eq; reflexivity.
  Qed.

  Lemma GlobalIdSubstSpec : forall E, E [ceg| GlobalIdSubst ] = E.
  Proof using.
    intro E; induction E; cbn; auto.
    all: try (fold (GlobalIdSubst)).
    all: try rewrite IHE; try rewrite IHE1; try rewrite IHE2; auto.
    2: rewrite CtrlExprGlobalSubstExt with (σ2 := GlobalIdSubst); [rewrite IHE2|]; auto.
    all: rewrite CtrlExprGlobalSubstExt with (σ2 := GlobalIdSubst); [rewrite IHE|]; auto.  
    apply GlobalUpIdSubst.
    intro n; rewrite GlobalUpSubstExt with (σ2 := GlobalIdSubst); apply GlobalUpIdSubst.
  Qed.

  Lemma CtrlExprGlobalRenameSubstFusion : forall E ξ σ,
      (E ⟨ceg| ξ⟩) [ceg| σ] = E [ceg| fun n => σ (ξ n)].
  Proof using.
    intro E; induction E; cbn; intros ξ σ; auto.
    all: try rewrite IHE; try rewrite IHE1; try rewrite IHE2; auto.
    all: apply f_equal.
    all: apply CtrlExprGlobalSubstExt.
    all: intro n; unfold GlobalUpSubst; unfold UpRename; cbn; auto.
    all: destruct n; auto.
    destruct n; auto.
  Qed.    

  Lemma LocalSubstGlobalRenameComm : forall E σ ξ, (E [cel| σ]) ⟨ceg| ξ⟩ = (E ⟨ceg| ξ⟩) [cel|σ].
  Proof using.
    intro E; induction E; cbn; intros σ ξ; auto.
    all: try rewrite IHE1; try rewrite IHE; try rewrite IHE2; auto.
  Qed.
  
  Lemma GlobalRenameSpec : forall E ξ, E ⟨ceg| ξ⟩ = E [ceg| fun n => Var (ξ n)].
  Proof using.
    intro E; induction E; cbn; intro ξ; auto.
    all: try rewrite IHE; try rewrite IHE1; try rewrite IHE2; auto.
    all: erewrite CtrlExprGlobalSubstExt; [reflexivity|].
    all: intro n; destruct n; cbn; auto; destruct n; cbn; auto.
  Qed.

  Lemma CtrlExprGlobalSubstRenameFusion : forall E σ ξ,
      (E [ceg| σ]) ⟨ceg| ξ⟩ = E [ceg| fun n => (σ n) ⟨ceg| ξ⟩].
  Proof using.
    intro E; induction E; intros σ ξ; cbn; auto.
    all: try rewrite IHE; try rewrite IHE1; try rewrite IHE2; auto.
    all: apply f_equal; apply CtrlExprGlobalSubstExt; intro n.
    1,2: repeat rewrite CtrlExprLocalRenameSpec; rewrite LocalSubstGlobalRenameComm; auto.
    all: repeat unfold GlobalUpSubst; repeat unfold UpRename; destruct n; auto.
    2: destruct n; auto.
    all: rewrite CtrlExprGlobalRenameFusion.
    1,2: repeat rewrite CtrlExprLocalRenameSpec.
    - repeat rewrite LocalSubstGlobalRenameComm.
      rewrite CtrlExprGlobalRenameFusion; reflexivity.
    - repeat rewrite CtrlExprGlobalRenameFusion; reflexivity.
  Qed.

  Lemma CtrlExprClosedAbove_GlobalSubst : forall E σ n m,
      CtrlExprClosedAbove E n m ->
      (forall k, k < n -> σ k = Var k) ->
      E [ceg| σ] = E.
  Proof using.
    intros E; induction E; try rename n into x; intros σ n m clsd σ_clsd; cbn; auto;
      cbn in *; repeat match goal with
                       | [ H : _ /\ _ |- _ ] => destruct H
                       end.
    all: try (erewrite IHE; eauto; fail).
    all: try (erewrite IHE1; eauto; erewrite IHE2; eauto; fail).
    - rewrite IHE with (n := n) (m := S m); auto.
      intros k k_lt_n; rewrite σ_clsd; auto.
    - rewrite IHE1 with (n := n) (m := m); auto.
      rewrite IHE2 with (n := n) (m := S m); auto.
      intros k k_lt_n; rewrite σ_clsd; auto.
    - rewrite IHE with (n := S n) (m := S m); auto.
      intros k k_lt_Sn; unfold GlobalUpSubst; destruct k; auto.
      apply lt_S_n in k_lt_Sn; rewrite σ_clsd; auto.
    - rewrite IHE with (n := S (S n)) (m := m); auto.
      intros k k_lt_Sn; unfold GlobalUpSubst; destruct k; auto; destruct k; auto.
      repeat apply lt_S_n in k_lt_Sn; rewrite σ_clsd; auto.
  Qed.
  
  Lemma CtrlExprValGlobalSubst : forall E σ, CtrlExprVal E -> CtrlExprVal (E [ceg| σ]).
  Proof using.
    intros E; induction E; intros σ val; inversion val; subst; cbn; constructor; auto.
  Qed.


  (* MERGING *)

  (* 
     Merging is one of the most-important operations we define on control expressions. When
     projecting the program for ℓ from an if controlled by ℓ', we project out both branches
     and then merge them together.

     The idea is that we insist on everything being the same until we find a left choice 
     on one side and a right choice on the other side. We then combine these into a LR 
     choice.

     We first define this as a trinary relation, and then as a binary function. These are
     equivalent, but the inductive relation is sometimes nice to work with.
   *)
  Inductive CtrlExprMergeRel : CtrlExpr -> CtrlExpr -> CtrlExpr -> Prop :=
  | MergeVar : forall n, CtrlExprMergeRel (Var n) (Var n) (Var n)
  | MergeUnit : CtrlExprMergeRel Unit Unit Unit
  | MergeRet : forall e, CtrlExprMergeRel (Ret e) (Ret e) (Ret e)
  | MergeIf : forall e E11 E12 E21 E22 E1 E2,
      CtrlExprMergeRel E11 E21 E1 ->
      CtrlExprMergeRel E12 E22 E2 ->
      CtrlExprMergeRel (If e E11 E12) (If e E21 E22) (If e E1 E2)
  | MergeSend : forall l e E1 E2 E,
      CtrlExprMergeRel E1 E2 E ->
      CtrlExprMergeRel (Send l e E1) (Send l e E2) (Send l e E)
  | MergeRecv : forall l E1 E2 E,
      CtrlExprMergeRel E1 E2 E ->
      CtrlExprMergeRel (Recv l E1) (Recv l E2) (Recv l E)
  | MergeChoose : forall l LR E1 E2 E,
      CtrlExprMergeRel E1 E2 E ->
      CtrlExprMergeRel (Choose l LR E1) (Choose l LR E2) (Choose l LR E)
  | MergeAllowChoiceLL : forall l E1 E2 E,
      CtrlExprMergeRel E1 E2 E ->
      CtrlExprMergeRel (AllowChoiceL l E1) (AllowChoiceL l E2) (AllowChoiceL l E)
  | MergeAllowChoiceLR : forall l E1 E2,
      CtrlExprMergeRel (AllowChoiceL l E1) (AllowChoiceR l E2) (AllowChoiceLR l E1 E2)
  | MergeAllowChoiceLLR : forall l E11 E12 E1 E2,
      CtrlExprMergeRel E11 E12 E1 ->
      CtrlExprMergeRel (AllowChoiceL l E11) (AllowChoiceLR l E12 E2) (AllowChoiceLR l E1 E2)
  | MergeAllowChoiceRL : forall l E1 E2,
      CtrlExprMergeRel (AllowChoiceR l E1) (AllowChoiceL l E2) (AllowChoiceLR l E2 E1)
  | MergeAllowChoiceRR : forall l E1 E2 E,
      CtrlExprMergeRel E1 E2 E ->
      CtrlExprMergeRel (AllowChoiceR l E1) (AllowChoiceR l E2) (AllowChoiceR l E)
  | MergeAllowChoiceRLR : forall l E21 E1 E22 E2,
      CtrlExprMergeRel E21 E22 E2 ->
      CtrlExprMergeRel (AllowChoiceR l E21) (AllowChoiceLR l E1 E22) (AllowChoiceLR l E1 E2)
  | MergeAllowChoiceLRL : forall l E11 E2 E12 E1,
      CtrlExprMergeRel E11 E12 E1 ->
      CtrlExprMergeRel (AllowChoiceLR l E11 E2) (AllowChoiceL l E12) (AllowChoiceLR l E1 E2)
  | MergeAllowChoiceLRR : forall l E1 E21 E22 E2,
      CtrlExprMergeRel E21 E22 E2 ->
      CtrlExprMergeRel (AllowChoiceLR l E1 E21) (AllowChoiceR l E22) (AllowChoiceLR l E1 E2)
  | MergeAllowChoiceLRLR : forall l E11 E21 E12 E22 E1 E2,
      CtrlExprMergeRel E11 E12 E1 ->
      CtrlExprMergeRel E21 E22 E2 ->
      CtrlExprMergeRel (AllowChoiceLR l E11 E21) (AllowChoiceLR l E12 E22)
                       (AllowChoiceLR l E1 E2)
  | MergeLetRet : forall E11 E21 E12 E22 E1 E2,
      CtrlExprMergeRel E11 E12 E1 ->
      CtrlExprMergeRel E21 E22 E2 ->
      CtrlExprMergeRel (LetRet E11 E21) (LetRet E12 E22) (LetRet E1 E2)
  | MergeFunLocal : forall E,
      CtrlExprMergeRel (FunLocal E) (FunLocal E) (FunLocal E)
  | MergeAppLocal : forall E1 E2 E e,
      CtrlExprMergeRel E1 E2 E ->
      CtrlExprMergeRel (AppLocal E1 e) (AppLocal E2 e) (AppLocal E e)
  | MergeFunGlobal : forall E,
      CtrlExprMergeRel (FunGlobal E) (FunGlobal E) (FunGlobal E)
  | MergeAppGlobal : forall E11 E21 E12 E22 E1 E2,
      CtrlExprMergeRel E11 E21 E1 ->
      CtrlExprMergeRel E12 E22 E2 ->
      CtrlExprMergeRel (AppGlobal E11 E12) (AppGlobal E21 E22) (AppGlobal E1 E2).
  Global Hint Constructors CtrlExprMergeRel : CtrlExpr.

  Fixpoint CtrlExprMerge (E1 E2 : CtrlExpr) : option CtrlExpr :=
    match E1 with
    | Var x =>
      match E2 with
      | Var y => if Nat.eq_dec x y
                then Some (Var x)
                else None
      | _ => None
      end
    | Unit =>
      match E2 with
      | Unit => Some Unit
      | _ => None
      end
    | Ret e1 =>
      match E2 with
      | Ret e2 => if ExprEqDec e1 e2
                 then Some (Ret e1)
                 else None
      | _ => None
      end
    | If e1 E11 E12 =>
      match E2 with
      | If e2 E21 E22 =>
        if ExprEqDec e1 e2
        then match CtrlExprMerge E11 E21, CtrlExprMerge E12 E22 with
             | Some E1', Some E2' => Some (If e1 E1' E2')
             | _, _ => None
             end
        else None
      | _ => None
      end
    | Send l1 e1 E1 =>
      match E2 with
      | Send l2 e2 E2 =>
        if L.eq_dec l1 l2
        then if ExprEqDec e1 e2
             then match CtrlExprMerge E1 E2 with
                  | Some E => Some (Send l1 e1 E)
                  | None => None
                  end
             else None
        else None
      | _ => None
      end
    | Recv l1 E1 =>
      match E2 with
      | Recv l2 E2 =>
        if L.eq_dec l1 l2
        then match CtrlExprMerge E1 E2 with
             | Some E => Some (Recv l1 E)
             | None => None
             end
        else None
      | _ => None
      end
    | Choose l1 LR1 E1 =>
      match E2 with
      | Choose l2 LR2 E2 =>
        if L.eq_dec l1 l2
        then if SyncLabelEqDec LR1 LR2
             then match CtrlExprMerge E1 E2 with
                  | Some E => Some (Choose l1 LR1 E)
                  | None => None
                  end
             else None
        else None
      | _ => None
      end
    | AllowChoiceL l1 E1 =>
      match E2 with
      | AllowChoiceL l2 E2 =>
        if L.eq_dec l1 l2
        then match CtrlExprMerge E1 E2 with
             | Some E => Some (AllowChoiceL l1 E)
             | None => None
             end
        else None
      | AllowChoiceR l2 E2 =>
        if L.eq_dec l1 l2
        then Some (AllowChoiceLR l1 E1 E2)
        else None
      | AllowChoiceLR l2 E21 E22 =>
        if L.eq_dec l1 l2
        then match CtrlExprMerge E1 E21 with
             | Some E => Some (AllowChoiceLR l1 E E22)
             | None => None
             end
        else None
      | _ => None
      end
    | AllowChoiceR l1 E1 =>
      match E2 with
      | AllowChoiceL l2 E2 =>
        if L.eq_dec l1 l2
        then Some (AllowChoiceLR l1 E2 E1)
        else None
      | AllowChoiceR l2 E2 =>
        if L.eq_dec l1 l2
        then match CtrlExprMerge E1 E2 with
             | Some E => Some (AllowChoiceR l1 E)
             | None => None
             end
        else None
      | AllowChoiceLR l2 E21 E22 =>
        if L.eq_dec l1 l2
        then match CtrlExprMerge E1 E22 with
             | Some E => Some (AllowChoiceLR l1 E21 E)
             | None => None
             end
        else None
      | _ => None
      end
    | AllowChoiceLR l1 E11 E12 =>
      match E2 with
      | AllowChoiceL l2 E2 =>
        if L.eq_dec l1 l2
        then match CtrlExprMerge E11 E2 with
             | Some E => Some (AllowChoiceLR l1 E E12)
             | None => None
             end
        else None
      | AllowChoiceR l2 E2 =>
        if L.eq_dec l1 l2
        then match CtrlExprMerge E12 E2 with
             | Some E => Some (AllowChoiceLR l1 E11 E)
             | None => None
             end
        else None
      | AllowChoiceLR l2 E21 E22 =>
        if L.eq_dec l1 l2
        then match CtrlExprMerge E11 E21, CtrlExprMerge E12 E22 with
             | Some E1, Some E2 => Some (AllowChoiceLR l1 E1 E2)
             | _, _ => None
             end
        else None
      | _ => None
      end
    | LetRet E11 E12 =>
      match E2 with
      | LetRet E21 E22 =>
        match CtrlExprMerge E11 E21, CtrlExprMerge E12 E22 with
        | Some E1, Some E2 => Some (LetRet E1 E2)
        | _, _ => None
        end
      | _ => None
      end
    | FunLocal E1 =>
      match E2 with
      | FunLocal E2 =>
        if CtrlExprEqDec E1 E2
        then Some (FunLocal E1)
        else None
      | _ => None
      end
    | AppLocal E1 e1 =>
      match E2 with
      | AppLocal E2 e2 =>
        if ExprEqDec e1 e2
        then match CtrlExprMerge E1 E2 with
             | Some E => Some (AppLocal E e1)
             | None => None
             end
        else None
      | _ => None
      end
    | FunGlobal E1 =>
      match E2 with
      | FunGlobal E2 =>
        if CtrlExprEqDec E1 E2
        then Some (FunGlobal E1)
        else None
      | _ => None
      end
    | AppGlobal E11 E12 =>
      match E2 with
      | AppGlobal E21 E22 =>
        match CtrlExprMerge E11 E21, CtrlExprMerge E12 E22 with
        | Some E1, Some E2 => Some (AppGlobal E1 E2)
        | _, _ => None
        end
      | _ => None
      end
    end.

  (*
    Equivalence of the two forms of merging is given below.
   *)
  Lemma CtrlExprMergeRelSpec1 : forall E1 E2 E, CtrlExprMergeRel E1 E2 E ->
                                          CtrlExprMerge E1 E2 = Some E.
  Proof using.
    intros E1 E2 E R; induction R; cbn; auto;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ |- context[Nat.eq_dec ?n ?n] ] =>
               let neq := fresh in
               destruct (Nat.eq_dec n n) as [_|neq]; [| destruct (neq eq_refl)]
             | [ |- context[ExprEqDec ?e ?e] ] =>
               let neq := fresh in
               destruct (ExprEqDec e e) as [_|neq]; [| destruct (neq eq_refl)]
             | [ |- context[L.eq_dec ?l ?l] ] =>
               let neq := fresh in
               destruct (L.eq_dec l l) as [_|neq]; [| destruct (neq eq_refl)]
             | [ |- context[SyncLabelEqDec ?c ?c] ] =>
               let neq := fresh in
               destruct (SyncLabelEqDec c c) as [_|neq]; [| destruct (neq eq_refl)]
             | [ |- context[CtrlExprEqDec ?E ?E] ] =>
               let neq := fresh in
               destruct (CtrlExprEqDec E E) as [_|neq]; [| destruct (neq eq_refl)]
             | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E |- context[CtrlExprMerge ?E1 ?E2]] =>
               rewrite H
             end.
  Qed.

  Lemma CtrlExprMergeRelSpec2 : forall E1 E2 E, CtrlExprMerge E1 E2 = Some E ->
                                          CtrlExprMergeRel E1 E2 E.
  Proof using.
    intro E1; induction E1; intros E2 E eq; destruct E2; cbn in *;
      repeat match goal with
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [ H : Some _ = None |- _ ] => inversion H
             | [ H : None = Some _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H
             | [ H : context[Nat.eq_dec ?n ?m] |- _ ] =>
               destruct (Nat.eq_dec n m); subst
             | [ H : context[ExprEqDec ?e1 ?e2] |- _ ] =>
               destruct (ExprEqDec e1 e2); subst
             | [ H : context[L.eq_dec ?l1 ?l2] |- _ ] =>
               destruct (L.eq_dec l1 l2); subst
             | [ H : context[SyncLabelEqDec ?c1 ?c2] |- _ ] =>
               destruct (SyncLabelEqDec c1 c2); subst
             | [ H : context[CtrlExprEqDec ?E1 ?E2] |- _ ] =>
               destruct (CtrlExprEqDec E1 E2); subst
             | [ H : context[CtrlExprMerge ?E1 ?E2 ] |- _ ] =>
               lazymatch type of H with
               | CtrlExprMerge E1 E2 = _ => fail
               | _ => lazymatch goal with
                     | [ eq : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite eq in H
                     | _ => let eq := fresh "eq" in destruct (CtrlExprMerge E1 E2) eqn:eq
                     end
               end

             end; try (econstructor; eauto).
  Qed.

  Corollary CtrlExprMergeRelUnique : forall E1 E2 E3 E4,
      CtrlExprMergeRel E1 E2 E3 -> CtrlExprMergeRel E1 E2 E4 -> E3 = E4.
  Proof using.
    intros E1 E2 E3 E4 H H0. apply CtrlExprMergeRelSpec1 in H.
    apply CtrlExprMergeRelSpec1 in H0. rewrite H in H0; inversion H0; auto.
  Qed.

  (* PROPERTIES OF MERGING *)
  
  (*
    Merging is idempotent, commutative, and associative. Since merging is partial,
    we have to show associativity both when merging is defined and when it is not defined.
   *)

  Lemma MergeIdempotent : forall E, CtrlExprMerge E E = Some E.
  Proof using.
    intro E; induction E; cbn; auto;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ |- context[Nat.eq_dec ?a ?a]] =>
               let n := fresh "n" in 
               destruct (Nat.eq_dec a a) as [_ | n]; [| destruct (n eq_refl)]
             | [ |- context[L.eq_dec ?a ?a]] =>
               let n := fresh "n" in 
               destruct (L.eq_dec a a) as [_ | n]; [| destruct (n eq_refl)]
             | [ |- context[ExprEqDec ?a ?a]] =>
               let n := fresh "n" in 
               destruct (ExprEqDec a a) as [_ | n]; [| destruct (n eq_refl)]
             | [ |- context[CtrlExprEqDec ?a ?a]] =>
               let n := fresh "n" in 
               destruct (CtrlExprEqDec a a) as [_ | n]; [| destruct (n eq_refl)]
             | [ |- context[SyncLabelEqDec ?a ?a]] =>
               let n := fresh "n" in 
               destruct (SyncLabelEqDec a a) as [_ | n]; [| destruct (n eq_refl)]
             | [ H : CtrlExprMerge ?a ?b = _ |- context[CtrlExprMerge ?a ?b]] => rewrite H
             end.
  Qed.
  
  Corollary MergeRelDiag : forall E, CtrlExprMergeRel E E E.
  Proof using.
    intro E; apply CtrlExprMergeRelSpec2; apply MergeIdempotent.
  Qed.

  Lemma MergeComm : forall E1 E2, CtrlExprMerge E1 E2 = CtrlExprMerge E2 E1.
  Proof using.
    intro E1; induction E1; intro E2; destruct E2; cbn; auto;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [|- context[Nat.eq_dec ?a ?b]] => destruct (Nat.eq_dec a b); subst
             | [|- context[ExprEqDec ?a ?b]] => destruct (ExprEqDec a b); subst
             | [|- context[CtrlExprEqDec ?a ?b]] => destruct (CtrlExprEqDec a b); subst
             | [|- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
             | [|- context[SyncLabelEqDec ?a ?b]] => destruct (SyncLabelEqDec a b); subst
             | [IH :forall E2, CtrlExprMerge ?E1 E2 = CtrlExprMerge E2 ?E1
                          |- context[CtrlExprMerge ?E1 ?E2]] =>
               rewrite (IH E2)
             | [ |- context[CtrlExprMerge ?E1 ?E2]] =>
               lazymatch goal with
               | [ H : CtrlExprMerge E1 E2 = _ |- _] => rewrite H
               | _ => let H := fresh in
                     destruct (CtrlExprMerge E1 E2) eqn:H; cbn in *
               end
             end.
  Qed.

  Lemma MergeAssoc : forall E1 E2 E3 E4 E5,
      CtrlExprMerge E1 E2 = Some E4 -> CtrlExprMerge E2 E3 = Some E5 ->
      CtrlExprMerge E1 E5 = CtrlExprMerge E4 E3.
  Proof using.
    intros E1; induction E1; intros E2 E3 E4 E5 eq1 eq2; destruct E2; destruct E3; cbn in *.
    all:
      repeat match goal with
             | [H : None = Some _ |- _ ] => inversion H
             | [H : Some _ = None |- _ ] => inversion H
             | [H : Some _ = Some _ |- _ ] => inversion H; subst; clear H; cbn in *
             | [ |- ?a = ?a ] => reflexivity
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [|- context[Nat.eq_dec ?a ?b]] => destruct (Nat.eq_dec a b); subst
             | [|- context[ExprEqDec ?a ?b]] => destruct (ExprEqDec a b); subst
             | [|- context[CtrlExprEqDec ?a ?b]] => destruct (CtrlExprEqDec a b); subst
             | [|- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
             | [|- context[SyncLabelEqDec ?a ?b]] => destruct (SyncLabelEqDec a b); subst
             | [|- context[CtrlExprMerge ?a ?b]] =>
               lazymatch goal with
               | [ H : CtrlExprMerge a b = _ |- _ ] => rewrite H
               | _ => let H := fresh in destruct (CtrlExprMerge a b) eqn:H; cbn in *
               end
             | [H : context[Nat.eq_dec ?a ?b] |- _] => destruct (Nat.eq_dec a b); subst
             | [H : context[ExprEqDec ?a ?b] |- _] => destruct (ExprEqDec a b); subst
             | [H : context[CtrlExprEqDec ?a ?b] |- _] => destruct (CtrlExprEqDec a b); subst
             | [H : context[L.eq_dec ?a ?b] |- _] => destruct (L.eq_dec a b); subst
             | [H : context[SyncLabelEqDec ?a ?b] |- _] =>
               destruct (SyncLabelEqDec a b); subst
             | [ H1 : CtrlExprMerge ?a ?b = CtrlExprMerge ?c ?d,
                      H2 : CtrlExprMerge ?a ?b = Some ?x,
                           H3 : CtrlExprMerge ?c ?d = Some ?y |- _ ] =>
               lazymatch goal with
               | [H : x = y |- _ ] => fail
               | _ => tryif unify x y
                 then fail
                 else let H1' := fresh in
                      pose proof H1 as H1';
                        rewrite H2 in H1'; rewrite H3 in H1';
                          inversion H1'; subst; clear H1'
               end
             | [ H1 : CtrlExprMerge ?a ?b = CtrlExprMerge ?c ?d,
                      H2 : CtrlExprMerge ?a ?b = Some ?x, H3 : CtrlExprMerge ?c ?d = None
                 |- _ ] =>
               rewrite H2 in H1; rewrite H3 in H1; inversion H1
             | [ H1 : CtrlExprMerge ?a ?b = CtrlExprMerge ?c ?d,
                      H2 : CtrlExprMerge ?a ?b = None, H3 : CtrlExprMerge ?c ?d = Some _
                 |- _ ] =>
               rewrite H2 in H1; rewrite H3 in H1; inversion H1
             | [H : context[CtrlExprMerge ?a ?b] |- _ ] =>
               lazymatch type of H with
               | CtrlExprMerge _ _ = _ => fail
               | _ =>
                 lazymatch goal with
                 | [ H' : CtrlExprMerge a b = ?c |- _ ] => rewrite H' in H
                 | _ => let H' := fresh in destruct (CtrlExprMerge a b) eqn:H'; cbn in *
                 end
               end
             | [IH : forall E2 E3 E4 E5,
                   CtrlExprMerge ?E1 E2 = Some E4 ->
                   CtrlExprMerge E2 E3 = Some E5 ->
                   CtrlExprMerge ?E1 E5 = CtrlExprMerge E4 E3,
                  H : CtrlExprMerge ?E1 ?E2 = Some ?E4,
                  H' : CtrlExprMerge ?E2 ?E3 = Some ?E5 |- _ ]=>
               lazymatch goal with
               | [ _ : CtrlExprMerge E1 E5 = CtrlExprMerge E4 E3 |- _ ] => fail
               | _ => pose proof (IH E2 E3 E4 E5 H H')
               end
             end.
  Qed.

  Lemma MergeAssocNone : forall E1 E2 E3 E4,
      CtrlExprMerge E1 E2 = None -> CtrlExprMerge E2 E3 = Some E4 ->
      CtrlExprMerge E1 E4 = None.
  Proof using.
    intros E1; induction E1; intros E2 E3 E4 eq1 eq2; destruct E2; destruct E3; cbn in *.
    all: repeat match goal with
             | [H : None = Some _ |- _ ] => inversion H
             | [H : Some _ = None |- _ ] => inversion H
             | [H : Some _ = Some _ |- _ ] => inversion H; subst; clear H; cbn in *
             | [H1 : ?a = Some _, H2 : ?a = None |- _ ] =>
               rewrite H2 in H1; inversion H1
             | [ |- ?a = ?a ] => reflexivity
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [|- context[Nat.eq_dec ?a ?b]] => destruct (Nat.eq_dec a b); subst
             | [|- context[ExprEqDec ?a ?b]] => destruct (ExprEqDec a b); subst
             | [|- context[CtrlExprEqDec ?a ?b]] => destruct (CtrlExprEqDec a b); subst
             | [|- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
             | [|- context[SyncLabelEqDec ?a ?b]] => destruct (SyncLabelEqDec a b); subst
             | [|- context[CtrlExprMerge ?a ?b]] =>
               lazymatch goal with
               | [ H : CtrlExprMerge a b = _ |- _ ] => rewrite H
               | _ => let H := fresh in destruct (CtrlExprMerge a b) eqn:H; cbn in *
               end
             | [H : context[Nat.eq_dec ?a ?b] |- _] => destruct (Nat.eq_dec a b); subst
             | [H : context[ExprEqDec ?a ?b] |- _] => destruct (ExprEqDec a b); subst
             | [H : context[CtrlExprEqDec ?a ?b] |- _] => destruct (CtrlExprEqDec a b); subst
             | [H : context[L.eq_dec ?a ?b] |- _] => destruct (L.eq_dec a b); subst
             | [H : context[SyncLabelEqDec ?a ?b] |- _] =>
               destruct (SyncLabelEqDec a b); subst
             | [ H1 : CtrlExprMerge ?a ?b = CtrlExprMerge ?c ?d,
                      H2 : CtrlExprMerge ?a ?b = Some ?x,
                           H3 : CtrlExprMerge ?c ?d = Some ?y |- _ ] =>
               lazymatch goal with
               | [H : x = y |- _ ] => fail
               | _ => tryif unify x y
                 then fail
                 else let H1' := fresh in
                      pose proof H1 as H1';
                        rewrite H2 in H1'; rewrite H3 in H1';
                          inversion H1'; subst; clear H1'
               end
             | [ H1 : CtrlExprMerge ?a ?b = CtrlExprMerge ?c ?d,
                      H2 : CtrlExprMerge ?a ?b = Some ?x,
                           H3 : CtrlExprMerge ?c ?d = None |- _ ] =>
               rewrite H2 in H1; rewrite H3 in H1; inversion H1
             | [ H1 : CtrlExprMerge ?a ?b = CtrlExprMerge ?c ?d,
                      H2 : CtrlExprMerge ?a ?b = None,
                           H3 : CtrlExprMerge ?c ?d = Some _ |- _ ] =>
               rewrite H2 in H1; rewrite H3 in H1; inversion H1
             | [H : context[CtrlExprMerge ?a ?b] |- _ ] =>
               lazymatch type of H with
               | CtrlExprMerge _ _ = _ => fail
               | _ =>
                 lazymatch goal with
                 | [ H' : CtrlExprMerge a b = ?c |- _ ] => rewrite H' in H
                 | _ => let H' := fresh in destruct (CtrlExprMerge a b) eqn:H'; cbn in *
                 end
               end
             | [ IH : forall E2 E3 E4,
                   CtrlExprMerge ?E1 E2 = None ->
                   CtrlExprMerge E2 E3 = Some E4 -> CtrlExprMerge ?E1 E4 = None,
                   H1 : CtrlExprMerge ?E1 ?E2 = None,
                   H2 : CtrlExprMerge ?E2 ?E3 = Some ?E4 |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprMerge E1 E4 = None |- _ ] => fail
               | _ => pose proof (IH E2 E3 E4 H1 H2)
               end
                end.
  Qed.

  (*
    Merging preserves most properties of the given programs. In particular, it preserves
    closedness, subsitutions, and values. Moreover, it preserves closedness on the nose---if
    the merge of two programs is closed above `n`, then both programs are closed above `n`.

    For values, we have something even stronger: the merge of two values `V₁` and `V₂` is
    only defined when `V₁ = V₂`, and then the result is always `V₁`.
   *)
  
  Lemma MergeRelClosed : forall E1 E2 E n m,
      CtrlExprMergeRel E1 E2 E ->
      CtrlExprClosedAbove E1 n m ->
      CtrlExprClosedAbove E2 n m ->
      CtrlExprClosedAbove E n m.
  Proof using.
    intros E1 E2 E n m merge; revert n m; induction merge;
      try rename n into x; intros n m clsd1 clsd2; cbn in *; 
        repeat match goal with
               | [ H : _ /\ _ |- _ ] => destruct H
               | [ |- _ /\ _ ] => split
               end; auto.
  Qed.

  Lemma MergeClosed : forall E1 E2 E n m,
      CtrlExprMerge E1 E2 = Some E ->
      CtrlExprClosedAbove E1 n m ->
      CtrlExprClosedAbove E2 n m ->
      CtrlExprClosedAbove E n m.
  Proof using.
    intros E1 E2 E n m H H0 H1. apply CtrlExprMergeRelSpec2 in H.
    eapply MergeRelClosed; eauto.
  Qed.

  Lemma UnmergeRelClosed : forall E1 E2 E n m,
      CtrlExprMergeRel E1 E2 E ->
      CtrlExprClosedAbove E n m ->
      CtrlExprClosedAbove E1 n m /\ CtrlExprClosedAbove E2 n m.
  Proof using.
    intros E1 E2 E n m merge; revert n m; induction merge;
      try rename n into x; intros n m clsd; cbn in *;
      repeat match goal with
             | [ H : _ /\ _ |- _ ] => destruct H
             | [ |- _ /\ _ ] => split
             | [IH : forall n m, CtrlExprClosedAbove ?E n m ->
                            CtrlExprClosedAbove ?E1 n m /\ CtrlExprClosedAbove ?E2 n m,
                  H : CtrlExprClosedAbove ?E ?n ?m |- _ ] =>
               specialize (IH n m H); destruct IH
             end; auto.
  Qed.

  Lemma UnmergeClosed : forall E1 E2 E n m,
      CtrlExprMerge E1 E2 = Some E ->
      CtrlExprClosedAbove E n m ->
      CtrlExprClosedAbove E1 n m /\ CtrlExprClosedAbove E2 n m.
  Proof using.
    intros E1 E2 E n m H. apply CtrlExprMergeRelSpec2 in H.
    eapply UnmergeRelClosed; eauto.
  Qed.

  Theorem MergeLocalSubst : forall E1 E2 E σ,
      CtrlExprMerge E1 E2 = Some E ->
      CtrlExprMerge (E1 [cel| σ]) (E2 [cel| σ]) = Some (E [cel| σ]).
  Proof using.
    intro E1; induction E1; intros E2 E σ eq; destruct E2; cbn in *.
    all: repeat match goal with
                | [ |- ?a = ?a ] => reflexivity
                | [ H : None = Some _ |- _ ] => inversion H
                | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H
                | [ H : context[Nat.eq_dec ?a ?b] |- _ ] => destruct (Nat.eq_dec a b); subst
                | [ H : context[ExprEqDec ?a ?b] |- _ ] => destruct (ExprEqDec a b); subst
                | [ H : context[CtrlExprEqDec ?a ?b] |- _ ] =>
                  destruct (CtrlExprEqDec a b); subst
                | [ H : context[L.eq_dec ?a ?b] |- _ ] => destruct (L.eq_dec a b); subst
                | [ H : context[SyncLabelEqDec ?a ?b] |- _ ] =>
                  destruct (SyncLabelEqDec a b); subst
                | [ H : context[CtrlExprMerge ?E1 ?E2] |- _ ] =>
                  lazymatch type of H with
                  | CtrlExprMerge E1 E2 = _ => fail
                  | _ => lazymatch goal with
                        | [ H' : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite H' in H
                        | _ => let eq := fresh "eq" in
                              destruct (CtrlExprMerge E1 E2) eqn:eq; cbn in *
                        end
                  end
                | [ |- context[ExprEqDec ?a ?b] ] =>
                  tryif unify a b
                  then let n := fresh "n" in
                       destruct (ExprEqDec a b) as [_ | n];
                         [| destruct (n eq_refl)]
                  else lazymatch goal with
                       | [ e : a = b |- _ ] =>
                         let n := fresh "n" in 
                         destruct (ExprEqDec a b) as [_ | n ]; [| destruct (n e)]
                       | [ e : b = a |- _ ] =>
                         let n := fresh "n" in 
                         destruct (ExprEqDec a b) as [_ | n ]; [| destruct (n (eq_sym e))]
                       | [ n : a <> b |- _ ] =>
                         let e := fresh "e" in
                         destruct (ExprEqDec a b) as [e | _ ]; [destruct (n e) |]
                       | [ n : b <> a |- _ ] =>
                         let e := fresh "e" in
                         destruct (ExprEqDec a b) as [e | _ ]; [destruct (n (eq_sym e)) |]
                       end
                | [ |- context[CtrlExprEqDec ?a ?b] ] =>
                  tryif unify a b
                  then let n := fresh "n" in
                       destruct (CtrlExprEqDec a b) as [_ | n];
                         [| destruct (n eq_refl)]
                  else lazymatch goal with
                       | [ e : a = b |- _ ] =>
                         let n := fresh "n" in 
                         destruct (CtrlExprEqDec a b) as [_ | n ]; [| destruct (n e)]
                       | [ e : b = a |- _ ] =>
                         let n := fresh "n" in 
                         destruct (CtrlExprEqDec a b) as [_ | n ];
                           [| destruct (n (eq_sym e))]
                       | [ n : a <> b |- _ ] =>
                         let e := fresh "e" in
                         destruct (CtrlExprEqDec a b) as [e | _ ]; [destruct (n e) |]
                       | [ n : b <> a |- _ ] =>
                         let e := fresh "e" in
                         destruct (CtrlExprEqDec a b) as [e | _ ];
                           [destruct (n (eq_sym e)) |]
                       end
                | [ IH : forall E2 E σ, CtrlExprMerge ?E1 E2 = Some E ->
                                   CtrlExprMerge (?E1 [cel| σ]) (E2 [cel| σ])
                                   = Some (E [cel|σ]),
                      H : CtrlExprMerge ?E1 ?E2 = Some ?E
                      |- context[CtrlExprMerge (?E1 [cel|?σ]) (?E2 [cel|?σ])] ] =>
                  rewrite (IH E2 E σ H); cbn
                end; cbn; auto.
  Qed.
  
  Theorem MergeGlobalSubst : forall E1 E2 E σ,
      CtrlExprMerge E1 E2 = Some E ->
      CtrlExprMerge (E1 [ceg| σ]) (E2 [ceg| σ]) = Some (E [ceg| σ]).
  Proof using.
    intro E1; induction E1; intros E2 E σ eq; destruct E2; cbn in *.
    all: repeat match goal with
                | [ |- ?a = ?a ] => reflexivity
                | [ H : None = Some _ |- _ ] => inversion H
                | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H; cbn
                | [ H : context[Nat.eq_dec ?a ?b] |- _ ] =>
                  destruct (Nat.eq_dec a b); subst; cbn in *
                | [ H : context[ExprEqDec ?a ?b] |- _ ] =>
                  destruct (ExprEqDec a b); subst; cbn in *
                | [ H : context[CtrlExprEqDec ?a ?b] |- _ ] =>
                  destruct (CtrlExprEqDec a b); subst; cbn in *
                | [ H : context[L.eq_dec ?a ?b] |- _ ] =>
                  destruct (L.eq_dec a b); subst; cbn
                | [ H : context[SyncLabelEqDec ?a ?b] |- _ ] =>
                  destruct (SyncLabelEqDec a b); subst; cbn in *
                | [|- context[CtrlExprMerge ?E ?E]] => rewrite MergeIdempotent; cbn in *
                | [ H : context[CtrlExprMerge ?E1 ?E2] |- _ ] =>
                  lazymatch type of H with
                  | CtrlExprMerge E1 E2 = _ => fail
                  | _ => lazymatch goal with
                        | [ H' : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite H' in H; cbn in *
                        | _ => let eq := fresh "eq" in
                              destruct (CtrlExprMerge E1 E2) eqn:eq; cbn in *
                        end
                  end
                | [ |- context[ExprEqDec ?a ?b] ] =>
                  tryif unify a b
                  then let n := fresh "n" in
                       destruct (ExprEqDec a b) as [_ | n];
                         [| destruct (n eq_refl)]
                  else lazymatch goal with
                       | [ e : a = b |- _ ] =>
                         let n := fresh "n" in 
                         destruct (ExprEqDec a b) as [_ | n ]; [| destruct (n e)]
                       | [ e : b = a |- _ ] =>
                         let n := fresh "n" in 
                         destruct (ExprEqDec a b) as [_ | n ]; [| destruct (n (eq_sym e))]
                       | [ n : a <> b |- _ ] =>
                         let e := fresh "e" in
                         destruct (ExprEqDec a b) as [e | _ ]; [destruct (n e) |]
                       | [ n : b <> a |- _ ] =>
                         let e := fresh "e" in
                         destruct (ExprEqDec a b) as [e | _ ]; [destruct (n (eq_sym e)) |]
                       end
                | [ |- context[CtrlExprEqDec ?a ?b] ] =>
                  tryif unify a b
                  then let n := fresh "n" in
                       destruct (CtrlExprEqDec a b) as [_ | n];
                         [| destruct (n eq_refl)]
                  else lazymatch goal with
                       | [ e : a = b |- _ ] =>
                         let n := fresh "n" in 
                         destruct (CtrlExprEqDec a b) as [_ | n ]; [| destruct (n e)]
                       | [ e : b = a |- _ ] =>
                         let n := fresh "n" in 
                         destruct (CtrlExprEqDec a b) as [_ | n ];
                           [| destruct (n (eq_sym e))]
                       | [ n : a <> b |- _ ] =>
                         let e := fresh "e" in
                         destruct (CtrlExprEqDec a b) as [e | _ ]; [destruct (n e) |]
                       | [ n : b <> a |- _ ] =>
                         let e := fresh "e" in
                         destruct (CtrlExprEqDec a b) as [e | _ ];
                           [destruct (n (eq_sym e)) |]
                       end
                | [ IH : forall E2 E σ, CtrlExprMerge ?E1 E2 = Some E ->
                                   CtrlExprMerge (?E1 [ceg|σ]) (E2 [ceg| σ]) =
                                   Some (E [ceg|σ]),
                      H : CtrlExprMerge ?E1 ?E2 = Some ?E
                      |- context[CtrlExprMerge (?E1 [ceg|?σ]) (?E2 [ceg| ?σ])]] =>
                  rewrite (IH E2 E σ H)
                end.
  Qed.

  Lemma MergeCtrlExprVals : forall V1 V2 V,
      CtrlExprMerge V1 V2 = Some V -> 
      CtrlExprVal V1 -> V1 = V /\ V2 = V.
  Proof using.
    intros V1 V2 V eq val; destruct val; destruct V2; cbn in eq; try discriminate.
    destruct (ExprEqDec v e); inversion eq; subst; clear eq; constructor; auto.
    inversion eq; subst; auto.
    1,2: destruct (CtrlExprEqDec B V2); inversion eq; subst; clear eq; auto.
  Qed.

  Lemma MergeRelToVal : forall V1 V2 V,
      CtrlExprMergeRel V1 V2 V ->
      CtrlExprVal V -> V1 = V /\ V2 = V.
  Proof using.
    intros V1 V2 V merge val; 
      inversion val; subst; inversion merge; subst; split; reflexivity.
  Qed.

  (*
    The If Equation

    One of the properties we want to prove is that mergins is proper with respect to
    equivalence. This is mostly easy, but we have to prove the following equation (where
    `⋈` is an infix version of merging):
    (E₁ ⋈ E₂) ⋈ (E₃ ⋈ E₄) = (E₁ ⋈ E₃) ⋈ (E₂ ⋈ E₄)
    The need for this equation comes from swapping two if expressions.

    We can prove this equation using associativity and commutativity. However, it turns out
    to be easier show how to merge a list of expressions, and then use those to show
    that any permutation of that list leads to the same result.
   *)

  Fixpoint MergeCtrlExprList (Es : list CtrlExpr) : option CtrlExpr :=
    match Es with
    | [] => None
    | E1 :: [] => Some E1
    | E1 :: Es => match MergeCtrlExprList Es with
                   | Some E2 => CtrlExprMerge E1 E2
                   | None => None
                   end
    end.

  Theorem MergeCtrlExprListPerm : forall Es1 Es2 : list CtrlExpr,
      Permutation Es1 Es2 -> MergeCtrlExprList Es1 = MergeCtrlExprList Es2.
  Proof using.
    intros Es1 Es2 perm; induction perm; cbn; auto.
    - destruct l; destruct l'; auto.
      destruct (Permutation_nil_cons perm).
      destruct (Permutation_nil_cons (Permutation_sym perm)).
      rewrite IHperm; destruct (MergeCtrlExprList l'); reflexivity.
    - destruct l; auto. apply MergeComm.
      destruct (MergeCtrlExprList (c :: l)); auto.
      destruct (CtrlExprMerge x c0) eqn:eq1. all:destruct (CtrlExprMerge y c0) eqn:eq2.
      symmetry. rewrite MergeComm with (E1 := y). apply MergeAssoc with (E2 := c0); auto.
      rewrite MergeComm; auto.
      apply (MergeAssocNone y c0 x c1); auto. rewrite MergeComm; auto.
      symmetry; apply (MergeAssocNone x c0 y c1); auto. rewrite MergeComm; auto.
      reflexivity.
    - rewrite IHperm1. rewrite IHperm2. auto.
  Qed.

  Theorem CtrlExprMergeCons : forall E Es E', MergeCtrlExprList Es = Some E' ->
                                         MergeCtrlExprList (E :: Es) = CtrlExprMerge E E'.
  Proof using.
    intros E Es; revert E; induction Es; intros E E' eq; cbn in *; try discriminate.
    destruct Es; [inversion eq; auto|].
    destruct (MergeCtrlExprList (c :: Es)) eqn:eq0; try discriminate.
    rewrite eq; auto.
  Qed.

  (* 
     Sadly, this has a problem: we return None for the empty list, but also whenever the 
     merge fails. To solve this, we use _non-empty_ lists, so that we know `None` comes from
     failure.
   *)
  
  Inductive NECtrlExprList : Set :=
  | SingletonNEList : CtrlExpr -> NECtrlExprList
  | ConsNEList : CtrlExpr -> NECtrlExprList -> NECtrlExprList.

  Fixpoint NECtrlExprListToList (l : NECtrlExprList) : list CtrlExpr :=
    match l with
    | SingletonNEList E => [E]
    | ConsNEList E Es => E :: NECtrlExprListToList Es
    end.

  Fixpoint ListToNECtrlExprList (l : list CtrlExpr) : option NECtrlExprList :=
    match l with
    | [] => None
    | E :: [] => Some (SingletonNEList E)
    | E :: l => match ListToNECtrlExprList l with
                 | Some Es => Some (ConsNEList E Es)
                 | None => None
                 end
    end.

  Fixpoint MergeNECtrlExprList (Es : NECtrlExprList) : option CtrlExpr :=
    match Es with
    | SingletonNEList E => Some E
    | ConsNEList E1 Es => match MergeNECtrlExprList Es with
                         | Some E2 => CtrlExprMerge E1 E2
                         | None => None
                         end
    end.

  Lemma MergeCtrlExprListToNE : forall Es Es',
      ListToNECtrlExprList Es = Some Es' ->
      MergeCtrlExprList Es = MergeNECtrlExprList Es'.
  Proof using.
    intro Es; induction Es; intros Es' eq; cbn in *; try discriminate.
    destruct Es. inversion eq; subst; clear eq. cbn; reflexivity.
    destruct (ListToNECtrlExprList (c :: Es)); inversion eq; subst; clear eq.
    specialize (IHEs n eq_refl). rewrite IHEs. cbn; reflexivity.
  Qed.

  Lemma OnlyNilNotNEList : forall Es, ListToNECtrlExprList Es = None -> Es = [].
  Proof using.
    intros Es; induction Es; cbn; intros es; auto.
    destruct Es. inversion es. destruct (ListToNECtrlExprList (c :: Es)); try discriminate.
    specialize (IHEs eq_refl). inversion IHEs.
  Qed.

  (* 
     We want to use `++` with non-empty lists, so we define it and show that the underlying
     lists are appended.
   *)
  Fixpoint NEApp (Es1 Es2 : NECtrlExprList) : NECtrlExprList :=
    match Es1 with
    | SingletonNEList E => ConsNEList E Es2
    | ConsNEList E Es1 => ConsNEList E (NEApp Es1 Es2)
    end.

  Theorem AppToNEApp : forall Es1 Es2 Es1' Es2',
      ListToNECtrlExprList Es1 = Some Es1' ->
      ListToNECtrlExprList Es2 = Some Es2' ->
      ListToNECtrlExprList (Es1 ++ Es2) = Some (NEApp Es1' Es2').
  Proof using.
    intro Es1; induction Es1; intros Es2 Es1' Es2' eq1 eq2; cbn in *; try discriminate.
    destruct Es1; cbn in *.
    destruct Es2; cbn in eq2; try discriminate.
    inversion eq1; subst; clear eq1; cbn.
    rewrite eq2; auto.
    destruct (match Es1 with
              | [] => Some (SingletonNEList c)
              | _ :: _ =>
                match ListToNECtrlExprList Es1 with
                | Some Es => Some (ConsNEList c Es)
                | None => None
                end
              end) eqn:eq3; try discriminate.
    specialize (IHEs1 Es2 n Es2' eq_refl eq2). rewrite IHEs1.
    inversion eq1; subst; cbn; reflexivity.
  Qed.

  Theorem NEAppToApp : forall Es1 Es2,
      NECtrlExprListToList (NEApp Es1 Es2) = (NECtrlExprListToList Es1) ++ (NECtrlExprListToList Es2).
  Proof using.
    intros Es1; induction Es1; intro Es2; cbn; auto.
    rewrite IHEs1; auto.
  Qed.

  (* 
     Here is where we use nonempty lists: appending two lists and then merging
     is the same as merging each list, and then merging the result.
   *)

  Theorem CtrlExprNEMergeApp : forall Es1 Es2 E1 E2,
      MergeNECtrlExprList Es1 = Some E1 ->
      MergeNECtrlExprList Es2 = Some E2 ->
      MergeNECtrlExprList (NEApp Es1 Es2) = CtrlExprMerge E1 E2.
  Proof using.
    intro Es1; induction Es1; intros Es2 E1 E2 eq1 eq2; cbn in *.
    - rewrite eq2; inversion eq1; auto.
    - destruct (MergeNECtrlExprList Es1) eqn:eq3; try discriminate.
      specialize (IHEs1 Es2 c0 E2 eq_refl eq2). rewrite IHEs1.
      destruct (CtrlExprMerge c0 E2) eqn:eq4.
      apply (MergeAssoc c c0 E2 E1 c1 eq1 eq4).
      symmetry; rewrite MergeComm; rewrite MergeComm in eq1; rewrite MergeComm in eq4;
        apply (MergeAssocNone E2 c0 c E1 eq4 eq1).
  Qed.      
  
  Theorem CtrlExprMergeApp : forall Es1 Es2 E1 E2,
      MergeCtrlExprList Es1 = Some E1 ->
      MergeCtrlExprList Es2 = Some E2 ->
      MergeCtrlExprList (Es1 ++ Es2) = CtrlExprMerge E1 E2.
  Proof using.
    intros Es1 Es2 E1 E2 H H0.
    destruct (ListToNECtrlExprList Es1) eqn:eq1;
      [|apply OnlyNilNotNEList in eq1; subst; cbn in H; inversion H].
    destruct (ListToNECtrlExprList Es2) eqn:eq2;
      [|apply OnlyNilNotNEList in eq2; subst; cbn in H0; inversion H0].
    pose proof (MergeCtrlExprListToNE Es1 n eq1).
    pose proof (MergeCtrlExprListToNE Es2 n0 eq2).
    rewrite H in H1; symmetry in H1; rewrite H0 in H2; symmetry in H2.
    pose proof (CtrlExprNEMergeApp n n0 E1 E2 H1 H2).
    pose proof (AppToNEApp Es1 Es2 n n0 eq1 eq2).
    pose proof (MergeCtrlExprListToNE (Es1 ++ Es2) (NEApp n n0) H4).
    etransitivity; eauto.
  Qed.

  (* And that lets us prove our equation *)
  Theorem MergeCtrlExprIfs : forall E1 E2 E3 E4 E5 E6 E7 E8,
      CtrlExprMerge E1 E2 = Some E5 ->
      CtrlExprMerge E3 E4 = Some E6 ->
      CtrlExprMerge E1 E3 = Some E7 ->
      CtrlExprMerge E2 E4 = Some E8 ->
      CtrlExprMerge E5 E6 = CtrlExprMerge E7 E8.
  Proof using.
    intros E1 E2 E3 E4 E5 E6 E7 E8 H H0 H1 H2.
    assert (Permutation [E1; E2; E3; E4] [E1; E3; E2; E4]) as p
      by (repeat constructor; fail);
      pose proof (MergeCtrlExprListPerm [E1; E2; E3; E4] [E1; E3; E2; E4] p); clear p.
    assert ([E1; E2] ++ [E3; E4] = [E1; E2; E3; E4]) as H4 by auto; rewrite <- H4 in H3;
       clear H4; erewrite CtrlExprMergeApp in H3; cbn; repeat rewrite MergeIdRight; eauto.
    assert ([E1; E3] ++ [E2; E4] = [E1; E3; E2; E4]) as H4 by auto; rewrite <- H4 in H3;
       clear H4; erewrite CtrlExprMergeApp in H3; cbn; repeat rewrite MergeIdRight; eauto.
  Qed.
    
  (* OPERATIONAL SEMANTICS *)

  (*
    Labels for the labeled-transition system.
   *)
  
  Inductive Label : Set :=
    Iota : Label (* Internal steps *)
  | SyncIota : Label (* Internal steps everyone needs to synchronize on *)
  | SendLabel : Loc -> Expr -> Label
  | RecvLabel : Loc -> Expr -> Label
  | ChooseLabel : Loc -> SyncLabel -> Label
  | AllowChoiceLabel : Loc -> SyncLabel -> Label
  | FunLabel : Label -> Label
  | ArgLabel : Label -> Label.
  Global Hint Constructors Label : CtrlExpr.

  (* 
     When defining the concurrent semantics, we have to reason about labels that match up.
     The existence of Fun and Arg labels make this harder, but are necessary for soundness.
     We thus inductively define different ways of matching.
   *)
  Inductive InternalLabel : Label -> Prop := (* Expresses an internal step *)
    IotaInternal : InternalLabel Iota
  | FunInternal : forall L, InternalLabel L -> InternalLabel (FunLabel L)
  | ArgInternal : forall L, InternalLabel L -> InternalLabel (ArgLabel L).
  Global Hint Constructors InternalLabel : CtrlExpr.

  (* One sends, the other receives *)
  Inductive CommLabelPair : Loc -> Expr -> Loc -> Label -> Label -> Prop :=
    BaseCommLabelPair : forall l1 v l2 ,
      CommLabelPair l1 v l2 (SendLabel l2 v) (RecvLabel l1 v)
  | BaseCommLabelPair' : forall l1 v l2,
      CommLabelPair l1 v l2 (RecvLabel l1 v) (SendLabel l2 v)
  | FunCommLabelPair : forall l1 v l2 L1 L2,
        CommLabelPair l1 v l2 L1 L2 -> CommLabelPair l1 v l2 (FunLabel L1) (FunLabel L2)
  | ArgCommLabelPair : forall l1 v l2 L1 L2,
      CommLabelPair l1 v l2 L1 L2 -> CommLabelPair l1 v l2 (ArgLabel L1) (ArgLabel L2).
  Global Hint Constructors CommLabelPair : CtrlExpr.

  (* One chooses, the other allows a choice. *)
  Inductive ChoiceLabelPair : Loc -> SyncLabel -> Loc -> Label -> Label -> Prop :=
    BaseChoiceLabelPair : forall l1 d l2,
      ChoiceLabelPair l1 d l2 (ChooseLabel l2 d) (AllowChoiceLabel l1 d)
  | FunChoiceLabelPair : forall l1 d l2 L1 L2,
      ChoiceLabelPair l1 d l2 L1 L2 -> ChoiceLabelPair l1 d l2 (FunLabel L1) (FunLabel L2)
  | ArgChoiceLabelPair : forall l1 d l2 L1 L2,
      ChoiceLabelPair l1 d l2 L1 L2 -> ChoiceLabelPair l1 d l2 (ArgLabel L1) (ArgLabel L2).
  Global Hint Constructors ChoiceLabelPair : CtrlExpr.

  (* Everybody has to synchronize *)
  Inductive IsSyncLabel : Label -> Prop :=
    SyncIotaSync : IsSyncLabel SyncIota
  | FunSync : forall L, IsSyncLabel L -> IsSyncLabel (FunLabel L)
  | ArgSync : forall L, IsSyncLabel L -> IsSyncLabel (ArgLabel L).
  Global Hint Constructors IsSyncLabel : CtrlExpr.


  (* The particular substitutions that we need to define the semantics. *)
  Definition ValSubst (e : Expr) : nat -> Expr :=
    fun n => match n with
          | 0 => e
          | S n => ExprVar n
          end.

  Definition FunLocalSubst (B : CtrlExpr) : nat -> CtrlExpr :=
    fun n => match n with
          | 0 => FunLocal B
          | S n => Var n
          end.

  Definition FunGlobalSubst (B : CtrlExpr) (A : CtrlExpr) : nat -> CtrlExpr := (* B : Body,  A : Arg *)
    fun n => match n with
          | 0 => A
          | 1 => FunGlobal B
          | S (S n) => Var n
          end.

  (* The labeled-transition system itself *)
  Inductive CtrlExprStep : CtrlExpr -> Label -> CtrlExpr -> Prop :=
  | RetStep : forall e1 e2, ExprStep e1 e2 -> CtrlExprStep (Ret e1) Iota (Ret e2)
  | IfEStep : forall e1 e2 E1 E2, ExprStep e1 e2 -> CtrlExprStep (If e1 E1 E2) Iota (If e2 E1 E2)
  | IfTTStep : forall E1 E2, CtrlExprStep (If tt E1 E2) Iota E1
  | IfFFStep : forall E1 E2, CtrlExprStep (If ff E1 E2) Iota E2
  | SendEStep : forall l e1 e2 E, ExprStep e1 e2 -> CtrlExprStep (Send l e1 E) Iota (Send l e2 E)
  | SendVStep : forall l v E, ExprVal v -> CtrlExprStep (Send l v E) (SendLabel l v) E
  | RecvStep : forall l v E, ExprVal v ->
                        CtrlExprStep (Recv l E) (RecvLabel l v) (E [cel| ValSubst v])
  | ChooseStep : forall l LR E, CtrlExprStep (Choose l LR E) (ChooseLabel l LR) E
  | AllowChoiceLStep : forall l E,
      CtrlExprStep (AllowChoiceL l E) (AllowChoiceLabel l Left) E
  | AllowChoiceRStep : forall l E,
      CtrlExprStep (AllowChoiceR l E) (AllowChoiceLabel l Right) E
  | AllowChoiceLRLStep : forall l E1 E2,
      CtrlExprStep (AllowChoiceLR l E1 E2) (AllowChoiceLabel l Left) E1
  | AllowChoiceLRRStep : forall l E1 E2,
      CtrlExprStep (AllowChoiceLR l E1 E2) (AllowChoiceLabel l Right) E2
  | LetRetEStep : forall E1 L E1' E2,
      CtrlExprStep E1 L E1' -> CtrlExprStep (LetRet E1 E2) (ArgLabel L) (LetRet E1' E2)
  | LetRetStep : forall v E2,
      ExprVal v -> CtrlExprStep (LetRet (Ret v) E2) SyncIota (E2 [cel| ValSubst v])
  | AppLocalFunStep : forall F1 L F2 e,
      CtrlExprStep F1 L F2 ->
      CtrlExprStep (AppLocal F1 e) (FunLabel L) (AppLocal F2 e)
  | AppLocalArgStep : forall F e1 e2,
      ExprStep e1 e2 ->
      CtrlExprStep (AppLocal F e1) Iota (AppLocal F e2)
  | AppLocalStep : forall F v,
      ExprVal v ->
      CtrlExprStep (AppLocal (FunLocal F) v) SyncIota
                  ((F [cel| ValSubst v]) [ceg| FunLocalSubst F])
  | AppGlobalFunStep : forall F1 L F2 A,
      CtrlExprStep F1 L F2 ->
      CtrlExprStep (AppGlobal F1 A) (FunLabel L) (AppGlobal F2 A)
  | AppGlobalArgStep : forall F A1 L A2,
      CtrlExprStep A1 L A2 ->
      CtrlExprStep (AppGlobal F A1) (ArgLabel L) (AppGlobal F A2)
  | AppGlobalStep : forall B V,
      CtrlExprVal V ->
      CtrlExprStep (AppGlobal (FunGlobal B) V) SyncIota (B [ceg| FunGlobalSubst B V]).
  Global Hint Constructors CtrlExprStep : CtrlExpr.

  Inductive CtrlExprSteps : CtrlExpr -> list Label -> CtrlExpr -> Prop :=
  | NoCtrlExprSteps : forall C, CtrlExprSteps C [] C
  | NextCtrlExprStep : forall C1 L C2 Ls C3, CtrlExprStep C1 L C2 -> CtrlExprSteps C2 Ls C3 ->
                                       CtrlExprSteps C1 (L :: Ls) C3.

  (*
    Values cannot take any steps.
   *)
  Lemma NoStepFromCtrlExprVal : forall V, CtrlExprVal V -> forall L E, ~ CtrlExprStep V L E.
  Proof using.
    intro V; induction V; intros val_V L E step; inversion val_V; subst;
      inversion step; subst.
    apply NoExprStepFromVal in H1; auto.
  Qed.

  (* A MORE SPECIFIC SEMANTICS *)

  (*
    In order to match up steps in the control language with those for Pirouette programs,
    it is sometimes convenient to use an equivalent labeled-transition system where the
    labels are closer to Pirouette's redices.
   *)
  Inductive CtrlExprRedex : Set :=
  | RetE : Expr -> Expr -> CtrlExprRedex
  | IfE : Expr -> Expr -> CtrlExprRedex
  | IfTT : CtrlExprRedex
  | IfFF : CtrlExprRedex
  | SendE : Loc -> Expr -> Expr -> CtrlExprRedex
  | SendV : Loc -> Expr -> CtrlExprRedex
  | RecvV : Loc -> Expr -> CtrlExprRedex
  | ChooseRedex : Loc -> SyncLabel -> CtrlExprRedex
  | AllowChoiceLRedex : Loc -> CtrlExprRedex
  | AllowChoiceRRedex : Loc -> CtrlExprRedex
  | LetRetRedex : Expr -> CtrlExprRedex
  | AppLocalE : Expr -> Expr -> CtrlExprRedex
  | AppLocalRedex : Expr -> CtrlExprRedex
  | AppGlobalRedex : CtrlExprRedex
  | FunRedex : CtrlExprRedex -> CtrlExprRedex
  | ArgRedex : CtrlExprRedex -> CtrlExprRedex.

  Inductive CtrlExprStep' : CtrlExpr -> CtrlExprRedex -> CtrlExpr -> Prop :=
  | RetStep' : forall e1 e2, ExprStep e1 e2 -> CtrlExprStep' (Ret e1) (RetE e1 e2) (Ret e2)
  | IfEStep' : forall e1 e2 E1 E2, ExprStep e1 e2 ->
                              CtrlExprStep' (If e1 E1 E2) (IfE e1 e2) (If e2 E1 E2)
  | IfTTStep' : forall E1 E2, CtrlExprStep' (If tt E1 E2) IfTT E1
  | IfFFStep' : forall E1 E2, CtrlExprStep' (If ff E1 E2) IfFF E2
  | SendEStep' : forall l e1 e2 E, ExprStep e1 e2 ->
                             CtrlExprStep' (Send l e1 E) (SendE l e1 e2) (Send l e2 E)
  | SendVStep' : forall l v E, ExprVal v -> CtrlExprStep' (Send l v E) (SendV l v) E
  | RecvStep' : forall l v E, ExprVal v ->
                        CtrlExprStep' (Recv l E) (RecvV l v) (E [cel| ValSubst v])
  | ChooseStep' : forall l LR E, CtrlExprStep' (Choose l LR E) (ChooseRedex l LR) E
  | AllowChoiceLStep' : forall l E,
      CtrlExprStep' (AllowChoiceL l E) (AllowChoiceLRedex l) E
  | AllowChoiceRStep' : forall l E,
      CtrlExprStep' (AllowChoiceR l E) (AllowChoiceRRedex l) E
  | AllowChoiceLRLStep' : forall l E1 E2,
      CtrlExprStep' (AllowChoiceLR l E1 E2) (AllowChoiceLRedex l) E1
  | AllowChoiceLRRStep' : forall l E1 E2,
      CtrlExprStep' (AllowChoiceLR l E1 E2) (AllowChoiceRRedex l) E2
  | LetRetEStep' : forall E1 R E1' E2,
      CtrlExprStep' E1 R E1' -> CtrlExprStep' (LetRet E1 E2) (ArgRedex R) (LetRet E1' E2)
  | LetRetStep' : forall v E2,
      ExprVal v -> CtrlExprStep' (LetRet (Ret v) E2) (LetRetRedex v) (E2 [cel| ValSubst v])
  | AppLocalFunStep' : forall F1 R F2 e,
      CtrlExprStep' F1 R F2 ->
      CtrlExprStep' (AppLocal F1 e) (FunRedex R) (AppLocal F2 e)
  | AppLocalArgStep' : forall F e1 e2,
      ExprStep e1 e2 ->
      CtrlExprStep' (AppLocal F e1) (AppLocalE e1 e2) (AppLocal F e2)
  | AppLocalStep' : forall F v,
      ExprVal v ->
      CtrlExprStep' (AppLocal (FunLocal F) v) (AppLocalRedex v)
                  ((F [cel| ValSubst v]) [ceg| FunLocalSubst F])
  | AppGlobalFunStep' : forall F1 R F2 A,
      CtrlExprStep' F1 R F2 ->
      CtrlExprStep' (AppGlobal F1 A) (FunRedex R) (AppGlobal F2 A)
  | AppGlobalArgStep' : forall F A1 R A2,
      CtrlExprStep' A1 R A2 ->
      CtrlExprStep' (AppGlobal F A1) (ArgRedex R) (AppGlobal F A2)
  | AppGlobalStep' : forall B V,
      CtrlExprVal V ->
      CtrlExprStep' (AppGlobal (FunGlobal B) V) AppGlobalRedex
                    (B [ceg| FunGlobalSubst B V]).
  Global Hint Constructors CtrlExprStep' : CtrlExpr.

  Fixpoint CtrlExprRedexToLabel (R : CtrlExprRedex) : Label :=
    match R with
    | RetE _ _ => Iota
    | IfE _ _ => Iota
    | IfTT => Iota
    | IfFF => Iota
    | SendE _ _ _ => Iota
    | SendV l v => SendLabel l v
    | RecvV l v => RecvLabel l v
    | ChooseRedex l d => ChooseLabel l d
    | AllowChoiceLRedex l => AllowChoiceLabel l Left
    | AllowChoiceRRedex l => AllowChoiceLabel l Right
    | LetRetRedex _ => SyncIota
    | AppLocalE _ _ => Iota
    | AppLocalRedex _ => SyncIota
    | AppGlobalRedex => SyncIota
    | FunRedex R => FunLabel (CtrlExprRedexToLabel R)
    | ArgRedex R => ArgLabel (CtrlExprRedexToLabel R)
    end.

  (*
    The following two theorems show that the two semantics are equivalent.
   *)
  Theorem CtrlExprStepToPrime : forall {E1 L E2},
      CtrlExprStep E1 L E2 -> exists R, CtrlExprRedexToLabel R = L /\ CtrlExprStep' E1 R E2.
  Proof using.
    intros E1 L E2 step; induction step;
      try (eexists; split; [|econstructor]; eauto; fail).
    all: try (destruct IHstep as [R [eq step']]; exists R; split; [|constructor]; auto; fail).
    all: destruct IHstep as [R [eq step']]; eexists; split; [|try econstructor]; eauto.
    all: cbn; rewrite eq; auto.
  Qed.

  Theorem CtrlExprStep'ToNormal : forall {E1 R E2},
      CtrlExprStep' E1 R E2 -> CtrlExprStep E1 (CtrlExprRedexToLabel R) E2.
  Proof using.
    intros E1 L E2 step; induction step; cbn; auto with CtrlExpr.
  Qed.

  (*
    MERGING AND OPERATIONAL SEMANTICS
   *)

  (*
    In order to prove soundness and completeness of endpoint projection, we need to reason
    about when we can lift steps in the operational semantics across merges. We prove 
    several important theorems towards this end, mostly using the more-specific semantics.

    The first theorem proves that merging after parallel steps gives rise to a step. In
    other words, if
    E₁₁ ⋈ E₁₂ = E₁
     |     |
    [R]   [R]
     |     |
     \/    \/
    E₂₁ ⋈ E₂₂ = E₂
    
    Then we can complete this picture:
    E₁₁ ⋈ E₁₂ = E₁
     |     |     |
    [R]   [R]   [R]
     |     |     |
     \/    \/    \/
    E₂₁ ⋈ E₂₂ = E₂
   *)  
  Lemma MergeStep' : forall E11 E12 E21 E22 E1 E2 R,
      CtrlExprStep' E11 R E21 ->
      CtrlExprStep' E12 R E22 ->
      CtrlExprMerge E11 E12 = Some E1 ->
      CtrlExprMerge E21 E22 = Some E2 ->
      CtrlExprStep' E1 R E2.
  Proof using.
    intros E11 E12 E21 E22 E1 E2 R step1;
      revert E12 E22 E1 E2; induction step1; intros E12 E22 E1'' E2'' step2 merge1 merge2;
        inversion step2; subst; cbn in *;
          repeat match goal with
                 | [ H : None = Some _ |- _ ] => inversion H
                 | [ H : Some _ = Some _ |- _ ] =>
                   inversion H; subst; clear H
                 | [ H : tt = ff |- _ ] => destruct (boolSeperation H)
                 | [ H : ff = tt |- _ ] => destruct (boolSeperation (eq_sym H))
                 | [ H : ExprStep tt ?e |- _ ] =>
                   destruct (NoExprStepFromVal tt ttValue e H)
                 | [ H : ExprStep ff ?e |- _ ] =>
                   destruct (NoExprStepFromVal ff ffValue e H)
                 | [ H : ExprStep ?v ?e, H' : ExprVal ?v |- _ ] =>
                   destruct (NoExprStepFromVal v H' e H)
                 | [ H : context[CtrlExprMerge ?E ?E] |- _ ] =>
                   rewrite MergeIdempotent in H
                 | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E3,
                         _ : context[?E1 [cel| ?σ]] |- _ ] =>
                   lazymatch goal with
                   | [ _ : CtrlExprMerge (E1 [cel| σ]) (E2 [cel| σ]) = Some (E3 [cel| σ])
                       |- _ ] => fail
                   | _ => pose proof (MergeLocalSubst E1 E2 E3 σ H)
                   end
                 | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E3,
                         _ : context[?E1 [ceg| ?σ]] |- _ ] =>
                   lazymatch goal with
                   | [ _ : CtrlExprMerge (E1 [ceg| σ]) (E2 [ceg| σ]) = Some (E3 [ceg| σ])
                       |- _ ] => fail
                   | _ => pose proof (MergeGlobalSubst E1 E2 E3 σ H)
                   end
                 | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E3,
                         H' : CtrlExprMerge ?E1 ?E2 = Some ?E4 |- _ ] =>
                   tryif unify E3 E4
                   then fail
                   else rewrite H in H'; inversion H'; subst; clear H'
                 | [ H : context[CtrlExprMerge ?E1 ?E2] |- _ ] =>
                   lazymatch type of H with
                   | CtrlExprMerge E1 E2 = _ => fail
                   | _ =>
                     lazymatch goal with
                     | [ H' : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite H' in H
                     | _ =>
                       let eq := fresh "eq" in
                       destruct (CtrlExprMerge E1 E2) eqn:eq
                     end
                   end
                 | [ H : context[ExprEqDec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (ExprEqDec e1 e2) as [_ | neq]; [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (ExprEqDec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (ExprEqDec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (ExprEqDec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (ExprEqDec e1 e2) as [eq|_]; [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                            let neq := fresh "neq" in
                            destruct (ExprEqDec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[CtrlExprEqDec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (CtrlExprEqDec e1 e2) as [_ | neq];
                          [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (CtrlExprEqDec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (CtrlExprEqDec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (CtrlExprEqDec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (CtrlExprEqDec e1 e2) as [eq|_];
                         [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                            let neq := fresh "neq" in
                            destruct (CtrlExprEqDec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[Nat.eq_dec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (Nat.eq_dec e1 e2) as [_ | neq]; [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (Nat.eq_dec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (Nat.eq_dec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (Nat.eq_dec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (Nat.eq_dec e1 e2) as [eq|_]; [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                            let neq := fresh "neq" in
                            destruct (Nat.eq_dec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[L.eq_dec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (L.eq_dec e1 e2) as [_ | neq]; [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (L.eq_dec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (L.eq_dec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (L.eq_dec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (L.eq_dec e1 e2) as [eq|_]; [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (L.eq_dec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[SyncLabelEqDec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (SyncLabelEqDec e1 e2) as [_ | neq];
                          [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (SyncLabelEqDec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (SyncLabelEqDec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (SyncLabelEqDec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (SyncLabelEqDec e1 e2) as [eq|_];
                         [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (SyncLabelEqDec e1 e2) as [eq|neq]; subst
                     end
                 end; try discriminate; eauto with CtrlExpr.
    - apply MergeCtrlExprVals in eq; auto; destruct eq; subst.
      rewrite MergeIdempotent in merge2; inversion merge2; clear merge2; subst.
      constructor; auto.
  Qed.

  (*
    But it turns out the assumptions in the previous lemma were too strong. In particular,
    we can prove the existence of E₂, rather than assuming it. Combining these two lemmas
    says that you can always complete the bottom-left corner of the diagram above.
   *)
  Lemma MergeStep'Exists : forall E11 E12 E21 E22 E1 R,
      CtrlExprStep' E11 R E21 ->
      CtrlExprStep' E12 R E22 ->
      CtrlExprMerge E11 E12 = Some E1 ->
      exists E2, CtrlExprMerge E21 E22 = Some E2.
  Proof using.
    intros E11 E12 E21 E22 E1 R step1;
      revert E12 E22 E1; induction step1; intros E12 E22 E1'' step2 merge1;
        inversion step2; subst; cbn in *;
                    repeat match goal with
                 | [ H : None = Some _ |- _ ] => inversion H
                 | [ H : Some _ = Some _ |- _ ] =>
                   inversion H; subst; clear H
                 | [ H : tt = ff |- _ ] => destruct (boolSeperation H)
                 | [ H : ff = tt |- _ ] => destruct (boolSeperation (eq_sym H))
                 | [ H : ExprStep tt ?e |- _ ] =>
                   destruct (NoExprStepFromVal tt ttValue e H)
                 | [ H : ExprStep ff ?e |- _ ] =>
                   destruct (NoExprStepFromVal ff ffValue e H)
                 | [ H : ExprStep ?v ?e, H' : ExprVal ?v |- _ ] =>
                   destruct (NoExprStepFromVal v H' e H)
                 | [ H : context[CtrlExprMerge ?E ?E] |- _ ] =>
                   rewrite MergeIdempotent in H
                 | [ IH : forall E12 E22 E2,
                       CtrlExprStep' E12 ?R E22 ->
                       CtrlExprMerge ?E1 E12 = Some E2 ->
                       exists E3, CtrlExprMerge ?E1' E22 = Some E3,
                       H1 : CtrlExprStep' ?E12 ?R ?E22,
                       H2 : CtrlExprMerge ?E1 ?E12 = Some ?E2 |- _ ] =>
                   lazymatch goal with
                   | [_ : CtrlExprMerge E1' E22 = Some _ |- _ ] => fail
                   | _ => let E3 := fresh "E" in
                         let eq := fresh "eq" in
                         destruct (IH E12 E22 E2 H1 H2) as [E3 eq]
                   end
                 | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E3,
                         _ : context[?E1 [cel| ?σ]] |- _ ] =>
                   lazymatch goal with
                   | [ _ : CtrlExprMerge (E1 [cel| σ]) (E2 [cel| σ]) = Some (E3 [cel| σ])
                       |- _ ] => fail
                   | _ => pose proof (MergeLocalSubst E1 E2 E3 σ H)
                   end
                 | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E3 |- context[?E1 [cel| ?σ]]] =>
                   lazymatch goal with
                   | [ _ : CtrlExprMerge (E1 [cel| σ]) (E2 [cel| σ]) = Some (E3 [cel| σ])
                       |- _ ] => fail
                   | _ => pose proof (MergeLocalSubst E1 E2 E3 σ H)
                   end
                 | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E3,
                         _ : context[?E1 [ceg| ?σ]] |- _ ] =>
                   lazymatch goal with
                   | [ _ : CtrlExprMerge (E1 [ceg| σ]) (E2 [ceg| σ]) = Some (E3 [ceg| σ])
                       |- _ ] => fail
                   | _ => pose proof (MergeGlobalSubst E1 E2 E3 σ H)
                   end
                 | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E3 |- context[?E1 [ceg| ?σ]]] =>
                   lazymatch goal with
                   | [ _ : CtrlExprMerge (E1 [ceg| σ]) (E2 [ceg| σ]) = Some (E3 [ceg| σ])
                       |- _ ] => fail
                   | _ => pose proof (MergeGlobalSubst E1 E2 E3 σ H)
                   end
                 | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E3,
                         H' : CtrlExprMerge ?E1 ?E2 = Some ?E4 |- _ ] =>
                   tryif unify E3 E4
                   then fail
                   else rewrite H in H'; inversion H'; subst; clear H'
                 | [ H : context[CtrlExprMerge ?E1 ?E2] |- _ ] =>
                   lazymatch type of H with
                   | CtrlExprMerge E1 E2 = _ => fail
                   | _ =>
                     lazymatch goal with
                     | [ H' : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite H' in H
                     | _ =>
                       let eq := fresh "eq" in
                       destruct (CtrlExprMerge E1 E2) eqn:eq
                     end
                   end
                 | [ |- context[CtrlExprMerge ?E1 ?E2]] =>
                   lazymatch goal with
                   | [ H' : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite H'
                   | _ =>
                     let eq := fresh "eq" in
                     destruct (CtrlExprMerge E1 E2) eqn:eq
                   end
                 | [ |- context[ExprEqDec ?e1 ?e2]] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (ExprEqDec e1 e2) as [_ | neq]; [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (ExprEqDec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (ExprEqDec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (ExprEqDec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (ExprEqDec e1 e2) as [eq|_]; [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                            let neq := fresh "neq" in
                            destruct (ExprEqDec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[ExprEqDec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (ExprEqDec e1 e2) as [_ | neq]; [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (ExprEqDec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (ExprEqDec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (ExprEqDec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (ExprEqDec e1 e2) as [eq|_]; [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                            let neq := fresh "neq" in
                            destruct (ExprEqDec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[CtrlExprEqDec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (CtrlExprEqDec e1 e2) as [_ | neq];
                          [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (CtrlExprEqDec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (CtrlExprEqDec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (CtrlExprEqDec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (CtrlExprEqDec e1 e2) as [eq|_];
                         [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                            let neq := fresh "neq" in
                            destruct (CtrlExprEqDec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[Nat.eq_dec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (Nat.eq_dec e1 e2) as [_ | neq]; [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (Nat.eq_dec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (Nat.eq_dec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (Nat.eq_dec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (Nat.eq_dec e1 e2) as [eq|_]; [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                            let neq := fresh "neq" in
                            destruct (Nat.eq_dec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[L.eq_dec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (L.eq_dec e1 e2) as [_ | neq]; [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (L.eq_dec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (L.eq_dec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (L.eq_dec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (L.eq_dec e1 e2) as [eq|_]; [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (L.eq_dec e1 e2) as [eq|neq]; subst
                     end
                 | [ H : context[SyncLabelEqDec ?e1 ?e2] |- _ ] =>
                   tryif unify e1 e2
                   then let neq := fresh in
                        destruct (SyncLabelEqDec e1 e2) as [_ | neq];
                          [| destruct (neq eq_refl)]
                   else
                     lazymatch goal with
                     | [ eq : e1 = e2 |- _ ] =>
                       let neq := fresh in
                       destruct (SyncLabelEqDec e1 e2) as [_ | neq]; [| destruct (neq eq)]
                     | [ eq : e2 = e1 |- _ ] =>
                       let neq := fresh in
                       destruct (SyncLabelEqDec e1 e2) as [_ | neq];
                         [| destruct (neq (eq_sym eq))]
                     | [ neq : e1 <> e2 |- _ ] =>
                       let eq := fresh in
                       destruct (SyncLabelEqDec e1 e2) as [eq|_]; [destruct (neq eq)|]
                     | [ neq : e2 <> e1 |- _ ] =>
                       let eq := fresh in
                       destruct (SyncLabelEqDec e1 e2) as [eq|_];
                         [destruct (neq (eq_sym eq))|]
                     | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (SyncLabelEqDec e1 e2) as [eq|neq]; subst
                     end
                           end; try discriminate; eauto with CtrlExpr.
    apply MergeCtrlExprVals in eq; auto; destruct eq; subst.
    rewrite MergeIdempotent in eq0. inversion eq0.
  Qed.

  (*
    This tells us that parallel reductions can always be merged when terms are mergable.
    However, not every step arises this way. For instance, 
    (AllowChoiceL ℓ E₁₁) ⋈ (AllowChoiceR ℓ E₁₂) = (AllowChoiceLR ℓ E₁₁ E₁₂)
    Clearly, 
    AllowChoiceLR ℓ E₁₁ E₁₂ ==[Choose L]==> E₁₁
    However, `AllowChoiceR ℓ E₁₂` can never take a `Choose L` step! 

    This turns out to be the only way that this mismatch can arise. It will take us several
    steps to prove this. First, we show that if the reduction is not a AllowChoiceLabel,
    then we can always lift:
    E₁₁ ⋈ E₁₂ = E₁
                 |
                [R]
                 |
                 \/
                 E₂
    to the following diagram:
    E₁₁ ⋈ E₁₂ = E₁
     |     |     |
    [R]   [R]   [R]
     |     |     |
     \/    \/    \/
    E₂₁ ⋈ E₂₂  = E₂
    
    We start by defining AllowChoiceLabels under any number of Fun and Arg label 
    constructors.
   *)
  Inductive IsAllowChoiceLabel : Label -> Prop :=
    IsAllowChoiceLabelBase : forall l d, IsAllowChoiceLabel (AllowChoiceLabel l d)
  | FunAllowChoiceLabel : forall L, IsAllowChoiceLabel L -> IsAllowChoiceLabel (FunLabel L)
  | ArgAllowChoiceLabel : forall L, IsAllowChoiceLabel L -> IsAllowChoiceLabel (ArgLabel L).

  Lemma UnmergeRelStep' : forall E11 E12 E1 R E2,
      CtrlExprStep' E1 R E2 ->
      ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R) ->
      CtrlExprMergeRel E11 E12 E1 ->
      exists E21 E22, CtrlExprMergeRel E21 E22 E2 /\
                 CtrlExprStep' E11 R E21 /\
                 CtrlExprStep' E12 R E22.
  Proof using.
    intros E11 E12 E1 R E2 step;
      revert E11 E12; induction step;
        intros E11 E12 iacl merge1; inversion merge1; subst;
          try (cbn in iacl; exfalso; apply iacl; constructor; auto; fail);
          repeat match goal with
                 | [ H : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel (ArgRedex R)) |- _ ] =>
                   lazymatch goal with
                   | [ _ : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R) |- _] => fail
                   | _ => assert (~ IsAllowChoiceLabel (CtrlExprRedexToLabel R))
                       by ( let H' := fresh in
                            cbn in H; intro H';
                            apply H; constructor; apply H')
                   end
                 | [ H : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel (FunRedex R)) |- _ ] =>
                   lazymatch goal with
                   | [ _ : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R) |- _] => fail
                   | _ => assert (~ IsAllowChoiceLabel (CtrlExprRedexToLabel R))
                       by ( let H' := fresh in
                            cbn in H; intro H';
                            apply H; constructor; apply H')
                   end
                 | [ IH : forall E11 E12,
                       ~ IsAllowChoiceLabel (CtrlExprRedexToLabel ?R) ->
                       CtrlExprMergeRel E11 E12 ?E1 ->
                       exists E21 E22, CtrlExprMergeRel E21 E22 ?E1' /\ CtrlExprStep' E11 ?R E21
                                  /\ CtrlExprStep' E12 ?R E22,
                       H1 : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel ?R),
                       H2 : CtrlExprMergeRel ?E11 ?E12 ?E1 |- _ ]=>
                   lazymatch goal with
                   | [_ : CtrlExprMergeRel _ _ E1' |- _ ] => fail
                   | _ => let E21 := fresh "E2" in
                         let E22 := fresh "E2" in
                         let merge := fresh "merge" in
                         let step1 := fresh "step" in
                         let step2 := fresh "step" in
                         destruct (IH E11 E12 H1 H2) as [E21 [E22 [merge [step1 step2]]]]
                   end
                 end.
    all: try (eexists; eexists; split; [econstructor; eauto | split];
              eauto with CtrlExpr; fail).
    all: try (eexists; eexists; split; [|split]; auto with CtrlExpr; auto; fail).
    - exists (E1 [cel| ValSubst v]); exists (E2 [cel| ValSubst v]); split; [|split];
        auto with CtrlExpr.
      apply CtrlExprMergeRelSpec1 in H3; apply CtrlExprMergeRelSpec2;
        rewrite MergeLocalSubst with (E := E); auto.
    - inversion H4; subst.
      exists (E21 [cel| ValSubst v]); exists (E22 [cel| ValSubst v]); split; [|split];
        auto with CtrlExpr.
      apply CtrlExprMergeRelSpec1 in H5; apply CtrlExprMergeRelSpec2;
        rewrite MergeLocalSubst with (E := E2); auto.
    - inversion H3; subst.
      exists ((F [cel| ValSubst v]) [ceg|FunLocalSubst F]);
        exists ((F [cel| ValSubst v]) [ceg| FunLocalSubst F]);
        split; [|split]; auto with CtrlExpr.
      apply MergeRelDiag.
    - inversion H4; subst; apply MergeRelToVal in H5; auto; destruct H5; subst.
      eexists; eexists; split; [|split]; eauto with CtrlExpr; eapply MergeRelDiag.
  Qed.

  (*
    In order to lift with choice labels, we need to know the direction of the choice. That
    is, are the choices left or right? We augment our inductive type with a direction in
    the form of a synchronization label. Of course, any allow choice label has a direction.
   *)

  Inductive AllowDirectedChoiceLabel : SyncLabel -> Label -> Prop :=
    AllowDirectedChoiceLabelBase : forall l d, AllowDirectedChoiceLabel d (AllowChoiceLabel l d)
  | FunAllowDirectedChoice : forall d L,
      AllowDirectedChoiceLabel d L -> AllowDirectedChoiceLabel d (FunLabel L)
  | ArgAllowDirectedChoice : forall d L,
      AllowDirectedChoiceLabel d L -> AllowDirectedChoiceLabel d (ArgLabel L).
  Global Hint Constructors AllowDirectedChoiceLabel IsAllowChoiceLabel : CtrlExpr.

  Lemma ChoiceLabelToDirected : forall L,
      IsAllowChoiceLabel L -> exists d, AllowDirectedChoiceLabel d L.
  Proof using.
    intros L alc; induction alc; try (eexists; econstructor; eauto; fail).
    all: destruct IHalc as [d IHalc]; try (eexists; econstructor; eauto; fail).
  Qed.

  Lemma DirectedChoiceToLabel : forall d L,
      AllowDirectedChoiceLabel d L -> IsAllowChoiceLabel L.
  Proof using.
    intros d L adcl; induction adcl; constructor; auto.
  Qed.

  Lemma DirectionUnique : forall d1 d2 L,
      AllowDirectedChoiceLabel d1 L -> AllowDirectedChoiceLabel d2 L -> d1 = d2.
  Proof using.
    intros d1 d2 R adcl1; revert d2; induction adcl1; intros d2 adcl2;
      inversion adcl2; subst; auto.
  Qed.

  (*
    We also have to know that everything is under the same stack of Arg and Fun 
    constructors. We define a partial equivalence relation that tells us when two labels
    are both allow choice labels under the same stack, even if their directions differ.
   *)
  Inductive SameChoiceRedexShape : CtrlExprRedex -> CtrlExprRedex -> Prop :=
    SameShapeLL : forall l,
      SameChoiceRedexShape (AllowChoiceLRedex l) (AllowChoiceLRedex l)
  | SameShapeLR : forall l,
      SameChoiceRedexShape (AllowChoiceLRedex l) (AllowChoiceRRedex l)
  | SameShapeRL : forall l,
      SameChoiceRedexShape (AllowChoiceRRedex l) (AllowChoiceLRedex l)
  | SameShapeRR : forall l,
      SameChoiceRedexShape (AllowChoiceRRedex l) (AllowChoiceRRedex l)
  | SameShapeFun : forall R1 R2,
      SameChoiceRedexShape R1 R2 -> SameChoiceRedexShape (FunRedex R1) (FunRedex R2)
  | SameChoiceRedexShapeArg : forall R1 R2,
      SameChoiceRedexShape R1 R2 -> SameChoiceRedexShape (ArgRedex R1) (ArgRedex R2).
  Global Hint Constructors SameChoiceRedexShape : CtrlExpr.

  Lemma SameChoiceRedexShapeRefl : forall R,
      IsAllowChoiceLabel (CtrlExprRedexToLabel R) ->
      SameChoiceRedexShape R R.
  Proof using.
    intros R; induction R; intros iacl; inversion iacl; subst; constructor; auto.
  Qed.

  Lemma SameChoiceRedexShapeSym : forall R1 R2,
      SameChoiceRedexShape R1 R2 -> SameChoiceRedexShape R2 R1.
  Proof using.
    intros R1 R2 scrs1; induction scrs1; constructor; auto.
  Qed.

  Lemma SameChoiceRedexShapeTrans : forall R1 R2 R3,
      SameChoiceRedexShape R1 R2 -> SameChoiceRedexShape R2 R3 -> SameChoiceRedexShape R1 R3.
  Proof using.
    intros R1 R2 R3 scrs1; revert R3; induction scrs1; intros R3 scrs2;
      inversion scrs2; subst; try (constructor; auto; fail).
  Qed.

  Global Hint Resolve SameChoiceRedexShapeRefl SameChoiceRedexShapeSym
         SameChoiceRedexShapeTrans : CtrlExpr.


  (*
    Now we can prove what we want to prove: if we want to lift allow choice labels, we can.
    However, we only _might_ be able to retain the merge property. This might be a bit
    surprising: we can always lift to two different allow choice steps. At least one of the
    steps will be in the direction of the merged programs. Moreover, if both allow 
    steps in the merged program, then the merge of the results is the result of stepping
    after merging.
   *)
  Lemma UnmergeRelACStep' : forall E11 E12 E1 d R E2,
      AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R) ->
      CtrlExprMergeRel E11 E12 E1 -> (* E₁ is the merge of E₁₁ and E₁₂ *)
      CtrlExprStep' E1 R E2 -> (* The merge takes a step *)
      exists d1 R1 E21 d2 R2 E22,
        CtrlExprStep' E11 R1 E21 /\ (* Both sides can take a step *)
        CtrlExprStep' E12 R2 E22 /\
        SameChoiceRedexShape R1 R /\ (* Those steps are choice steps in the same place *)
        SameChoiceRedexShape R2 R /\ (* as the merged step *)
        AllowDirectedChoiceLabel d1 (CtrlExprRedexToLabel R1) /\(* Those are in two *)
        AllowDirectedChoiceLabel d2 (CtrlExprRedexToLabel R2) /\ (* possibly-different 
                                                                   directions*)
        ((d1 = d /\ d2 = d /\ CtrlExprMergeRel E21 E22 E2) \/ (* If they're both in the
                                                              direction of the merged step,
                                                              then the merge of the results
                                                              is the same as the result of
                                                              the merge. *)
         (d1 = d /\ d2 <> d) \/ (*Otherwise, at least one is in the same direction as the *)
         (d1 <> d /\ d2 = d)). (* merged step *)
  Proof using.
    intros E11 E12 E1 d R E2 adcl merge step;
      revert E11 E12 d adcl merge; induction step;
        intros E11 E12 d adcl merge; inversion adcl; subst; inversion merge; subst;
          cbn in *.
      all: try match goal with
      | [IH : forall E11 E12 d,
            AllowDirectedChoiceLabel d (CtrlExprRedexToLabel ?R) ->
            CtrlExprMergeRel E11 E12 ?E1 ->
            exists d1 R1 E21 d2 R2 E22,
              CtrlExprStep' E11 R1 E21 /\
              CtrlExprStep' E12 R2 E22 /\
              SameChoiceRedexShape R1 ?R /\
              SameChoiceRedexShape R2 ?R /\
              AllowDirectedChoiceLabel d1 (CtrlExprRedexToLabel R1) /\
              AllowDirectedChoiceLabel d2 (CtrlExprRedexToLabel R2) /\
              ((d1 = d /\ d2 = d /\ CtrlExprMergeRel E21 E22 ?E2) \/
               (d1 = d /\ d2 <> d) \/
               (d1 <> d /\ d2 = d)),
           H1 : AllowDirectedChoiceLabel ?d (CtrlExprRedexToLabel ?R),
           H2 : CtrlExprMergeRel ?E11 ?E12 ?E1 |- _ ] =>
        let d1 := fresh "d" in
        let R1 := fresh "R" in
        let E21 := fresh "E2" in
        let d2 := fresh "d" in
        let R2 := fresh "R" in
        let E22 := fresh "E2" in
        let step1 := fresh "step" in
        let step2 := fresh "step" in
        let scrs1 := fresh "scrs" in
        let scrs2 := fresh "scrs" in
        let adcl1 := fresh "adcl" in
        let adcl2 := fresh "adcl" in
        let eq1 := fresh "eq" in
        let eq2 := fresh "eq" in
        let neq := fresh "neq" in
        let merge := fresh "merge" in
        destruct (IH E11 E12 d)
          as [d1 [R1 [E21 [d2 [R2 [E22 [step1 [step2 [scrs1 [scrs2 [adcl1 [adcl2 [[eq1 [eq2 merge]] | [[eq1 neq] | [eq1 neq]]]]]]]]]]]]]]]; subst; clear IH
      end; auto with CtrlExpr.

    all: try (eexists; eexists; eexists; eexists; eexists; eexists;
              repeat match goal with |[|- _ /\ _ ] => split end;
              repeat (cbn; eauto with CtrlExpr); fail).

    (* In order to avoid repetitive solving of existential types, I rolled a bit of 
       custom LTac for metaprogramming reasons. *)
    Ltac UnmergeRelACStep'Branch d1 d2 l :=
      match d1 with
      | Left =>
          match d2 with
            | Left => exists d1; exists (AllowChoiceLRedex l); eexists;
                        exists d2; exists (AllowChoiceLRedex l); eexists
            | Right => exists d1; exists (AllowChoiceLRedex l); eexists;
                        exists d2; exists (AllowChoiceRRedex l); eexists
          end
      | Right =>
        match d2 with
        | Left => exists d1; exists (AllowChoiceRRedex l); eexists;
                    exists d2; exists (AllowChoiceLRedex l); eexists
        | Right => exists d1; exists (AllowChoiceRRedex l); eexists;
                    exists d2; exists (AllowChoiceRRedex l); eexists
        end
      end;
      repeat match goal with |[|- _ /\ _ ] => split end;
      repeat (cbn; eauto with CtrlExpr).
    - UnmergeRelACStep'Branch Left Right l. 
      right; left; split; auto; discriminate. 
    - UnmergeRelACStep'Branch Left Left l.
    - UnmergeRelACStep'Branch Right Left l.
      right; right; split; auto. intro eq; inversion eq.
    - UnmergeRelACStep'Branch Right Left l.
      right; right; split; auto. discriminate.
    - UnmergeRelACStep'Branch Left Left l.
    - UnmergeRelACStep'Branch Left Right l.
      right; left; split; auto; discriminate.
    - UnmergeRelACStep'Branch Left Left l.
    - UnmergeRelACStep'Branch Left Right l.
      right; right; split; auto; discriminate.
    - UnmergeRelACStep'Branch Left Right l.
      right; right; split; auto; discriminate.
    - UnmergeRelACStep'Branch Right Left l.
      right; left; split; auto; discriminate.
    - UnmergeRelACStep'Branch Right Left l.
      right; left; split; auto; discriminate.
  Qed.

  (* 
     Now that we have the unmerge-step result for the more-specific semantics, we can 
     get it for the less-specific semantics as a corollary.
   *)
  Corollary UnmergeRelStep : forall E11 E12 E1 L E2,
      CtrlExprStep E1 L E2 ->
      ~ IsAllowChoiceLabel L ->
      CtrlExprMergeRel E11 E12 E1 ->
      exists E21 E22, CtrlExprMergeRel E21 E22 E2 /\
                 CtrlExprStep E11 L E21 /\
                 CtrlExprStep E12 L E22.
  Proof using.
    intros E11 E12 E1 L E2 H H0 H1.
    apply CtrlExprStepToPrime in H; destruct H as [R [eq H]]; subst.
    destruct (UnmergeRelStep' E11 E12 E1 _ E2 H H0 H1) as [E21 [E22 [merge [step1 step2]]]].
    apply CtrlExprStep'ToNormal in step1; apply CtrlExprStep'ToNormal in step2.
    exists E21; exists E22; split; [|split]; auto.
  Qed.

  (*
    Similarly, we can get the result for specifically steps that are purely internal.
   *)
  Lemma UnmergeRelIotaStep' : forall E11 E12 E1 R E2,
      CtrlExprStep' E1 R E2 ->
      InternalLabel (CtrlExprRedexToLabel R) ->
      CtrlExprMergeRel E11 E12 E1 ->
      exists E21 E22, CtrlExprMergeRel E21 E22 E2 /\
                 CtrlExprStep' E11 R E21 /\
                 CtrlExprStep' E12 R E22.
  Proof using.
    intros E11 E12 E1 R E2 H H0 H1.
    assert (~ IsAllowChoiceLabel (CtrlExprRedexToLabel R)).
    clear E11 E12 E1 E2 H H1.
    induction R; cbn in H0; inversion H0; cbn; try (intro H'; inversion H'; subst; fail).
    1,2: intro H'; inversion H'; subst; apply IHR in H1; destruct (H1 H3).
    eapply UnmergeRelStep'; eauto.
  Qed.
  
  Lemma UnmergeRelIotaStep : forall E11 E12 E1 E2 L,
      CtrlExprStep E1 L E2 ->
      InternalLabel L ->
      CtrlExprMergeRel E11 E12 E1 ->
      exists E21 E22, CtrlExprMergeRel E21 E22 E2 /\
                 CtrlExprStep E11 L E21 /\
                 CtrlExprStep E12 L E22.
  Proof using.
    intros E11 E12 E1 E2 L H H0 H1.
    assert (~ IsAllowChoiceLabel L).
    clear E11 E12 E1 E2 H H1.
    induction L; inversion H0; subst. 1-3: intro H'; inversion H'; subst.
    1,2: apply (IHL H1 H2).
    eapply UnmergeRelStep; eauto.
  Qed.

  (* EQUALITY OF ALLOW CHOICE LABELS *)
  (*
    Knowing that two Redexes are both of the same shape (e.g., they have the same stack of
    Arg and Fun constructors on top of them) and in the same direction tells you that they
    are the same label.
   *)
  Lemma SameShapeSameDirection : forall d R1 R2,
      SameChoiceRedexShape R1 R2 ->
      AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R1) ->
      AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R2) ->
      R1 = R2.
  Proof using.
    intros d R1 R2 scrs; revert d; induction scrs; cbn; intros d adcl1 adcl2;
      inversion adcl1; inversion adcl2; subst; try discriminate; auto.
    all: erewrite IHscrs; eauto.
  Qed.

  (* LESS NONDETERMINISM *)

  (*
    The less-nondetermism relation describes when one program is the same as another,
    BUT might be missing allow choice branches. This comes up when a Pirouette program takes
    a step, since we may then know what branch will be taken.
   *)
  
  Inductive LessNondet : CtrlExpr -> CtrlExpr -> Prop :=
  | LessNondetVar : forall n, LessNondet (Var n) (Var n)
  | LessNondetUnit : LessNondet Unit Unit
  | LessNondetRet : forall e, LessNondet (Ret e) (Ret e)
  | LessNondetIf : forall e E11 E12 E21 E22,
      LessNondet E11 E21 ->
      LessNondet E12 E22 ->
      LessNondet (If e E11 E12) (If e E21 E22)
  | LessNondetRecv : forall l E1 E2,
      LessNondet E1 E2 ->
      LessNondet (Recv l E1) (Recv l E2)
  | LessNondetSend : forall l v E1 E2,
      LessNondet E1 E2 ->
      LessNondet (Send l v E1) (Send l v E2)
  | LessNondetChoose : forall l d E1 E2,
      LessNondet E1 E2 ->
      LessNondet (Choose l d E1) (Choose l d E2)
  | LessNondetChoiceLL : forall l E1 E2,
      LessNondet E1 E2 ->
      LessNondet (AllowChoiceL l E1) (AllowChoiceL l E2)
  | LessNondetChoiceLLR : forall l E1 E21 E22,
      LessNondet E1 E21 ->
      LessNondet (AllowChoiceL l E1) (AllowChoiceLR l E21 E22)
  | LessNondetChoiceRR : forall l E1 E2,
      LessNondet E1 E2 ->
      LessNondet (AllowChoiceR l E1) (AllowChoiceR l E2)
  | LessNondetChoiceRLR : forall l E1 E21 E22,
      LessNondet E1 E22 ->
      LessNondet (AllowChoiceR l E1) (AllowChoiceLR l E21 E22)
  | LessNondetChoiceLRLR : forall l E11 E12 E21 E22,
      LessNondet E11 E21 ->
      LessNondet E12 E22 ->
      LessNondet (AllowChoiceLR l E11 E12) (AllowChoiceLR l E21 E22)
  | LessNondetLetRet : forall E11 E12 E21 E22,
      LessNondet E11 E21 ->
      LessNondet E12 E22 ->
      LessNondet (LetRet E11 E12) (LetRet E21 E22)
  | LessNondetFunLocal : forall E,
      LessNondet (FunLocal E) (FunLocal E)
  | LessNondetAppLocal : forall E1 E2 e,
      LessNondet E1 E2 ->
      LessNondet (AppLocal E1 e) (AppLocal E2 e)
  | LessNondetFunGlobal : forall E,
      LessNondet (FunGlobal E) (FunGlobal E)
  | LessNondetAppGlobal : forall E11 E12 E21 E22,
      LessNondet E11 E21 ->
      LessNondet E12 E22 ->
      LessNondet (AppGlobal E11 E12) (AppGlobal E21 E22).
  Global Hint Constructors LessNondet : CtrlExpr.

  Lemma LessNondetRefl : forall E, LessNondet E E.
  Proof using.
    intro E; induction E; eauto with CtrlExpr.
  Qed.
  Global Hint Resolve LessNondetRefl : CtrlExpr.

  Lemma LessNondetTrans : forall E1 E2 E3, LessNondet E1 E2 -> LessNondet E2 E3 -> LessNondet E1 E3.
  Proof using.
    intros E1 E2 E3 lnd1; revert E3; induction lnd1; intros E3 lnd2; inversion lnd2; subst;
      auto with CtrlExpr.
  Qed.

  Lemma LessNondetAntisym : forall E1 E2, LessNondet E1 E2 -> LessNondet E2 E1 -> E1 = E2.
  Proof using.
    intros E1 E2 lnd1; induction lnd1; intros lnd2; inversion lnd2; subst; 
      repeat match goal with
             | [IH : LessNondet ?E2 ?E1 -> ?E1 = ?E2,
                     H : LessNondet ?E2 ?E1 |- _ ] =>
               specialize (IH H); subst
             end; auto.
  Qed.

  Instance : Reflexive LessNondet := LessNondetRefl.
  Instance : Transitive LessNondet := LessNondetTrans.
  Instance : Antisymmetric CtrlExpr (@eq CtrlExpr) LessNondet := LessNondetAntisym.

  (* MERGING AND LESS NONDETERMINSM *)

  (*
    Merging programs can only increase nondeterminism. Moreover, merging preserves order on
    both sides. Lessnondeterminsm also tells us that merging is possible: if both sides of 
    a merge are less nondeterminstic than the two sides of a defined merge, then the less-
    nondetermistic merge is defined.
   *)
  
  Lemma MergeLessNondet : forall E1 E2 E,
      CtrlExprMerge E1 E2 = Some E ->
      LessNondet E1 E.
  Proof using.
    intro E1; induction E1; intros E2 E eq; destruct E2; cbn in eq;
      repeat match goal with
             | [H : Some _ = None |- _ ] => inversion H
             | [H : None = Some _ |- _ ] => inversion H
             | [H : Some _ = Some _ |- _ ] => inversion H; clear H; subst
             | [H : context[Nat.eq_dec ?a ?b] |- _ ] =>
               tryif unify a b
               then let n := fresh in
                    destruct (Nat.eq_dec a b) as [_ | n];
                      [| destruct (n eq_refl)]
               else lazymatch goal with
                    | [e : a = b |- _ ] =>
                      let n := fresh in
                      destruct (Nat.eq_dec a b) as [_ | n];
                        [| destruct (n e)]
                    | [e : b = a |- _ ] =>
                      let n := fresh in
                      destruct (Nat.eq_dec a b) as [_ | n];
                        [| destruct (n (eq_sym e))]
                    | [n : a <> b |- _ ] =>
                      let e := fresh in
                      destruct (Nat.eq_dec a b) as [e | _];
                        [destruct (n e)|]
                    | [n : b <> a |- _ ] =>
                      let e := fresh in
                      destruct (Nat.eq_dec a b) as [e | _];
                        [destruct (n (eq_sym e))|]
                    | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (Nat.eq_dec a b) as [eq|neq]; subst
                    end
             | [H : context[L.eq_dec ?a ?b] |- _ ] =>
               tryif unify a b
               then let n := fresh in
                    destruct (L.eq_dec a b) as [_ | n];
                      [| destruct (n eq_refl)]
               else lazymatch goal with
                    | [e : a = b |- _ ] =>
                      let n := fresh in
                      destruct (L.eq_dec a b) as [_ | n];
                        [| destruct (n e)]
                    | [e : b = a |- _ ] =>
                      let n := fresh in
                      destruct (L.eq_dec a b) as [_ | n];
                        [| destruct (n (eq_sym e))]
                    | [n : a <> b |- _ ] =>
                      let e := fresh in
                      destruct (L.eq_dec a b) as [e | _];
                        [destruct (n e)|]
                    | [n : b <> a |- _ ] =>
                      let e := fresh in
                      destruct (L.eq_dec a b) as [e | _];
                        [destruct (n (eq_sym e))|]
                    | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (L.eq_dec a b) as [eq|neq]; subst
                    end
             | [H : context[ExprEqDec ?a ?b] |- _ ] =>
               tryif unify a b
               then let n := fresh in
                    destruct (ExprEqDec a b) as [_ | n];
                      [| destruct (n eq_refl)]
               else lazymatch goal with
                    | [e : a = b |- _ ] =>
                      let n := fresh in
                      destruct (ExprEqDec a b) as [_ | n];
                        [| destruct (n e)]
                    | [e : b = a |- _ ] =>
                      let n := fresh in
                      destruct (ExprEqDec a b) as [_ | n];
                        [| destruct (n (eq_sym e))]
                    | [n : a <> b |- _ ] =>
                      let e := fresh in
                      destruct (ExprEqDec a b) as [e | _];
                        [destruct (n e)|]
                    | [n : b <> a |- _ ] =>
                      let e := fresh in
                      destruct (ExprEqDec a b) as [e | _];
                        [destruct (n (eq_sym e))|]
                    | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (ExprEqDec a b) as [eq|neq]; subst
                    end
             | [H : context[SyncLabelEqDec ?a ?b] |- _ ] =>
               tryif unify a b
               then let n := fresh in
                    destruct (SyncLabelEqDec a b) as [_ | n];
                      [| destruct (n eq_refl)]
               else lazymatch goal with
                    | [e : a = b |- _ ] =>
                      let n := fresh in
                      destruct (SyncLabelEqDec a b) as [_ | n];
                        [| destruct (n e)]
                    | [e : b = a |- _ ] =>
                      let n := fresh in
                      destruct (SyncLabelEqDec a b) as [_ | n];
                        [| destruct (n (eq_sym e))]
                    | [n : a <> b |- _ ] =>
                      let e := fresh in
                      destruct (SyncLabelEqDec a b) as [e | _];
                        [destruct (n e)|]
                    | [n : b <> a |- _ ] =>
                      let e := fresh in
                      destruct (SyncLabelEqDec a b) as [e | _];
                        [destruct (n (eq_sym e))|]
                    | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (SyncLabelEqDec a b) as [eq|neq]; subst
                    end
             | [H : context[CtrlExprEqDec ?a ?b] |- _ ] =>
               tryif unify a b
               then let n := fresh in
                    destruct (CtrlExprEqDec a b) as [_ | n];
                      [| destruct (n eq_refl)]
               else lazymatch goal with
                    | [e : a = b |- _ ] =>
                      let n := fresh in
                      destruct (CtrlExprEqDec a b) as [_ | n];
                        [| destruct (n e)]
                    | [e : b = a |- _ ] =>
                      let n := fresh in
                      destruct (CtrlExprEqDec a b) as [_ | n];
                        [| destruct (n (eq_sym e))]
                    | [n : a <> b |- _ ] =>
                      let e := fresh in
                      destruct (CtrlExprEqDec a b) as [e | _];
                        [destruct (n e)|]
                    | [n : b <> a |- _ ] =>
                      let e := fresh in
                      destruct (CtrlExprEqDec a b) as [e | _];
                        [destruct (n (eq_sym e))|]
                    | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (CtrlExprEqDec a b) as [eq|neq]; subst
                    end
             | [ H : context[CtrlExprMerge ?E1 ?E2] |- _ ] =>
               lazymatch type of H with
               | CtrlExprMerge E1 E2 = _ => fail
               | _ => lazymatch goal with
                      | [eq : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite eq in H
                      | _ => let eq := fresh "eq" in destruct (CtrlExprMerge E1 E2) eqn:eq
                      end
               end
             | [ IH : forall E2 E, CtrlExprMerge ?E1 E2 = Some E -> LessNondet ?E1 E,
                   eq : CtrlExprMerge ?E1 ?E2 = Some ?E |- _ ] =>
               lazymatch goal with
               | [_ : LessNondet E1 E |- _ ] => fail
               | _ => pose proof (IH E2 E eq)
               end
             end; eauto with CtrlExpr.
  Qed.

  Lemma LessNondetMerge : forall E11 E12 E21 E22 E1 E2,
      LessNondet E11 E21 ->
      LessNondet E12 E22 ->
      CtrlExprMerge E11 E12 = Some E1 ->
      CtrlExprMerge E21 E22 = Some E2 ->
      LessNondet E1 E2.
  Proof using.
    intro E11; induction E11; intros E12 E21 E22 E1 E2 lnd1 lnd2 eq1 eq2;
      inversion lnd1; subst; destruct E12; inversion lnd2; subst; cbn in eq1; cbn in eq2;
        repeat match goal with
               | [H : Some _ = None |- _ ] => inversion H
               | [H : None = Some _ |- _ ] => inversion H
               | [H : Some _ = Some _ |- _ ] => inversion H; clear H; subst
               | [H : context[Nat.eq_dec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (Nat.eq_dec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (Nat.eq_dec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (Nat.eq_dec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (Nat.eq_dec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (Nat.eq_dec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (Nat.eq_dec a b) as [eq|neq]; subst
                      end
               | [H : context[L.eq_dec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (L.eq_dec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (L.eq_dec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (L.eq_dec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (L.eq_dec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (L.eq_dec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (L.eq_dec a b) as [eq|neq]; subst
                      end
               | [H : context[ExprEqDec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (ExprEqDec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (ExprEqDec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (ExprEqDec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (ExprEqDec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (ExprEqDec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (ExprEqDec a b) as [eq|neq]; subst
                      end
               | [H : context[SyncLabelEqDec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (SyncLabelEqDec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (SyncLabelEqDec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (SyncLabelEqDec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (SyncLabelEqDec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (SyncLabelEqDec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (SyncLabelEqDec a b) as [eq|neq]; subst
                      end
               | [H : context[CtrlExprEqDec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (CtrlExprEqDec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (CtrlExprEqDec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (CtrlExprEqDec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (CtrlExprEqDec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (CtrlExprEqDec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (CtrlExprEqDec a b) as [eq|neq]; subst
                      end
               | [ H : context[CtrlExprMerge ?E1 ?E2] |- _ ] =>
                 lazymatch type of H with
                 | CtrlExprMerge E1 E2 = _ => fail
                 | _ => lazymatch goal with
                        | [eq : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite eq in H
                        | _ => let eq := fresh "eq" in destruct (CtrlExprMerge E1 E2) eqn:eq
                        end
                 end
               | [ IH : forall E12 E21 E22 E1 E2,
                     LessNondet ?E11 E21 ->
                     LessNondet E12 E22 ->
                     CtrlExprMerge ?E11 E12 = Some E1 ->
                     CtrlExprMerge E21 E22 = Some E2 ->
                     LessNondet E1 E2,
                     lnd1 : LessNondet ?E11 ?E21,
                     lnd2 : LessNondet ?E12 ?E22,
                     eq1 : CtrlExprMerge ?E11 ?E12 = Some ?E1,
                     eq2 : CtrlExprMerge ?E21 ?E22 = Some ?E2 |- _ ] =>
                 lazymatch goal with
                 | [ _ : LessNondet E1 E2 |- _ ] => fail
                 | _ => pose proof (IH E12 E21 E22 E1 E2 lnd1 lnd2 eq1 eq2)
                 end
               | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E |- _ ] =>
                 lazymatch goal with
                 | [_ : LessNondet E1 E |- _ ] => fail
                 | _ => pose proof (MergeLessNondet E1 E2 E H)
                 end
               | [ H : CtrlExprMerge ?E1 ?E2 = ?OE |- _ ] =>
                 lazymatch goal with
                 | [_ : CtrlExprMerge E2 E1 = OE |- _ ] => fail
                 | _ => assert (CtrlExprMerge E2 E1 = OE) by (rewrite MergeComm in H; assumption)
                 end
               | [ H1 : LessNondet ?E1 ?E2, H2 : LessNondet ?E2 ?E3 |- _ ] =>
                 lazymatch goal with
                 | [_ : LessNondet E1 E3 |- _ ] => fail
                 | _ => pose proof (LessNondetTrans E1 E2 E3 H1 H2)
                 end
               end; auto with CtrlExpr.
  Qed.

  Lemma LessNondetMergeExists : forall E11 E12 E21 E22 E2,
      LessNondet E11 E21 ->
      LessNondet E12 E22 ->
      CtrlExprMerge E21 E22 = Some E2 ->
      exists E1, CtrlExprMerge E11 E12 = Some E1.
  Proof using.
    intro E11; induction E11; intros E12 E21 E22 E2 lnd1 lnd2 eq2;
      inversion lnd1; subst; destruct E12; inversion lnd2; subst; cbn; cbn in eq2;
        repeat match goal with
               | [H : Some _ = None |- _ ] => inversion H
               | [H : None = Some _ |- _ ] => inversion H
               | [H : Some _ = Some _ |- _ ] => inversion H; clear H; subst
               | [ H1 : ?a = Some _, H2 : ?a = None |- _ ] =>
                 rewrite H1 in H2; inversion H2
               | [|- context[CtrlExprMerge ?E ?E]] => rewrite MergeIdempotent
               | [H : context[Nat.eq_dec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (Nat.eq_dec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (Nat.eq_dec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (Nat.eq_dec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (Nat.eq_dec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (Nat.eq_dec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (Nat.eq_dec a b) as [eq|neq]; subst
                      end
               | [H : context[L.eq_dec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (L.eq_dec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (L.eq_dec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (L.eq_dec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (L.eq_dec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (L.eq_dec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (L.eq_dec a b) as [eq|neq]; subst
                      end
               | [H : context[ExprEqDec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (ExprEqDec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (ExprEqDec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (ExprEqDec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (ExprEqDec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (ExprEqDec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (ExprEqDec a b) as [eq|neq]; subst
                      end
               | [H : context[SyncLabelEqDec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (SyncLabelEqDec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (SyncLabelEqDec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (SyncLabelEqDec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (SyncLabelEqDec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (SyncLabelEqDec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (SyncLabelEqDec a b) as [eq|neq]; subst
                      end
               | [H : context[CtrlExprEqDec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (CtrlExprEqDec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (CtrlExprEqDec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (CtrlExprEqDec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (CtrlExprEqDec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (CtrlExprEqDec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (CtrlExprEqDec a b) as [eq|neq]; subst
                      end
               | [|- context[CtrlExprEqDec ?a ?b]] =>
                 tryif unify a b
                 then let n := fresh in
                      destruct (CtrlExprEqDec a b) as [_ | n];
                        [| destruct (n eq_refl)]
                 else lazymatch goal with
                      | [e : a = b |- _ ] =>
                        let n := fresh in
                        destruct (CtrlExprEqDec a b) as [_ | n];
                          [| destruct (n e)]
                      | [e : b = a |- _ ] =>
                        let n := fresh in
                        destruct (CtrlExprEqDec a b) as [_ | n];
                          [| destruct (n (eq_sym e))]
                      | [n : a <> b |- _ ] =>
                        let e := fresh in
                        destruct (CtrlExprEqDec a b) as [e | _];
                          [destruct (n e)|]
                      | [n : b <> a |- _ ] =>
                        let e := fresh in
                        destruct (CtrlExprEqDec a b) as [e | _];
                          [destruct (n (eq_sym e))|]
                      | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (CtrlExprEqDec a b) as [eq|neq]; subst
                      end
               | [ H : context[CtrlExprMerge ?E1 ?E2] |- _ ] =>
                 lazymatch type of H with
                 | CtrlExprMerge E1 E2 = _ => fail
                 | _ => lazymatch goal with
                        | [eq : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite eq in H
                        | _ => let eq := fresh "eq" in destruct (CtrlExprMerge E1 E2) eqn:eq
                        end
                 end
               | [ |- context[CtrlExprMerge ?E1 ?E2] ] =>
                 lazymatch goal with
                 | [eq : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite eq
                 | _ => let eq := fresh "eq" in destruct (CtrlExprMerge E1 E2) eqn:eq
                 end
               | [ IH : forall E12 E21 E22 E2,
                     LessNondet ?E11 E21 ->
                     LessNondet E12 E22 ->
                     CtrlExprMerge E21 E22 = Some E2 ->
                     exists E1, CtrlExprMerge ?E11 E12 = Some E1,
                     lnd1 : LessNondet ?E11 ?E21,
                     lnd2 : LessNondet ?E12 ?E22,
                     eq2 : CtrlExprMerge ?E21 ?E22 = Some ?E2 |- _ ] =>
                 lazymatch goal with
                 | [ _ : CtrlExprMerge E11 E12 = Some _ |- _ ] => fail
                 | _ => let E1 := fresh "E" in
                       let eq := fresh eq in
                       destruct (IH E12 E21 E22 E2 lnd1 lnd2 eq2) as [E1 eq]
                 end
               | [ H : CtrlExprMerge ?E1 ?E2 = Some ?E |- _ ] =>
                 lazymatch goal with
                 | [_ : LessNondet E1 E |- _ ] => fail
                 | _ => pose proof (MergeLessNondet E1 E2 E H)
                 end
               | [ H : CtrlExprMerge ?E1 ?E2 = ?OE |- _ ] =>
                 lazymatch goal with
                 | [_ : CtrlExprMerge E2 E1 = OE |- _ ] => fail
                 | _ => assert (CtrlExprMerge E2 E1 = OE) by (rewrite MergeComm in H; assumption)
                 end
               | [ H1 : LessNondet ?E1 ?E2, H2 : LessNondet ?E2 ?E3 |- _ ] =>
                 lazymatch goal with
                 | [_ : LessNondet E1 E3 |- _ ] => fail
                 | _ => pose proof (LessNondetTrans E1 E2 E3 H1 H2)
                 end
               end; auto with CtrlExpr; try (eexists; eauto with CtrlExpr; fail).
  Qed.

  (* LESS NONDETERMISM AND SUBSTITUTION *)
  (*
    Less-nondetermism is proper for both types of substitution and renaming.
    
    Surprisingly, if something is less-nondetermistic than a value, then it must be
    equal to the value.
   *)
  Lemma LessNondetLocalSubst : forall E1 E2 σ,
      LessNondet E1 E2 ->
      LessNondet (E1 [cel| σ]) (E2 [cel| σ]).
  Proof using.
    intros E1 E2 σ lnd; revert σ; induction lnd; intro σ; cbn; eauto with CtrlExpr.
  Qed.

  Lemma LessNondetRename : forall E1 E2 ξ,
      LessNondet E1 E2 ->
      LessNondet (E1 ⟨ceg| ξ⟩) (E2 ⟨ceg| ξ⟩).
  Proof using.
    intros E1 E2 ξ lnd; revert ξ; induction lnd; intro ξ; cbn; eauto with CtrlExpr.
  Qed.

  Lemma LessNondetGlobalSubst : forall E1 E2 σ,
      LessNondet E1 E2 ->
      LessNondet (E1 [ceg| σ]) (E2 [ceg| σ]).
  Proof using.
    intros E1 E2 σ lnd; revert σ; induction lnd; intros σ;
      cbn; eauto with CtrlExpr.
  Qed.

  Lemma LessNondetVals : forall V1 V2,
      LessNondet V1 V2 -> CtrlExprVal V1 \/ CtrlExprVal V2 -> V1 = V2.
  Proof using.
    intros V1 V2 lnd val; destruct val as [val| val]; destruct val; inversion lnd; subst;
      reflexivity.
  Qed.

  (*
           E11          ==[L]==>         E12
            |            \                |
     [nondeterminism]     \        [nondeterminism]
           \/              >              \/
          E21           ==[L]==>         E22
    
    Essentially, if E11 is less nondeterministic than E21, then we know that any step E11
    can take, E21 can also take. In other words, E21 has more choices available to it.
   *)

  Lemma LiftStepWithNondet : forall E11 L E12 E21,
      CtrlExprStep E11 L E12 ->
      LessNondet E11 E21 ->
      exists E22, LessNondet E12 E22 /\ CtrlExprStep E21 L E22.
  Proof using.
    intros E11 L E12 E21 step; revert E21; induction step; intros E21 lnd;
      inversion lnd; subst; try (eexists; split; eauto with CtrlExpr; fail).
    - exists (If e2 E0 E22); split; eauto with CtrlExpr.
    - exists (Send l e2 E2); split; eauto with CtrlExpr.
    - exists (E2 [cel|ValSubst v]); split; eauto with CtrlExpr. apply LessNondetLocalSubst; auto.
    - destruct (IHstep E0 H1) as [E2' [lnd' step']].
      exists (LetRet E2' E22); split; auto with CtrlExpr.
    - inversion H2; subst. exists (E22 [cel|ValSubst v]); split; auto with CtrlExpr.
      apply LessNondetLocalSubst; auto.
    - destruct (IHstep E2 H2) as [E22 [lnd' step']].
      exists (AppLocal E22 e); split; auto with CtrlExpr.
    - exists (AppLocal E2 e2); split; auto with CtrlExpr.
    - inversion H3; subst.
      exists ((F [cel|ValSubst v]) [ceg| FunLocalSubst F]);
        split; auto with CtrlExpr.
    - destruct (IHstep E0 H1) as [F2' [lnd' step']].
      exists (AppGlobal F2' E22); split; auto with CtrlExpr.
    - destruct (IHstep E22 H3) as [A2' [lnd' step']].
      exists (AppGlobal E0 A2'); split; auto with CtrlExpr.
    - inversion H2; subst. apply LessNondetVals in H4; auto; subst.
      exists (B [ceg| FunGlobalSubst B E22]); split; auto with CtrlExpr.
  Qed.
  
  (*
    Whenever the step is not an allow choice label, however, less and more nondetermistic
    programs behave the same.
   *)
  Lemma LowerStepNondet : forall E11 L E12 E21,
      CtrlExprStep E11 L E12 ->
      LessNondet E21 E11 ->
      ~ IsAllowChoiceLabel L ->
      exists E22, LessNondet E22 E12 /\ CtrlExprStep E21 L E22.
  Proof using.
    intros E11 L E12 E21 step; revert E21; induction step; intros E21 lnd neq;
      inversion lnd; subst;
        repeat match goal with
               | [ H : ~ IsAllowChoiceLabel (AllowChoiceLabel ?l ?d) |- _ ] =>
                 exfalso; apply H; constructor
               | [ H: ~ IsAllowChoiceLabel (ArgLabel ?L) |- _ ] =>
                 lazymatch goal with
                 | [ _ : ~ IsAllowChoiceLabel L |- _ ] => fail
                 | _ => assert (~IsAllowChoiceLabel L)
                     by (let H' := fresh in intro H'; apply H; constructor; auto)
                 end
               | [ H: ~ IsAllowChoiceLabel (FunLabel ?L) |- _ ] =>
                 lazymatch goal with
                 | [ _ : ~ IsAllowChoiceLabel L |- _ ] => fail
                 | _ => assert (~IsAllowChoiceLabel L)
                     by (let H' := fresh in intro H'; apply H; constructor; auto)
                 end
               | [IH : forall E21, LessNondet E21 ?E11 -> 
                              ~IsAllowChoiceLabel ?L ->  
                              exists E22, LessNondet E22 ?E12 /\ CtrlExprStep E21 L E22,
                    H1 : LessNondet ?E21 ?E11, H2 : ~ IsAllowChoiceLabel ?L |- _ ] =>
                 lazymatch goal with
                 | [ _ : LessNondet _ E12 |- _ ] => fail
                 | _ => let E22 := fresh "E2" in
                       let lnd := fresh "lnd" in
                       let step := fresh "step" in
                       destruct (IH E21 H1 H2) as [E22 [lnd step]]
                 end
               end; try (eexists; split; eauto with CtrlExpr; fail).
    - exists (If e2 E11 E12); split; auto with CtrlExpr.
    - exists (Send l e2 E1); split; auto with CtrlExpr.
    - exists (E1 [cel|ValSubst v]); split; auto with CtrlExpr; apply LessNondetLocalSubst; auto.
    - exists (LetRet E0 E12); split; auto with CtrlExpr.
    - inversion H3; subst. exists (E12[cel|ValSubst v]); split; auto with CtrlExpr.
      apply LessNondetLocalSubst; auto.
    - exists (AppLocal E2 e); split; auto with CtrlExpr.
    - exists (AppLocal E1 e2); split; auto with CtrlExpr.
    - inversion H2; subst.
      exists ((F [cel|ValSubst v]) [ceg| FunLocalSubst F]);
        split; auto with CtrlExpr.
    - exists (AppGlobal E2 E12); split; auto with CtrlExpr.
    - exists (AppGlobal E11 E2); split; auto with CtrlExpr.
    - inversion H3; subst. apply LessNondetVals in H4; auto; subst.
      exists (B [ceg| FunGlobalSubst B V]); split; auto with CtrlExpr.
  Qed.

  (* CONCURRENT SYSTEMS *)

  (*
    We formalize concurrent systems as maps from locations to control expressions. We then
    give a labeled-transition system for their operational semantics, matching up labels
    in the control-expression LTS.
   *)
  
  Definition ConSystem := LM.t CtrlExpr.

  Inductive SystemLabel : Set :=
    SysIota : SystemLabel (* Internal steps *)
  | SysSyncIota : SystemLabel
  | CommLabel : Loc -> Expr -> Loc -> SystemLabel
  | ChoiceLabel : Loc -> SyncLabel -> Loc -> SystemLabel.
  Global Hint Constructors SystemLabel : CtrlExpr.

  Inductive SystemStep : ConSystem -> SystemLabel -> ConSystem -> Prop :=
    IotaStep : forall p Π1 Π2 E1 L E2,
      LM.MapsTo p E1 Π1 ->
      CtrlExprStep E1 L E2 ->
      InternalLabel L ->
      LM.MapsTo p E2 Π2 ->
      (forall q E, p <> q -> LM.MapsTo q E Π1 <-> LM.MapsTo q E Π2) ->
      SystemStep Π1 SysIota Π2
  | SyncIotaStep : forall Π1 Π2 L,
      IsSyncLabel L ->
      (forall p E1 E2,
          LM.MapsTo p E1 Π1 ->
          LM.MapsTo p E2 Π2 ->
          CtrlExprStep E1 L E2) ->
      (forall p, LM.In p Π1 <-> LM.In p Π2) ->
      SystemStep Π1 SysSyncIota Π2
  | CommStep : forall p v q Π1 Π2 pE1 pE2 qE1 qE2 L1 L2,
      p <> q ->
      CommLabelPair p v q L1 L2 ->
      LM.MapsTo p pE1 Π1 ->
      LM.MapsTo q qE1 Π1 ->
      CtrlExprStep pE1 L1 pE2 ->
      CtrlExprStep qE1 L2 qE2 ->
      LM.MapsTo p pE2 Π2 ->
      LM.MapsTo q qE2 Π2 ->
      (forall r E, p <> r -> q <> r -> LM.MapsTo r E Π1 <-> LM.MapsTo r E Π2) ->
      SystemStep Π1 (CommLabel p v q) Π2
  | ChoiceStep : forall p LR q Π1 Π2 pE1 pE2 qE1 qE2 L1 L2,
      p <> q ->
      ChoiceLabelPair p LR q L1 L2 ->
      LM.MapsTo p pE1 Π1 ->
      LM.MapsTo q qE1 Π1 ->
      CtrlExprStep pE1 L1 pE2 ->
      CtrlExprStep qE1 L2 qE2 ->
      LM.MapsTo p pE2 Π2 ->
      LM.MapsTo q qE2 Π2 ->
      (forall r E, p <> r -> q <> r -> LM.MapsTo r E Π1 <-> LM.MapsTo r E Π2) ->
      SystemStep Π1 (ChoiceLabel p LR q) Π2.
  Global Hint Constructors SystemStep : CtrlExpr.

  Inductive SystemSteps : ConSystem -> list SystemLabel -> ConSystem -> Prop :=
    ZeroSteps : forall C1 C2, LM.Equal C1 C2 -> SystemSteps C1 [] C2
  | PlusStep : forall C1 L C2 Ls C3, SystemStep C1 L C2 -> SystemSteps C2 Ls C3 ->
                                SystemSteps C1 (L :: Ls) C3.
  Global Hint Constructors SystemSteps : CtrlExpr.

  Definition ValueSystem : ConSystem -> Prop :=
    fun Π => forall l E, LM.MapsTo l E Π -> CtrlExprVal E.

  Lemma EqValSystem : forall Π1 Π2,
      LM.Equal Π1 Π2 -> ValueSystem Π1 -> ValueSystem Π2.
  Proof using.
    intros Π1 Π2 eq val.
    unfold ValueSystem. intros l E mt.
    unfold LM.Equal in eq. apply LM.find_1 in mt. rewrite <- (eq l) in mt.
    apply LM.find_2 in mt. apply val in mt. auto.
  Qed.

  (* 
     One system Π₁ is less-nondetermistic than another Π₂ if for every location ℓ, if ℓ maps
     to E₁ in Π₁ and E₂ in Π₂, then E₁ is less nondeterministic than E₂. We also require the
     other way around. This is a preorder.
   *)
     
  Definition LessNondetSystem (Π1 Π2 : ConSystem) : Prop :=
    (forall (l : Loc) (E1 : CtrlExpr),
        LM.MapsTo l E1 Π1 ->
        exists E2 : CtrlExpr, LM.MapsTo l E2 Π2 /\ LessNondet E1 E2)
    /\ (forall (l : Loc) (E2 : CtrlExpr),
          LM.MapsTo l E2 Π2 ->
          exists E1, LM.MapsTo l E1 Π1 /\ LessNondet E1 E2).

  Lemma LessNondetSystemRefl : forall Π, LessNondetSystem Π Π.
  Proof using.
    intro Π; unfold LessNondetSystem; split; [intros l E1 mt | intros l E2 mt].
    - exists E1; split; auto. apply LessNondetRefl.
    - exists E2; split; auto. apply LessNondetRefl.
  Qed.

  Lemma LessNondetSystemTrans : forall Π1 Π2 Π3,
      LessNondetSystem Π1 Π2 -> LessNondetSystem Π2 Π3 -> LessNondetSystem Π1 Π3.
  Proof using.
    intros Π1 Π2 Π3 lns1 lns2; unfold LessNondetSystem in *; split;
      [intros l E1 mt | intros l E2 mt];
      destruct lns1 as [lns11 lns12]; destruct lns2 as [lns21 lns22].
    - apply lns11 in mt; auto; destruct mt as [E2 [mt lnd1]].
      apply lns21 in mt; auto; destruct mt as [E3 [mt lnd2]].
      exists E3; split; auto. apply LessNondetTrans with (E2 := E2);auto.
    - apply lns22 in mt; destruct mt as [E1 [mt lnd1]]; auto.
      apply lns12 in mt as [E3 [mt lnd2]]. 
      exists E3; split; auto; apply LessNondetTrans with (E2 := E1); auto.
  Qed.

  Instance : Reflexive LessNondetSystem := LessNondetSystemRefl.
  Instance : Transitive LessNondetSystem := LessNondetSystemTrans.

  Lemma LessNondetValSystem : forall Π1 Π2,
      LessNondetSystem Π1 Π2 -> ValueSystem Π1 -> LM.Equal Π1 Π2.
  Proof using.
    intros Π1 Π2 lnds vals; unfold LM.Equal.
    intros y; destruct (LM.find y Π1) eqn:eq1.
    apply LM.find_2 in eq1. pose proof (vals y c eq1).
    apply lnds in eq1. destruct eq1 as [E2 [mt1 lnd]].
    apply LessNondetVals in lnd; auto; subst. apply LM.find_1 in mt1; rewrite mt1; auto.
    destruct (LM.find y Π2)eqn:eq2; auto.
    apply LM.find_2 in eq2. apply lnds in eq2. destruct eq2 as [E1 [mt lnd]].
    apply LM.find_1 in mt. rewrite mt in eq1; inversion eq1.
  Qed.
  
  Lemma LessNondetSystemAdd : forall Π1 Π2 l E1 E2,
      LessNondetSystem Π1 Π2 ->
      LessNondet E1 E2 ->
      LessNondetSystem (LM.add l E1 Π1) (LM.add l E2 Π2).
  Proof using.
    intros Π1 Π2 l E1 E2 H H0; unfold LessNondetSystem; split;
      unfold LessNondetSystem in H; destruct H.
    - intros l0 E0 H2. destruct (L.eq_dec l0 l); subst.
      pose proof (LMF.MapsToUnique H2 (LM.add_1 Π1 l E1)); subst.
      exists E2; split; auto. apply LM.add_1.
      apply LM.add_3 in H2; auto.
      apply H in H2. destruct H2 as [E3 [mt lnd]].
      exists E3; split; auto with CtrlExpr.
      apply LM.add_2; auto.
    - intros l0 E0 H2; destruct (L.eq_dec l0 l); subst.
      pose proof (LMF.MapsToUnique H2 (LM.add_1 Π2 l E2)); subst; clear H2.
      exists E1; split; auto; apply LM.add_1.
      apply LM.add_3 in H2; auto. apply H1 in H2.
      destruct H2 as [E3 [mt lnd]].
      exists E3; split; auto. apply LM.add_2; auto.
  Qed.

  Lemma EqLessNondet : forall Π1 Π2, LM.Equal Π1 Π2 -> LessNondetSystem Π1 Π2.
  Proof using.
    intros Π1 Π2 H; unfold LessNondetSystem; split.
    intros l E1 mt; exists E1; split; [|reflexivity].
    apply LM.find_2; apply LM.find_1 in mt; rewrite <- H; auto.
    intros l E2 mt; exists E2; split; [|reflexivity].
    apply LM.find_2; apply LM.find_1 in mt; rewrite H; auto.
  Qed.

  (* LIFTING SYSTEM STEPS ACROSS NONDETERMINSM *)

  (* 
     In order to prove deadlock-freedom by construction, we need to lift system steps across
     nondeterminism. We know how to do this for control-language expressions. However, to
     do this with synciota steps, we need to "complete the diagram" above with every 
     location. 
     
     In order to do this, we develop a function that takes a lookup list of "incomplete
     diagrams" and returns their "completions". This turns out to be quite involved.
   *)


  (* NoRepeatsN holds of a lookup list for a loc to N control expressions if no location
     appears in the lookup list more than once. *)
  Fixpoint NoRepeats1 (ℓ : list (Loc * CtrlExpr)) : Prop :=
    match ℓ with
    | [] => True
    | (p, _) :: ℓ =>
      NoRepeats1 ℓ /\ forall q E, In (q, E) ℓ -> p <> q
    end.

  Fixpoint NoRepeats2 (ℓ : list (Loc * CtrlExpr * CtrlExpr)) : Prop :=
    match ℓ with
    | [] => True
    | (p, _, _) :: ℓ => NoRepeats2 ℓ /\ forall q E1 E2, In (q, E1, E2) ℓ -> p <> q
    end.
  
  Fixpoint NoRepeats3 (ℓ : list (Loc * CtrlExpr * CtrlExpr * CtrlExpr)) : Prop :=
    match ℓ with
    | [] => True
    | (p, _, _, _) :: ℓ =>
      NoRepeats3 ℓ /\ forall q E1 E2 E3, In (q, E1, E2, E3) ℓ -> p <> q
    end.
  
  Lemma NoRepeats1Map : forall ℓ, NoRepeats1 ℓ -> forall p E1 E1',
        In (p, E1) ℓ -> In (p, E1') ℓ -> E1' = E1.
  Proof using.
    intros ℓ; induction ℓ as [|q E2]; intros nr p E1 E1' i i'.
    - inversion i.
    - cbn in nr; destruct i as [eq |i]; [symmetry in eq; inversion eq; clear eq; subst|].
      destruct i' as [eq |i']; [symmetry in eq; inversion eq; clear eq; subst; auto|].
      destruct nr. apply H0 in i'. destruct (i' eq_refl).
      destruct i' as [eq |i']; [symmetry in eq; inversion eq; clear eq; subst|].
      destruct nr. apply H0 in i. destruct (i eq_refl).
      destruct q.
      eapply IHE2; eauto. apply nr.
  Qed.
  
  Lemma NoRepeats3Map : forall ℓ, NoRepeats3 ℓ -> forall p E1 E2 E3 E1' E2' E3',
        In (p, E1, E2, E3) ℓ -> In (p, E1',E2',E3') ℓ -> E1' = E1 /\ E2' = E2 /\ E3' = E3.
  Proof using.
    intros ℓ; induction ℓ as [|[[[q E1'']E2'']E3''] ℓ];
      intros nr p E1 E2 E3 E1' E2' E3' i i'.
    - inversion i.
    - cbn in nr; destruct i as [eq |i]; [symmetry in eq; inversion eq; clear eq; subst|].
      destruct i' as [eq |i']; [symmetry in eq; inversion eq; clear eq; subst|].
      split; [|split]; reflexivity.
      destruct nr. apply H0 in i'. destruct (i' eq_refl).
      destruct i' as [eq |i']; [symmetry in eq; inversion eq; clear eq; subst|].
      destruct nr. apply H0 in i. destruct (i eq_refl).
      eapply IHℓ; eauto. apply nr.
  Qed.

  (*
    We build our lookup lists in layers, adding more and more expressions from systems.
   *)
  Fixpoint AddFromMap1 (ℓ : list (Loc * CtrlExpr)) (Π : ConSystem)
    : list (Loc * CtrlExpr * CtrlExpr) :=
    match ℓ with
    | [] => []
    | (p, E1) :: ℓ => match LM.find p Π with
                    | Some E2 => (p, E1, E2) :: AddFromMap1 ℓ Π
                    | None => AddFromMap1 ℓ Π
                    end
    end.

  Lemma InAddFromMap1 : forall ℓ Π l E1 E2,
      In (l, E1, E2) (AddFromMap1 ℓ Π) <-> In (l, E1) ℓ /\ LM.MapsTo l E2 Π.
  Proof using.
    intro ℓ; induction ℓ as [|l ℓ]; intros Π l' E1 E2.
    all: split; [intro i; split | intros [i mt]]; cbn in *; auto.
    - destruct i.
    - destruct l as [q Eq1].
      destruct (LM.find q Π) eqn:eq.
      destruct i as [eq' | i]; [symmetry in eq'; inversion eq'; clear eq'; subst|].
      -- left; auto.
      -- right; eapply IHℓ; eauto.
      -- right; eapply IHℓ; eauto.
    - destruct l as [q Eq1].
      destruct (LM.find q Π) eqn:eq. 
      destruct i as [eq' | i]; [symmetry in eq'; inversion eq'; clear eq'; subst|].
      -- apply LM.find_2; auto.
      -- eapply IHℓ; eauto.
      -- eapply IHℓ; eauto.
    - destruct l as [q Eq1].
      destruct (LM.find q Π) eqn:eq.
      all: destruct i as [eq' | i]; [symmetry in eq'; inversion eq'; clear eq'; subst|].
      -- apply LM.find_1 in mt; rewrite mt in eq; inversion eq; clear eq; subst.
         left; reflexivity.
      -- right; eapply IHℓ; eauto.
      -- apply LM.find_1 in mt; rewrite mt in eq; inversion eq; clear eq; subst.
      -- eapply IHℓ; eauto.
  Qed.

  Lemma AddFromMap1NoRepeats : forall ℓ Π, NoRepeats1 ℓ -> NoRepeats2 (AddFromMap1 ℓ Π).
  Proof using.
    intros ℓ; induction ℓ as [|l ℓ]; intros Π nr; cbn in *; auto.
    destruct l as [p Ep]. destruct nr as [nr H].
    destruct (LM.find p Π); cbn; [split|].
    - apply IHℓ; auto.
    - intros q E1 E2 i.
      apply InAddFromMap1 in i; destruct i.
      eapply H; eauto.
    - apply IHℓ; auto.
  Qed.
  
  Fixpoint AddFromMap2 (ℓ : list (Loc * CtrlExpr * CtrlExpr)) (Π : ConSystem)
    : list (Loc * CtrlExpr * CtrlExpr * CtrlExpr) :=
    match ℓ with
    | [] => []
    | (p, E1, E2) :: ℓ => match LM.find p Π with
                        | Some E3 => (p, E1, E2, E3) :: AddFromMap2 ℓ Π
                        | None => AddFromMap2 ℓ Π
                        end
    end.

  Lemma InAddFromMap2 : forall ℓ Π p E1 E2 E3,
      In (p, E1, E2, E3) (AddFromMap2 ℓ Π) <-> In (p, E1, E2) ℓ /\ LM.MapsTo p E3 Π.
  Proof using.
    intro ℓ; induction ℓ as [|l ℓ]; intros Π p E1 E2.
    all: split; [intro i; split | intros [i mt]]; cbn in *; auto.
    - destruct i.
    - destruct l as [[q Eq1] Eq2].
      destruct (LM.find q Π) eqn:eq.
      destruct i as [eq' | i]; [symmetry in eq'; inversion eq'; clear eq'; subst|].
      -- left; auto.
      -- right; eapply IHℓ; eauto.
      -- right; eapply IHℓ; eauto.
    - destruct l as [[q Eq1] Eq2].
      destruct (LM.find q Π) eqn:eq. 
      destruct i as [eq' | i]; [symmetry in eq'; inversion eq'; clear eq'; subst|].
      -- apply LM.find_2; auto.
      -- eapply IHℓ; eauto.
      -- eapply IHℓ; eauto.
    - destruct l as [[q Eq1] Eq2].
      destruct (LM.find q Π) eqn:eq.
      all: destruct i as [eq' | i]; [symmetry in eq'; inversion eq'; clear eq'; subst|].
      -- apply LM.find_1 in mt; rewrite mt in eq; inversion eq; clear eq; subst.
         left; reflexivity.
      -- right; eapply IHℓ; eauto.
      -- apply LM.find_1 in mt; rewrite mt in eq; inversion eq; clear eq; subst.
      -- eapply IHℓ; eauto.
  Qed.

  Lemma AddFromMap2NoRepeats : forall ℓ Π, NoRepeats2 ℓ -> NoRepeats3 (AddFromMap2 ℓ Π).
  Proof using.
    intros ℓ; induction ℓ as [|l ℓ]; intros Π nr; cbn in *; auto.
    destruct l as [[p Ep1] Ep2]. destruct nr as [nr H].
    destruct (LM.find p Π); cbn; [split|].
    - apply IHℓ; auto.
    - intros q E1 E2 E3 i.
      apply InAddFromMap2 in i; destruct i.
      eapply H; eauto.
    - apply IHℓ; auto.
  Qed.

  (*
    Now we solve each of the diagrams in our list. For deadlock-freedom, we need to be able
    to lift steps, while for soundness we need to be able to lower steps. Of course, we can
    only lower steps when it they are not choice steps.
   *)
  Lemma LiftListOfSteps : forall (ℓ : list (Loc * CtrlExpr * CtrlExpr * CtrlExpr)) (L : Label),
      (* The diagrams: *)
      (forall l E1 E2 E3, In (l, E1, E2, E3) ℓ -> LessNondet E1 E2 /\ CtrlExprStep E1 L E3) ->
      NoRepeats3 ℓ ->
      exists ℓ' : list (Loc * CtrlExpr),
        (* ℓ' contains solutions to the diagrams: *)
        (forall l E, In (l, E) ℓ' -> exists E1 E2 E3, In (l, E1, E2, E3) ℓ /\
                                           LessNondet E3 E /\
                                           CtrlExprStep E2 L E) /\
        NoRepeats1 ℓ' /\
        (* And it solves *all* of the diagrams. *)
        (forall l E1 E2 E3, In (l, E1, E2, E3) ℓ -> exists E, In (l, E) ℓ').
  Proof using.
    intro ℓ; induction ℓ as [| [[[p E1] E2] E3] ℓ]; intros L steps uniq.
    - exists []; split; [|split]. intros p E i; inversion i. auto.
      intros p E1 E2 E3 i; inversion i.
    - destruct (IHℓ L) as [ℓ' [P [Q R]]].
      intros q E1' E2' E3' i. apply steps with (l := q); right; auto.
      cbn in uniq; apply uniq.
      specialize (steps p E1 E2 E3 ltac:(left; auto)); destruct steps as [lnd step].
      destruct (LiftStepWithNondet E1 L E3 E2 step lnd) as [E4 [lnd' step']].
      exists ((p, E4) :: ℓ'); split; [|split].
      -- intros q E i; destruct i as [eq |i]. symmetry in eq; inversion eq; clear eq; subst.
         exists E1; exists E2; exists E3; split; [left; reflexivity|split]; auto.
         destruct (P q E i) as [E1' [E2' [E3' [i' [lnd'' step'']]]]].
         exists E1'; exists E2'; exists E3'; split; [right; auto|split]; auto.
      -- cbn; split; auto.
         cbn in uniq; destruct uniq. intros q E i.
         destruct (P q E i) as [E' [E'' [E''' [i' _]]]].
         eapply H0; eauto.
      -- intros q E E' E'' i; destruct i as [eq | i];
           [symmetry in eq; inversion eq; clear eq; subst|].
         exists E4; left; reflexivity.
         destruct (R q E E' E'' i) as [E''' i']. exists E'''; right; auto.
  Qed.         

  Lemma LowerListOfSteps : forall (ℓ : list (Loc * CtrlExpr * CtrlExpr * CtrlExpr)) (L : Label),
      ~ IsAllowChoiceLabel L ->
      (forall l E1 E2 E3, In (l, E1, E2, E3) ℓ -> LessNondet E2 E1 /\ CtrlExprStep E1 L E3) ->
      NoRepeats3 ℓ ->
        exists ℓ' : list (Loc * CtrlExpr),
          (forall l E, In (l, E) ℓ' -> exists E1 E2 E3, In (l, E1, E2, E3) ℓ /\
                                            LessNondet E E3 /\
                                            CtrlExprStep E2 L E) /\
          NoRepeats1 ℓ' /\
          (forall l E1 E2 E3, In (l, E1, E2, E3) ℓ -> exists E, In (l, E) ℓ').
  Proof using.
    intro ℓ; induction ℓ as [| [[[p E1] E2] E3] ℓ]; intros L neq steps uniq.
    - exists []; split; [|split]. intros p E i; inversion i. auto.
      intros p E1 E2 E3 i; inversion i.
    - destruct (IHℓ L) as [ℓ' [P [Q R]]]; auto.
      intros q E1' E2' E3' i. apply steps with (l := q); right; auto.
      cbn in uniq; apply uniq.
      specialize (steps p E1 E2 E3 ltac:(left; auto)); destruct steps as [lnd step].
      destruct (LowerStepNondet E1 L E3 E2 step lnd neq) as [E4 [lnd' step']].
      exists ((p, E4) :: ℓ'); split; [|split].
      -- intros q E i; destruct i as [eq |i]. symmetry in eq; inversion eq; clear eq; subst.
         exists E1; exists E2; exists E3; split; [left; reflexivity|split]; auto.
         destruct (P q E i) as [E1' [E2' [E3' [i' [lnd'' step'']]]]].
         exists E1'; exists E2'; exists E3'; split; [right; auto|split]; auto.
      -- cbn; split; auto.
         cbn in uniq; destruct uniq. intros q E i.
         destruct (P q E i) as [E' [E'' [E''' [i' _]]]].
         eapply H0; eauto.
      -- intros q E E' E'' i; destruct i as [eq | i];
           [symmetry in eq; inversion eq; clear eq; subst|].
         exists E4; left; reflexivity.
         destruct (R q E E' E'' i) as [E''' i']. exists E'''; right; auto.
  Qed.

  (*
    Finally, we can repackage the solutions into a new system.
   *)
  Fixpoint OutputListToSystem (ℓ : list (Loc * CtrlExpr)) : ConSystem :=
    match ℓ with
    | [] => LM.empty
    | (p, E) :: ℓ => LM.add p E (OutputListToSystem ℓ)
    end.

  Lemma OutputUniqueToSystem : forall ℓ,
      NoRepeats1 ℓ -> forall p E, In (p, E) ℓ <-> LM.MapsTo p E (OutputListToSystem ℓ).
  Proof using.
    intros ℓ; induction ℓ as [| [l E'] ℓ]; intros H p E; cbn.
    split; intro i; [inversion i|]; apply LM.empty_1 in i; auto.
    split; [intro i | intro mt].
    - destruct i as [eq | i]; [inversion eq; subst|].
      apply LM.add_1.
      cbn in H; destruct H. specialize (H0 p E i). apply LM.add_2; auto. apply IHℓ; auto.
    - destruct (L.eq_dec p l); subst.
      pose proof (LMF.MapsToUnique mt (LM.add_1 (OutputListToSystem ℓ) l E')); subst.
      left; reflexivity.
      cbn in H; destruct H.
      right; apply IHℓ; auto. apply LM.add_3 in mt; auto.
  Qed.

  (*
    Finally, we can prove the lifting lemma. We use lowering directly in the proof of 
    soundness.
   *)
  Lemma LiftSystemNondetStep : forall Π11 L Π12 Π21,
      SystemStep Π11 L Π12 ->
      LessNondetSystem Π11 Π21 ->
      exists Π22, LessNondetSystem Π12 Π22 /\ SystemStep Π21 L Π22.
  Proof using.
    intros Π11 L Π12 Π21 step; revert Π21; induction step; intros Π21 lnds.
    - apply lnds in H; destruct H as [E1' [mt1' lnd1]].
      destruct (LiftStepWithNondet E1 L E2 E1' H0 lnd1) as [E2' [lnd2 step']].
      exists (LM.add p E2' Π21); split; auto.
      2: { eapply IotaStep; eauto. apply LM.add_1.
           intros q E H; split; intro mt. apply LM.add_2; auto.
           apply LM.add_3 in mt; auto. }
      unfold LessNondetSystem; split.
      -- intros l E2'' mt2.
         destruct (L.eq_dec p l); subst.
         exists E2'; split; [apply LM.add_1|]; pose proof (LMF.MapsToUnique mt2 H2); subst; auto.
         apply H3 in mt2; auto. apply lnds in mt2.
         destruct mt2 as [E2''' [mt21 lnd']].
         exists E2'''; split; [apply LM.add_2|]; auto.
      -- intros l E21 mt21.
         destruct (L.eq_dec p l); subst.
         pose proof (LMF.MapsToUnique mt21 (LM.add_1 Π21 l E2')); subst.
         exists E2; split; auto.
         apply LM.add_3 in mt21; auto.
         apply lnds in mt21. destruct mt21 as [E1'' [mt1'' lnd'']].
         apply H3 in mt1''; auto. exists E1''; split; auto.
    - refine (let l :=  (AddFromMap2 (AddFromMap1 (LM.elements Π1) Π21) Π2) in _).
      assert (forall p E1 E2 E3, In (p, E1, E2, E3) l <->
                            LM.MapsTo p E1 Π1 /\
                            LM.MapsTo p E2 Π21 /\
                            LM.MapsTo p E3 Π2)
        by (pose proof (InAddFromMap2 (AddFromMap1 (LM.elements Π1) Π21) Π2);
            pose proof (InAddFromMap1 (LM.elements Π1) Π21);
            intros p E1 E2 E3; split; [intro i; split; [|split] | intros [mt1 [mt2 mt3]]];
            [ apply H2 in i; destruct i; apply H3 in H4; destruct H4;
              apply LM.elements_2; auto
            |  apply H2 in i; destruct i; apply H3 in H4; apply H4
            |  apply H2 in i; apply i
            | apply LM.elements_1 in mt1;
              pose proof (proj2 (H3 p E1 E2) ltac:(split; auto));
              apply H2; split ; auto]).
      assert (forall p E1 E2 E3, In (p, E1, E2, E3) l -> LessNondet E1 E2 /\ CtrlExprStep E1 L E3).
      intros p E1 E2 E3 i; apply H2 in i; destruct i as [mt1 [mt2 mt3]]; split.
      apply lnds in mt1; destruct mt1 as [E2' [mt21 lnd]].
      pose proof (LMF.MapsToUnique mt21 mt2); subst; auto.
      apply (H0 p); auto.
      assert (NoRepeats3 l); unfold l. apply AddFromMap2NoRepeats.
      apply AddFromMap1NoRepeats. pose proof (LM.elements_3w Π1).
      revert H4. generalize (LM.elements Π1). clear Π1 L H H0 H1 Π21 lnds l H2 H3 Π2.
      intro l; induction l; intro nd; cbn; auto.
      destruct a as [p E]; cbn in *.
      inversion nd; subst. split; [apply IHl; auto|].
      intros q E' i eq; apply H1; subst. apply InA_altdef; apply Exists_exists.
      exists (q, E'); split; cbn; auto. unfold LM.eq_key; cbn; auto.
      destruct (LiftListOfSteps l L H3 H4) as [ℓ [nd [nr eqv]]].
      exists (OutputListToSystem ℓ); split; [unfold LessNondetSystem; split|
                                        eapply SyncIotaStep; eauto].
      -- intros p E2 mt2.
         destruct (proj2 (H1 p) ltac:(exists E2; apply mt2)) as [E1 mt1].
         destruct (proj1 lnds p E1 mt1) as [E3 [mt3 lnd]].
         pose proof (proj2 (H2 p E1 E3 E2) ltac:(split; [|split]; auto)).
         destruct (eqv p E1 E3 E2 H5) as [E i].
         pose proof (proj1 (OutputUniqueToSystem ℓ nr p E) i).
         destruct (nd p E i) as [E1' [E2' [E3' [i' [lnd' step]]]]].
         destruct (NoRepeats3Map l H4 _ _ _ _ _ _ _ H5 i') as [eq1 [eq2 eq3]]; subst.
         exists E; split; auto.
      -- intros p E mt. apply OutputUniqueToSystem in mt; auto.
         destruct (nd p E mt) as [E1 [E2 [E3 [i [lnd step]]]]].
         exists E3; split; auto. eapply H2; eauto.
      -- intros p E21 E22 mt21 mt22.
         apply OutputUniqueToSystem in mt22; auto.
         destruct (nd p E22 mt22) as [E1 [E22' [E2 [i [lnd step]]]]].
         destruct (proj1 (H2 p E1 E22' E2) i) as [mt1 [mt21' mt2]].
         pose proof (LMF.MapsToUnique mt21' mt21); subst. auto.
      -- intros p; split; [intros [E mt21] | intros [E mt22]].
         --- destruct (proj2 lnds p E mt21) as [E1 [mt1 lnd]].
             destruct (proj1 (H1 p) ltac:(exists E1; apply mt1)) as [E2 mt2].
             pose proof (proj2 (H2 p E1 E E2) ltac:(split; [|split]; auto)).
             destruct (eqv p E1 E E2 H5) as [E' i].
             apply OutputUniqueToSystem in i; auto.
             exists E'; auto.
         --- apply OutputUniqueToSystem in mt22; auto.
             destruct (nd p E mt22) as [E1 [E21 [E2 [i [lnd step]]]]].
             destruct (proj1 (H2 p E1 E21 E2) i) as [mt1 [mt21 mt2]].
             exists E21; auto.
    - apply lnds in H1. destruct H1 as [pE21 [mt211 lnd1]].
      apply lnds in H2. destruct H2 as [qE21 [mt212 lnd2]].
      destruct (LiftStepWithNondet pE1 L1 pE2 pE21 H3 lnd1) as [pE22 [lnd1' step1']].
      destruct (LiftStepWithNondet qE1 L2 qE2 qE21 H4 lnd2) as [qE22 [lnd2' step2']].
      exists (LM.add p pE22 (LM.add q qE22 Π21)); split.
      2: { eapply CommStep; eauto. apply LM.add_1. apply LM.add_2; auto; apply LM.add_1.
           intros r E neq1 neq2; split; intro mt.
           repeat apply LM.add_2; auto. do 2 apply LM.add_3 in mt; auto. }
      unfold LessNondetSystem; split; intros l E.
      -- intro mt2. destruct (L.eq_dec l p); [subst|destruct (L.eq_dec l q); subst].
         exists pE22; split; auto. apply LM.add_1.
         pose proof (LMF.MapsToUnique mt2 H5); subst; auto.
         exists qE22; split; auto. apply LM.add_2; auto. apply LM.add_1.
         pose proof (LMF.MapsToUnique mt2 H6); subst; auto.
         apply H7 in mt2; auto. apply lnds in mt2.
         destruct mt2 as [E' [mt21 lnd]].
         exists E'; split; auto. apply LM.add_2; auto. apply LM.add_2; auto.
      -- intro mt21. destruct (L.eq_dec l p); [subst|destruct (L.eq_dec l q); subst].
         exists pE2; split; auto.
         pose proof (LMF.MapsToUnique mt21 (LM.add_1 (LM.add q qE22 Π21) p pE22));
           subst; auto.
         apply LM.add_3 in mt21; auto.
         exists qE2; split; auto.
         pose proof (LMF.MapsToUnique mt21 (LM.add_1 Π21 q qE22)); subst; auto.
         do 2 apply LM.add_3 in mt21; auto.
         apply lnds in mt21. destruct mt21 as [E1 [mt1 lnd]].
         exists E1; split; auto. apply H7; auto.
    - apply lnds in H1. destruct H1 as [pE21 [mt211 lnd1]].
      apply lnds in H2. destruct H2 as [qE21 [mt212 lnd2]].
      destruct (LiftStepWithNondet pE1 L1 pE2 pE21 H3 lnd1) as [pE22 [lnd1' step1']].
      destruct (LiftStepWithNondet qE1 L2 qE2 qE21 H4 lnd2) as [qE22 [lnd2' step2']].
      exists (LM.add p pE22 (LM.add q qE22 Π21)); split.
      2: { eapply ChoiceStep; eauto. apply LM.add_1. apply LM.add_2; auto; apply LM.add_1.
           intros r E neq1 neq2; split; intro mt.
           repeat apply LM.add_2; auto. do 2 apply LM.add_3 in mt; auto. }
      unfold LessNondetSystem; split; intros l E.
      -- intro mt2. destruct (L.eq_dec l p); [subst|destruct (L.eq_dec l q); subst].
         exists pE22; split; auto. apply LM.add_1.
         pose proof (LMF.MapsToUnique mt2 H5); subst; auto.
         exists qE22; split; auto. apply LM.add_2; auto. apply LM.add_1.
         pose proof (LMF.MapsToUnique mt2 H6); subst; auto.
         apply H7 in mt2; auto. apply lnds in mt2.
         destruct mt2 as [E' [mt21 lnd]].
         exists E'; split; auto. apply LM.add_2; auto. apply LM.add_2; auto.
      -- intro mt21. destruct (L.eq_dec l p); [subst|destruct (L.eq_dec l q); subst].
         exists pE2; split; auto.
         pose proof (LMF.MapsToUnique mt21 (LM.add_1 (LM.add q qE22 Π21) p pE22));
           subst; auto.
         apply LM.add_3 in mt21; auto.
         exists qE2; split; auto.
         pose proof (LMF.MapsToUnique mt21 (LM.add_1 Π21 q qE22)); subst; auto.
         do 2 apply LM.add_3 in mt21; auto.
         apply lnds in mt21. destruct mt21 as [E1 [mt1 lnd]].
         exists E1; split; auto. apply H7; auto.
  Qed.         

  Definition DeadlockFree : ConSystem -> Prop :=
    fun Π => forall Ls Π', SystemSteps Π Ls Π' -> ValueSystem Π' \/ exists L Π'', SystemStep Π' L Π''.
  
End CtrlLang.
