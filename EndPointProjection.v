Require Export LocalLang.
Require Export SoundlyTypedLocalLang.
Require Export Locations.
Require Import LocationMap.
Require Export Pirouette.
Require Export SoundlyTypedPirouette.
Require Export CtrlLang.

Require Import Coq.Arith.Arith.
Require Import Coq.Lists.List.
Require Import Permutation.
Require Import Coq.Program.Equality.
Require Import Coq.Lists.SetoidList.


Import ListNotations.

Module EndPointProjection (Import LL : LocalLang) (L : Locations) (LM : LocationMap L)
       (TLL : TypedLocalLang LL) (STLL : SoundlyTypedLocalLang LL TLL)
       (Import SL : SyncLabels).

  Module ICL := (CtrlLang LL L LM SL).
  Import ICL.

  Definition Loc := L.t.
  Module C := (SoundlyTypedPirouette LL TLL STLL L SL).

  (*
    We get right to the definition of endpoint projection. The rest of this module is 
    dedicated to properties of EPP.
   *)
  Fixpoint EPP (C : C.PirExpr) (l : Loc) : option CtrlExpr :=
    match C with
    | C.Done l' e =>
      if L.eq_dec l l'
      then Some (Ret e)
      else Some Unit
    | C.Var x => Some (Var x)
    | C.Send l1 e l2 C =>
      if L.eq_dec l l1
      then if L.eq_dec l l2
           then None
           else match EPP C l with
                | Some E => Some (Send l2 e E)
                | None => None
                end
      else if L.eq_dec l l2
           then match EPP C l with
                | Some E => Some (Recv l1 E)
                | None => None
                end
           else EPP C l
    | C.If l' e C1 C2 =>
      match EPP C1 l with
      | Some E1 =>
        match EPP C2 l with
        | Some E2 =>
          if L.eq_dec l l'
          then Some (If e E1 E2)
          else CtrlExprMerge E1 E2
        | None => None
        end
      | None => None
      end
    | C.Sync l1 LR l2 C =>
      if L.eq_dec l l1
      then if L.eq_dec l l2
           then None
           else match EPP C l with
                | Some E => Some (Choose l2 LR E)
                | None => None
                end
      else if L.eq_dec l l2
           then match EPP C l with
                | Some E => match LR with
                            | Left => Some (AllowChoiceL l1 E)
                            | Right => Some (AllowChoiceR l1 E)
                            end
                | None => None
                end
           else EPP C l
    | C.DefLocal l' C1 C2 =>
      if L.eq_dec l l'
      then match EPP C1 l, EPP C2 l with
           | Some E1, Some E2 => Some (LetRet E1 E2)
           | _, _ => None
           end
      else match EPP C1 l, EPP C2 l with
           | Some E1, Some E2 => Some (AppGlobal (FunGlobal (E2 ⟨ceg| fun n => 2 + n⟩)) E1) (* Use renaming to make sure nothing binds to this binder *)
           | _, _ => None
           end
    | C.FunLocal l' C =>
      if L.eq_dec l l'
      then match EPP C l with
           | Some E => Some (FunLocal E)
           | None => None
           end
      else match EPP C l with
           | Some E => Some (FunGlobal (E ⟨ceg| S⟩)) (* Global so we can feed it a dummy value *)
           | None => None
           end
    | C.FunGlobal C =>
      match EPP C l with
      | Some E => Some (FunGlobal E)
      | None => None
      end
    | C.AppLocal l' C e =>
      if L.eq_dec l' l
      then match EPP C l with
           | Some E => Some (AppLocal E e)
           | None => None
           end
      else match EPP C l with
           | Some E => Some (AppGlobal E Unit)
           | None => None
           end
    | C.AppGlobal C1 C2 =>
      match EPP C1 l, EPP C2 l with
      | Some E1, Some E2 => Some (AppGlobal E1 E2)
      | _, _ => None
      end
    end.

  Fixpoint ProjectSystem (C : C.PirExpr) (nms : list L.t) : option ConSystem :=
    match nms with
    | nil => Some LM.empty
    | cons l nms => match EPP C l, ProjectSystem C nms with
                   | Some E, Some M => Some (LM.add l E M)
                   | _, _ => None
                   end
    end.
  
  (*
    Equivalence (≡) begets equality.
   *)
  Theorem EPPEquivToEq : forall C1 C2 l,
      C.equiv C1 C2 -> EPP C1 l = EPP C2 l.
  Proof using.
    intros C1 C2 l eqv; revert l; induction eqv; intro p; cbn; auto;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ H : ?a <> ?a |- _ ] => inversion H
             | [ d : SyncLabel |- _ ] => destruct d; cbn in *
             | [ H : Some _ = None |- _ ] => inversion H
             | [ H : None = Some _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H; cbn in *
             | [ H1 : ?a = Some _, H2 : ?a = None |- _ ] =>
               rewrite H2 in H1; inversion H1
             | [ |- context[L.eq_dec ?a ?b]] =>
               tryif unify a b
               then let n := fresh in
                    destruct (L.eq_dec a b) as [_ | n];
                      [| destruct (n eq_refl)]
               else lazymatch goal with
                    | [e : a = b |- _ ] =>
                      let n := fresh in
                      destruct (L.eq_dec a b) as [_ | n];
                        [| destruct (n e)]
                    | [e : b = a |- _ ] =>
                      let n := fresh in
                      destruct (L.eq_dec a b) as [_ | n];
                        [| destruct (n (eq_sym e))]
                    | [n : a <> b |- _ ] =>
                      let e := fresh in
                      destruct (L.eq_dec a b) as [e | _];
                        [destruct (n e)|]
                    | [n : b <> a |- _ ] =>
                      let e := fresh in
                      destruct (L.eq_dec a b) as [e | _];
                        [destruct (n (eq_sym e))|]
                    | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (L.eq_dec a b) as [eq|neq]; subst
                    end
             | [ |- context[ExprEqDec ?a ?b]] =>
               tryif unify a b
               then let n := fresh in
                    destruct (ExprEqDec a b) as [_ | n];
                      [| destruct (n eq_refl)]
               else lazymatch goal with
                    | [e : a = b |- _ ] =>
                      let n := fresh in
                      destruct (ExprEqDec a b) as [_ | n];
                        [| destruct (n e)]
                    | [e : b = a |- _ ] =>
                      let n := fresh in
                      destruct (ExprEqDec a b) as [_ | n];
                        [| destruct (n (eq_sym e))]
                    | [n : a <> b |- _ ] =>
                      let e := fresh in
                      destruct (ExprEqDec a b) as [e | _];
                        [destruct (n e)|]
                    | [n : b <> a |- _ ] =>
                      let e := fresh in
                      destruct (ExprEqDec a b) as [e | _];
                        [destruct (n (eq_sym e))|]
                    | _ => let eq := fresh "eq" in
                           let neq := fresh "neq" in
                           destruct (ExprEqDec a b) as [eq|neq]; subst
                    end
             | [ IH : forall l, EPP ?C1 l = EPP ?C2 l |- context[EPP ?C1 ?l]] =>
               rewrite (IH l)
             | [ |- context[EPP ?C ?l]] =>
               lazymatch goal with
               | [e : EPP C l = _ |- _ ] => rewrite e; cbn
               | _ => let eq := fresh "eq" in
                      destruct (EPP C l) eqn:eq; cbn
               end
             | [ |- context[CtrlExprMerge ?E1 ?E2]] =>
               lazymatch goal with
               | [e : CtrlExprMerge E1 E2 = _ |- _ ] => rewrite e; cbn
               | _ => let eq := fresh "eq" in
                      destruct (CtrlExprMerge E1 E2) eqn:eq; cbn
               end
             | [H1 : CtrlExprMerge ?E1 ?E2 = Some ?E5,
                     H2 : CtrlExprMerge ?E3 ?E4 = Some ?E6,
                          H3 : CtrlExprMerge ?E1 ?E3 = Some ?E7,                
                               H4 : CtrlExprMerge ?E2 ?E4 = Some ?E8,
                                    H5 : CtrlExprMerge ?E5 ?E6 = ?a,
                                         H6 : CtrlExprMerge ?E7 ?E8 = ?b |- _ ] =>
               lazymatch goal with
               | [ _ : a = b  |- _ ] => fail
               | _ => let H := fresh in
                     pose proof (MergeCtrlExprIfs E1 E2 E3 E4 E5 E6 E7 E8 H1 H2 H3 H4)
                       as H;
                        rewrite H5 in H; rewrite H6 in H
               end
             | [ H : CtrlExprMerge ?a ?b = ?c |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprMerge b a = c |- _ ] => fail
               | _ => let H' := fresh in pose proof (MergeComm a b) as H'; rewrite H in H';
                                          symmetry in H'
               end
             | [ H1 : CtrlExprMerge ?E1 ?E2 = None, H2 : CtrlExprMerge ?E2 ?E3 = Some ?E4
                 |- _ ] =>
               lazymatch goal with
               | [ _ : CtrlExprMerge E1 E4 = None |- _ ] => fail
               | _ => pose proof (MergeAssocNone E1 E2 E3 E4 H1 H2)
               end
             end.
  Qed.        

  (* EPP AND SUBSTITUTION *)
  
  Theorem EPPLocalSubst : forall C l E σ,
      EPP C l = Some E ->
      EPP (C.PirExprLocalSubst C σ) l = Some (E [cel| σ l]).
  Proof using.
    intros C; induction C; intros p E σ eq; cbn in *;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ H : None = Some _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H; cbn in *
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [ H : context[L.eq_dec ?a ?b] |- _ ] =>
               destruct (L.eq_dec a b); subst; cbn in *
             | [ H : context[EPP ?C ?l] |- _ ] =>
               lazymatch type of H with
               | EPP C l = _ => fail
               | _ => lazymatch goal with
                      | [ H' : EPP C l = _ |- _ ] => rewrite H' in H
                      | _ => let eq := fresh "eq" in
                             destruct (EPP C l) eqn:eq; subst; cbn in *
                      end
               end
             | [ eq: EPP ?C ?l = Some ?E,
                     IH : forall l E σ,
                     EPP ?C l = Some E ->
                     EPP (C.PirExprLocalSubst ?C σ) l = Some (E [cel| σ l])
                     |- context[EPP (C.PirExprLocalSubst ?C ?σ) ?l] ] =>
               rewrite (IH l E σ eq)
             | [ n : ?p <> ?q |- context[C.PirExprUpLocalSubst σ ?p ?q]] =>
               unfold C.PirExprUpLocalSubst;
                 destruct (L.eq_dec p q) as [e |_];
                 [destruct (n (eq_sym e))|]
             | [ n : ?q <> ?p |- context[C.PirExprUpLocalSubst σ ?p ?q]] =>
               let e := fresh "e" in 
               unfold C.PirExprUpLocalSubst;
                 destruct (L.eq_dec p q) as [e |_];
                 [destruct (n (eq_sym e))|]
             | [ |- context[C.PirExprUpLocalSubst σ ?p ?p]] =>
               let n := fresh "n" in 
               unfold C.PirExprUpLocalSubst;
                 destruct (L.eq_dec p p) as [_ |n];
                 [|destruct (n eq_refl)];
                 unfold ExprUpSubst
             end; try reflexivity.
    - erewrite MergeLocalSubst; eauto.
    - destruct d; inversion eq; subst; auto.
    - rewrite LocalSubstGlobalRenameComm; reflexivity.
    - rewrite LocalSubstGlobalRenameComm; reflexivity.
  Qed.

  Theorem ProjectUpSubstAllLocalNeq : forall σ l l' σ',
      (forall n, EPP (σ n) l = Some (σ' n)) ->
      l <> l' ->
      forall n, EPP (C.PirExprUpSubstAllLocal σ l' n) l = Some (σ' n).
  Proof using.
    intros σ l l' σ' eq neq n.
    unfold C.PirExprUpSubstAllLocal. rewrite C.PirExprLocalRenameSpec.
    rewrite EPPLocalSubst with (E := σ' n); auto.
    destruct (L.eq_dec l' l) as [e | _ ]; [destruct (neq (eq_sym e))|].
    rewrite CtrlExprLocalIdSubst. reflexivity.
  Qed.

  Theorem ProjectUpSubstAllLocalEq : forall σ l σ',
      (forall n, EPP (σ n) l = Some (σ' n)) ->
      forall n, EPP (C.PirExprUpSubstAllLocal σ l n) l = Some (σ' n ⟨cel| S⟩).
  Proof using.
    intros σ l σ' H n.
    unfold C.PirExprUpSubstAllLocal. rewrite C.PirExprLocalRenameSpec.
    rewrite EPPLocalSubst with (E := σ' n); auto.
    destruct (L.eq_dec l l) as [_ | neq]; [| destruct (neq eq_refl)].
    rewrite CtrlExprLocalRenameSpec; auto.
  Qed.
  
  Theorem EPPRename : forall C l E ξ,
      EPP C l = Some E ->
      EPP (C.PirExprRename C ξ) l = Some (E ⟨ceg| ξ⟩).
  Proof using.
    intros C; induction C; intros p E ξ eq; cbn in *;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ d : SyncLabel |- _ ] => destruct d
             | [ H : None = Some _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H; cbn in *
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [ H : context[L.eq_dec ?a ?b] |- _ ] =>
               destruct (L.eq_dec a b); subst; cbn in *
             | [ H : context[EPP ?C ?l] |- _ ] =>
               lazymatch type of H with
               | EPP C l = _ => fail
               | _ => lazymatch goal with
                     | [ H' : EPP C l = _ |- _ ] => rewrite H' in H
                     | _ => let eq := fresh "eq" in
                           destruct (EPP C l) eqn:eq; subst; cbn in *
                     end
               end
             | [ IH: forall l E ξ, EPP ?C l = Some E ->
                              EPP (C.PirExprRename ?C ξ) l = Some (E ⟨ceg| ξ⟩),
                   H : EPP ?C ?l = Some ?E |- context[EPP (C.PirExprRename ?C ?ξ) ?l]] =>
               rewrite (IH l E ξ H)
             end.
    - repeat rewrite GlobalRenameSpec. apply MergeGlobalSubst; auto.
    - repeat rewrite CtrlExprGlobalRenameFusion; cbn. reflexivity.
    - unfold C.PirExprUpRename. unfold UpRename. reflexivity.
    - repeat rewrite CtrlExprGlobalRenameFusion; cbn.
      unfold C.PirExprUpRename; unfold UpRename.
      reflexivity.
    - repeat unfold C.PirExprUpRename. repeat unfold UpRename. reflexivity.
  Qed.

  Theorem ProjectUpSubst : forall σ l σ',
      (forall n, EPP (σ n) l = Some (σ' n)) ->
      forall n, EPP (C.PirExprUpSubst σ n) l = Some (GlobalUpSubst σ' n).
  Proof using.
    intros σ l σ' H n.
    unfold C.PirExprUpSubst; destruct n; cbn; auto.
    apply EPPRename; auto.
  Qed.

  Theorem EPPSubst : forall C l E σ σ',
      (forall n, EPP (σ n) l = Some (σ' n)) ->
      EPP C l = Some E ->
      EPP (C.PirExprSubst C σ) l = Some (E [ceg| σ']).
  Proof using.
    intro C; induction C; intros p E σ σ' eq_σ eq_C; cbn in *;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ d : SyncLabel |- _ ] => destruct d
             | [ H : None = Some _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H; cbn in *
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [ H : context[L.eq_dec ?a ?b] |- _ ] =>
               destruct (L.eq_dec a b); subst; cbn in *
             | [ H : context[EPP ?C ?l] |- _ ] =>
               lazymatch type of H with
               | EPP C l = _ => fail
               | _ => lazymatch goal with
                     | [ H' : EPP C l = _ |- _ ] => rewrite H' in H
                     | _ => let eq := fresh "eq" in
                           destruct (EPP C l) eqn:eq; subst; cbn in *
                     end
               end
             | [ IH : (forall l E σ σ',
                          (forall n, EPP (σ n) l = Some (σ' n)) ->
                          EPP ?C l = Some E ->
                          EPP (C.PirExprSubst ?C σ) l = Some (E [ceg| σ'])),
                      H : EPP ?C ?l = Some ?E,
                          H' : (forall n, EPP (?σ n) ?l = Some (?σ' n))
                 |- context[EPP (C.PirExprSubst ?C ?σ) ?l]] =>
               rewrite (IH l E σ σ' H' H)
             | [ H : (forall n, EPP (?σ n) ?l = Some (?σ' n)),
                     neq : ?l <> ?l' |- context[C.PirExprUpSubstAllLocal ?σ ?l'] ] =>
               lazymatch goal with
               | [ _ : forall n, EPP (C.PirExprUpSubstAllLocal σ l' n) l = Some (σ' n) |- _ ] =>
                 fail
               | _ => pose proof (ProjectUpSubstAllLocalNeq σ l l' σ' H neq)
               end
             | [ H : (forall n, EPP (?σ n) ?l = Some (?σ' n)),
                     neq : ?l' <> ?l |- context[C.PirExprUpSubstAllLocal ?σ ?l'] ] =>
               lazymatch goal with
               | [ _ : forall n, EPP (C.PirExprUpSubstAllLocal σ l' n) l = Some (σ' n) |- _ ] =>
                 fail
               | _ =>
                 pose proof (ProjectUpSubstAllLocalNeq
                               σ l l' σ' H
                               ltac:(let eq := fresh in
                                     intro eq;
                                     destruct (neq (eq_sym eq))
                                    )
                            )
               end
             | [ H : (forall n, EPP (?σ n) ?l = Some (σ' n))
                 |- context[C.PirExprUpSubstAllLocal ?σ ?l]] =>
               lazymatch goal with
               | [ _ : forall n, EPP (C.PirExprUpSubstAllLocal σ l n) l =
                            Some ((fun m => σ' m ⟨cel| S⟩) n) |- _ ] => fail
               | _ =>  assert (forall n,
                                 EPP (C.PirExprUpSubstAllLocal σ l n) l =
                                 Some ((fun m => σ' m ⟨cel| S⟩) n))
                   by (apply ProjectUpSubstAllLocalEq; auto)
               end
             end.
    - apply eq_σ.
    - apply MergeGlobalSubst; auto.
    - apply f_equal.
      rewrite CtrlExprGlobalRenameSubstFusion.
      rewrite CtrlExprGlobalSubstRenameFusion. cbn.
      erewrite CtrlExprGlobalSubstExt; [reflexivity|].
      intro m; cbn. rewrite CtrlExprGlobalRenameFusion; reflexivity.
    - rewrite IHC with (E := c) (σ' := GlobalUpSubst (fun n => σ' n ⟨cel| S⟩)); auto.
      apply ProjectUpSubst. intro n; apply ProjectUpSubstAllLocalEq; auto.
    - rewrite IHC with (E := c) (σ' := GlobalUpSubst σ'); auto.
      2: { apply ProjectUpSubst. apply ProjectUpSubstAllLocalNeq; auto. }
      rewrite CtrlExprGlobalSubstRenameFusion.
      rewrite CtrlExprGlobalRenameSubstFusion; reflexivity.
    - rewrite IHC with (E := c) (σ' := GlobalUpSubst (GlobalUpSubst σ')); auto.
      repeat apply ProjectUpSubst; auto.
  Qed.      

  (* PROJECT SYSTEM LOOKUP *)

  (*
    Looking up the expression bound to a location `l` in a system projected from a pirouette
    program `C` yields the projection of `C` at `l`. If nothing can be found, then `l` 
    cannot be projected from `C`.
   *)

  Lemma ProjectSystemLookup : forall C nms Π,
      ProjectSystem C nms = Some Π ->
      forall l E, LM.MapsTo l E Π <->
             (In l nms /\ EPP C l = Some E).
  Proof using.
    intros C nms; revert C; induction nms as [| l nms]; intros C Π eq l' E; cbn in eq.
    - inversion eq; subst; clear eq. split; [intro mt | intro H; destruct H as [i eq]].
      exfalso; apply LM.empty_1 in mt; assumption. inversion i.
    - destruct (EPP C l) as [E'|] eqn:eq0; try discriminate.
      destruct (ProjectSystem C nms) as [Π'|] eqn:eq1; inversion eq; subst; clear eq.
      destruct (L.eq_dec l l'); subst.
      all: split; [intro mt; split | intros [i eq2]]; cbn in *; auto.
      pose proof (LMF.MapsToUnique mt (LM.add_1 Π' l' E')); subst; auto.
      rewrite eq2 in eq0; inversion eq0; subst; apply LM.add_1.
      right.
      1,2: apply LM.add_3 in mt; auto; pose proof (proj1 (IHnms C Π' eq1 l' E) mt) as H;
        destruct H; auto.
      destruct i as [eq | i]; [destruct (n eq)|].
      apply LM.add_2; auto.
      eapply IHnms; eauto.
  Qed.

  Lemma ProjectSystemLookupNone : forall C nms l,
      In l nms ->
      EPP C l = None ->
      ProjectSystem C nms = None.
  Proof using.
    intros C nms; revert C; induction nms as [| l' nms]; intros C l i eq.
    inversion i.
    destruct i as [H | i]; subst; cbn.
    rewrite eq; auto.
    destruct (EPP C l'); [| reflexivity].
    rewrite IHnms with (l := l); auto.
  Qed.

  (* PROJECT REDEX *)
  Fixpoint ProjectRedex (R : C.Redex) (l : Loc) : option Label :=
    match R with
    | C.RDone p _ _ =>
      if L.eq_dec l p
      then Some Iota
      else None
    | C.RIfE p _ _ =>
      if L.eq_dec l p
      then Some Iota
      else None
    | C.RIfTT p =>
      if L.eq_dec l p
      then Some Iota
      else None
    | C.RIfFF p =>
      if L.eq_dec l p
      then Some Iota
      else None
    | C.RSendE p e1 e2 q =>
      if L.eq_dec l p
      then Some Iota
      else None 
    | C.RSendV p v q =>
      if L.eq_dec l p
      then if L.eq_dec l q
           then None
           else Some (SendLabel q v)
      else if L.eq_dec l q
           then Some (RecvLabel p v)
           else None
    | C.RSync p LR q =>
      if L.eq_dec l p
      then if L.eq_dec l q
           then None
           else Some (ChooseLabel q LR)
      else if L.eq_dec l q
           then Some (AllowChoiceLabel p LR)
           else None 
    | C.RDefLocal p _ => Some SyncIota
    | C.RAppLocalE p _ _ =>
      if L.eq_dec l p
      then Some Iota
      else None
    | C.RAppLocal _ _ =>
      Some SyncIota
    | C.RAppGlobal => Some SyncIota
    | C.RFun R => match ProjectRedex R l with
                 | Some L => Some (FunLabel L)
                 | None => None
                 end
    | C.RArg R => match ProjectRedex R l with
                 | Some L => Some (ArgLabel L)
                 | None => None
                 end
    end.

  Theorem ProjectRedexBlocked : forall C1 R B C2 l,
      C.PirStep R B C1 C2 ->
      In l B ->
      ProjectRedex R l = None.
  Proof using.
    intros C1 R B C2 l step; revert l; induction step; intros p i; cbn;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ H : ?P |- ?P ] => exact H
             | [H : ?P, H' : ~?P |- _ ] => destruct (H' H)
             | [H : In _ [] |- _ ] => inversion H
             | [|- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
             | [ i : In ?p B, IH : forall l, In l B -> _ |- _ ] =>
               specialize (IH p i)
             | [ i : In ?p B, IH : forall l, In l (_ :: B) -> _ |- _ ] =>
               specialize (IH p ltac:(right; exact i))
             | [ i : In ?p B, IH : forall l, In l (_ :: _ :: B) -> _ |- _ ] =>
               specialize (IH p ltac:(right; right; exact i))
             | [ |- context[ProjectRedex ?R ?l]] =>
               let eq := fresh in
               destruct (ProjectRedex R l) eqn:eq
             end; try discriminate.
  Qed.

  (* 
     A control-expression redex and a Pirouette redex are compatible at a location `l` if
     the control-expression redex represents the same reduction as the Pirouette redex
     from `l`'s point of view.
   *)
  Inductive CompatibleRedices (l : Loc) : CtrlExprRedex -> C.Redex -> Prop :=
  | RetCompat : forall e1 e2, CompatibleRedices l (RetE e1 e2) (C.RDone l e1 e2)
  | IfECompat : forall e1 e2, CompatibleRedices l (IfE e1 e2) (C.RIfE l e1 e2)
  | IfTTCompat : CompatibleRedices l IfTT (C.RIfTT l)
  | IfFFCompat : CompatibleRedices l IfFF (C.RIfFF l)
  | SendECompat : forall l' e1 e2,
      CompatibleRedices l (SendE l' e1 e2) (C.RSendE l e1 e2 l')
  | SendVCompat : forall l' v,
      l <> l' -> CompatibleRedices l (SendV l' v) (C.RSendV l v l')
  | RecvCompat : forall l' v,
      l <> l' -> CompatibleRedices l (RecvV l' v) (C.RSendV l' v l)
  | ChooseCompat : forall l' d,
      l <> l' -> CompatibleRedices l (ChooseRedex l' d) (C.RSync l d l')
  | ChoiceLCompat : forall l',
      l <> l' -> CompatibleRedices l (AllowChoiceLRedex l') (C.RSync l' Left l)
  | ChoiceRCompat : forall l',
      l <> l' -> CompatibleRedices l (AllowChoiceRRedex l') (C.RSync l' Right l)
  | LetRetCompat : forall v, CompatibleRedices l (LetRetRedex v) (C.RDefLocal l v)
  | AppLocalECompat : forall e1 e2, CompatibleRedices l (AppLocalE e1 e2) (C.RAppLocalE l e1 e2)
  | AppLocalCompat : forall v, CompatibleRedices l (AppLocalRedex v) (C.RAppLocal l v)
  | AppGlobalDefCompat : forall l' v,
      l <> l' -> CompatibleRedices l AppGlobalRedex (C.RDefLocal l' v)
  | AppGlobalLocCompat : forall l' v,
      l <> l' -> CompatibleRedices l AppGlobalRedex (C.RAppLocal l' v)
  | AppGlobalCompat : CompatibleRedices l AppGlobalRedex C.RAppGlobal
  | RFunCompat : forall R1 R2,
      CompatibleRedices l R1 R2 -> CompatibleRedices l (FunRedex R1) (C.RFun R2)
  | RArgCompat : forall R1 R2,
      CompatibleRedices l R1 R2 -> CompatibleRedices l (ArgRedex R1) (C.RArg R2).

  (*A functional version of compatibility above. *)
  Fixpoint ProjectRedexToRedex (R : C.Redex) (l : Loc) : option CtrlExprRedex :=
    match R with
    | C.RDone l' e1 e2 =>
      if L.eq_dec l l'
      then Some (RetE e1 e2)
      else None
    | C.RIfE l' e1 e2 =>
      if L.eq_dec l l'
      then Some (IfE e1 e2)
      else None
    | C.RIfTT l' =>
      if L.eq_dec l l'
      then Some IfTT
      else None
    | C.RIfFF l' =>
      if L.eq_dec l l'
      then Some IfFF
      else None
    | C.RSendE l' e1 e2 l'' =>
      if L.eq_dec l l'
      then Some (SendE l'' e1 e2)
      else None
    | C.RSendV l' v l'' =>
      if L.eq_dec l l'
      then if L.eq_dec l l''
           then None
           else Some (SendV l'' v)
      else if L.eq_dec l l''
           then Some (RecvV l' v)
           else None
    | C.RSync l' d l'' =>
      if L.eq_dec l l'
      then if L.eq_dec l l''
           then None
           else Some (ChooseRedex l'' d)
      else if L.eq_dec l l''
           then match d with
                | Left => Some (AllowChoiceLRedex l')
                | Right => Some (AllowChoiceRRedex l')
                end
           else None
    | C.RDefLocal l' v =>
      if L.eq_dec l l'
      then Some (LetRetRedex v)
      else Some AppGlobalRedex
    | C.RAppLocalE l' e1 e2 =>
      if L.eq_dec l l'
      then Some (AppLocalE e1 e2)
      else None
    | C.RAppLocal l' v =>
      if L.eq_dec l l'
      then Some (AppLocalRedex v)
      else Some AppGlobalRedex
    | C.RAppGlobal => Some AppGlobalRedex
    | C.RFun R => match ProjectRedexToRedex R l with
                 | Some R => Some (FunRedex R)
                 | None => None
                 end
    | C.RArg R => match ProjectRedexToRedex R l with
                 | Some R => Some (ArgRedex R)
                 | None => None
                 end
    end.

  Lemma ProjectCompat : forall {l R1 R2},
      ProjectRedexToRedex R1 l = Some R2 ->
      CompatibleRedices l R2 R1.
  Proof using.
    intros l R1; revert l; induction R1; intros p R2; cbn; intro eq;
      repeat match goal with
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [ H : Some _ = Some _ |- _] => inversion H; clear H; subst
             | [ H : context[L.eq_dec ?a ?b] |- _ ] => destruct (L.eq_dec a b); subst
             | [ H: context[ProjectRedexToRedex ?R ?l] |- _ ] =>
               lazymatch type of H with
               | ProjectRedexToRedex R l = _ => fail
               | _ => let eq := fresh "eq" in destruct (ProjectRedexToRedex R l) eqn:eq
               end
             end; try discriminate; try (constructor; auto; fail).
    destruct s; inversion eq; clear eq; subst; constructor; auto.
  Qed.

  Lemma CompatProject : forall l R1 R2,
      CompatibleRedices l R1 R2 ->
      ProjectRedexToRedex R2 l = Some R1.
  Proof using.
    intros l R1 R2; revert l R1; induction R2; intros p R1 compat;
      cbn; inversion compat; subst;
      repeat match goal with
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [ |- ?a = ?a ] => reflexivity
             | [ |- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
             | [ |- context[ProjectRedexToRedex ?R ?l]] =>
                 let eq := fresh "eq" in destruct (ProjectRedexToRedex R l) eqn:eq
             end.
    all: apply IHR2 in H1; rewrite H1 in eq; inversion eq; subst; auto.
  Qed.

  Theorem ProjectRedexTriangle1: forall R l R',
      ProjectRedexToRedex R l = Some R' ->
      ProjectRedex R l = Some (CtrlExprRedexToLabel R').
  Proof using.
    intro R; induction R; intros p R'; cbn; intro eq;
      repeat match goal with
             | [ |- ?a = ?a] => reflexivity
             | [ H : ?a <> ?a |- _] => destruct (H eq_refl)
             | [ H : Some _ = None |- _ ] => inversion H
             | [ H : None = Some _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] =>
               inversion H; clear H; subst; cbn
             | [ H : context[L.eq_dec ?a ?b] |- _] => destruct (L.eq_dec a b); subst
             | [ H : context[ProjectRedexToRedex ?R ?l] |- _ ] =>
               lazymatch type of H with
               | ProjectRedexToRedex R l = _ => fail
               | _ =>
                 let eq := fresh "eq" in destruct (ProjectRedexToRedex R l) eqn:eq
               end
             end.
    - destruct s; cbn; inversion eq; subst; auto.
    - apply IHR in eq0; auto. rewrite eq0; auto.
    - apply IHR in eq0; auto; rewrite eq0; reflexivity.
  Qed.
  
  Theorem ProjectRedexTriangle2 : forall R l,
      ProjectRedexToRedex R l = None ->
      ProjectRedex R l = None.
  Proof using.
    intro R; induction R; intros p; cbn; intro eq;
      repeat match goal with
             | [ |- ?a = ?a] => reflexivity
             | [ H : ?a <> ?a |- _] => destruct (H eq_refl)
             | [ H : Some _ = None |- _ ] => inversion H
             | [ H : None = Some _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] =>
               inversion H; clear H; subst; cbn
             | [ H : context[L.eq_dec ?a ?b] |- _] => destruct (L.eq_dec a b); subst
             | [ H : context[ProjectRedexToRedex ?R ?l] |- _ ] =>
               lazymatch type of H with
               | ProjectRedexToRedex R l = _ => fail
               | _ =>
                 let eq := fresh "eq" in destruct (ProjectRedexToRedex R l) eqn:eq
               end
             end.
    - destruct s; cbn; inversion eq; subst; auto.
    - apply IHR in eq0; auto; rewrite eq0; reflexivity.
    - apply IHR in eq0; auto; rewrite eq0; reflexivity.
  Qed.

  Theorem ProjectRedexTriangle3 : forall R l,
      ProjectRedex R l = None ->
      ProjectRedexToRedex R l = None.
  Proof using.
    intro R; induction R; intros p; cbn; intro eq;
      repeat match goal with
             | [ |- ?a = ?a] => reflexivity
             | [ H : ?a <> ?a |- _] => destruct (H eq_refl)
             | [ H : Some _ = None |- _ ] => inversion H
             | [ H : None = Some _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] =>
               inversion H; clear H; subst; cbn
             | [ H : context[L.eq_dec ?a ?b] |- _] => destruct (L.eq_dec a b); subst
             | [ |- context[ProjectRedexToRedex ?R ?l]] =>
                 let eq := fresh "eq" in destruct (ProjectRedexToRedex R l) eqn:eq
             end.
    all: destruct (ProjectRedex R p) eqn:eq1; try discriminate.
    all: apply IHR in eq1; rewrite eq1 in eq0; inversion eq0.
  Qed.
  
  Theorem ProjectRedexToRedexBlocked : forall C1 R B C2 l,
      C.PirStep R B C1 C2 ->
      In l B ->
      ProjectRedexToRedex R l = None.
  Proof using.
    intros C1 R B C2 l step; revert l; induction step; intros p i; cbn;
      repeat match goal with
             | [ |- ?a = ?a ] => reflexivity
             | [ H : ?P |- ?P ] => exact H
             | [H : ?P, H' : ~?P |- _ ] => destruct (H' H)
             | [H : In _ [] |- _ ] => inversion H
             | [|- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
             | [ i : In ?p B, IH : forall l, In l B -> _ |- _ ] =>
               specialize (IH p i)
             | [ i : In ?p B, IH : forall l, In l (_ :: B) -> _ |- _ ] =>
               specialize (IH p ltac:(right; exact i))
             | [ i : In ?p B, IH : forall l, In l (_ :: _ :: B) -> _ |- _ ] =>
               specialize (IH p ltac:(right; right; exact i))
             | [ |- context[ProjectRedexToRedex ?R]] =>
               lazymatch goal with
               | [H : ProjectRedexToRedex R = _ |- _ ] => rewrite H
               | _ => let eq := fresh "eq" in destruct (ProjectRedexToRedex R) eqn:eq
               end
             end; try discriminate.
  Qed.

  Fixpoint RedexToSystemLabel (R : C.Redex) : SystemLabel :=
    match R with
    | C.RDone x x0 x1 => SysIota
    | C.RIfE x x0 x1 => SysIota
    | C.RIfTT x => SysIota
    | C.RIfFF x => SysIota
    | C.RSendE l e1 e2 l' => SysIota
    | C.RSendV l v l' => CommLabel l v l'
    | C.RSync l ch l' => ChoiceLabel l ch l'
    | C.RDefLocal x x0 => SysSyncIota
    | C.RAppLocalE x x0 x1 => SysIota
    | C.RAppLocal x x0 => SysSyncIota
    | C.RAppGlobal => SysSyncIota
    | C.RFun R => RedexToSystemLabel R
    | C.RArg R => RedexToSystemLabel R
    end.

  Fixpoint RedicesToSystemLabels (Rs : list C.Redex) : list SystemLabel :=
    match Rs with
    | [] => nil
    | R :: Rs => (RedexToSystemLabel R) :: RedicesToSystemLabels Rs
    end.

  Fixpoint CtrlExprRedexToRedex (R : CtrlExprRedex) (l : Loc) : C.Redex :=
    match R with
    | RetE e1 e2 => C.RDone l e1 e2
    | IfE e1 e2 => C.RIfE l e1 e2
    | IfTT => C.RIfTT l
    | IfFF => C.RIfFF l
    | SendE l' e1 e2 => C.RSendE l e1 e2 l'
    | SendV l' v => C.RSendV l v l'
    | RecvV l' v => C.RSendV l' v l
    | ChooseRedex l' d => C.RSync l d l'
    | AllowChoiceLRedex l' => C.RSync l' Left l
    | AllowChoiceRRedex l' => C.RSync l' Right l (* I changed this while cleaning.*)
    | LetRetRedex v => C.RDefLocal l v
    | AppLocalE e1 e2 => C.RAppLocalE l e1 e2
    | AppLocalRedex v => C.RAppLocal l v
    | AppGlobalRedex => C.RAppGlobal
    | FunRedex R => C.RFun (CtrlExprRedexToRedex R l)
    | ArgRedex R => C.RArg (CtrlExprRedexToRedex R l)
    end.


  (* EPP PRESERVES PROPERTIES *)
  (*
    In particular, EPP preserves closedness-above and the property of being a value.
   *)
  Theorem EPPClosedAbove : forall C l E f m,
      EPP C l = Some E ->
      C.PirExprClosedAbove f m C ->
      CtrlExprClosedAbove E m (f l).
  Proof using.
    intro C; induction C; cbn; intros p E f m eq clsd; auto;
      inversion clsd; subst;
      repeat match goal with
             | [H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [H : Some _ = Some _ |- _ ] => inversion H; clear H; subst; cbn in *
             | [ |- _ /\ _ ] => split
             | [H : context[L.eq_dec ?a ?b] |- _ ] => destruct (L.eq_dec a b); subst
             | [H : context[EPP ?C ?l] |- _ ] =>
               lazymatch type of H with
               | EPP C l = _ => fail
               | _ => lazymatch goal with
                     | [H' : EPP C l = _ |- _ ] => rewrite H' in H
                     | _ => let eq := fresh "eq" in destruct (EPP C l) eqn:eq
                     end
               end
             | [ IH :forall l E f m, EPP ?C l = Some E -> C.PirExprClosedAbove f m ?C ->
                                CtrlExprClosedAbove E m (f l),
                   H1 : EPP ?C ?l = Some ?E, H2 : C.PirExprClosedAbove ?f ?m ?C |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprClosedAbove E m (f l) |- _ ] => fail
               | _ => pose proof (IH l E f m H1 H2)
               end
             end; cbn in *; try discriminate; auto.
    - destruct (L.eq_dec ℓ2 ℓ1); subst; [destruct (n eq_refl)|]; auto.
    - destruct (L.eq_dec ℓ2 ℓ2) as [_|neq]; [|destruct (neq eq_refl)]; auto.
    - destruct (L.eq_dec ℓ2 p); subst; [destruct (n0 eq_refl)|]; auto.
    - eapply MergeClosed; eauto.
    - destruct d; inversion eq; clear eq; subst; cbn in *; auto.
    - destruct (L.eq_dec ℓ ℓ) as [_|neq]; [|destruct (neq eq_refl)]; auto.
    - destruct (L.eq_dec ℓ p); subst; [destruct (n eq_refl)|]; auto.
      apply CtrlExprClosedAboveGlobalRenaming with (ξ := fun n => S (S n)) (n := m) (m := f p);
        auto.
      intros k l' k_lt_l'; repeat apply lt_n_S; auto.
    - destruct (L.eq_dec ℓ ℓ) as [_|neq]; [|destruct (neq eq_refl)]; auto.
    - destruct (L.eq_dec ℓ p); subst; [destruct (n eq_refl)|].
      apply CtrlExprClosedAboveGlobalRenaming with (n := S m) (m := f p); auto.
      intros k l0 k_lt_l0; apply lt_n_S; auto.
  Qed.

  Theorem EPPValue : forall V l E,
      C.PirExprVal V ->
      EPP V l = Some E ->
      CtrlExprVal E.
  Proof using.
    intros V l E H H0; inversion H; subst; cbn in H0.
    destruct (L.eq_dec l l0); subst; cbn in H0; inversion H0; subst; clear H0; 
      constructor; auto.
    destruct (L.eq_dec l l0); destruct (EPP C l) eqn:eq; subst;
      inversion H0; subst; clear H0; constructor; eauto.
    destruct (EPP C l) eqn:eq; inversion H0; subst; clear H0; constructor; auto.
  Qed.

  (* EPP PRODUCES LESS-NONDETERMISTIC PROGRAMS *)
  (*
    When a location does not participate in a step, then projecting that location from both
    Pirouette programs results in a less-nondeterminstic program.
   *)
  
  Lemma EPPBlocked : forall R B C1 C2 l E1 E2,
      In l B ->
      C.PirStep R B C1 C2 ->
      EPP C1 l = Some E1 ->
      EPP C2 l = Some E2 ->
      LessNondet E2 E1.
  Proof using.
    intros R B C1 C2 l E1 E2 i step; revert l E1 E2 i; induction step;
      intros p E1 E2 i eq1 eq2; cbn in *;
        repeat match goal with
               | [ |- ?a = ?a ] => reflexivity
               | [ H :?P |- ?P ] => exact H
               | [ |- LessNondet ?a ?a ] => apply LessNondetRefl
               | [ H1 : ?P, H2 : ~?P |- _ ] => destruct (H2 H1)
               | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
               | [ H : In ?a [] |- _ ] => inversion H
               | [ H : False |- _ ] => destruct H
               | [ H : Some _ = None |- _ ] => inversion H
               | [ H : None = Some _ |- _ ] => inversion H
               | [ H : Some _ = Some _ |- _ ] => inversion H; clear H; subst
               | [ H: context[L.eq_dec ?a ?b] |- _ ] =>
                 tryif unify a b
                 then
                   let neq := fresh in
                   destruct (L.eq_dec a b) as [_|neq]; [|destruct (neq eq_refl)]
                 else
                   lazymatch goal with
                   | [ e : a = b |- _ ] =>
                     let neq := fresh in
                     destruct (L.eq_dec a b) as [_|neq];[| destruct (neq e)]
                   | [ e : b = a |- _ ] =>
                     let neq := fresh in
                     destruct (L.eq_dec a b) as [_|neq];[|destruct (neq (eq_sym e))]
                   | [ n : a <> b |- _ ] =>
                     let eq := fresh in
                     destruct (L.eq_dec a b) as [eq|_]; [destruct (n eq)|]
                   | [ n : b <> a |- _ ] =>
                     let eq := fresh in
                     destruct (L.eq_dec a b) as [eq|_];[destruct (n (eq_sym eq))|]
                   | _ =>
                     let eq := fresh "eq" in
                     let neq := fresh "neq" in
                     destruct (L.eq_dec a b) as [eq|neq]; [subst|]
                   end
               | [ H: context[EPP ?C ?l] |- _] =>
                   lazymatch type of H with
                   | EPP C l = _ => fail
                   | _ =>
                     lazymatch goal with
                     | [ eq : EPP C l = _ |- _ ] => rewrite eq in H; cbn in H
                     | _ =>
                       let eq := fresh "eq" in
                       destruct (EPP C l) eqn:eq; cbn in *
                     end
                   end
               | [ H1 : EPP ?C ?l = Some ?E1,
                        H2 : EPP ?C ?l = Some ?E2 |- _ ] =>
                 tryif unify E1 E2
                 then clear H2
                 else rewrite H1 in H2; inversion H2; clear H2; subst
               | [ H1 : CtrlExprMerge ?E1 ?E2 = Some ?E,
                        H2 : CtrlExprMerge ?E1 ?E2 = Some ?E' |- _ ] =>
                 tryif unify E E'
                 then clear H2
                 else rewrite H1 in H2; inversion H2; clear H2; subst
               | [ IH : forall l E1 E2,
                     ?l1 = l \/ _ -> EPP ?C1 l = Some E1 -> EPP ?C2 l = Some E2 ->
                     LessNondet E2 E1,
                     H1 : EPP ?C1 ?l1 = Some ?E1,
                     H2 : EPP ?C2 ?l2 = Some ?E2 |- _ ] =>
                 lazymatch goal with
                 | [_ : LessNondet E2 E1 |- _ ] => fail
                 | _ => pose proof (IH l1 E1 E2 ltac:(left; reflexivity) H1 H2)
                 end
               | [ IH : forall l E1 E2,
                     _ \/ In l ?B -> EPP ?C1 l = Some E1 -> EPP ?C2 l = Some E2 ->
                     LessNondet E2 E1,
                     i : In ?l1 ?B,
                     H1 : EPP ?C1 ?l1 = Some ?E1,
                     H2 : EPP ?C2 ?l2 = Some ?E2 |- _ ] =>
                 lazymatch goal with
                 | [_ : LessNondet E2 E1 |- _ ] => fail
                 | _ => pose proof (IH l1 E1 E2 ltac:(right; exact i) H1 H2)
                 end
               end; try (eauto with CtrlExpr; fail).
    - rewrite EPPLocalSubst with (E := E1) in eq2; auto;
        inversion eq2; clear eq2; subst; unfold C.ValueSubst.
      destruct (L.eq_dec l2 p) as [eq|_]; [destruct (neq0 (eq_sym eq))|].
      rewrite CtrlExprLocalIdSubst; apply LessNondetRefl.
    - apply (LessNondetMerge c c0 c1 c2); auto.
    - apply MergeLessNondet in eq1; auto.
    - rewrite MergeComm in eq1; apply MergeLessNondet in eq1; auto.
    - destruct d; inversion eq1; clear eq1; subst; inversion eq2; clear eq2; subst;
        eauto with CtrlExpr.
  Qed.

  Lemma EPPRedex : forall C1 R B C2 l E1 E2,
      EPP C1 l = Some E1 ->
      EPP C2 l = Some E2 ->
      ProjectRedex R l = None ->
      C.PirStep R B C1 C2 ->
      LessNondet E2 E1.
  Proof using.
    intros C1 R B C2 l E1 E2 eq1 eq2 eq3 step;
      revert l E1 E2 eq1 eq2 eq3; induction step; cbn;
        intros p E1 E2 eq1 eq2 eq3;
        repeat (try discriminate;
                match goal with
                | [ H: Some _ = Some _ |- _ ] => inversion H; clear H; subst
                | [ H : context[L.eq_dec ?a ?b] |- _ ] =>
                  tryif unify a b
                  then let n := fresh in
                       destruct (L.eq_dec a b) as [_ | n];
                       [| destruct (n eq_refl)]
                  else lazymatch goal with
                       | [e : a = b |- _ ] =>
                         let n := fresh in
                         destruct (L.eq_dec a b) as [_ | n];
                         [| destruct (n e)]
                       | [e : b = a |- _ ] =>
                         let n := fresh in
                         destruct (L.eq_dec a b) as [_ | n];
                         [| destruct (n (eq_sym e))]
                       | [n : a <> b |- _ ] =>
                         let e := fresh in
                         destruct (L.eq_dec a b) as [e | _];
                         [destruct (n e)|]
                       | [n : b <> a |- _ ] =>
                         let e := fresh in
                         destruct (L.eq_dec a b) as [e | _];
                         [destruct (n (eq_sym e))|]
                       | _ => let eq := fresh "eq" in
                             let neq := fresh "neq" in
                             destruct (L.eq_dec a b) as [eq|neq]; subst
                       end
                | [ H : context[EPP ?C ?l] |- _ ] =>
                  lazymatch type of H with
                  | EPP C l = _ => fail
                  | _ => lazymatch goal with
                        | [ H' : EPP C l = _ |- _ ] => rewrite H' in H
                        | _ => let eq := fresh "eq" in destruct (EPP C l) eqn:eq
                        end
                  end
                | [ H : context[ProjectRedex ?R ?l] |- _ ] =>
                  lazymatch type of H with
                  | ProjectRedex R l = _ => fail
                  | _ => lazymatch goal with
                        | [ H' : ProjectRedex R l = _ |- _ ] => rewrite H' in H
                        | _ => let eq := fresh "eq" in destruct (ProjectRedex R l) eqn:eq
                        end
                  end
                | [H1 : EPP ?C ?p = _, H2 : EPP ?C ?p = _ |- _ ] =>
                  rewrite H1 in H2
                | [H1 : CtrlExprMerge ?C ?p = _, H2 : CtrlExprMerge ?C ?p = _ |- _ ] =>
                  rewrite H1 in H2
                | [ IH : forall l E1 E2, EPP ?C1 l = Some E1 ->
                                    EPP ?C2 l = Some E2 ->
                                    ProjectRedex ?R l = None ->
                                    LessNondet E2 E1,
                      H1 : EPP ?C1 ?l = Some ?E1,
                      H2 : EPP ?C2 ?l = Some ?E2,
                      H3 : ProjectRedex ?R ?l = None |- _ ] =>
                  lazymatch goal with
                  | [ _ : LessNondet E2 E1 |- _ ] => fail
                  | _ => pose proof (IH l E1 E2 H1 H2 H3)
                  end
                end); auto with CtrlExpr.
    - rewrite EPPLocalSubst with (E := E1) in eq2; auto;
        inversion eq2; clear eq2; subst.
      unfold C.ValueSubst.
      destruct (L.eq_dec l2 p) as [e | _]; [destruct (neq0 (eq_sym e))|].
      rewrite CtrlExprLocalIdSubst; reflexivity.
    - apply (LessNondetMerge c0 c c2 c1); auto.
      all: rewrite MergeComm; auto.
    - apply MergeLessNondet in eq1; auto.
    - rewrite MergeComm in eq1; apply MergeLessNondet in eq1; auto.
    -  destruct d; inversion eq2; inversion eq1; clear eq1 eq2; subst;
         auto with CtrlExpr.
  Qed.

  Corollary EPPRedex' : forall C1 R B C2 l E1 E2,
      EPP C1 l = Some E1 ->
      EPP C2 l = Some E2 ->
      ProjectRedexToRedex R l = None ->
      C.PirStep R B C1 C2 ->
      LessNondet E2 E1.
  Proof using.
    intros C1 R B C2 l E1 E2 H H0 H1 H2.
    apply ProjectRedexTriangle2 in H1; eapply EPPRedex; eauto.
  Qed.

  (* LOCAL COMPLETENESS *)
  (*
    We first prove local completeness for the more-specific semantics, and then we use that
    to get local completeness for the less-specific semantics.
   *)
  
  Theorem LocalCompleteness' : forall C1 R B C2 l E1 E2 R',
      EPP C1 l = Some E1 ->
      EPP C2 l = Some E2 ->
      ProjectRedexToRedex R l = Some R' ->
      C.PirStep R B C1 C2 ->
      CtrlExprStep' E1 R' E2.
  Proof using.
    intros C1 R B C2 l E1 E2 R' eqC1 eqC2 eqR step;
      revert l E1 E2 R' eqC1 eqC2 eqR; induction step; intros p E1 E2 R' eqC1 eqC2 eqR;
        cbn in *;
        repeat match goal with
               | [ H : Some _ = None |- _ ] => inversion H
               | [ H : None = Some _ |- _ ] => inversion H
               | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H
               | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
               | [ H : ?a = Some _, H' : ?a = None |- _ ] =>
                 rewrite H in H'; inversion H'
               | [ H : context[L.eq_dec ?a ?b] |- _ ] => destruct (L.eq_dec a b); subst
               | [ H : context[EPP ?C ?l] |- _ ] =>
                 lazymatch type of H with
                 | EPP C l = _ => fail
                 | _ => lazymatch goal with
                       | [ H' : EPP C l = _ |- _ ] => rewrite H' in H
                       | _ => let eq := fresh "eq" in
                             destruct (EPP C l) eqn:eq
                       end
                 end
               | [ H : context[ProjectRedexToRedex ?R ?l] |- _ ] =>
                 lazymatch type of H with
                 | ProjectRedexToRedex R l = _ => fail
                 | _ => lazymatch goal with
                       | [ H' : ProjectRedexToRedex R l = _ |- _ ] => rewrite H' in H
                       | _ => let eq := fresh "eq" in
                             destruct (ProjectRedexToRedex R l) eqn:eq
                       end
                 end
               | [ H : C.PirStep ?R (?p :: ?B) ?C1 ?C2 |- _ ] =>
                 lazymatch goal with
                 | [_ : ProjectRedexToRedex R p = None |- _ ] => fail
                 | _ => pose proof (ProjectRedexToRedexBlocked C1 R (p :: B) C2 p H ltac:(left;reflexivity))
                 end
               | [ H : C.PirStep ?R (?p :: ?q :: ?B) ?C1 ?C2 |- _ ] =>
                 lazymatch goal with
                 | [_ : ProjectRedexToRedex R p = None, _ : ProjectRedexToRedex R q = None |- _ ] => fail
                 | _ => pose proof (ProjectRedexToRedexBlocked C1 R (p :: q :: B) C2 p H ltac:(left;reflexivity));
                         pose proof (ProjectRedexToRedexBlocked C1 R (p :: q :: B) C2 q H ltac:(right; left;reflexivity))
                 end
               | [ H :context[EPP (C.PirExprLocalSubst ?C ?σ) ?l],
                      H' : EPP ?C ?l = Some ?E |- _] =>
                 rewrite (EPPLocalSubst C l E σ H') in H
               end.
    all: try (econstructor; eauto; fail).
    all: try (eapply IHstep; eauto; fail).
    - unfold C.ValueSubst.
      destruct (L.eq_dec l2 l1) as [eq'|_]; [destruct (n (eq_sym eq'))|].
      rewrite CtrlExprLocalIdSubst; constructor; auto.
    - unfold C.ValueSubst.
      destruct (L.eq_dec l2 l2) as [_|neq]; [| destruct (neq eq_refl)].
      fold (ValSubst v). constructor; auto.
    - apply (MergeStep' c1 c2 c c0); auto.
      eapply IHstep1; eauto. eapply IHstep2; eauto.
    (* - pose proof (IHstep p c1 c R' eq1 eq eqR); auto with CtrlExpr. *)
    (*   apply  *)
    - unfold C.ValueSubst. destruct (L.eq_dec l l) as [_|neq];[|destruct (neq eq_refl)].
      fold (ValSubst v). constructor; auto.
    - unfold C.ValueSubst. destruct (L.eq_dec l p) as [e |_]; [destruct (n (eq_sym e))|].
      rewrite CtrlExprLocalIdSubst.
      pose proof (AppGlobalStep' (c ⟨ceg| fun n => S (S n)⟩) Unit ltac:(constructor)).
      rewrite CtrlExprGlobalRenameSubstFusion in H0; cbn in H0.
      rewrite GlobalIdSubstSpec in H0. auto.
    (* - pose proof (IHstep p c0 c L eq0 eq eqR); auto with CtrlExpr. *)
    - pose proof (EPPLocalSubst C l c (C.ValueSubst l v) eq).
      assert (forall n : nat, EPP (C.AppLocalSubst l C n) l = Some (FunLocalSubst c n)) as eq'
          by (intro n; destruct n; cbn; auto;
              destruct (L.eq_dec l l) as [_|neq];[| destruct (neq eq_refl)];
              rewrite eq; reflexivity).
      pose proof (EPPSubst _ l _ (C.AppLocalSubst l C) (FunLocalSubst c) eq' H0).
      rewrite H1 in eqC2; inversion eqC2; subst; clear eqC2.
      unfold C.ValueSubst. destruct (L.eq_dec l l) as [_|neq]; [|destruct (neq eq_refl)].
      fold (ValSubst v).
      apply AppLocalStep'; auto.
    - pose proof (EPPLocalSubst C p c (C.ValueSubst l v) eq).
      assert (forall n, EPP (C.AppLocalSubst l C n) p =
                   Some ((fun m => match m with
                                | 0 => FunGlobal (c ⟨ceg| S⟩)
                                | S m => Var m
                                end) n)).
      intro m. unfold C.AppLocalSubst. destruct m. cbn.
      destruct (L.eq_dec p l) as [eq'|_];[destruct (n eq')|].
      rewrite eq; auto.
      cbn; auto.
      pose proof (EPPSubst _ p _ (C.AppLocalSubst l C) (fun n => match n with
                                                                      | 0 => FunGlobal (c ⟨ceg| S⟩)
                                                                      | S m => Var m
                                                                      end) H1 H0).
      rewrite H2 in eqC2; inversion eqC2; subst; clear eqC2.
      pose proof (AppGlobalStep' (c ⟨ceg| S⟩) Unit UnitVal).
      unfold C.ValueSubst. destruct (L.eq_dec l p) as [eq' | _ ]; [destruct (n0 eq')|].
      rewrite CtrlExprLocalIdSubst.
      assert ((c ⟨ceg| S⟩) [ceg|FunGlobalSubst (c⟨ceg| S⟩) Unit] =
              c [ceg| fun m => match m with
                            | 0 => FunGlobal (c ⟨ceg| S⟩)
                            | S m => Var m
                            end]).
      unfold FunGlobalSubst.
      rewrite CtrlExprGlobalRenameSubstFusion. reflexivity.
      rewrite <- H4. constructor. constructor.
    - assert (forall n, EPP (C.AppGlobalSubst C1 C2 n) p = Some (match n with
                                                                    | 0 => c0
                                                                    | 1 => FunGlobal c
                                                                    | S (S n) => Var n
                                                                    end)).
      intro n; destruct n; cbn; auto.
      destruct n; auto. cbn; rewrite eq; reflexivity.
      pose proof (EPPSubst _ p _ _ _ H0 eq).
      rewrite H1 in eqC2; inversion eqC2; subst; clear eqC2.
      fold (FunGlobalSubst c c0). constructor. eapply EPPValue; eauto.
    - destruct d; inversion eqC1; subst; clear eqC1; inversion eqR; subst;
        constructor.
  Qed.

  Corollary LocalCompleteness : forall C1 R B C2 l E1 E2 L,
      EPP C1 l = Some E1 ->
      EPP C2 l = Some E2 ->
      ProjectRedex R l = Some L ->
      C.PirStep R B C1 C2 ->
      CtrlExprStep E1 L E2.
  Proof using.
    intros C1 R B C2 l E1 E2 L H H0 H1 H2.
    destruct (ProjectRedexToRedex R l) as [R'|] eqn:eqR;
      [|apply ProjectRedexTriangle2 in eqR; rewrite eqR in H1; inversion H1].
    pose proof (LocalCompleteness' C1 R B C2 l E1 E2 R' H H0 eqR H2).
    apply CtrlExprStep'ToNormal in H3.
    apply ProjectRedexTriangle1 in eqR. rewrite H1 in eqR; inversion eqR; clear eqR; subst.
    auto.
  Qed.

  Theorem LocalCompletenessExists : forall C1 R B C2 l E1,
      EPP C1 l = Some E1 ->
      C.PirStep R B C1 C2 ->
      exists E2, EPP C2 l = Some E2.
  Proof using.
    intros C1 R B C2 l E1 eq step; revert l E1 eq; induction step;
      intros p E1 eq; cbn in *;
        repeat match goal with
               | [ H : Some _ = None |- _ ] => inversion H
               | [ H : None = Some _ |- _ ] => inversion H
               | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H
               | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
               | [ H : ?a = Some _, H' : ?a = None |- _ ] =>
                 rewrite H in H'; inversion H'
               | [ IH : forall l E1, EPP ?C1 l = Some E1 -> exists E2, EPP ?C2 l = Some E2,
                     H : EPP ?C1 ?l = Some ?E1 |- _ ] =>
                 lazymatch goal with
                 | [_ : EPP C2 l = Some _ |- _ ] => fail
                 | _ => let E2 := fresh "E" in let eq := fresh "eq" in destruct (IH l E1 H) as [E2 eq]
                 end
               | [ H : context[L.eq_dec ?a ?b] |- _ ] => destruct (L.eq_dec a b); subst
               | [ H : context[EPP ?C ?l] |- _ ] =>
                 lazymatch type of H with
                 | EPP C l = _ => fail
                 | _ => lazymatch goal with
                       | [ H' : EPP C l = _ |- _ ] => rewrite H' in H
                       | _ => let eq := fresh "eq" in
                             destruct (EPP C l) eqn:eq
                       end
                 end
               | [ |- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
               | [ |- context[EPP ?C ?l]] =>
                 lazymatch goal with
                 | [ H' : EPP C l = _ |- _ ] => rewrite H'
                 | _ => let eq := fresh "eq" in
                       destruct (EPP C l) eqn:eq
                 end
               | [H1 : EPP ?C ?l = Some ?E,
                       H2 : EPP (C.PirExprLocalSubst ?C ?σ) ?l = None |- _ ] =>
                 rewrite (EPPLocalSubst C l E σ H1) in H2; inversion H2
               end; try (eexists; eauto; fail).
    - destruct (ProjectRedexToRedex R p) as [R'|] eqn:eqR.
      -- pose proof (LocalCompleteness' C1 R _ C3 p c E R' eq0 eq1 eqR step1).
         pose proof (LocalCompleteness' C2 R _ C4 p c0 E0 R' eq2 eq3 eqR step2).
         destruct (MergeStep'Exists _ _ _ _ _ _ H H0 eq); eexists; eauto.
      -- pose proof (EPPRedex' _ _ _ _ _ _ _ eq0 eq1 eqR step1).
         pose proof (EPPRedex' _ _ _ _ _ _ _ eq2 eq3 eqR step2).
         destruct (LessNondetMergeExists _ _ _ _ _ H H0 eq) as [E2 eq'].
         exists E2; auto.
    - pose proof (EPPLocalSubst C p c (C.ValueSubst p v) eq0).
      assert (forall n : nat, EPP (C.AppLocalSubst p C n) p = Some (FunLocalSubst c n)) as eq'
          by (intro n; destruct n; cbn; auto;
              destruct (L.eq_dec p p) as [_|neq];[| destruct (neq eq_refl)];
              rewrite eq0; reflexivity).
      rewrite (EPPSubst _ p _ _ _ eq' H0) in eq. inversion eq.
    - pose proof (EPPLocalSubst C p c (C.ValueSubst l v) eq0).
      assert (forall n, EPP (C.AppLocalSubst l C n) p =
                   Some ((fun m => match m with
                                | 0 => FunGlobal (c ⟨ceg| S⟩)
                                | S m => Var m
                                end) n)).
      intro m; destruct m; cbn; auto.
      destruct (L.eq_dec p l) as [eqpl|_];[destruct (n0 eqpl)|]; rewrite eq0; auto.
      rewrite (EPPSubst _ p _ _ _ H1 H0) in eq. inversion eq.
    - assert (forall n, EPP (C.AppGlobalSubst C1 C2 n) p = Some (match n with
                                                                    | 0 => c0
                                                                    | 1 => FunGlobal c
                                                                    | S (S n) => Var n
                                                                    end)).
      intro n; destruct n; cbn; auto.
      destruct n; auto. cbn; rewrite eq0; reflexivity.
      rewrite (EPPSubst _ _ _ _ _ H0 eq0) in eq; inversion eq.
    - destruct d; eexists; eauto; fail.
  Qed.

  Corollary FullLocalCompleteness : forall R B C1 C2 E1 p,
      C.PirStep R B C1 C2 ->
      EPP C1 p = Some E1 ->
      (exists L E2, CtrlExprStep E1 L E2
               /\ ProjectRedex R p = Some L
               /\ EPP C2 p = Some E2)
      \/ (exists E2, EPP C2 p = Some E2
               /\ ProjectRedex R p = None
               /\ LessNondet E2 E1).
  Proof using.
    intros R B C1 C2 E1 p step eq.
    destruct (LocalCompletenessExists C1 R B C2 p E1 eq step) as [E2 eq2].
    destruct (ProjectRedex R p) as [L|] eqn:eqR; [left|right].
    - exists L; exists E2; split; [|split]; auto; eapply LocalCompleteness; eauto.
    - exists E2; split; [|split]; auto. eapply EPPRedex; eauto.
  Qed.
  
  (* USEFUL LTAC *)
  
  (*
    We use these often to deal with steps from projections.
   *)
  Ltac ProjectSystemStepDestructor :=
    repeat (try discriminate;
            match goal with
            | [ |- LessNondetSystem _ _ ] => unfold LessNondetSystem
            | [ |- _ /\ _ ] => split
            | [ H : Some _ = Some _ |- _ ] => inversion H; subst; clear H
            | [ H : context[L.eq_dec ?a ?b] |- _ ] =>
              tryif unify a b
              then let n := fresh in
                   destruct (L.eq_dec a b) as [_ | n];
                   [| destruct (n eq_refl)]
              else lazymatch goal with
                   | [e : a = b |- _ ] =>
                     let n := fresh in
                     destruct (L.eq_dec a b) as [_ | n];
                     [| destruct (n e)]
                   | [e : b = a |- _ ] =>
                     let n := fresh in
                     destruct (L.eq_dec a b) as [_ | n];
                     [| destruct (n (eq_sym e))]
                   | [n : a <> b |- _ ] =>
                     let e := fresh in
                     destruct (L.eq_dec a b) as [e | _];
                     [destruct (n e)|]
                   | [n : b <> a |- _ ] =>
                     let e := fresh in
                     destruct (L.eq_dec a b) as [e | _];
                     [destruct (n (eq_sym e))|]
                   | _ => let eq := fresh "eq" in
                         let neq := fresh "neq" in
                         destruct (L.eq_dec a b) as [eq|neq]; subst
                   end
            | [ |- context[L.eq_dec ?a ?b]] =>
              tryif unify a b
              then let n := fresh in
                   destruct (L.eq_dec a b) as [_ | n];
                   [| destruct (n eq_refl)]
              else lazymatch goal with
                   | [e : a = b |- _ ] =>
                     let n := fresh in
                     destruct (L.eq_dec a b) as [_ | n];
                     [| destruct (n e)]
                   | [e : b = a |- _ ] =>
                     let n := fresh in
                     destruct (L.eq_dec a b) as [_ | n];
                     [| destruct (n (eq_sym e))]
                   | [n : a <> b |- _ ] =>
                     let e := fresh in
                     destruct (L.eq_dec a b) as [e | _];
                     [destruct (n e)|]
                   | [n : b <> a |- _ ] =>
                     let e := fresh in
                     destruct (L.eq_dec a b) as [e | _];
                     [destruct (n (eq_sym e))|]
                   | _ => let eq := fresh "eq" in
                          let neq := fresh "neq" in
                          destruct (L.eq_dec a b) as [eq|neq]; subst
                   end
            | [H : context[EPP ?C ?l] |- _ ] =>
              lazymatch type of H with
              | EPP C l = _ => fail
              | _ => lazymatch goal with
                     | [ H' : EPP C l = _ |- _ ] => rewrite H' in H
                     | _ => let eq := fresh "eq" in destruct (EPP C l) eqn:eq
                     end
              end
            | [|- context[EPP ?C ?l] ] =>
              lazymatch goal with
              | [ H : EPP C l = _ |- _ ] => rewrite H
              | _ => let eq := fresh "eq" in destruct (EPP C l) eqn:eq
              end
            | [H : context[ProjectSystem ?C ?l] |- _ ] =>
              lazymatch type of H with
              | ProjectSystem C l = _ => fail
              | _ => lazymatch goal with
                     | [ H' : ProjectSystem C l = _ |- _ ] => rewrite H' in H
                     | _ => let eq := fresh "eq" in destruct (ProjectSystem C l) eqn:eq
                     end
              end
            | [|- context[ProjectSystem ?C ?l]] =>
              lazymatch goal with
              | [ H : ProjectSystem C l = _ |- _ ] => rewrite H
              | _ => let eq := fresh "eq" in destruct (ProjectSystem C l) eqn:eq
              end
            | [ H : In ?a [] |- _ ] => inversion H
            | [ H : In ?a [?b] |- _ ] =>
              tryif unify a b
              then clear H
              else let eq := fresh "eq" in
                   destruct H as [eq | H]; [subst|inversion H]
            | [ |- LM.MapsTo ?p ?E1' (LM.add ?p ?E2 ?Π)] =>
              tryif unify E1' E2
              then apply LM.add_1; auto
              else fail
            | [ H1 : ~ In ?a (?b :: ?l) |- _ ] =>
              let eq := fresh in
              let i := fresh in
              assert (a <> b) by (intro eq; apply H1; left; symmetry; assumption);
              assert (~ In a l) by (intro i; apply H1; right; assumption);
              clear H1
            | [ H : ~ In ?a [] |- _ ] => clear H
            | [ H : ?a <> ?b |- _ ] =>
              lazymatch goal with
              | [ _ : b <> a |- _ ] => fail
              | _ => let eq := fresh in
                     assert (b <> a) by (intro eq; apply H; symmetry; assumption)
              end
            | [ H' : LM.MapsTo ?p ?E (LM.add ?l ?E' ?Π) |- _ ] =>
              tryif unify p l
              then
                tryif unify E E'
                then fail
                else lazymatch goal with
                     | [eq : E = E' |- _ ] => fail
                     | [eq : E' = E |- _ ] => fail
                     | _ => pose proof (LMF.MapsToUnique H' (LM.add_1 Π l E')); subst
                     end
              else
                lazymatch goal with
                | [ eq : p = l |- _ ] => fail
                | [ eq : l = p |- _ ] => fail
                | [ neq : l <> p |- _ ] => 
                  lazymatch goal with
                  | [ _ : LM.MapsTo p E Π |- _ ] => fail
                  | _ => pose proof (LM.add_3 neq H')
                  end
                | _ => destruct (L.eq_dec l p); subst
                end
            | [ H : LM.MapsTo ?a ?b LM.empty |- _ ] =>
              destruct (LM.empty_1 H)
            | [ H : ?l <> ?p |- LM.MapsTo ?p ?E (LM.add ?l ?E' ?Π) ] => apply LM.add_2; auto
            (* | [ |- UniqueLocList [?l]] => exact (SingletonUniqueLocList l) *)
            | [ eq : ProjectSystem ?C ?nms = Some ?Π,
                     mt : LM.MapsTo ?l ?E ?Π |- _ ] =>
              lazymatch goal with
              | [_ : In l nms |- _ ] => fail
              | _ => let i := fresh "i" in
                    let eq' := fresh "eq" in
                    pose proof (proj1 (ProjectSystemLookup C nms Π eq l E) mt) as [i eq'];
                    cbn in eq'
              end
            end).

  Ltac EPPDestructor :=
    repeat match goal with
           | [ H : Some _ = Some _ |- _ ] => inversion H; clear H; subst
           | [ H : _ /\ _ |- _ ] => destruct H
           | [ H : context[L.eq_dec ?a ?b] |- _] =>
             tryif unify a b
             then let n := fresh in
                  destruct (L.eq_dec a b) as [_ | n];
                  [| destruct (n eq_refl)]
             else lazymatch goal with
                  | [e : a = b |- _ ] =>
                    let n := fresh in
                    destruct (L.eq_dec a b) as [_ | n];
                    [| destruct (n e)]
                  | [e : b = a |- _ ] =>
                    let n := fresh in
                    destruct (L.eq_dec a b) as [_ | n];
                    [| destruct (n (eq_sym e))]
                  | [n : a <> b |- _ ] =>
                    let e := fresh in
                    destruct (L.eq_dec a b) as [e | _];
                    [destruct (n e)|]
                  | [n : b <> a |- _ ] =>
                    let e := fresh in
                    destruct (L.eq_dec a b) as [e | _];
                    [destruct (n (eq_sym e))|]
                  | _ => let eq := fresh "eq" in
                        let neq := fresh "neq" in
                        destruct (L.eq_dec a b) as [eq|neq]; subst
                  end
           | [ |- context[L.eq_dec ?a ?b]] =>
             tryif unify a b
             then let n := fresh in
                  destruct (L.eq_dec a b) as [_ | n];
                  [| destruct (n eq_refl)]
             else lazymatch goal with
                  | [e : a = b |- _ ] =>
                    let n := fresh in
                    destruct (L.eq_dec a b) as [_ | n];
                    [| destruct (n e)]
                  | [e : b = a |- _ ] =>
                    let n := fresh in
                    destruct (L.eq_dec a b) as [_ | n];
                    [| destruct (n (eq_sym e))]
                  | [n : a <> b |- _ ] =>
                    let e := fresh in
                    destruct (L.eq_dec a b) as [e | _];
                    [destruct (n e)|]
                  | [n : b <> a |- _ ] =>
                    let e := fresh in
                    destruct (L.eq_dec a b) as [e | _];
                    [destruct (n (eq_sym e))|]
                  | _ => let eq := fresh "eq" in
                         let neq := fresh "neq" in
                         destruct (L.eq_dec a b) as [eq|neq]; subst
                  end
           | [ H : context[ExprEqDec ?a ?b] |- _] =>
             tryif unify a b
             then let n := fresh in
                  destruct (ExprEqDec a b) as [_ | n];
                  [| destruct (n eq_refl)]
             else lazymatch goal with
                  | [e : a = b |- _ ] =>
                    let n := fresh in
                    destruct (ExprEqDec a b) as [_ | n];
                    [| destruct (n e)]
                  | [e : b = a |- _ ] =>
                    let n := fresh in
                    destruct (ExprEqDec a b) as [_ | n];
                    [| destruct (n (eq_sym e))]
                  | [n : a <> b |- _ ] =>
                    let e := fresh in
                    destruct (ExprEqDec a b) as [e | _];
                    [destruct (n e)|]
                  | [n : b <> a |- _ ] =>
                    let e := fresh in
                    destruct (ExprEqDec a b) as [e | _];
                    [destruct (n (eq_sym e))|]
                  | _ => let eq := fresh "eq" in
                         let neq := fresh "neq" in
                         destruct (ExprEqDec a b) as [eq|neq]; subst
                  end
           | [ |-context[ExprEqDec ?a ?b]] =>
             tryif unify a b
             then let n := fresh in
                  destruct (ExprEqDec a b) as [_ | n];
                  [| destruct (n eq_refl)]
             else lazymatch goal with
                  | [e : a = b |- _ ] =>
                    let n := fresh in
                    destruct (ExprEqDec a b) as [_ | n];
                    [| destruct (n e)]
                  | [e : b = a |- _ ] =>
                    let n := fresh in
                    destruct (ExprEqDec a b) as [_ | n];
                    [| destruct (n (eq_sym e))]
                  | [n : a <> b |- _ ] =>
                    let e := fresh in
                    destruct (ExprEqDec a b) as [e | _];
                    [destruct (n e)|]
                  | [n : b <> a |- _ ] =>
                    let e := fresh in
                    destruct (ExprEqDec a b) as [e | _];
                    [destruct (n (eq_sym e))|]
                  | _ => let eq := fresh "eq" in
                         let neq := fresh "neq" in
                         destruct (ExprEqDec a b) as [eq|neq]; subst
                  end
           | [ H : context[EPP ?C ?l] |- _ ] =>
             lazymatch type of H with
             | EPP C l = _ => fail
             | _ =>
               lazymatch goal with
               | [eq : EPP C l = _ |- _ ] => rewrite eq in H
               | _ => let eq := fresh "eq" in destruct (EPP C l) eqn:eq
               end
             end
           | [ |- context[EPP ?C ?l] ] =>
             lazymatch goal with
             | [eq : EPP C l = _ |- _ ] => rewrite eq
             | _ => let eq := fresh "eq" in destruct (EPP C l) eqn:eq
             end
           | [ H : context[ProjectRedex ?R ?l] |- _ ] =>
             lazymatch type of H with
             | ProjectRedex R l = _ => fail
             | _ =>
               lazymatch goal with
               | [eq : ProjectRedex R l = _ |- _ ] => rewrite eq in H
               | _ => let eq := fresh "eq" in destruct (ProjectRedex R l) eqn:eq
               end
             end
           | [ |- context[ProjectRedex ?R ?l] ] =>
             lazymatch goal with
             | [eq : ProjectRedex R l = _ |- _ ] => rewrite eq
             | _ => let eq := fresh "eq" in destruct (ProjectRedex R l) eqn:eq
             end
           | [ H : context[ProjectRedexToRedex ?R ?l] |- _ ] =>
             lazymatch type of H with
             | ProjectRedexToRedex R l = _ => fail
             | _ =>
               lazymatch goal with
               | [eq : ProjectRedexToRedex R l = _ |- _ ] => rewrite eq in H
               | _ => let eq := fresh "eq" in destruct (ProjectRedexToRedex R l) eqn:eq
               end
             end
           | [ |- context[ProjectRedexToRedex ?R ?l] ] =>
             lazymatch goal with
             | [eq : ProjectRedexToRedex R l = _ |- _ ] => rewrite eq
             | _ => let eq := fresh "eq" in destruct (ProjectRedexToRedex R l) eqn:eq
             end
           | [d : SyncLabel |- _ ] => destruct d
           | [H : CtrlExprMergeRel ?a ?b ?c |- context[CtrlExprMerge ?a ?b]] =>
             rewrite (CtrlExprMergeRelSpec1 _ _ _ H)
           | [H : CtrlExprMergeRel ?a ?b ?c |- context[CtrlExprMerge ?b ?a]] =>
             rewrite MergeComm; rewrite (CtrlExprMergeRelSpec1 _ _ _ H)

           end.

  (* IOTA COMPLETENESS *)
  (*
    In order to extend local completeness to global completeness, we go through each of the
    possible system labels. We start with SysIota. First, we prove some results about the 
    redices that project to SysIota. Then we show completeness for these labels.
   *)
  Lemma ProjectIotaSystemIota : forall R l L,
      InternalLabel L ->
      ProjectRedex R l = Some L -> RedexToSystemLabel R = SysIota.
  Proof using.
    intro R; induction R; intros l' L; cbn; intros il eq;
      EPPDestructor; try reflexivity; try discriminate;
        inversion il; subst.
    all: eapply IHR; eauto.
  Qed.
  
  Lemma ProjectSystemIotaIota : forall R l L,
      RedexToSystemLabel R = SysIota ->
      ProjectRedex R l = Some L ->
      InternalLabel L.
  Proof using.
    intro R; induction R; intros p L eq1 eq2; cbn in *; EPPDestructor;
      try discriminate; cbn; auto.
    7,8:  constructor; eapply IHR; eauto.
    all: constructor.
  Qed.
  
  Lemma IotaUnique : forall R, RedexToSystemLabel R = SysIota ->
                          exists l L, InternalLabel L /\ ProjectRedex R l = Some L /\
                                 forall l', l <> l' -> ProjectRedex R l' = None.
  Proof using.
    intros R; induction R; intro eq; cbn in eq; EPPDestructor; try discriminate; cbn.
    all: try (exists l; eexists; split; [|split]; EPPDestructor; eauto; try constructor;
                intros l' neq; EPPDestructor; reflexivity).
    all: destruct (IHR eq) as [l [L [il [eq1 eq2]]]].
    exists l; exists (FunLabel L).
    2: exists l; exists (ArgLabel L).
         all: split; [|split]; EPPDestructor; eauto; try constructor; eauto;
           intros l' neq; EPPDestructor; auto.
         all: rewrite (eq2 l' neq) in eq0; inversion eq0.
  Qed.

  Lemma IotaUniqueInvolved : forall R, RedexToSystemLabel R = SysIota ->
                                  exists l, C.InvolvedWithRedex R l /\
                                       forall l', l <> l' -> ~ C.InvolvedWithRedex R l'.
  Proof using.
    intros R; induction R; intro eq; cbn in *; try discriminate.
    all: try match goal with
             | [ |- exists a, a = ?l /\ forall b, a <> b -> b <> ?l] =>
               exists l; split; auto
             end.
    all: apply IHR; auto.
  Qed.

  Lemma ProjectInvolved : forall R,
      RedexToSystemLabel R = SysIota ->
      forall l, C.InvolvedWithRedex R l <->
           exists L, ProjectRedex R l = Some L.
  Proof using.
    intros R; induction R; cbn; EPPDestructor;
      intros eq p; split; intro H; subst;
        try (eexists; EPPDestructor; eauto; fail);
        try (destruct H as [L eq']; EPPDestructor; auto); try discriminate.
    - specialize (IHR eq p). apply IHR in H.
      destruct H as [L eq']; exists (FunLabel L); rewrite eq'; auto.
    - specialize (IHR eq p). apply IHR; exists l; auto.
    - specialize (IHR eq p). apply IHR in H.
      destruct H as [L eq']; exists (ArgLabel L); rewrite eq'; auto.
    - specialize (IHR eq p). apply IHR; exists l; auto.
  Qed.
  
  Theorem IotaStepSystemCompleteness : forall R B C1 C2 Π1 Π2 nms,
      ProjectSystem C1 nms = Some Π1 ->
      ProjectSystem C2 nms = Some Π2 ->
      RedexToSystemLabel R = SysIota ->
      C.PirStep R B C1 C2 ->
      (forall l, C.InvolvedWithRedex R l -> In l nms) ->
      exists Π2', SystemStep Π1 SysIota Π2' /\ LessNondetSystem Π2 Π2'.
  Proof using.
    intros R B C1 C2 Π1 Π2 nms eq1 eq2 eq3 step1 inv_R.
    destruct (IotaUniqueInvolved R eq3) as [l [inv_l ninv]].
    pose proof (inv_R l inv_l) as in_l.
    destruct (EPP C1 l) as [E1|] eqn:proj_l1;
      [|rewrite (ProjectSystemLookupNone _ _ _ in_l proj_l1) in eq1; inversion eq1].
    destruct (EPP C2 l) as [E2|] eqn:proj_l2;
      [|rewrite (ProjectSystemLookupNone _ _ _ in_l proj_l2) in eq2; inversion eq2].
    pose proof (proj2 (ProjectSystemLookup _ _ _ eq1 l E1) ltac:(split; auto)) as mt1.
    pose proof (proj2 (ProjectSystemLookup _ _ _ eq2 l E2) ltac:(split; auto)) as mt2.
    destruct (proj1 (ProjectInvolved R eq3 l) inv_l) as [L proj_lR].
    pose proof (LocalCompleteness C1 R B C2 l E1 E2 L proj_l1 proj_l2 proj_lR step1)
      as lstep.
    destruct (IotaUnique R eq3) as [l' [L' [il_L [eqR' neqR']]]].
    destruct (L.eq_dec l' l) as [ e|n]; subst;
      [|apply neqR' in n; rewrite n in proj_lR; inversion proj_lR].
    rewrite proj_lR in eqR'; inversion eqR'; clear eqR'; symmetry in H0; subst.
    exists (LM.add l E2 Π1); split; [| unfold LessNondetSystem; split].
    - eapply IotaStep; eauto. apply LM.add_1.
      intros q E neq; split; intro mt.
      apply LM.add_2; auto.
      apply LM.add_3 in mt; auto.
    - intros l' E' mt.
      destruct (L.eq_dec l' l); subst.
      pose proof (LMF.MapsToUnique mt mt2); subst.
      exists E2; split; auto with CtrlExpr. apply LM.add_1.
      pose proof (neqR' l' ltac:(intro e; apply n; symmetry; exact e)) as proj_R_l'.
      destruct (proj1 (ProjectSystemLookup _ _ _ eq2 l' E') mt) as [i proj_l'2]; subst.
      destruct (EPP C1 l') as [E''|] eqn:proj_l'1;
        [|rewrite (ProjectSystemLookupNone C1 nms l' i proj_l'1) in eq1; inversion eq1].
      pose proof (EPPRedex C1 R B C2 l' E'' E' proj_l'1 proj_l'2 proj_R_l' step1).
      exists E''; split; auto with CtrlExpr. apply LM.add_2; auto.
      apply (proj2 (ProjectSystemLookup _ _ _ eq1 l' E'') ltac:(split; auto)).
    - intros l' E' mt.
      destruct (L.eq_dec l' l); subst.
      -- pose proof (LMF.MapsToUnique mt (LM.add_1 Π1 l E2)); subst.
         exists E2; split; auto with CtrlExpr.
      -- apply LM.add_3 in mt; auto.
         apply (ProjectSystemLookup _ _ _ eq1 l' E') in mt; destruct mt as [i proj_l'1].
         destruct (EPP C2 l') as [E''|] eqn:proj_l'2;
           [|rewrite (ProjectSystemLookupNone C2 nms l' i proj_l'2) in eq2; inversion eq2].
         pose proof (EPPRedex C1 R B C2 l' E' E'' proj_l'1 proj_l'2
                              (neqR' l' ltac:(intro eq; apply n; symmetry; exact eq)) step1).
         exists E''; split; auto. apply (ProjectSystemLookup _ _ _ eq2 l' E''); split; auto.
  Qed.

  (* OTHER LABEL PROJECTION THEOREMS *)
  (*
    Other types of system steps we do not prove as lemmas, but prove them as part of the 
    overall theorem. However, we do prove some label-projection theorems.
   *)

  (* SYNC IOTA LABELS *)
  Lemma RedexSysSyncIotaSyncIota : forall R,
      RedexToSystemLabel R = SysSyncIota ->
      exists L, IsSyncLabel L /\ forall l, ProjectRedex R l = Some L.
  Proof using.
    intros R; induction R; intros eq; cbn in *; try discriminate; try reflexivity.
    1-3: exists SyncIota; split; auto; constructor.
                all: destruct (IHR eq) as [L [eq' sync_L]]; clear eq.
                exists (FunLabel L).
                2: exists (ArgLabel L).
              all: split; [constructor; auto|]; intro l;
                specialize (sync_L l); auto; rewrite sync_L;
                  auto; fail.
  Qed.

  (* COMM LABELS*)
  Lemma InvolvedInCommLabel: forall R l v l',
      RedexToSystemLabel R = CommLabel l v l' ->
      C.InvolvedWithRedex R l /\
      C.InvolvedWithRedex R l' /\
      forall l'', l'' <> l -> l'' <> l' -> ~ C.InvolvedWithRedex R l''.
  Proof using.
    intros R; induction R; intros p v q eq; cbn in *; try discriminate.
    - inversion eq; subst; clear eq; split; [| split]; auto;
        intros l'' neq1 neq2 eqs; destruct eqs; subst;
          try match goal with
              | [H : ?a <> ?a |- _] => destruct (H eq_refl)
              end.
    - apply IHR in eq; auto.
    - apply IHR in eq; auto.
  Qed.

  Lemma PirStepCommDistinguish : forall R B C1 C2 l v l',
      RedexToSystemLabel R = CommLabel l v l' ->
      C.PirStep R B C1 C2 ->
      l <> l'.
  Proof using.
    intros R B C1 C2 l v l' eq step; revert l v l' eq; induction step;
      intros p v' q eq; cbn in eq; try discriminate;
        try match goal with
            | [IH : forall l v l', RedexToSystemLabel ?R = CommLabel l v l' -> l <> l',
                 H : RedexToSystemLabel ?R = CommLabel ?p ?e ?q |- _ ] =>
              apply IH in H; clear IH
            end; auto.
    inversion eq; subst; clear eq; auto.
  Qed.

  Lemma ProjectCommRedex : forall R l v l',
      l <> l' ->
      RedexToSystemLabel R = CommLabel l v l' ->
      exists L1 L2, ProjectRedex R l = Some L1 /\
                    ProjectRedex R l' = Some L2 /\
                    CommLabelPair l v l' L1 L2 /\
                    forall l'', l'' <> l -> l'' <> l' -> ProjectRedex R l'' = None.
  Proof using.
    intro R; induction R; intros p v q neq eq; cbn in eq; try discriminate.
    inversion eq; clear eq; subst.
    2,3: apply IHR in eq; auto; destruct eq as [L1 [L2 [eq1 [eq2 [pr H]]]]].
    all: cbn; EPPDestructor;
      match goal with
      |[ |- exists L1 L2, Some ?L1' = Some L1 /\ Some ?L2' = Some L2 /\ _ /\ _ ] =>
       exists L1'; exists L2'; split; [reflexivity|split;[reflexivity|split]]
      end.
    1,3,5: constructor; auto.
    all: intros l'' neq1 neq2; EPPDestructor; auto.
    all: rewrite H in eq; auto; inversion eq.
  Qed.

  (* CHOICE LABELS *)
  Lemma InvolvedInChoiceLabel: forall R l d l',
      RedexToSystemLabel R = ChoiceLabel l d l' ->
      C.InvolvedWithRedex R l /\
      C.InvolvedWithRedex R l' /\
      forall l'', l'' <> l -> l'' <> l' -> ~ C.InvolvedWithRedex R l''.
  Proof using.
    intros R; induction R; intros p d q eq; cbn in *; try discriminate.
    - inversion eq; subst; clear eq; split; [| split]; auto;
        intros l'' neq1 neq2 eqs; destruct eqs; subst;
          try match goal with
              | [H : ?a <> ?a |- _] => destruct (H eq_refl)
              end.
    - apply IHR in eq; auto.
    - apply IHR in eq; auto.
  Qed.
  
  Lemma PirStepChoiceDistinguish : forall R B C1 C2 l d l',
      RedexToSystemLabel R = ChoiceLabel l d l' ->
      C.PirStep R B C1 C2 ->
      l <> l'.
  Proof using.
    intros R B C1 C2 l d l' eq step; revert l d l' eq;
      induction step; intros p dir q eq; cbn in *; try discriminate;
        try match goal with
            | [IH : forall l d l', RedexToSystemLabel ?R = ChoiceLabel l d l' -> l <> l',
                 H : RedexToSystemLabel ?R = ChoiceLabel ?p ?dir ?q |- _ ] =>
              apply IH in H; clear IH
            end; auto.
    inversion eq; subst; auto.
  Qed.
  
  Lemma ProjectChoiceRedex : forall R l d l',
      l <> l' ->
      RedexToSystemLabel R = ChoiceLabel l d l' ->
      exists L1 L2, ProjectRedex R l = Some L1 /\
                    ProjectRedex R l' = Some L2 /\
                    ChoiceLabelPair l d l' L1 L2 /\
                    forall l'', l'' <> l -> l'' <> l' -> ProjectRedex R l'' = None.
  Proof using.
    intro R; induction R; intros p v q neq eq; cbn in eq; try discriminate.
    inversion eq; clear eq; subst.
    2,3: apply IHR in eq; auto; destruct eq as [L1 [L2 [eq1 [eq2 [pr H]]]]].
    all: cbn; EPPDestructor;
      match goal with
      |[ |- exists L1 L2, Some ?L1' = Some L1 /\ Some ?L2' = Some L2 /\ _ /\ _ ] =>
       exists L1'; exists L2'; split; [reflexivity|split;[reflexivity|split]]
      end.
    1,3,5,7,9,11: constructor; auto.
    all: intros l'' neq1 neq2; EPPDestructor; auto.
    all: rewrite H in eq; auto; inversion eq.
  Qed.

  (* SYSTEM COMPLETENESS *)
  Theorem SystemCompleteness : forall R B C1 C2 Π1 Π2 nms,
      ProjectSystem C1 nms = Some Π1 ->
      ProjectSystem C2 nms = Some Π2 ->
      C.PirStep R B C1 C2 ->
      (forall l : Loc, C.InvolvedWithRedex R l -> In l nms) ->
      exists Π2', SystemStep Π1 (RedexToSystemLabel R) Π2' /\ LessNondetSystem Π2 Π2'.
  Proof using.
    intros R B C1 C2 Π1 Π2 nms eq1 eq2 step allinvolved.
    destruct (RedexToSystemLabel R) eqn:eqR.
    - eapply IotaStepSystemCompleteness; eauto.
    - clear allinvolved.
      destruct (RedexSysSyncIotaSyncIota R eqR) as [L [projl_R syncL]].
      exists Π2; split; [|reflexivity]; econstructor; eauto.
      intros l E1 E2 mt1 mt2.
      destruct (proj1 (ProjectSystemLookup C1 nms Π1 eq1 l E1) mt1) as [i proj_l_1].
      destruct (proj1 (ProjectSystemLookup C2 nms Π2 eq2 l E2) mt2) as [_ proj_l_2].
      apply (LocalCompleteness C1 R B C2 l E1 E2 L proj_l_1 proj_l_2 (syncL l) step).
      intro p; split; intro i; destruct i as [E mt].
      pose proof (proj1 (ProjectSystemLookup _ _ _ eq1 p E) mt); destruct H.
      destruct (EPP C2 p) as [E2|] eqn:eq3.
      pose proof (proj2 (ProjectSystemLookup _ _ _ eq2 p E2) ltac:(split; auto)).
      exists E2; auto.
      eapply ProjectSystemLookupNone in eq3; eauto; rewrite eq3 in eq2; inversion eq2.
      pose proof (proj1 (ProjectSystemLookup _ _ _ eq2 p E) mt); destruct H.
      destruct (EPP C1 p) as [E1|] eqn:eq3.
      pose proof (proj2 (ProjectSystemLookup _ _ _ eq1 p E1) ltac:(split; auto)).
      exists E1; auto.
      eapply ProjectSystemLookupNone in eq3; eauto; rewrite eq3 in eq1; inversion eq1.
    - rename l into p; rename l0 into q.
      destruct (InvolvedInCommLabel R p e q eqR) as [inv_p [inv_q ninv]].
      destruct (EPP C1 p) eqn:eq_p_1;
        [|apply ProjectSystemLookupNone with (nms := nms) in eq_p_1; auto;
          rewrite eq_p_1 in eq1; inversion eq1].
      destruct (EPP C1 q) eqn:eq_q_1;
        [|apply ProjectSystemLookupNone with (nms := nms) in eq_q_1; auto;
          rewrite eq_q_1 in eq1; inversion eq1].
      destruct (EPP C2 p) eqn:eq_p_2;
        [|apply ProjectSystemLookupNone with (nms := nms) in eq_p_2; auto;
          rewrite eq_p_2 in eq2; inversion eq2].
      destruct (EPP C2 q) eqn:eq_q_2;
        [|apply ProjectSystemLookupNone with (nms := nms) in eq_q_2; auto;
          rewrite eq_q_2 in eq2; inversion eq2].
      pose proof (proj2 (ProjectSystemLookup C1 nms Π1 eq1 p c) ltac:(split; auto)).
      pose proof (proj2 (ProjectSystemLookup C1 nms Π1 eq1 q c0) ltac:(split; auto)).
      pose proof (proj2 (ProjectSystemLookup C2 nms Π2 eq2 p c1) ltac:(split; auto)).
      pose proof (proj2 (ProjectSystemLookup C2 nms Π2 eq2 q c2) ltac:(split; auto)).
      pose proof (PirStepCommDistinguish _ _ _ _ _ _ _ eqR step).
      destruct (ProjectCommRedex R p e q H3 eqR) as [L1 [L2 [projR1 [projR2 [pr projR3]]]]].
      exists (LM.add p c1 (LM.add q c2 Π1)); split; [| unfold LessNondetSystem; split].
      --  pose proof (LocalCompleteness _ R B _ _ _ _
                                        L1 eq_p_1 eq_p_2 projR1 step).
          pose proof (LocalCompleteness _ R B _ _ _ _
                                        L2 eq_q_1 eq_q_2 projR2 step).
          econstructor; eauto.
          apply LM.add_1. apply LM.add_2; auto; apply LM.add_1.
          intros r E neq1 neq2; split; intro mt.
          repeat (apply LM.add_2; auto). repeat (apply LM.add_3 in mt; auto).
      -- intros l E1 mt. unfold ICL.Loc in l; fold Loc in l.
         destruct (L.eq_dec l p); subst; [| destruct (L.eq_dec l q); subst].
         --- pose proof (LMF.MapsToUnique mt H1); subst.
             exists c1; split; auto with CtrlExpr. apply LM.add_1.
         --- pose proof (LMF.MapsToUnique mt H2); subst.
             exists c2; split; auto with CtrlExpr. apply LM.add_2; auto. apply LM.add_1.
         --- apply (ProjectSystemLookup _ _ _ eq2) in mt; destruct mt as [i eq_l_2].
             destruct (EPP C1 l) eqn:eq_l_1;
               [|apply ProjectSystemLookupNone with (nms := nms) in eq_l_1; auto;
                 rewrite eq_l_1 in eq1; inversion eq1].
             assert (ProjectRedex R l = None) by (apply projR3; auto).
             pose proof (EPPRedex _ _ _ _ _ _ _ eq_l_1 eq_l_2 H4 step).
             pose proof (proj2 (ProjectSystemLookup C1 nms Π1 eq1 l c3)
                               ltac:(split; auto)).
             exists c3; split; auto. repeat (apply LM.add_2; auto).
      -- intros l E2 mt. unfold ICL.Loc in l; fold Loc in l.
         destruct (L.eq_dec l p); subst; [| destruct (L.eq_dec l q); subst].
         --- pose proof (LMF.MapsToUnique mt (LM.add_1 (LM.add q c2 Π1) p c1)); subst.
             exists c1; split; auto with CtrlExpr.
         --- apply LM.add_3 in mt; auto.
             pose proof (LMF.MapsToUnique mt (LM.add_1 Π1 q c2)); subst.
             exists c2; split; auto with CtrlExpr.
         --- repeat (apply LM.add_3 in mt; auto).
             apply (ProjectSystemLookup _ _ _ eq1) in mt; destruct mt as [i eq_l_1].
             destruct (EPP C2 l) eqn:eq_l_2;
               [|apply ProjectSystemLookupNone with (nms := nms) in eq_l_2; auto;
                 rewrite eq_l_2 in eq2; inversion eq2].
             pose proof (proj2 (ProjectSystemLookup C2 nms Π2 eq2 l c3) ltac:(split; auto)).
             assert (ProjectRedex R l = None) by (apply projR3; auto).
             pose proof (EPPRedex C1 _ B C2 l E2 c3 eq_l_1 eq_l_2 H5 step).
             exists c3; split; auto.
    - rename l into p; rename l0 into q; rename s into d.
      pose proof (PirStepChoiceDistinguish R B C1 C2 p d q eqR step).
      destruct (ProjectChoiceRedex R p d q H eqR)
        as [L1 [L2 [projR1 [projR2 [pr projR3]]]]].
      destruct (InvolvedInChoiceLabel R p d q eqR) as [p_inv [q_inv all_inv]].
      cbn in allinvolved; pose proof (allinvolved p ltac:(auto)) as i_p;
        pose proof (allinvolved q ltac:(auto)) as i_q; clear allinvolved.
      destruct (EPP C1 p) as [Ep1|] eqn:eq_p_1;
        [|apply ProjectSystemLookupNone with (nms := nms) in eq_p_1; auto;
          rewrite eq_p_1 in eq1; inversion eq1].
      destruct (EPP C1 q) as [Eq1|] eqn:eq_q_1;
        [|apply ProjectSystemLookupNone with (nms := nms) in eq_q_1; auto;
          rewrite eq_q_1 in eq1; inversion eq1].
      destruct (EPP C2 p) as [Ep2|] eqn:eq_p_2;
        [|apply ProjectSystemLookupNone with (nms := nms) in eq_p_2; auto;
          rewrite eq_p_2 in eq2; inversion eq2].
      destruct (EPP C2 q) as [Eq2|] eqn:eq_q_2;
        [|apply ProjectSystemLookupNone with (nms := nms) in eq_q_2; auto;
          rewrite eq_q_2 in eq2; inversion eq2].      
      pose proof (proj2 (ProjectSystemLookup C1 nms Π1 eq1 p Ep1) ltac:(split; auto)).
      pose proof (proj2 (ProjectSystemLookup C1 nms Π1 eq1 q Eq1) ltac:(split; auto)).
      pose proof (proj2 (ProjectSystemLookup C2 nms Π2 eq2 p Ep2) ltac:(split; auto)).
      pose proof (proj2 (ProjectSystemLookup C2 nms Π2 eq2 q Eq2) ltac:(split; auto)).
      exists (LM.add p Ep2 (LM.add q Eq2 Π1)); split; [|unfold LessNondetSystem; split].
      -- pose proof (LocalCompleteness _ _ _ _ _ _ _ _ eq_p_1 eq_p_2 projR1 step).
         pose proof (LocalCompleteness _ _ _ _ _ _ _ _ eq_q_1 eq_q_2 projR2 step).
         econstructor; eauto with CtrlExpr. apply LM.add_1.
         apply LM.add_2; auto; apply LM.add_1.
         intros r E neq1 neq2; split; intro mt.
         repeat (apply LM.add_2; auto). repeat (apply LM.add_3 in mt; auto).
      -- intros l E1 mt; unfold ICL.Loc in l; fold Loc in l.
         destruct (L.eq_dec l p); subst; [|destruct (L.eq_dec l q); subst].
         --- pose proof (LMF.MapsToUnique mt H2); subst; clear mt.
             exists Ep2; split; [apply LM.add_1 | reflexivity].
         --- pose proof (LMF.MapsToUnique mt H3); subst; clear mt.
             exists Eq2; split; [apply LM.add_2; auto; apply LM.add_1| reflexivity].
         --- apply (ProjectSystemLookup _ _ _ eq2) in mt; destruct mt as [i eq_l_2].
             rename E1 into El2.
             destruct (EPP C1 l) as [El1|] eqn:eq_l_1;
               [|apply ProjectSystemLookupNone with (nms := nms) in eq_l_1; auto;
                 rewrite eq_l_1 in eq1; inversion eq1].
             pose proof (proj2 (ProjectSystemLookup _ _ _ eq1 l El1) ltac:(split; auto)).
             assert (ProjectRedex R l = None) by (apply projR3; auto).
             pose proof (EPPRedex _ _ _ _ _ _ _ eq_l_1 eq_l_2 H5 step).
             exists El1; split; auto. repeat (apply LM.add_2; auto).
      -- intros l E2 mt; unfold ICL.Loc in l; fold Loc in l.
         destruct (L.eq_dec l p); subst; [|destruct (L.eq_dec l q); subst].
         --- pose proof (LMF.MapsToUnique mt (LM.add_1(LM.add q Eq2 Π1) p Ep2)); subst;
               clear mt.
             exists Ep2; split; auto; reflexivity.
         --- apply LM.add_3 in mt; auto.
             pose proof (LMF.MapsToUnique mt (LM.add_1 Π1 q Eq2)); subst;
               clear mt.
             exists Eq2; split; auto; reflexivity.
         --- repeat (apply LM.add_3 in mt; auto).
             apply (ProjectSystemLookup _ _ _ eq1) in mt; destruct mt as [i eq_l_1].
             rename E2 into El1.
             destruct (EPP C2 l) as [El2|] eqn:eq_l_2;
               [|apply ProjectSystemLookupNone with (nms := nms) in eq_l_2; auto;
                 rewrite eq_l_2 in eq2; inversion eq2].
             pose proof (proj2 (ProjectSystemLookup _ _ _ eq2 l El2) ltac:(split; auto)).
             assert (ProjectRedex R l = None) by (apply projR3; auto).
             pose proof (EPPRedex _ _ _ _ _ _ _ eq_l_1 eq_l_2 H5 step).
             exists El2; split; auto.
  Qed.

  Lemma SystemCompletenessExists : forall R B C1 C2 Π1 nms,
      ProjectSystem C1 nms = Some Π1 ->
      C.PirStep R B C1 C2 ->
      exists Π2, ProjectSystem C2 nms = Some Π2.
  Proof using.
    intros R B C1 C2 Π1 nms; revert R B C1 C2 Π1; induction nms as [| l nms];
      intros R B C1 C2 Π1 eq step; cbn in *.
    - exists LM.empty; auto.
    - destruct (EPP C1 l) as [E1|] eqn:proj_l_1; try discriminate.
      destruct (ProjectSystem C1 nms) as [Π1'|] eqn:eq1; try discriminate.
      inversion eq; clear eq; subst.
      destruct (LocalCompletenessExists C1 R B C2 l E1 proj_l_1 step) as [E2 proj_l_2];
        rewrite proj_l_2.
      destruct (IHnms R B C1 C2 Π1' eq1 step) as [Π2' eq2].
      rewrite eq2. eexists; eauto.
  Qed.

  Corollary FullCompleteness : forall R B C1 C2 Π1 nms,
      ProjectSystem C1 nms = Some Π1 ->
      C.PirStep R B C1 C2 ->
      (forall l : Loc, C.InvolvedWithRedex R l -> In l nms) ->
      exists Π2 Π2',
        (ProjectSystem C2 nms = Some Π2
         /\ SystemStep Π1 (RedexToSystemLabel R) Π2'
         /\ LessNondetSystem Π2 Π2').
  Proof using.
    intros R B C1 C2 Π1 nms H H0 H1.
    destruct (SystemCompletenessExists R B C1 C2 Π1 nms H H0) as [Π2 eq].
    destruct (SystemCompleteness R B C1 C2 Π1 Π2 nms H eq H0 H1) as [Π2' [parstep lnd]].
    exists Π2; exists Π2'; split; [|split]; auto.
  Qed.

  (* A quick inductive types for tactics (to ensure we don't get into infinite loops). *)
  Inductive LocalCompleteAppliedIH : Loc -> CtrlExpr -> CtrlExprRedex -> CtrlExpr ->
                                     C.PirExpr -> Prop :=
  | LCAIH : forall l E1 R E2 C, LocalCompleteAppliedIH l E1 R E2 C.


  (* LOCAL IOTA SOUNDNESS *)
  
  Lemma LocalIotaSoundness' : forall C1 p E1 R E2,
      InternalLabel (CtrlExprRedexToLabel R) ->
      CtrlExprStep' E1 R E2 ->
      EPP C1 p = Some E1 ->
      exists C2, EPP C2 p = Some E2 /\
                 forall B, ~ In p B -> C.PirStep (CtrlExprRedexToRedex R p) B C1 C2.
  Proof using.
    intro C1; induction C1; cbn; intros p E1 R E2 pf step eq;
      destruct R; cbn in pf; try discriminate; cbn.

    all: EPPDestructor; try discriminate; inversion step; subst;
      inversion pf; subst; cbn.

    all: try (eexists; split; intros; eauto with PirExpr; cbn;
              EPPDestructor; auto; fail).

    all: repeat match goal with
                | [ eq : CtrlExprMerge ?a ?b = Some ?c |- _ ] =>
                  tryif let H := fresh "" c in idtac
                  then fail
                  else lazymatch goal with
                       | [ _ : CtrlExprMergeRel a b c |- _ ] => fail
                       | _ => let H := fresh "merge" in
                             pose proof (CtrlExprMergeRelSpec2 _ _ _ eq) as H;
                               inversion H; subst
                       end
                | [ IH : forall p E1 R E2,
                      InternalLabel (CtrlExprRedexToLabel R) ->
                      CtrlExprStep' E1 R E2 ->
                      EPP ?C1 p = Some E1 ->
                      exists C2, EPP C2 p = Some E2 /\
                            (forall B, ~ In p B -> C.PirStep (CtrlExprRedexToRedex R p) B ?C1 C2),
                      H : InternalLabel (CtrlExprRedexToLabel ?R),
                      step : CtrlExprStep' ?E1 ?R ?E2,
                      eq : EPP ?C1 ?p = Some ?E1 |- _ ] =>
                  lazymatch goal with
                  | [ _ : LocalCompleteAppliedIH p E1 R E2 C1 |- _ ] => fail
                  | _ =>
                    let lcaih := fresh "lcaih" in
                    let C2 := fresh "C" in
                    let step' := fresh "step" in
                    let eq' := fresh "eq" in
                    pose proof (LCAIH p E1 R E2 C1) as lcaih;
                      destruct (IH p E1 R E2 H step eq)
                      as [C2 [eq' step']];
                      cbn in step'
                  end
                | [ IH : forall p E1 R E2,
                      InternalLabel (CtrlExprRedexToLabel R) ->
                      CtrlExprStep' E1 R E2 ->
                      EPP ?C1 p = Some E1 ->
                      exists C2, EPP C2 p = Some E2 /\
                            (forall B, ~ In p B -> C.PirStep (CtrlExprRedexToRedex R p) B ?C1 C2),
                      step : CtrlExprStep' ?E1 ?R ?E2,
                      eq : EPP ?C1 ?p = Some ?E1 |- _ ] =>
                  lazymatch goal with
                  | [ _ : LocalCompleteAppliedIH p E1 R E2 C1 |- _ ] => fail
                  | _ =>
                    let lcaih := fresh "lcaih" in
                    let C2 := fresh "C" in
                    let step' := fresh "step" in
                    let eq' := fresh "eq" in
                    pose proof (LCAIH p E1 R E2 C1) as lcaih;
                      destruct (IH p E1 R E2 ltac:(constructor; auto) step eq)
                      as [C2 [eq' step']];
                      cbn in step'
                  end
                | [H : CtrlExprMergeRel ?E1 ?E2 ?E3,
                       H' : CtrlExprStep' ?E3 ?R ?E4 |- _ ] =>
                  lazymatch goal with
                  | [ _ : CtrlExprStep' E1 _ _ |- _ ] => fail
                  | _ =>
                    let E1' := fresh "E" in
                    let E2' := fresh "E" in
                    let merge := fresh "merge" in
                    let step1 := fresh "step" in
                    let step2 := fresh "step" in
                    destruct (UnmergeRelIotaStep' E1 E2 E3 _ E4 H' ltac:(cbn; auto) H)
                      as [E1' [E2' [merge [step1 step2]]]]
                  end
                end.

    all: try (eexists; split; [| intros; eauto with PirExpr; econstructor;
                                 eauto with PirExpr;
                                 repeat match goal with
                                        | [ neq : ?p <> ?l, ni : ~ In ?p ?B
                                            |- context[?l :: ?B]] =>
                                          lazymatch goal with
                                          | [ _ : ~ In p (l :: B) |- _ ] => fail
                                          | _ =>
                                            let i := fresh in
                                            let eq := fresh in
                                            assert (~ In p (l :: B))
                                              by  (intro i; destruct i as [eq|i];
                                                   [ destruct (neq (eq_sym eq))
                                                   | destruct (ni i)])
                                          end
                                        | [ step : forall B, ~ In ?p B -> C.PirStep ?R B ?C1 ?C2,
                                              ni : ~ In ?p ?B |- _ ] =>
                                          lazymatch goal with
                                          | [ _ : C.PirStep R B C1 C2 |- _ ] => fail
                                          | _ => pose proof (step B ni)
                                          end
                                        end; auto];
              repeat (cbn; EPPDestructor; eauto); fail);
      try match goal with
          | [ H :CtrlExprStep' (FunGlobal _) _ _ |- _ ] => inversion H
          | [ H :CtrlExprStep' Unit _ _ |- _ ] => inversion H
          end.
  Qed.      

  Corollary LocalIotaSoundness : forall C1 p E1 E2 L,
      EPP C1 p = Some E1 ->
      CtrlExprStep E1 L E2 ->
      InternalLabel L ->
      exists R C2, ProjectRedex R p = Some L /\
                   EPP C2 p = Some E2 /\
                   C.PirStep R [] C1 C2.
  Proof using.
    intros C1 p E1 E2 L H H0 H12.
    destruct (CtrlExprStepToPrime H0) as [R [pf step]]; clear H0; subst.
    destruct (LocalIotaSoundness' C1 p E1 R E2 H12 step H) as [C2 [eq2 step']].
    exists (CtrlExprRedexToRedex R p); exists C2; split; [|split]; auto.
    clear step' eq2 C2 step H E1 E2 C1.
    induction R; cbn; EPPDestructor; try discriminate; auto; inversion H12; subst.
    all: apply IHR in H0; inversion H0; subst; auto.
  Qed.

  (* LOCAL COMMUNICATION SOUNDNESS *)

  (*
    In the control language, communication happens in two places: the sender and the 
    receiver. The redices must match up; in particular, the same value must be received
    as was sent. Moreover, the same stack of Arg and Fun constructors must be on top.
    We also ensure that these match up with a Pirouette redex, ensuring that the locations
    in the Pirouette redex do the sending and receiving to/from their partners.
   *)
  Inductive MatchedCommRedices : Loc -> Expr -> Loc -> CtrlExprRedex -> CtrlExprRedex ->
                                 C.Redex -> Prop :=
    BaseCommRedices1 : forall {l1 v l2},
      l1 <> l2 ->
      MatchedCommRedices l1 v l2 (SendV l2 v) (RecvV l1 v) (C.RSendV l1 v l2)
  | BaseCommRedices2 : forall {l1 v l2},
      l1 <> l2 ->
      MatchedCommRedices l1 v l2 (RecvV l1 v) (SendV l2 v) (C.RSendV l1 v l2)
  | ArgCommRedices : forall l1 v l2 R1 R2 R,
      MatchedCommRedices l1 v l2 R1 R2 R ->
      MatchedCommRedices l1 v l2 (ArgRedex R1) (ArgRedex R2) (C.RArg R)
  | FunCommRedices : forall l1 v l2 R1 R2 R,
      MatchedCommRedices l1 v l2 R1 R2 R ->
      MatchedCommRedices l1 v l2 (FunRedex R1) (FunRedex R2) (C.RFun R).

  Lemma MatchedCommRedicesSym : forall l1 v l2 R1 R2 R,
      MatchedCommRedices l1 v l2 R1 R2 R -> MatchedCommRedices l1 v l2 R2 R1 R.
  Proof using.
    intros l1 v l2 R1 R2 R matched1; induction matched1; try (constructor; auto).
  Qed.

  Lemma MatchedCommRedicesUnique : forall l1 v l2 R1 R2 R R',
      MatchedCommRedices l1 v l2 R1 R2 R ->
      MatchedCommRedices l1 v l2 R1 R2 R' ->
      R = R'.
  Proof using.
    intros l1 v l2 R1 R2 R R' matched1; revert R'; induction matched1; intros R' matched2;
      inversion matched2; subst; try reflexivity.
    all: apply IHmatched1 in H4; subst; auto.
  Qed.

  Lemma MatchedCommRedicesProject : forall l1 v l2 R1 R2 R,
      MatchedCommRedices l1 v l2 R1 R2 R ->
      exists L1 L2, CommLabelPair l1 v l2 L1 L2 /\
                    ProjectRedex R l1 = Some L1 /\
                    ProjectRedex R l2 = Some L2.
  Proof using.
    intros l1 v l2 R1 R2 R matched; induction matched.
    - eexists; eexists; cbn; EPPDestructor; split; [|split]; eauto.
      constructor.
    - eexists; eexists; cbn; EPPDestructor; split; [|split]; eauto.
      constructor.
    - destruct IHmatched as [L1 [L2 [pr [eq1 eq2]]]];
        exists (ArgLabel L1); exists (ArgLabel L2); cbn; EPPDestructor;
          split; [|split]; auto; constructor; auto.
    - destruct IHmatched as [L1 [L2 [pr [eq1 eq2]]]];
        exists (FunLabel L1); exists (FunLabel L2); cbn; EPPDestructor;
          split; [|split]; auto; constructor; auto.
  Qed.

  (*
    Earlier, we introduced communication label pairs. MatchedCommRedices expresses
    at least this much, but for redices instead of labels.
   *)
  Lemma MatchedCommRedicesToLabels : forall l1 v l2 R1 R2 R,
      MatchedCommRedices l1 v l2 R1 R2 R ->
      CommLabelPair l1 v l2 (CtrlExprRedexToLabel R1) (CtrlExprRedexToLabel R2).
  Proof using.
    intros l1 v l2 R1 R2 R matched; induction matched; cbn; constructor; auto.
  Qed.

  Lemma MatchedCommRedicesDistinguish : forall l1 v l2 R1 R2 R,
      MatchedCommRedices l1 v l2 R1 R2 R -> l1 <> l2.
  Proof using.
    intros l1 v l2 R1 R2 R matched; induction matched; auto.
  Qed.

  Lemma MatchedCommRedicesNotAllowChoice : forall l1 v l2 R1 R2 R,
      MatchedCommRedices l1 v l2 R1 R2 R ->
      ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R1) /\
      ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R2).
  Proof using.
    intros l1 v l2 R1 R2 R matched; induction matched; cbn; split;
      intro H'; inversion H'; subst.
    all: apply IHmatched in H0; auto.
  Qed.
  
  Lemma LocalCommSoundness' : forall C1 l1 v l2 E11 E12 E21 E22 R1 R2 R,
      EPP C1 l1 = Some E11 ->
      EPP C1 l2 = Some E21 ->
      CtrlExprStep' E11 R1 E12 ->
      CtrlExprStep' E21 R2 E22 ->
      MatchedCommRedices l1 v l2 R1 R2 R ->
      exists C2, EPP C2 l1 = Some E12
                 /\ EPP C2 l2 = Some E22
                 /\ ProjectRedexToRedex R l1 = Some R1
                 /\ ProjectRedexToRedex R l2 = Some R2
                 /\ (forall B, ~ In l1 B -> ~ In l2 B -> C.PirStep R B C1 C2).
  Proof using.
    intro C1; induction C1; intros l1 v l2 E11 E12 E21 E22 R1 R2 R; cbn;
      intros eq1 eq2 lstep1 lstep2 matched; EPPDestructor;
        try discriminate; inversion lstep1; subst; inversion lstep2; subst;
          inversion matched; subst; cbn in *; EPPDestructor;
            repeat match goal with
                   | [ H : Some _ = Some _ |- _ ] =>
                     inversion H; clear H; subst
                   | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
                   | [ H : CtrlExprStep' (FunGlobal _) _ _ |- _ ] => inversion H
                   | [ H : CtrlExprStep' Unit _ _ |- _ ] => inversion H
                   | [ H : SendLabel ?a ?b = SendLabel ?c ?d |- _ ] =>
                     inversion H; clear H; subst
                   | [ H: RecvLabel ?a ?b = RecvLabel ?c ?d |- _ ] =>
                     inversion H; clear H; subst
                   | [ H : MatchedCommRedices ?l1 _ ?l2 _ _ _ |- _ ] =>
                     lazymatch goal with
                     | [ _ : l1 <> l2 |- _ ] => fail
                     | _ => pose proof (MatchedCommRedicesDistinguish _ _ _ _ _ _ H)
                     end
                   | [ H : MatchedCommRedices _ _ _ ?R1 ?R2 _ |- _ ] =>
                     lazymatch goal with
                     | [ _ : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R1),
                             _ : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R2) |- _ ] =>
                       fail
                     | _ => destruct (MatchedCommRedicesNotAllowChoice _ _ _ _ _ _ H)
                     end
                   | [ H : CtrlExprStep' ?E1 ?R ?E2,
                           H' : CtrlExprMerge ?E11 ?E12 = Some ?E1,
                                H'' : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel ?R) |- _ ] =>
                     lazymatch goal with
                     | [_ : CtrlExprStep' E11 R _ |- _ ] => fail
                     | _ =>
                       let E21 := fresh "E" in
                       let E22 := fresh "E" in
                       let merge := fresh "merge" in
                       let step1 := fresh "step" in
                       let step2 := fresh "step" in
                       destruct (UnmergeRelStep' _ _ _ _ _ H H''
                                                 (CtrlExprMergeRelSpec2 _ _ _ H'))
                         as [E21 [E22 [merge [step1 step2]]]];
                         apply CtrlExprMergeRelSpec1 in merge
                     end
                   | [ H : CtrlExprStep' ?E1 ?R ?E2,
                           H' : CtrlExprMerge ?E11 ?E12 = Some ?E1,
                                H'' : CtrlExprRedexToLabel ?R = RecvLabel ?l ?v |- _ ] =>
                     lazymatch goal with
                     | [_ : CtrlExprStep' E11 R _ |- _ ] => fail
                     | _ =>
                       let E21 := fresh "E" in
                       let E22 := fresh "E" in
                       let merge := fresh "merge" in
                       let step1 := fresh "step" in
                       let step2 := fresh "step" in
                       destruct (UnmergeRelCommStep'
                                   _ _ _ _ _ l v H
                                   ltac:(right; exact H'') (CtrlExprMergeRelSpec2 _ _ _ H'))
                                 as [E21 [E22 [merge [step1 step2]]]];
                         apply CtrlExprMergeRelSpec1 in merge
                     end
                   | [ H : CtrlExprStep' ?E1 (SendV ?l ?v) ?E2,
                           H' : CtrlExprMerge ?E11 ?E12 = Some ?E1 |- _ ] =>
                     lazymatch goal with
                     | [_ : CtrlExprStep' E11 (SendV l v) _ |- _ ] => fail
                     | _ =>
                       let E21 := fresh "E" in
                       let E22 := fresh "E" in
                       let merge := fresh "merge" in
                       let step1 := fresh "step" in
                       let step2 := fresh "step" in
                       destruct (UnmergeRelCommStep'
                                   _ _ _ _ _ l v H
                                   ltac:(left; cbn; EPPDestructor; auto)
                                          (CtrlExprMergeRelSpec2 _ _ _ H'))
                         as [E21 [E22 [merge [step1 step2]]]];
                         apply CtrlExprMergeRelSpec1 in merge
                     end
                   | [ H : CtrlExprStep' ?E1 (RecvV ?l ?v) ?E2,
                           H' : CtrlExprMerge ?E11 ?E12 = Some ?E1 |- _ ] =>
                     lazymatch goal with
                     | [_ : CtrlExprStep' E11 (RecvV l v) _ |- _ ] => fail
                     | _ =>
                       let E21 := fresh "E" in
                       let E22 := fresh "E" in
                       let merge := fresh "merge" in
                       let step1 := fresh "step" in
                       let step2 := fresh "step" in
                       destruct (UnmergeRelCommStep'
                                   _ _ _ _ _ l v H
                                   ltac:(right; cbn; EPPDestructor; auto)
                                          (CtrlExprMergeRelSpec2 _ _ _ H'))
                         as [E21 [E22 [merge [step1 step2]]]];
                         apply CtrlExprMergeRelSpec1 in merge
                     end
                   | [ IH : forall l1 v l2 E11 E12 E21 E22 R1 R2 R,
                         EPP ?C1 l1 = Some E11 ->
                         EPP ?C1 l2 = Some E21 ->
                         CtrlExprStep' E11 R1 E12 ->
                         CtrlExprStep' E21 R2 E22 ->
                         MatchedCommRedices l1 v l2 R1 R2 R ->
                         exists C2,
                           EPP C2 l1 = Some E12 /\
                           EPP C2 l2 = Some E22 /\
                           ProjectRedexToRedex R l1 = Some R1 /\
                           ProjectRedexToRedex R l2 = Some R2 /\
                           (forall B, ~ In l1 B -> ~ In l2 B -> C.PirStep R B ?C1 C2),
                         H1 : EPP ?C1 ?l1 = Some ?E11,
                         H2 : EPP ?C1 ?l2 = Some ?E21,
                         H3 : CtrlExprStep' ?E11 ?R1 ?E12,
                         H4 : CtrlExprStep' ?E21 ?R2 ?E22,
                         H7 : MatchedCommRedices ?l1 ?v ?l2 ?R1 ?R2 ?R |- _ ] =>
                     lazymatch goal with
                     | [_ : EPP _ l1 = Some E12 |- _] => fail
                     | _ => let C2 := fresh "C" in
                            let eq1 := fresh "eq" in
                            let eq2 := fresh "eq" in
                            let eq3 := fresh "eq" in
                            let eq4 := fresh "eq" in
                            let cstep := fresh "cstep" in
                            destruct (IH l1 v l2 E11 E12 E21 E22 R1 R2 R H1 H2 H3 H4 H7)
                              as [C2 [eq1 [eq2 [eq3 [eq4 cstep]]]]]
                     end
                   | [ IH : forall l1 v l2 E11 E12 E21 E22 R1 R2 R,
                         EPP ?C1 l1 = Some E11 ->
                         EPP ?C1 l2 = Some E21 ->
                         CtrlExprStep' E11 R1 E12 ->
                         CtrlExprStep' E21 R2 E22 ->
                         MatchedCommRedices l1 v l2 R1 R2 R ->
                         exists C2,
                           EPP C2 l1 = Some E12 /\
                           EPP C2 l2 = Some E22 /\
                           ProjectRedexToRedex R l1 = Some R1 /\
                           ProjectRedexToRedex R l2 = Some R2 /\
                           (forall B, ~ In l1 B -> ~ In l2 B -> C.PirStep R B ?C1 C2),
                         H1 : EPP ?C1 ?l1 = Some ?E11,
                         H2 : EPP ?C1 ?l2 = Some ?E21,
                         H3 : CtrlExprStep' ?E11 (SendV ?l2 ?v) ?E12,
                         H4 : CtrlExprStep' ?E21 (RecvV ?l1 ?v) ?E22,
                         H7 : MatchedCommRedices ?l1 ?v ?l2 ?R1 ?R2 ?R |- _ ] =>
                     lazymatch goal with
                     | [_ : EPP _ l1 = Some E12 |- _] => fail
                     | _ => let C2 := fresh "C" in
                            let eq1 := fresh "eq" in
                            let eq2 := fresh "eq" in
                            let cstep := fresh "cstep" in
                            let eq3 := fresh "eq" in
                            let eq4 := fresh "eq" in
                            destruct (IH l1 v l2 E11 E12 E21 E22 R1 R2 R H1 H2 H3 H4 H7)
                              as [C2 [eq1 [eq2 [eq3 [eq4 cstep]]]]]
                     end
                   | [H1 : CtrlExprRedexToLabel ?R = ?L, H2 : context[ArgRedex ?R] |- _ ] =>
                     lazymatch goal with
                     | [ _ : CtrlExprRedexToLabel (ArgRedex R) = L |- _ ] => fail
                     | _ => assert (CtrlExprRedexToLabel (ArgRedex R) = L) by
                           (cbn; apply H1)
                     end
                   | [H1 : CtrlExprRedexToLabel ?R = ?L, H2 : context[FunRedex ?R] |- _ ] =>
                     lazymatch goal with
                     | [ _ : CtrlExprRedexToLabel (FunRedex R) = L |- _ ] => fail
                     | _ => assert (CtrlExprRedexToLabel (FunRedex R) = L) by
                           (cbn; apply H1)
                     end
                   | [H1 : ?a = ?b, H2 : ?a = ?c |- _ ] =>
                     tryif unify b c
                     then fail
                     else lazymatch goal with
                          | [ _ : b = c |- _ ] => fail
                          | [ _ : c = b |- _ ] => fail
                          | _ => assert (b = c) by (rewrite H1 in H2; exact H2)
                          end
                   end; try discriminate.
    all: try (eexists; split;
              [|split; [| split; [|split;
                                   [| intros; eauto with PirExpr; econstructor;
                                      eauto with PirExpr;
                                      repeat match goal with
                                             | [ neq : ?p <> ?l, ni : ~ In ?p ?B
                                                 |- context[?l :: ?B]] =>
                                               lazymatch goal with
                                               | [ _ : ~ In p (l :: B) |- _ ] => fail
                                               | _ =>
                                                 let i := fresh in
                                                 let eq := fresh in
                                                 assert (~ In p (l :: B))
                                                   by  (intro i; destruct i as [eq|i];
                                                        [ destruct (neq (eq_sym eq))
                                                        | destruct (ni i)])
                                               end
                                             | [ step : forall B, ~ In ?p B ->
                                                             C.PirStep ?R B ?C1 ?C2,
                                                   ni : ~ In ?p ?B |- _ ] =>
                                               lazymatch goal with
                                               | [ _ : C.PirStep R B C1 C2 |- _ ] => fail
                                               | _ => pose proof (step B ni)
                                               end
                                             end; auto]]]];
              repeat (cbn in *; EPPDestructor; eauto); fail).
    exists (C.PirExprLocalSubst C1 (C.ValueSubst ℓ2 v0));
      repeat match goal with | [|-_ /\ _] => split end; auto.
    - rewrite EPPLocalSubst with (E := E12); auto.
      unfold C.ValueSubst; EPPDestructor;
        rewrite CtrlExprLocalIdSubst; reflexivity.
    - rewrite EPPLocalSubst with (E := c); auto.
      unfold C.ValueSubst; EPPDestructor; reflexivity.
    - intros B ni1 ni2; auto with PirExpr.
  Qed.

  (* 
     In order to move from the more-expressive semantics, we have to reconstruct a Pirouette
     redex from labels.
   *)
  Lemma CommLabelPairToMatchedRedices: forall l1 v l2 L1 L2 R1 R2,
      l1 <> l2 ->
      CommLabelPair l1 v l2 L1 L2 ->
      CtrlExprRedexToLabel R1 = L1 ->
      CtrlExprRedexToLabel R2 = L2 ->
      exists R, MatchedCommRedices l1 v l2 R1 R2 R.
  Proof using.
    intros l1 v l2 L1 L2 R1 R2 neq pr; revert R1 R2 neq; induction pr; intros R1 R2 neq eq1 eq2;
      destruct R1; destruct R2; cbn in *; try discriminate.
    all: inversion eq1; inversion eq2; clear eq1 eq2; subst.
    - eexists; econstructor; auto.
    - eexists; econstructor; auto.
    - specialize (IHpr R1 R2 neq eq_refl eq_refl);
        destruct IHpr as [R matched]; exists (C.RFun R); constructor; auto.
    - specialize (IHpr R1 R2 neq eq_refl eq_refl);
        destruct IHpr as [R matched]; exists (C.RArg R); constructor; auto.
  Qed.

  Corollary LocalCommSoundness : forall C1 l1 v l2 E11 E12 E21 E22 L1 L2,
      l1 <> l2 ->
      EPP C1 l1 = Some E11 ->
      EPP C1 l2 = Some E21 ->
      CtrlExprStep E11 L1 E12 ->
      CtrlExprStep E21 L2 E22 ->
      CommLabelPair l1 v l2 L1 L2 ->
      exists R C2, EPP C2 l1 = Some E12
                   /\ EPP C2 l2 = Some E22
                   /\ ProjectRedex R l1 = Some L1
                   /\ ProjectRedex R l2 = Some L2
                   /\ C.PirStep R [] C1 C2.
  Proof using.
    intros C1 l1 v l2 E11 E12 E21 E22 L1 L2 neq H H0 H1 H2 H3.
    destruct (CtrlExprStepToPrime H1) as [R1 [eq1 step1]].
    destruct (CtrlExprStepToPrime H2) as [R2 [eq2 step2]].
    destruct (CommLabelPairToMatchedRedices l1 v l2 L1 L2 R1 R2 neq H3 eq1 eq2)
      as [R matched].
    destruct (LocalCommSoundness'
                C1 l1 v l2 E11 E12 E21 E22 R1 R2 R H H0 step1 step2 matched)
      as [C2 [eq3 [eq4 [eq5 [eq6 step]]]]].
    exists R; exists C2; split; [|split; [|split; [|split]]]; auto.
    rewrite ProjectRedexTriangle1 with (R' := R1); auto; rewrite eq1; reflexivity.
    rewrite ProjectRedexTriangle1 with (R' := R2); auto; rewrite eq2; reflexivity.
  Qed.

  (* VALUE SOUNDNESS *)
  (*
    If every location mentioned in a Pirouette program project to a value, then the
    Pirouette program must itself be a value.
   *)
  Lemma ValueSoundness : forall C ls,
      ls <> [] ->
      (forall l, In l (C.LocsInPirExpr C) -> In l ls) ->
      (forall l, In l ls -> exists E, EPP C l = Some E /\ CtrlExprVal E) ->
      C.PirExprVal C.
  Proof using.
    intros C; induction C; cbn; intros ls nempty tn eq.
    all: try (destruct (eq ℓ (tn ℓ ltac:(left; reflexivity))) as [E [eq' val]];
              EPPDestructor; inversion val; subst; constructor; auto; fail).
    all: try (destruct (eq ℓ (tn ℓ ltac:(left; reflexivity))) as [E [eq' val]];
              EPPDestructor; try discriminate; inversion val; fail).
    all: try (destruct (eq ℓ1 (tn ℓ1 ltac:(left; reflexivity))) as [E [eq' val]];
              EPPDestructor; try discriminate; inversion val; fail).
    - destruct ls; [destruct (nempty eq_refl)|].
      destruct (eq l ltac:(left; reflexivity)) as [E [eq' val]]; EPPDestructor.
      inversion val.
    - destruct ls; [destruct (nempty eq_refl)|].
      constructor; destruct (eq l ltac:(left; reflexivity)) as [E [eq' val]]; EPPDestructor;
        try discriminate. 
    - destruct ls; [destruct (nempty eq_refl)|].
      destruct (eq l ltac:(left; reflexivity)) as [E [eq' val]]; EPPDestructor;
        try discriminate.
      inversion val.
  Qed.
  
  (* SYNCRHONIZATION IOTA SOUNDNESS *)
  
  Lemma LocalSyncIotaSoundness' : forall C1 L ls,
      ls <> [] ->
      IsSyncLabel L ->
      (forall l, In l (C.LocsInPirExpr C1) -> In l ls) ->
      (forall l, In l ls ->
                 exists R' E1 E2, EPP C1 l = Some E1 /\
                                  CtrlExprRedexToLabel R' = L /\
                                  CtrlExprStep' E1 R' E2) ->
      exists C2 R, (forall l, ProjectRedex R l = Some L) /\ C.PirStep R [] C1 C2.
  Proof using.
    intros C; induction C; intros L ls nempty syncL in_tn steps.
    all: try (cbn in in_tn; EPPDestructor; try discriminate;
              repeat match goal with
                     | [H : forall l', ?l = l' \/ _ -> In l' ?ls |- _ ] =>
                       specialize (H l ltac:(left; reflexivity))
                     | [H : forall l, In l ?ls -> exists R' E1 E2,
                             EPP ?C l = Some E1 /\
                             CtrlExprRedexToLabel R' = ?L /\
                             CtrlExprStep' E1 R' E2,
                          H' : In ?l ?ls |- _ ] =>
                       let R' := fresh "R" in
                       let E1 := fresh "E" in
                       let E2 := fresh "E" in
                       let eq1 := fresh "eq" in
                       let eq2 := fresh "eq" in
                       let step := fresh "step" in
                       specialize (H l H');
                       destruct H as [R [E1 [E2 [eq1 [eq2 step]]]]];
                       cbn in *; EPPDestructor; try discriminate;
                       inversion step; subst
                     | [ H : IsSyncLabel _ |- _ ] => inversion H; fail
                     end; fail).
    1,3: destruct ls; try (exfalso; apply nempty; reflexivity);
      destruct (steps l ltac:(left; reflexivity)) as [R' [E1 [E2 [eq1 [eq2 step]]]]];
      cbn in eq1; EPPDestructor; try discriminate; inversion step.
    all: cbn in *; EPPDestructor; try discriminate.
    - pose proof (in_tn ℓ ltac:(left; reflexivity)).
      destruct (steps ℓ H) as [R' [E1 [E2 [eq1 [eq2 step]]]]];
        EPPDestructor; try discriminate.
      destruct R'; cbn in syncL; inversion syncL; subst; inversion step; subst.
      -- exists (C.PirExprLocalSubst C2 (C.ValueSubst ℓ e)); exists (C.RDefLocal ℓ e); cbn;
           split; auto with PirExpr.
         destruct C1; cbn in eq; EPPDestructor; try discriminate.
         all: try (cbn in in_tn; specialize (in_tn ℓ ltac:(right; left; reflexivity));
                   specialize (steps ℓ in_tn); EPPDestructor; try discriminate;
                   destruct steps as [R' [E1' [E2' [eq1' [eq2' step']]]]];
                   inversion eq1'; inversion eq2'; clear eq1' eq2'; subst;
                   cbn in eq1; EPPDestructor; try discriminate;
                   inversion step'; subst; cbn in H2; EPPDestructor; try discriminate;
                   inversion H6; fail).
         all: try (cbn in in_tn; specialize (in_tn ℓ1 ltac:(right; left; reflexivity));
                   specialize (steps ℓ1 in_tn); EPPDestructor; try discriminate;
                   destruct steps as [R' [E1' [E2' [eq1' [eq2' step']]]]];
                   inversion eq1'; inversion eq2'; clear eq1' eq2'; subst;
                   cbn in eq1; EPPDestructor; try discriminate;
                   inversion step'; subst; cbn in H2; EPPDestructor; try discriminate;
                   inversion H6; fail).
         2: {cbn in in_tn; specialize (in_tn ℓ0 ltac:(right; left; reflexivity));
             specialize (steps ℓ0 in_tn); EPPDestructor; try discriminate;
             destruct steps as [R' [E1' [E2' [eq1' [eq2' step']]]]];
             inversion eq1'; inversion eq2'; clear eq1' eq2'; subst;
             inversion step'; subst; cbn in H2; EPPDestructor; try discriminate.
             cbn in eq3; EPPDestructor; try discriminate. inversion H6. }
         auto with PirExpr.
      -- destruct (IHC1 (CtrlExprRedexToLabel R') ls nempty H1) as [C2' [R [projR step']]].
         --- intros l' i; apply in_tn; right; apply in_or_app; left; auto.
         --- intros l0 i; destruct (steps l0 i) as [R'' [E1 [E2 [eq1' [eq2' step']]]]];
               cbn in *; EPPDestructor; try discriminate.
             exists R'; exists c; exists E1'; split; [|split]; auto.
             inversion step'; subst.
             inversion H6. cbn in eq2'; inversion eq2'.
             exists R; exists c1; exists A2; split; [|split]; auto.
             cbn in eq2'; inversion eq2'.
         --- exists (C.DefLocal ℓ C2' C2); exists (C.RArg R); split; auto with PirExpr.
             intro l'; cbn; specialize (projR l'); EPPDestructor; try discriminate;
               reflexivity.
    - pose proof (in_tn ℓ ltac:(left; reflexivity)) as in_l.
      destruct (steps ℓ in_l) as [R' [E1 [E2 [eq1 [eq2 step]]]]];
        EPPDestructor; try discriminate.
      inversion step; subst; [|cbn in syncL; inversion syncL|].
      -- cbn in syncL; inversion syncL; subst.
         destruct (IHC (CtrlExprRedexToLabel R) ls nempty H0)
           as [C2 [R' [eq' step']]].
         --- intros l' i; apply in_tn; right; auto.
         --- intros l' i; specialize (steps l' i); EPPDestructor; try discriminate.
             all:destruct steps as [R'' [E1' [E2' [eq1' [eq2' step']]]]];
               inversion eq1'; clear eq1'; subst;
                 destruct R''; cbn in eq2'; try discriminate; inversion eq2'; subst.
             inversion step'; subst.
             exists R''; exists c; exists F0; split; [|split]; auto.
             inversion step'; subst.
             exists R''; exists c0; exists F0; split; [|split]; auto.
         --- exists (C.AppLocal ℓ C2 e); exists (C.RFun R'); split; auto with PirExpr.
             intro l'; specialize (eq' l'); cbn; EPPDestructor; auto.
      -- destruct C; cbn in eq; EPPDestructor; try discriminate.
         all: try (cbn in in_tn; specialize (in_tn ℓ1 ltac:(right; left; reflexivity));
                   specialize (steps ℓ1 in_tn); EPPDestructor; try discriminate;
                   destruct steps as [R' [E1' [E2' [eq1' [eq2' step']]]]];
                   inversion eq1'; inversion eq2'; clear eq1' eq2'; subst;
                   cbn in eq0; EPPDestructor; try discriminate;
                   inversion step'; subst; cbn in H5; EPPDestructor;
                   try discriminate; fail).
         cbn in in_tn; specialize (in_tn ℓ0 ltac:(right; left; reflexivity)).
         specialize (steps ℓ0 in_tn); EPPDestructor; try discriminate;
           destruct steps as [R' [E1 [E2 [eq3 [eq4 step']]]]];
           cbn in eq2; EPPDestructor; try discriminate;
             cbn in eq4; inversion step'; subst; cbn in eq4; try discriminate.
         exists (C.PirExprSubst (C.PirExprLocalSubst C (C.ValueSubst ℓ0 e))
                           (C.AppLocalSubst ℓ0 C));
           exists (C.RAppLocal ℓ0 e);
           split; auto with PirExpr.
    - destruct syncL.
      -- destruct C1.
         all: try (cbn in in_tn; pose proof (in_tn ℓ ltac:(left;reflexivity)) as in_l;
                   specialize (steps ℓ in_l); EPPDestructor; try discriminate;
                   cbn in eq; EPPDestructor; try discriminate;
                   destruct steps as [R' [E1 [E2 [eq1' [eq2' step]]]]];
                   EPPDestructor;
                   inversion step; subst; cbn in eq2'; try discriminate).
         all: try (cbn in in_tn; pose proof (in_tn ℓ1 ltac:(left;reflexivity)) as in_l;
                   specialize (steps ℓ1 in_l); EPPDestructor; try discriminate;
                   cbn in eq; EPPDestructor; try discriminate;
                   destruct steps as [R' [E1 [E2 [eq1' [eq2' step]]]]];
                   EPPDestructor;
                   inversion step; subst; cbn in eq2'; try discriminate).
         cbn in steps.
         assert (exists l, In l ls)
           by (destruct ls as [|l ls]; [destruct (nempty eq_refl)| exists l; left; reflexivity]).
         destruct H as [l i].
         specialize (steps l i); destruct steps as [R' [E1 [E2 [eq1' [eq2' step]]]]].
         EPPDestructor; try discriminate.
    inversion step; subst; cbn in eq2'; try discriminate.
    2: { cbn in steps.
         assert (exists l, In l ls)
           by (destruct ls as [|l ls];
               [destruct (nempty eq_refl)| exists l; left; reflexivity]).
         destruct H as [l i].
         specialize (steps l i); destruct steps as [R' [E1 [E2 [eq1' [eq2' step]]]]].
         EPPDestructor; try discriminate.
         inversion step; subst; cbn in eq2'; try discriminate. }
    assert (C.PirExprVal C2).
    apply (ValueSoundness C2 ls nempty).
    intros l i; apply in_tn; apply in_or_app; auto.
    intros l i; destruct (steps l i) as [R' [E1 [E2 [eq1 [eq2 step]]]]];
      cbn in eq1; EPPDestructor; try discriminate.
    destruct R'; cbn in eq2; try discriminate; inversion step; subst.
    exists c0; split; auto.
    exists (C.PirExprSubst C1 (C.AppGlobalSubst C1 C2)); exists (C.RAppGlobal);
      split; auto with PirExpr.
    -- destruct (IHC1 L ls nempty syncL) as [C3 [R [eqs step]]].
       intros l i; apply in_tn; apply in_or_app; left; auto.
       intros l i; destruct (steps l i) as [R' [E1 [E2 [eq1 [eq2 step]]]]];
         EPPDestructor; try discriminate.
       destruct R'; cbn in eq2; try discriminate; inversion step; subst.
       inversion eq2; clear eq2; subst.
       exists R'; exists c; exists F2; split; [|split]; auto.
       exists (C.AppGlobal C3 C2); exists (C.RFun R); split; auto with PirExpr.
       intro l; cbn; rewrite (eqs l); auto.
    -- destruct (IHC2 L ls nempty syncL) as [C3 [R [eqs step]]].
       intros l i; apply in_tn; apply in_or_app; right; auto.
       intros l i; destruct (steps l i) as [R' [E1 [E2 [eq1 [eq2 step]]]]];
         EPPDestructor; try discriminate.
       destruct R'; cbn in eq2; try discriminate; inversion step; subst.
       inversion eq2; clear eq2; subst.
       exists R'; exists c0; exists A2; split; [|split]; auto.
       exists (C.AppGlobal C1 C3); exists (C.RArg R); split; auto with PirExpr.
       intro l; cbn; rewrite (eqs l); auto.
  Qed.
  
  Corollary LocalSyncIotaSoundness : forall C1 L ls,
      ls <> [] ->
      IsSyncLabel L ->
      (forall l, In l (C.LocsInPirExpr C1) -> In l ls) ->
      (forall l, In l ls ->
                 exists E1 E2, EPP C1 l = Some E1 /\
                               CtrlExprStep E1 L E2) ->
      exists C2 R, (forall l, ProjectRedex R l = Some L) /\ C.PirStep R [] C1 C2.
  Proof using.
    intros C1 L ls H H0 H1 H2.
    eapply LocalSyncIotaSoundness'; eauto.
    intros l i; destruct (H2 l i) as [E1 [E2 [eq1 step]]].
    destruct (CtrlExprStepToPrime step) as [R [eq2 step']].
    exists R; exists E1; exists E2; split; [|split]; auto.
  Qed.

  (* SYNCHRONIZATION SOUNDNESS *)

  (*
    As with communication, we have to denote when synchronization redices match a Pirouette
    redex.
   *)
  Inductive MatchedSyncRedices : Loc -> SyncLabel -> Loc -> CtrlExprRedex -> CtrlExprRedex ->
                                 C.Redex -> Prop :=
    BaseSyncRedicesL : forall {l1 l2},
      l1 <> l2 ->
      MatchedSyncRedices l1 Left l2 (ChooseRedex l2 Left) (AllowChoiceLRedex l1)
                         (C.RSync l1 Left l2)
  | BaseSyncRedicesR : forall {l1 l2},
      l1 <> l2 ->
      MatchedSyncRedices l1 Right l2 (ChooseRedex l2 Right) (AllowChoiceRRedex l1)
                         (C.RSync l1 Right l2)
  | ArgSyncRedices : forall l1 d l2 R1 R2 R,
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      MatchedSyncRedices l1 d l2 (ArgRedex R1) (ArgRedex R2) (C.RArg R)
  | FunSyncRedices : forall l1 d l2 R1 R2 R,
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      MatchedSyncRedices l1 d l2 (FunRedex R1) (FunRedex R2) (C.RFun R).

  Lemma MatchedSyncRedicesUnique : forall l1 d l2 l1' d' l2' R1 R2 R R',
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      MatchedSyncRedices l1' d' l2' R1 R2 R' ->
      l1 = l1' /\ l2 = l2' /\ d = d' /\ R = R'.
  Proof using.
    intros l1 d l2 l1' d' l2' R1 R2 R R' matched1;
      revert l1' d' l2' R'; induction matched1; intros l1' d' l2' R' matched2;
        inversion matched2; subst; auto. 
    all: apply IHmatched1 in H4;
      repeat match goal with | [H : _ /\ _ |- _ ] => destruct H | [ |- _ /\ _ ] => split end;
      subst; auto.
  Qed.

  Lemma MatchedSyncRedicesUnique1 : forall l1 d l2 d' R1 R2 R2' R R',
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      MatchedSyncRedices l1 d' l2 R1 R2' R' ->
      d = d' /\ R = R' /\ R2 = R2'.
  Proof using.
    intros l1 d l2 d' R1 R2 R2' R R' matched1;
      revert d' R2' R'; induction matched1; intros d' R2' R' matched2;
        inversion matched2; subst; auto. 
    all: apply IHmatched1 in H3;
      repeat match goal with | [H : _ /\ _ |- _ ] => destruct H | [ |- _ /\ _ ] => split end;
      subst; auto.
  Qed.

  Lemma MatchedSyncRedicesUnique2 : forall l1 d l2 d' R1 R1' R2 R R',
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      MatchedSyncRedices l1 d' l2 R1' R2 R' ->
      d = d' /\ R = R' /\ R1 = R1'.
  Proof using.
    intros l1 d l2 d' R1 R1' R2 R R' matched1;
      revert d' R1' R'; induction matched1; intros d' R1' R' matched2;
        inversion matched2; subst; auto. 
    all: apply IHmatched1 in H4;
      repeat match goal with | [H : _ /\ _ |- _ ] => destruct H | [ |- _ /\ _ ] => split end;
      subst; auto.
  Qed.
  
  Lemma MatchedSyncRedicesProject : forall l1 d l2 R1 R2 R,
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      exists L1 L2, ChoiceLabelPair l1 d l2 L1 L2 /\
                    ProjectRedex R l1 = Some L1 /\
                    ProjectRedex R l2 = Some L2.
  Proof using.
    intros l1 d l2 R1 R2 R matched; induction matched.
    - eexists; eexists; cbn; EPPDestructor; split; [|split]; eauto.
      constructor.
    - eexists; eexists; cbn; EPPDestructor; split; [|split]; eauto.
      constructor.
    - destruct IHmatched as [L1 [L2 [pr [eq1 eq2]]]];
        exists (ArgLabel L1); exists (ArgLabel L2); cbn; EPPDestructor.
      all: split; [|split]; auto; constructor; auto.
    - destruct IHmatched as [L1 [L2 [pr [eq1 eq2]]]];
        exists (FunLabel L1); exists (FunLabel L2); cbn; EPPDestructor.
      all: split; [|split]; auto; constructor; auto.
  Qed.

  Lemma MatchedSyncRedicesToLabels : forall l1 d l2 R1 R2 R,
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      ChoiceLabelPair l1 d l2 (CtrlExprRedexToLabel R1) (CtrlExprRedexToLabel R2).
  Proof using.
    intros l1 d l2 R1 R2 R matched; induction matched; cbn; constructor; auto.
  Qed.

  Lemma MatchedSyncRedicesDistinguish : forall l1 d l2 R1 R2 R,
      MatchedSyncRedices l1 d l2 R1 R2 R -> l1 <> l2.
  Proof using.
    intros l1 d l2 R1 R2 R matched; induction matched; auto.
  Qed.

  Lemma MatchedSyncRedicesDistinguishRedices : forall {l1 d l2 R1 R2 R},
      MatchedSyncRedices l1 d l2 R1 R2 R -> R1 <> R2.
  Proof using.
    intros l1 d l2 R1 R2 R matched; induction matched; try discriminate.
    all: intro eq; inversion eq; subst; destruct (IHmatched eq_refl).
  Qed.
  
  Lemma MatchedSyncRedicesNotAllowChoice : forall l1 d l2 R1 R2 R,
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R1).
  Proof using.
    intros l1 d l2 R1 R2 R matched; induction matched; cbn; intro H'; inversion H'; subst.
    all: apply IHmatched in H0; auto.
  Qed.
  
  Lemma MatchedSyncRedicesDCL : forall l1 d l2 R1 R2 R,
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R2).
  Proof using.
    intros l1 d l2 R1 R2 R msr; induction msr; cbn; try (constructor; auto; fail).
  Qed.

  Lemma ProjectMatchedSyncRedices : forall {l1 d l2 R1 R2 R},
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      ProjectRedexToRedex R l1 = Some R1 /\ ProjectRedexToRedex R l2 = Some R2.
  Proof using.
    intros l1 d l2 R1 R2 R mtchd; induction mtchd; cbn; EPPDestructor; auto.
  Qed.

  (*
    Unlike communication, moving along less-nondeterminism can break the ability of
    redices to apply. Thus, it is easier to maintain the invariant that they are
    _almost_ matched, where we do not ensure that the directions match up.
    
    Later, we will prove that anything that comes out of a projection will be matched.
    This allows us to restore the property of being properly matched from something only
    almost-matched.
   *)
  Inductive AlmostMatchedSyncRedices : Loc -> Loc -> CtrlExprRedex -> CtrlExprRedex -> Prop :=
    ABaseSyncRedicesL : forall {l1 l2} d,
      l1 <> l2 ->
      AlmostMatchedSyncRedices l1 l2 (ChooseRedex l2 d) (AllowChoiceLRedex l1)
  | ABaseSyncRedicesR : forall {l1 l2 d},
      l1 <> l2 ->
      AlmostMatchedSyncRedices l1 l2 (ChooseRedex l2 d) (AllowChoiceRRedex l1)
  | AArgSyncRedices : forall l1 l2 R1 R2,
      AlmostMatchedSyncRedices l1 l2 R1 R2 ->
      AlmostMatchedSyncRedices l1 l2 (ArgRedex R1) (ArgRedex R2)
  | AFunSyncRedices : forall l1 l2 R1 R2,
      AlmostMatchedSyncRedices l1 l2 R1 R2 ->
      AlmostMatchedSyncRedices l1 l2 (FunRedex R1) (FunRedex R2).
  Global Hint Constructors AlmostMatchedSyncRedices : CtrlExpr.
  
  Lemma AlmostMatchedSyncRedicesNotAllowChoice : forall l1 l2 R1 R2,
      AlmostMatchedSyncRedices l1 l2 R1 R2 ->
      ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R1).
  Proof using.
    intros l1 l2 R1 R2 matched; induction matched; cbn; intro H'; inversion H'; subst.
    all: apply IHmatched in H0; auto.
  Qed.

  Lemma AlmostMatchedSyncRedicesDCL : forall l1 l2 R1 R2,
      AlmostMatchedSyncRedices l1 l2 R1 R2 ->
      exists d, AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R2).
  Proof using.
    intros l1 l2 R1 R2 amsr; induction amsr; try (eexists; econstructor; eauto; fail).
    all: destruct IHamsr; eexists; cbn; econstructor; eauto.
  Qed.

  Lemma AlmostMatchedSyncRedicesDistinguish : forall l1 l2 R1 R2,
      AlmostMatchedSyncRedices l1 l2 R1 R2 -> l1 <> l2.
  Proof using.
    intros l1 l2 R1 R2 amsr; induction amsr; auto.
  Qed.
  
  (*
    If almost-matched labels come from projection then they are, in fact, properly matched.
   *)
  Lemma AllowChoiceProject : forall C l1 l2 E11 R1 E12 E21 R2 E22,
      EPP C l1 = Some E11 ->
      EPP C l2 = Some E21 ->
      CtrlExprStep' E11 R1 E12 ->
      CtrlExprStep' E21 R2 E22 ->
      AlmostMatchedSyncRedices l1 l2 R1 R2 ->
      exists d R, MatchedSyncRedices l1 d l2 R1 R2 R.
  Proof using.
    intro C; induction C; intros p q E11 R1 E12 E21 R2 E22; cbn; EPPDestructor;
      intros proj_l1 proj_l2 step1 step2 am;
      inversion am; subst; inversion step1; inversion step2; subst; try discriminate.
    all:
      repeat match goal with
             | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
             | [H : CtrlExprStep' (FunGlobal _) _ _ |- _ ] => inversion H
             | [H : CtrlExprStep' Unit _ _ |- _ ] => inversion H
             | [ H : Some _ = Some _ |- _ ] => inversion H; clear H; subst
             | [ H : context[Choose ?q ?d ?E] |- _ ] =>
               lazymatch goal with
               | [ _ : CtrlExprStep' (Choose q d E) (ChooseRedex q d) E |- _ ] => fail
               | _ => assert (CtrlExprStep' (Choose q d E) (ChooseRedex q d) E)
                   by (constructor; auto)
               end
             | [ H : context[AllowChoiceL ?l ?E] |- _ ]=>
               lazymatch goal with
               | [ _ : CtrlExprStep' (AllowChoiceL l E) (AllowChoiceLRedex l) E |- _ ] => fail
               | _ => assert (CtrlExprStep' (AllowChoiceL l E) (AllowChoiceLRedex l) E)
                   by (constructor; auto)
               end
             | [ H : context[AllowChoiceR ?l ?E] |- _ ]=>
               lazymatch goal with
               | [ _ : CtrlExprStep' (AllowChoiceR l E) (AllowChoiceRRedex l) E |- _ ] => fail
               | _ => assert (CtrlExprStep' (AllowChoiceR l E) (AllowChoiceRRedex l) E)
                   by (constructor; auto)
               end
             | [ H : context[AllowChoiceLR ?l ?E1 ?E2] |- _ ]=>
               lazymatch goal with
               | [ _ : CtrlExprStep' (AllowChoiceLR l E1 E2) (AllowChoiceRRedex l) E2
                   |- _ ] => fail
               | _ => assert (CtrlExprStep' (AllowChoiceLR l E1 E2) (AllowChoiceRRedex l) E2)
                   by (constructor; auto);
                       assert (CtrlExprStep' (AllowChoiceLR l E1 E2) (AllowChoiceLRedex l)
                                             E1)
                       by (constructor; auto)
               end
             | [IH : forall l1 l2 E11 R1 E12 E21 R2 E22,
                   EPP ?C l1 = Some E11 ->
                   EPP ?C l2 = Some E21 ->
                   CtrlExprStep' E11 R1 E12 ->
                   CtrlExprStep' E21 R2 E22 ->
                   AlmostMatchedSyncRedices l1 l2 R1 R2 ->
                   exists d R, MatchedSyncRedices l1 d l2 R1 R2 R,
                  H1 : EPP ?C ?l1 = Some ?E11,
                  H2 : EPP ?C ?l2 = Some ?E21,
                  H3 : CtrlExprStep' ?E11 ?R1 ?E12,
                  H4 : CtrlExprStep' ?E21 ?R2 ?E22,
                  H5 : AlmostMatchedSyncRedices ?l1 ?l2 ?R1 ?R2 |- _ ] =>
               lazymatch goal with
               | [_ : MatchedSyncRedices l1 _ l2 R1 R2 _ |- _] => fail
               | _ => let d := fresh "d" in
                      let R := fresh "R" in
                      let amsr := fresh "amsr" in
                      destruct (IH l1 l2 E11 R1 E12 E21 R2 E22 H1 H2 H3 H4 H5)
                        as [d [R amsr]]
               end
             | [ H : CtrlExprMerge ?a ?b = Some ?c |- _ ] =>
               apply CtrlExprMergeRelSpec2 in H; inversion H; subst
             | [H : AlmostMatchedSyncRedices ?p ?q ?R1 ?R2 |- _ ] =>
               lazymatch goal with
               | [_ : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel R1) |- _ ] => fail
               | _ => pose proof (AlmostMatchedSyncRedicesNotAllowChoice p q R1 R2 H)
               end
             | [H : AlmostMatchedSyncRedices ?p ?q ?R1 ?R2 |- _ ] =>
               lazymatch goal with
               | [_ : AllowDirectedChoiceLabel _ (CtrlExprRedexToLabel R2) |- _ ] =>
                 fail
               | _ => let d := fresh "d" in
                      let adcl := fresh "adcl" in
                      destruct (AlmostMatchedSyncRedicesDCL p q R1 R2 H) as [d adcl]
               end
             | [H : AlmostMatchedSyncRedices ?p ?q ?R1 ?R2 |- _ ] =>
               lazymatch goal with
               | [_ : p <> q |- _ ] =>  fail
               | _ => pose proof (AlmostMatchedSyncRedicesDistinguish p q R1 R2 H)
               end
             | [ H1 : CtrlExprStep' ?E1 ?R ?E2,
                      H2 : ~ IsAllowChoiceLabel (CtrlExprRedexToLabel ?R),
                           H3 : CtrlExprMergeRel ?E11 ?E12 ?E1 |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprMergeRel _ _ E2 |- _] => fail
               | _ => let E21 := fresh "E2" in
                      let E22 := fresh "E2" in
                      let merge := fresh "merge" in
                      let step1 := fresh "step" in
                      let step2 := fresh "step" in
                      destruct (UnmergeRelStep' E11 E12 E1 R E2 H1 H2 H3)
                        as [E21 [E22 [merge [step1 step2]]]]
               end
             | [ H :AllowDirectedChoiceLabel ?d (CtrlExprRedexToLabel ?R),
                    H' : CtrlExprStep' ?E1 ?R ?E2,
                         H'' : CtrlExprMergeRel ?E11 ?E12 ?E1 |- _ ] =>
               lazymatch goal with
               | [_ : SameChoiceRedexShape _ ?R |- _ ] => fail
               | _ => let d1 := fresh "d" in
                      let d2 := fresh "d" in
                      let R1 := fresh "R" in
                      let R2 := fresh "R" in
                      let E21 := fresh "E2" in
                      let E22 := fresh "E2" in
                      let step1 := fresh "step" in
                      let step2 := fresh "step" in
                      let scrs1 := fresh "scrs" in
                      let scrs2 := fresh "scrs" in
                      let adcl1 := fresh "adcl" in
                      let adcl2 := fresh "adcl" in
                      let eq1 := fresh "eq" in
                      let eq2 := fresh "eq" in
                      let neq := fresh "neq" in
                      let merge := fresh "merge" in
                      destruct (UnmergeRelACStep' E11 E12 E1 d R E2 H H'' H')
                        as [d1 [R1 [E21 [d2 [R2 [E22 [step1 [step2 [scrs1 [scrs2 [adcl1 [adcl2 [[eq1 [eq2 merge]] | [[eq1 neq] | [eq1 neq]]]]]]]]]]]]]]]; subst
               end
             | [ H : context[LetRet ?E1 ?E2], H' : CtrlExprStep' ?E1 ?R ?E1' |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprStep' (LetRet E1 E2) (ArgRedex R) (LetRet E1' E2) |- _ ] => fail
               | _ => assert (CtrlExprStep' (LetRet E1 E2) (ArgRedex R) (LetRet E1' E2)) by (constructor; auto)
               end
             | [ H : context[AppGlobal ?E1 ?E2], H' : CtrlExprStep' ?E2 ?R ?E2' |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprStep' (AppGlobal E1 E2) (ArgRedex R) (AppGlobal E1 E2') |- _ ] => fail
               | _ => assert (CtrlExprStep' (AppGlobal E1 E2) (ArgRedex R) (AppGlobal E1 E2')) by (constructor; auto)
               end
             | [ H : context[AppGlobal ?E1 ?E2], H' : CtrlExprStep' ?E1 ?R ?E1' |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprStep' (AppGlobal E1 E2) (FunRedex R) (AppGlobal E1' E2) |- _ ] => fail
               | _ => assert (CtrlExprStep' (AppGlobal E1 E2) (FunRedex R) (AppGlobal E1' E2)) by (constructor; auto)
               end
             | [ H : context[AppLocal ?E ?e], H' : CtrlExprStep' ?E ?R ?E' |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprStep' (AppLocal E e) (FunRedex R) (AppLocal E' e) |- _ ] => fail
               | _ => assert (CtrlExprStep' (AppLocal E e) (FunRedex R) (AppLocal E' e)) by (constructor; auto)
               end
             | [ H : SameChoiceRedexShape ?R1 ?R2,
                     H' : AllowDirectedChoiceLabel ?d (CtrlExprRedexToLabel ?R1),
                          H'' : AllowDirectedChoiceLabel ?d (CtrlExprRedexToLabel ?R2) |- _ ] =>
               tryif unify R1 R2
               then fail
               else
                 pose proof (SameShapeSameDirection d R1 R2 H H' H'');
                 subst
             | [ H : AllowDirectedChoiceLabel ?d1 ?L, H' : AllowDirectedChoiceLabel ?d2 ?L |- _ ] =>
               tryif unify d1 d2
               then fail
               else pose proof (DirectionUnique d1 d2 L H H'); subst
             | [H : AllowDirectedChoiceLabel ?d (CtrlExprRedexToLabel (ArgRedex ?R)) |- _ ] =>
               lazymatch goal with
               | [_ : AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R) |- _ ] => fail
               | _ => assert (AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R))
                   by (cbn in H; inversion H; subst; auto)
               end
             | [H : AllowDirectedChoiceLabel ?d (CtrlExprRedexToLabel (FunRedex ?R)) |- _ ] =>
               lazymatch goal with
               | [_ : AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R) |- _ ] => fail
               | _ => assert (AllowDirectedChoiceLabel d (CtrlExprRedexToLabel R))
                   by (cbn in H; inversion H; subst; auto)
               end
             end; try (eexists; eexists; econstructor; eauto; fail);
      try (eexists; eexists; eauto; fail).
  Qed.

  Lemma SameShapeAlmostMatched : forall R1' R2' R1 R2 R l1 d l2,
      SameChoiceRedexShape R1' R2 ->
      SameChoiceRedexShape R2' R2 ->
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      AlmostMatchedSyncRedices l1 l2 R1 R1' /\ AlmostMatchedSyncRedices l1 l2 R1 R2'.
  Proof using.
    intros R1' R2' R1 R2 R l1 d l2 shp1 shp2 mtchd; revert R1' R2' shp1 shp2;
      induction mtchd; intros R1' R2' shp1 shp2; inversion shp1; subst;
        inversion shp2; subst; split; try (constructor; auto).
    all: eapply IHmtchd; eauto.
  Qed.


  Lemma LocalSyncSoundness' : forall C1 l1 d l2 E11 E12 E21 E22 R1 R2 R,
      EPP C1 l1 = Some E11 ->
      EPP C1 l2 = Some E21 ->
      CtrlExprStep' E11 R1 E12 ->
      CtrlExprStep' E21 R2 E22 ->
      MatchedSyncRedices l1 d l2 R1 R2 R ->
      exists C2, EPP C2 l1 = Some E12
                 /\ EPP C2 l2 = Some E22
                 /\ ProjectRedexToRedex R l1 = Some R1
                 /\ ProjectRedexToRedex R l2 = Some R2
                 /\ (forall B, ~ In l1 B -> ~ In l2 B -> C.PirStep R B C1 C2).
  Proof using.
    intro C1; induction C1; intros p d' q E11 E12 E21 E22 R1 R2 R;
      cbn; intros projp1 projp2 step1 step2 mtchd;
        destruct (ProjectMatchedSyncRedices mtchd) as [projR1 projR2];
        EPPDestructor; try discriminate.
    all: try (inversion step1; subst; inversion step2; subst; inversion mtchd; fail).
    all:
      repeat match goal with
             | [ H : Some _ = Some _ |- _ ] =>
               inversion H; clear H; subst
             | [ H : MatchedSyncRedices _ _ _ (AppLocalRedex _) _ _ |- _ ] =>
               inversion H
             | [ H : MatchedSyncRedices _ _ _ (AppLocalE _ _) _ _ |- _ ] =>
               inversion H
             | [ H : MatchedSyncRedices _ _ _ AppGlobalRedex _ _ |- _ ] =>
               inversion H
             | [ H : MatchedSyncRedices _ _ _ _ (AppLocalRedex _) _ |- _ ] =>
               inversion H
             | [ H : MatchedSyncRedices _ _ _ _ (AppLocalE _ _) _ |- _ ] =>
               inversion H
             | [ H : MatchedSyncRedices _ _ _ _ AppGlobalRedex _ |- _ ] =>
               inversion H
             | [ H : MatchedSyncRedices ?a ?b ?c (ArgRedex ?R) _ _ |- _ ] =>
               lazymatch goal with
               | [_ : MatchedSyncRedices a b c R _ _ |- _ ] => fail
               | _ => inversion H; subst
               end
             | [ H : MatchedSyncRedices ?a ?b ?c _ (ArgRedex ?R) _ |- _ ] =>
               lazymatch goal with
               | [_ : MatchedSyncRedices a b c _ R _ |- _ ] => fail
               | _ => inversion H; subst
               end
             | [ H : MatchedSyncRedices ?a ?b ?c (FunRedex ?R) _ _ |- _ ] =>
               lazymatch goal with
               | [_ : MatchedSyncRedices a b c R _ _ |- _ ] => fail
               | _ => inversion H; subst
               end
             | [ H : MatchedSyncRedices ?a ?b ?c _ (FunRedex ?R) _ |- _ ] =>
               lazymatch goal with
               | [_ : MatchedSyncRedices a b c _ R _ |- _ ] => fail
               | _ => inversion H; subst
               end
             | [ H : CtrlExprStep' (LetRet ?A _) (ArgRedex ?R) _ |- _ ] =>
               lazymatch goal with
               | [_ : CtrlExprStep' A _ _ |- _ ] => fail
               | _ => inversion H; subst
               end
             | [ H : CtrlExprStep' (AppLocal ?F ?A) _ _ |- _] =>
               lazymatch goal with
               | [ _ : CtrlExprStep' F _ _ |- _ ] => fail
               | _ => inversion H; subst
               end
             | [ H : CtrlExprStep' (AppGlobal ?F ?A) _ _ |- _ ] =>
               lazymatch goal with
               | [ _ : CtrlExprStep' F _ _ |- _ ] => fail
               | [ _ : CtrlExprStep' A _ _ |- _ ] => fail
               | _ => inversion H; subst
               end
             | [ H : CtrlExprStep' (FunGlobal _) _ _ |- _ ] => inversion H
             | [ H : CtrlExprStep' Unit _ _ |- _ ] => inversion H
             | [IH : forall l1 d l2 E11 E12 E21 E22 R1 R2 R,
                   EPP ?C1 l1 = Some E11 ->
                   EPP ?C1 l2 = Some E21 ->
                   CtrlExprStep' E11 R1 E12 ->
                   CtrlExprStep' E21 R2 E22 ->
                   MatchedSyncRedices l1 d l2 R1 R2 R ->
                   exists C2,
                     EPP C2 l1 = Some E12 /\
                     EPP C2 l2 = Some E22 /\
                     ProjectRedexToRedex R l1 = Some R1 /\
                     ProjectRedexToRedex R l2 = Some R2 /\
                     (forall B, ~ In l1 B -> ~ In l2 B -> C.PirStep R B ?C1 C2),
                  H1 : EPP ?C1 ?l1 = Some ?E11,
                  H2 : EPP ?C1 ?l2 = Some ?E21,
                  H3 : CtrlExprStep' ?E11 ?R1 ?E12,
                  H4 : CtrlExprStep' ?E21 ?R2 ?E22,
                  H5 : MatchedSyncRedices ?l1 ?d ?l2 ?R1 ?R2 ?R |- _ ] =>
               lazymatch goal with
               | [_ : EPP _ l1 = Some E12 |- _ ] => fail
               | _ => let C2 := fresh "C" in
                      let proj1 := fresh "proj" l1 in
                      let proj2 := fresh "proj" l2 in
                      let projR1 := fresh "projR" l1 in
                      let projR2  := fresh "projR" l2 in
                      let step := fresh "step" in
                      destruct (IH l1 d l2 E11 E12 E21 E22 R1 R2 R H1 H2 H3 H4 H5)
                        as [C2 [proj1 [proj2 [projR1 [projR2 step]]]]]
               end
             | [H1 : CtrlExprRedexToLabel ?R = ?L, H2 : context[ArgRedex ?R] |- _ ] =>
               lazymatch goal with
               | [ _ : CtrlExprRedexToLabel (ArgRedex R) = L |- _ ] => fail
               | _ => assert (CtrlExprRedexToLabel (ArgRedex R) = L) by
                     (cbn; apply H1)
               end
             | [H1 : CtrlExprRedexToLabel ?R = ?L, H2 : context[FunRedex ?R] |- _ ] =>
               lazymatch goal with
               | [ _ : CtrlExprRedexToLabel (FunRedex R) = L |- _ ] => fail
               | _ => assert (CtrlExprRedexToLabel (FunRedex R) = L) by
                     (cbn; apply H1)
               end
             | [H1 : ?a = ?b, H2 : ?a = ?c |- _ ] =>
               tryif unify b c
               then fail
               else lazymatch goal with
                    | [ _ : b = c |- _ ] => fail
                    | [ _ : c = b |- _ ] => fail
                    | _ => assert (b = c) by (rewrite H1 in H2; exact H2)
                    end
             | [ H : MatchedSyncRedices ?l1 ?d ?l2 ?R ?R ?R' |- _ ] =>
               destruct (MatchedSyncRedicesDistinguishRedices H eq_refl)
             end.

    all: try (eexists; split;
              [|split;
                [| split;
                   [|split;
                     [| intros; eauto with PirExpr; econstructor; eauto with PirExpr;
                        repeat match goal with
                               | [ neq : ?p <> ?l, ni : ~ In ?p ?B |- context[?l :: ?B]] =>
                                 lazymatch goal with
                                 | [ _ : ~ In p (l :: B) |- _ ] => fail
                                 | _ =>
                                   let i := fresh in
                                   let eq := fresh in
                                   assert (~ In p (l :: B))
                                     by  (intro i; destruct i as [eq|i];
                                          [destruct (neq (eq_sym eq)) | destruct (ni i)])
                                 end
                               | [ step : forall B, ~ In ?p B -> C.PirStep ?R B ?C1 ?C2,
                                     ni : ~ In ?p ?B |- _ ] =>
                                 lazymatch goal with
                                 | [ _ : C.PirStep R B C1 C2 |- _ ] => fail
                                 | _ => pose proof (step B ni)
                                 end
                               end; auto]]]];
              repeat (cbn in *; EPPDestructor; eauto); fail).

    1,2: pose proof (MatchedSyncRedicesNotAllowChoice _ _ _ _ _ _ mtchd) as nac;
      destruct (UnmergeRelStep' _ _ _ _ _ step1 nac (CtrlExprMergeRelSpec2 _ _ _ projp1))
      as [E11' [E12' [merge1 [step3 step4]]]];
      pose proof (MatchedSyncRedicesDCL _ _ _ _ _ _ mtchd) as adcl;
      destruct (UnmergeRelACStep' _ _ _ _ _ _ adcl (CtrlExprMergeRelSpec2 _ _ _ projp2)
                                  step2)
        as [d1
              [R1'
                 [E21'
                    [d2
                       [R2'
                          [E22'
                             [step5
                                [step6
                                   [scrs1
                                      [scrs2
                                         [adcl1
                                            [adcl2
                                               [ [eq3 [eq4 merge]]
                                               | [[eq3 neq']
                                                 | [eq3 neq']
                                                 ]
                                               ]
                                            ]
                                         ]
                                      ]
                                   ]
                                ]
                             ]
                          ]
                       ]
                    ]
                 ]
              ]
           ]; subst;
        repeat match goal with
               | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
               | [ H1 : SameChoiceRedexShape ?R1' ?R2,
                        H2 : SameChoiceRedexShape ?R2' ?R2,
                             H3 : MatchedSyncRedices ?l1 ?d ?l2 ?R1 ?R2 ?R |- _ ] =>
                 lazymatch goal with
                 | [ _ : AlmostMatchedSyncRedices l1 l2 R1 R1' |- _ ] => fail
                 | _ => destruct (SameShapeAlmostMatched R1' R2' R1 R2 R l1 d l2 H1 H2 H3)
                 end
               | [ H1 : EPP ?C ?l1 = Some ?E11,
                        H2 : EPP ?C ?l2 = Some ?E21,
                             H3 : CtrlExprStep' ?E11 ?R1 ?E12,
                                  H4 : CtrlExprStep' ?E21 ?R2 ?E22,
                                       H5 : AlmostMatchedSyncRedices ?l1 ?l2 ?R1 ?R2 |- _ ] =>
                 lazymatch goal with
                 | [_ : MatchedSyncRedices l1 _ l2 R1 R2 _ |- _ ] => fail
                 | _ => let d := fresh "d" in
                        let R := fresh "R" in
                        let msr := fresh "matched" in
                        destruct (AllowChoiceProject
                                    C l1 l2 E11 R1 E12 E21 R2 E22 H1 H2 H3 H4 H5)
                          as [d [R msr]]
                 end
               | [H1 : SameChoiceRedexShape ?R1 ?R,
                       H2 :  SameChoiceRedexShape ?R2 ?R,
                             H3 : AllowDirectedChoiceLabel ?d (CtrlExprRedexToLabel ?R1),
                                  H4 : AllowDirectedChoiceLabel ?d
                                                                (CtrlExprRedexToLabel ?R2)
                  |- _ ] =>
                 tryif unify R1 R2
                 then fail
                 else assert (R1 = R2) by
                     (apply SameChoiceRedexShapeSym in H2;
                      let H := fresh in
                      pose proof (SameChoiceRedexShapeTrans _ _ _ H1 H2) as H;
                      apply (SameShapeSameDirection d R1 R2 H H3 H4));
                   subst; clear H2 H4
               | [ H : MatchedSyncRedices ?p ?d1 ?q ?R1 ?R2 _,
                       H' : MatchedSyncRedices ?p ?d2 ?q ?R1 ?R2' _ |- _ ] =>
                 tryif unify d1 d2
                 then fail
                 else
                   let eq1 := fresh in
                   let eq2 := fresh in
                   let eq3 := fresh in
                   let eq4 := fresh in
                   destruct (MatchedSyncRedicesUnique1 p d1 q d2 R1 R2 R2' _ _ H H')
                     as [eq1 [eq4 eq3]]; subst; clear H'
               | [ H1 : AllowDirectedChoiceLabel ?d1 ?L,
                        H2 : AllowDirectedChoiceLabel ?d2 ?L |-_ ] =>
                 tryif unify d1 d2
                 then fail
                 else pose proof (DirectionUnique _ _ _ H1 H2); subst; clear H2
               end;
        destruct (IHC1_1 _ _ _ _ _ _ _ _ _ _ eq1 eq step3 step5 mtchd)
          as [C0' [projp0' [projq0' [_ [_ step]]]]];
        destruct (IHC1_2 _ _ _ _ _ _ _ _ _ _ eq2 eq0 step4 step6 mtchd)
          as [C2' [projp2' [projq2' [_ [_ step']]]]];
        exists (C.If ℓ e C0' C2'); repeat split; cbn; EPPDestructor;
          auto with CtrlExpr PirExpr;
          intros B ni1 ni2;
          assert (~ In p (ℓ :: B)) by (intro i; destruct i as [eq' | i];
                                       [destruct (neq0 (eq_sym eq')) | destruct (ni1 i)]);
          assert (~ In q (ℓ :: B)) by (intro i; destruct i as [eq' | i];
                                       [destruct (neq (eq_sym eq')) | destruct (ni2 i)]);
          apply C.CIfIStep; auto. 
    all: try (inversion step1; subst; inversion mtchd; subst; inversion step2; subst;
              exists C1; repeat split; auto with CtrlExpr PirExpr; fail).
    all: try (inversion step2; subst; inversion mtchd; subst; inversion step1; subst;
              exists C1; repeat split; auto with CtrlExpr PirExpr;
              destruct (neq0 eq_refl); fail).
  Qed.

  Lemma ChoiceLabelPairToRedices : forall l1 d l2 L1 L2 R1 R2,
      l1 <> l2 ->
      ChoiceLabelPair l1 d l2 L1 L2 ->
      CtrlExprRedexToLabel R1 = L1 ->
      CtrlExprRedexToLabel R2 = L2 ->
      exists R, MatchedSyncRedices l1 d l2 R1 R2 R.
  Proof using.
    intros l1 d l2 L1 L2 R1 R2 neq pr; revert R1 R2 neq; induction pr;
      cbn; intros R1 R2 neq eq1 eq2; destruct R1; destruct R2; cbn in *;
        EPPDestructor; try discriminate.
    - inversion eq1; clear eq1; subst; inversion eq2; clear eq2; subst;
        eexists; econstructor; eauto.
    - inversion eq1; clear eq1; subst; inversion eq2; clear eq2; subst;
        eexists; econstructor; eauto.
    - inversion eq1; clear eq1; subst; inversion eq2; clear eq2; subst.
      destruct (IHpr R1 R2 neq eq_refl eq_refl) as [R mtchd];
        exists (C.RFun R); constructor; auto.
    - inversion eq1; inversion eq2; subst; clear eq1 eq2.
      destruct (IHpr R1 R2 neq eq_refl eq_refl) as [R mtchd];
        exists (C.RFun R); constructor; auto.
    - inversion eq1; clear eq1; subst; inversion eq2; clear eq2; subst.
      destruct (IHpr R1 R2 neq eq_refl eq_refl) as [R mtchd];
        exists (C.RArg R); constructor; auto.
    - inversion eq1; clear eq1; subst; inversion eq2; clear eq2; subst.
      destruct (IHpr R1 R2 neq eq_refl eq_refl) as [R mtchd];
        exists (C.RArg R); constructor; auto.
  Qed.

  Corollary LocalSyncSoundness : forall C1 l1 d l2 E11 E12 E21 E22 L1 L2,
      l1 <> l2 ->
      EPP C1 l1 = Some E11 ->
      EPP C1 l2 = Some E21 ->
      CtrlExprStep E11 L1 E12 ->
      CtrlExprStep E21 L2 E22 ->
      ChoiceLabelPair l1 d l2 L1 L2 ->
      exists R C2, EPP C2 l1 = Some E12
                   /\ EPP C2 l2 = Some E22
                   /\ ProjectRedex R l1 = Some L1
                   /\ ProjectRedex R l2 = Some L2
                   /\ C.PirStep R [] C1 C2.
  Proof using.
    intros C1 l1 d l2 E11 E12 E21 E22 L1 L2 H H0 H1 H2 H3 H4.
    destruct (CtrlExprStepToPrime H2) as [R1 [eq1 step1]];
      destruct (CtrlExprStepToPrime H3) as [R2 [eq2 step2]].
    destruct (ChoiceLabelPairToRedices l1 d l2 L1 L2 R1 R2 H H4 eq1 eq2)
      as [R mtchd].
    destruct (LocalSyncSoundness' C1 l1 d l2 E11 E12 E21 E22 R1 R2 R
                                  H0 H1 step1 step2 mtchd)
      as [C2 [proj21 [proj22 [projR1 [projR2 step]]]]].
    exists R; exists C2; split; [|split; [|split; [|split]]]; auto.
    rewrite <- eq1; apply ProjectRedexTriangle1; auto.
    rewrite <- eq2; apply ProjectRedexTriangle1; auto.
  Qed.

  (* SYSTEM IOTA SOUNDNESS *)
  
  Theorem SysIotaSoundness : forall C1 nms Π1 Π2,
      ProjectSystem C1 nms = Some Π1 ->
      SystemStep Π1 SysIota Π2 ->
      exists R C2 Π2', ProjectSystem C2 nms = Some Π2'
                       /\ C.PirStep R [] C1 C2
                       /\ LessNondetSystem Π2' Π2
                       /\ RedexToSystemLabel R = SysIota.
  Proof using.
    intros C1 nms Π1 Π2 eq step;
      revert C1 nms eq; dependent induction step; intros C1 nms eq.
    assert (In p nms) by (apply (ProjectSystemLookup C1 nms Π1 eq) in H; destruct H; auto).
    assert (EPP C1 p = Some E1)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H; destruct H; auto).
    destruct (LocalIotaSoundness C1 p E1 E2 L H5 H0 H1) as [R [C2 [eq1 [eq2 cstep]]]].
    pose proof (ProjectIotaSystemIota R p L H1 eq1).
    destruct (IotaUnique R H6) as [q [L' [iL' [eq' neq]]]].
    destruct (L.eq_dec q p) as [e |n]; subst;
      [clear eq'| apply neq in n; rewrite n in eq1; inversion eq1].
    destruct (SystemCompletenessExists R [] C1 C2 Π1 nms eq cstep) as [Π2' eq3].
    exists R; exists C2; exists Π2'; split; [|split; [|split]]; auto.
    unfold LessNondetSystem; split.
    - intros l E0 mt; destruct (L.eq_dec l p); subst.
      -- exists E2; split; auto.
         destruct (proj1 (ProjectSystemLookup C2 nms Π2' eq3 p E0) mt) as [_ mt'].
         rewrite eq2 in mt'; inversion mt'; clear mt'; subst; reflexivity.
      -- destruct (proj1 (ProjectSystemLookup C2 nms Π2' eq3 l E0) mt) as [i eq4].
         destruct (EPP C1 l) as [E0'|] eqn:eq5.
         2: exfalso; eapply ProjectSystemLookupNone in eq5; eauto;
           rewrite eq5 in eq; inversion eq.
         pose proof (proj2 (ProjectSystemLookup C1 nms Π1 eq l E0') ltac:(split; auto)).
         exists E0'; split; [apply H3; auto|].
         specialize (neq l ltac:(auto)). eapply EPPRedex; eauto.
    - intros l E0 mt.
      destruct (L.eq_dec l p); subst.
      -- pose proof (LMF.MapsToUnique mt H2); subst; clear H1.
         exists E2; split; [|reflexivity].
         apply (proj2 (ProjectSystemLookup C2 nms Π2' eq3 p E2) ltac:(split; auto)).
      -- apply H3 in mt; auto.
         destruct (proj1 (ProjectSystemLookup C1 nms Π1 eq l E0) mt) as [i eq4].
         destruct (EPP C2 l) as [E0'|] eqn:eq5.
         2: exfalso; eapply ProjectSystemLookupNone in eq5; eauto;
           rewrite eq5 in eq3; inversion eq3.
         pose proof (proj2 (ProjectSystemLookup _ _ _ eq3 l E0') ltac:(split; auto)).
         exists E0'; split; auto.
         eapply EPPRedex; eauto.
  Qed.

  (* COMMUNICATION SYSTEM SOUNDNESS *)

  Theorem ProjectCommSystem : forall R l v l' L1 L2,
      ProjectRedex R l = Some L1 ->
      ProjectRedex R l' = Some L2 ->
      CommLabelPair l v l' L1 L2 ->
      RedexToSystemLabel R = CommLabel l v l'.
  Proof using.
    intros R; induction R; intros p v q L1 L2; cbn;
      intros eq1 eq2 pr; EPPDestructor; inversion pr; subst; try discriminate;
        try match goal with
            | [ H : ?a <> ?a |- _] => destruct (H eq_refl)
            end; try (eapply IHR; eauto); reflexivity.
  Qed.

  Theorem CommRedexPair : forall R l v l',
      RedexToSystemLabel R = CommLabel l v l' ->
      l <> l' ->
      exists L1 L2, ProjectRedex R l = Some L1 /\
               ProjectRedex R l' = Some L2 /\
               CommLabelPair l v l' L1 L2 /\
               (forall l'', l'' <> l -> l'' <> l' -> ProjectRedex R l'' = None).
  Proof using.
    intros R; induction R; intros p v q eq neq; cbn in *;
      EPPDestructor; try discriminate;
        repeat match goal with
               | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
               | [ H : CommLabel ?a ?b ?c = CommLabel ?d ?e ?f |- _ ] =>
                 inversion H; clear H; subst
               | [ |- exists L1 L2, Some ?L1' = Some L1 /\ Some ?L2' = Some L2 /\ _ /\ _ ] =>
                 exists L1'; exists L2'; split; [reflexivity|split; [reflexivity|split]]
               | [ IH : forall l v l', RedexToSystemLabel ?R = CommLabel l v l' ->
                                       l <> l' ->
                                       exists L1 L2,
                                         ProjectRedex R l = Some L1 /\
                                         ProjectRedex R l' = Some L2 /\
                                         CommLabelPair l v l' L1 L2 /\
                                         (forall l'', l'' <> l -> l'' <> l' ->
                                                 ProjectRedex R l'' = None),
                     H1 : RedexToSystemLabel ?R = CommLabel ?l ?v ?l',
                     H2 : ?l <> ?l' |- _ ] =>
                 specialize (IH l v l' H1 H2);
                   let L1 := fresh "L" in
                   let L2 := fresh "L" in
                   let eq1 := fresh "eq" in
                   let eq2 := fresh "eq" in
                   let neq := fresh "neq" in
                   destruct IH as [L1 [L2 [eq1 [eq2 neq]]]]
               | [H1 : ?a = ?b, H2 : ?a = ?c |- _ ] =>
                 tryif unify b c
                 then fail
                 else assert (b = c) by (rewrite H1 in H2; exact H2); clear H2; subst
               end; eauto with CtrlExpr; try intros; EPPDestructor;
          try discriminate; auto with CtrlExpr; try (constructor; auto; fail).
    all: rewrite H0 in eq0; auto; inversion eq0.
  Qed.
  
  Theorem CommSoundness : forall C1 nms Π1 Π2 l v l',
      ProjectSystem C1 nms = Some Π1 ->
      SystemStep Π1 (CommLabel l v l') Π2 ->
      exists R C2 Π2', ProjectSystem C2 nms = Some Π2'
                       /\ C.PirStep R [] C1 C2
                       /\ LessNondetSystem Π2' Π2
                       /\ RedexToSystemLabel R = CommLabel l v l'.
  Proof using.
    intros C1 nms Π1 Π2 p v q eq step; revert C1 nms eq;
      dependent induction step; intros C1 nms eq.
    assert (In p nms)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H1; destruct H1; auto).
    assert (In q nms)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H2; destruct H2; auto).
    assert (EPP C1 p = Some pE1)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H1; destruct H1; auto).
    assert (EPP C1 q = Some qE1)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H2; destruct H2; auto).
    destruct (LocalCommSoundness C1 p v q pE1 pE2 qE1 qE2 L1 L2 H H10 H11 H3 H4 H0)
      as [R [C2 [proj_l1_2 [proj_l2_2 [proj_r1 [proj_r2 step]]]]]].
    pose proof (ProjectCommSystem R p v q L1 L2 proj_r1 proj_r2 H0).
    destruct (CommRedexPair R p v q H12 H) as [L1' [L2' [proj_r1' [proj_r2' [_ eq_none]]]]].
    rewrite proj_r1 in proj_r1'; rewrite proj_r2 in proj_r2';
      inversion proj_r1'; inversion proj_r2'; clear proj_r1' proj_r2'; subst;
        rename L1' into L1; rename L2' into L2.
    destruct (SystemCompletenessExists _ _ _ _ _ _ eq step) as [Π2' eq'].
    pose proof (proj2 (ProjectSystemLookup _ _ _ eq' p pE2) ltac:(split; auto)).
    pose proof (proj2 (ProjectSystemLookup _ _ _ eq' q qE2) ltac:(split; auto)).
    exists R; exists C2; exists Π2'; split; [|split; [|split]]; auto.
    unfold LessNondetSystem; split.
    - intros l E1 mt.
      destruct (L.eq_dec l p); subst; [|destruct (L.eq_dec l q); subst].
      -- exists pE2; split; auto.
         pose proof (LMF.MapsToUnique mt H13); subst; auto with CtrlExpr.
      -- exists qE2; split; auto.
         pose proof (LMF.MapsToUnique mt H14); subst; auto with CtrlExpr.
      -- assert (ProjectRedex R l = None) by (apply eq_none; auto).
         destruct (proj1 (ProjectSystemLookup _ _ _ eq' l E1) mt) as [i proj_l2].
         destruct (EPP C1 l) as [E1'|] eqn:proj_l1;
           [|rewrite (ProjectSystemLookupNone _ _ _ i proj_l1) in eq; inversion eq].
         pose proof (EPPRedex _ _ _ _ _ _ _  proj_l1 proj_l2 H15 step).
         exists E1'; split; auto. 
         apply H7; auto.
         apply (proj2 (ProjectSystemLookup _ _ _ eq l E1') ltac:(split; auto)).
    - intros l E2 mt.
      destruct (L.eq_dec l p); subst; [|destruct (L.eq_dec l q); subst].
      -- exists pE2; split; auto. pose proof (LMF.MapsToUnique mt H5); subst; reflexivity.
      -- exists qE2; split; auto. pose proof (LMF.MapsToUnique mt H6); subst; reflexivity.
      -- apply H7 in mt; auto.
         destruct (proj1 (ProjectSystemLookup _ _ _ eq l E2) mt) as [i proj_l_1].
         destruct (EPP C2 l) as [E2'|] eqn:proj_l2;
           [|rewrite (ProjectSystemLookupNone _ _ _ i proj_l2) in eq'; inversion eq'].
         assert (ProjectRedex R l = None) by (apply eq_none; auto).
         pose proof (EPPRedex _ _ _ _ _ _ _ proj_l_1 proj_l2 H15 step).
         pose proof (proj2 (ProjectSystemLookup _ _ _ eq' l E2') ltac:(split; auto)).
         exists E2' ;split; auto.
  Qed.

  (* CHOICE SYSTEM SOUNDNESS *)
  
  Theorem ProjectChoiceSystem : forall R l d l' L1 L2,
      ProjectRedex R l = Some L1 ->
      ProjectRedex R l' = Some L2 ->
      ChoiceLabelPair l d l' L1 L2 ->
      RedexToSystemLabel R = ChoiceLabel l d l'.
  Proof using.
    intros R; induction R; intros p v q L1 L2; cbn;
      intros eq1 eq2 pr; EPPDestructor; inversion pr; subst; try discriminate;
        try match goal with
            | [ H : ?a <> ?a |- _] => destruct (H eq_refl)
            end; try (eapply IHR; eauto); reflexivity.
  Qed.

  Theorem ChoiceRedexPair : forall R l d l',
      RedexToSystemLabel R = ChoiceLabel l d l' ->
      l <> l' ->
      exists L1 L2, ProjectRedex R l = Some L1 /\
                    ProjectRedex R l' = Some L2 /\
                    ChoiceLabelPair l d l' L1 L2 /\
                    (forall l'', l'' <> l -> l'' <> l' -> ProjectRedex R l'' = None).
  Proof using.
    intros R; induction R; intros p v q eq neq; cbn in *;
      EPPDestructor; try discriminate;
        repeat match goal with
               | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
               | [ H : ChoiceLabel ?a ?b ?c = ChoiceLabel ?d ?e ?f |- _ ] =>
                 inversion H; clear H; subst
               | [ |- exists L1 L2, Some ?L1' = Some L1 /\ Some ?L2' = Some L2 /\ _ /\ _ ] =>
                 exists L1'; exists L2'; split; [reflexivity|split; [reflexivity|split]]
               | [ IH : forall l v l', RedexToSystemLabel ?R = ChoiceLabel l v l' ->
                                       l <> l' ->
                                       exists L1 L2,
                                         ProjectRedex R l = Some L1 /\
                                         ProjectRedex R l' = Some L2 /\
                                         ChoiceLabelPair l v l' L1 L2 /\
                                         (forall l'', l'' <> l -> l'' <> l' ->
                                                 ProjectRedex R l'' = None),
                     H1 : RedexToSystemLabel ?R = ChoiceLabel ?l ?v ?l',
                     H2 : ?l <> ?l' |- _ ] =>
                 specialize (IH l v l' H1 H2);
                   let L1 := fresh "L" in
                   let L2 := fresh "L" in
                   let eq1 := fresh "eq" in
                   let eq2 := fresh "eq" in
                   let neq := fresh "neq" in
                   destruct IH as [L1 [L2 [eq1 [eq2 neq]]]]
               | [H1 : ?a = ?b, H2 : ?a = ?c |- _ ] =>
                 tryif unify b c
                 then fail
                 else assert (b = c) by (rewrite H1 in H2; exact H2); clear H2; subst
               end; eauto with CtrlExpr; try intros; EPPDestructor;
          try discriminate; auto with CtrlExpr; try (constructor; auto; fail).
    all: rewrite H0 in eq0; auto; inversion eq0.
  Qed.

  Theorem ChoiceSoundness : forall C1 nms Π1 Π2 l d l',
      ProjectSystem C1 nms = Some Π1 ->
      SystemStep Π1 (ChoiceLabel l d l') Π2 ->
      exists R C2 Π2', ProjectSystem C2 nms = Some Π2'
                       /\ C.PirStep R [] C1 C2
                       /\ LessNondetSystem Π2' Π2
                       /\ RedexToSystemLabel R = ChoiceLabel l d l'.
  Proof using.
    intros C1 nms Π1 Π2 p d q eq step; revert C1 nms eq;
      dependent induction step; intros C1 nms eq.
    assert (In p nms)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H1; destruct H1; auto).
    assert (In q nms)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H2; destruct H2; auto).
    assert (EPP C1 p = Some pE1)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H1; destruct H1; auto).
    assert (EPP C1 q = Some qE1)
      by (apply (ProjectSystemLookup C1 nms Π1 eq) in H2; destruct H2; auto).
    destruct (LocalSyncSoundness C1 p d q pE1 pE2 qE1 qE2 L1 L2 H H10 H11 H3 H4 H0)
      as [R [C2 [proj_l1_2 [proj_l2_2 [proj_r1 [proj_r2 step]]]]]].
    pose proof (ProjectChoiceSystem R p d q L1 L2 proj_r1 proj_r2 H0).
    destruct (ChoiceRedexPair R p d q H12 H)
      as [L1' [L2' [proj_r1' [proj_r2' [_ eq_none]]]]].
    rewrite proj_r1 in proj_r1'; rewrite proj_r2 in proj_r2';
      inversion proj_r1'; inversion proj_r2'; clear proj_r1' proj_r2'; subst;
        rename L1' into L1; rename L2' into L2.
    destruct (SystemCompletenessExists _ _ _ _ _ _ eq step) as [Π2' eq'].
    pose proof (proj2 (ProjectSystemLookup _ _ _ eq' p pE2) ltac:(split; auto)).
    pose proof (proj2 (ProjectSystemLookup _ _ _ eq' q qE2) ltac:(split; auto)).
    exists R; exists C2; exists Π2'; split; [|split; [|split]]; auto.
    unfold LessNondetSystem; split.
    - intros l E1 mt.
      destruct (L.eq_dec l p); subst; [|destruct (L.eq_dec l q); subst].
      -- exists pE2; split; auto.
         pose proof (LMF.MapsToUnique mt H13); subst; auto with CtrlExpr.
      -- exists qE2; split; auto.
         pose proof (LMF.MapsToUnique mt H14); subst; auto with CtrlExpr.
      -- assert (ProjectRedex R l = None) by (apply eq_none; auto).
         destruct (proj1 (ProjectSystemLookup _ _ _ eq' l E1) mt) as [i proj_l2].
         destruct (EPP C1 l) as [E1'|] eqn:proj_l1;
           [|rewrite (ProjectSystemLookupNone _ _ _ i proj_l1) in eq; inversion eq].
         pose proof (EPPRedex _ _ _ _ _ _ _  proj_l1 proj_l2 H15 step).
         exists E1'; split; auto. 
         apply H7; auto.
         apply (proj2 (ProjectSystemLookup _ _ _ eq l E1') ltac:(split; auto)).
    - intros l E2 mt.
      destruct (L.eq_dec l p); subst; [|destruct (L.eq_dec l q); subst].
      -- exists pE2; split; auto. pose proof (LMF.MapsToUnique mt H5); subst; reflexivity.
      -- exists qE2; split; auto. pose proof (LMF.MapsToUnique mt H6); subst; reflexivity.
      -- apply H7 in mt; auto.
         destruct (proj1 (ProjectSystemLookup _ _ _ eq l E2) mt) as [i proj_l_1].
         destruct (EPP C2 l) as [E2'|] eqn:proj_l2;
           [|rewrite (ProjectSystemLookupNone _ _ _ i proj_l2) in eq'; inversion eq'].
         assert (ProjectRedex R l = None) by (apply eq_none; auto).
         pose proof (EPPRedex _ _ _ _ _ _ _ proj_l_1 proj_l2 H15 step).
         pose proof (proj2 (ProjectSystemLookup _ _ _ eq' l E2') ltac:(split; auto)).
         exists E2' ;split; auto.
  Qed.

  (* SYNCRHONIZATION IOTA SYSTEM SOUNDNESS *)
  Theorem CtrlExprStepSyncDet : forall E1 L E2 E2',
      IsSyncLabel L ->
      CtrlExprStep E1 L E2 ->
      CtrlExprStep E1 L E2' ->
      E2 = E2'.
  Proof using.
    intros E1 L E2 E2' syncL step; revert syncL E2'; induction step; intros syncL E2' step';
      inversion syncL; subst; inversion step'; subst.
    all: try (erewrite IHstep; eauto; fail). 
    all: reflexivity.
  Qed.

  Theorem SyncSoundness : forall C1 nms Π1 Π2,
      nms <> [] ->
      (forall p, In p (C.LocsInPirExpr C1) -> In p nms) ->
      ProjectSystem C1 nms = Some Π1 ->
      SystemStep Π1 SysSyncIota Π2 ->
      exists R C2 Π2', ProjectSystem C2 nms = Some Π2'
                       /\ C.PirStep R [] C1 C2
                       /\ LessNondetSystem Π2' Π2
                       /\ RedexToSystemLabel R = SysSyncIota.
  Proof using.
    intros C1 nms Π1 Π2 nempty tn eq step;
      revert C1 nms nempty tn eq; dependent induction step;
        intros C1 nms nempty tn eq.
    destruct (LocalSyncIotaSoundness C1 L nms nempty H tn) as [C2 [R [eq' step]]].
    - intros l i;
        destruct (EPP C1 l) as [E1|] eqn:proj_l1;
        [|rewrite (ProjectSystemLookupNone C1 nms l i proj_l1) in eq; inversion eq].
      pose proof (proj2 (ProjectSystemLookup C1 nms Π1 eq l E1) ltac:(split; auto)).
      assert (LM.In l Π2) by (apply H1; exists E1; auto). destruct H3 as [E2 mt2].
      pose proof (H0 l E1 E2 H2 mt2). exists E1; exists E2; split; auto.
    - destruct (SystemCompletenessExists R [] C1 C2 Π1 nms eq step)
        as [Π2' eq''].
      exists R; exists C2; exists Π2'; split; [|split; [|split]]; auto.
      2: { destruct nms; [destruct (nempty eq_refl)|].
           specialize (eq' l).
           clear Π1 Π2 H0 H1 C1 nms nempty tn eq C2 step Π2' eq''.
           revert L H l eq'; induction R; cbn; intros L H p eq';
             EPPDestructor; cbn; auto; try discriminate;
               inversion H; subst; eapply IHR; eauto. }
      unfold LessNondetSystem; split; intros l E mt.
      -- rename mt into mt2; rename E into E2.
         destruct (proj1 (ProjectSystemLookup C2 nms Π2' eq'' l E2) mt2) as [i proj_l2].
         destruct (EPP C1 l) as [E1|] eqn:proj_l1;
           [|rewrite (ProjectSystemLookupNone C1 nms l i proj_l1) in eq; inversion eq].
         pose proof (proj2 (ProjectSystemLookup C1 nms Π1 eq l E1) ltac:(split; auto))
           as mt1.
         destruct (proj1 (H1 l) ltac:(exists E1; auto)) as [E2' mt2'].
         pose proof (H0 l E1 E2' mt1 mt2').
         pose proof (LocalCompleteness C1 R [] C2 l E1 E2 L proj_l1 proj_l2 (eq' l) step).
         pose proof (CtrlExprStepSyncDet E1 L E2 E2' H  H3 H2); subst.
         exists E2'; split; [auto|reflexivity].
      -- rename mt into mt2; rename E into E2.
         destruct (proj2 (H1 l) ltac:(exists E2; auto)) as [E1 mt1].
         pose proof (H0 l E1 E2 mt1 mt2).
         destruct (proj1 (ProjectSystemLookup C1 nms Π1 eq l E1) mt1) as [i proj_l1].
         destruct (EPP C2 l) as [E2'|] eqn:proj_l2;
           [|rewrite (ProjectSystemLookupNone C2 nms l i proj_l2) in eq''; inversion eq''].
         pose proof (proj2 (ProjectSystemLookup C2 nms Π2' eq'' l E2') ltac:(split; auto))
           as mt2'.
         pose proof (LocalCompleteness C1 R [] C2 l E1 E2' L proj_l1 proj_l2 (eq' l) step).
         pose proof (CtrlExprStepSyncDet E1 L E2 E2' H H2 H3); subst.
         exists E2'; split; [auto|reflexivity].
  Qed.

  (* SYSTEM SOUNDNESS *)
  Theorem Soundness : forall C1 nms Π1 L Π2,
      nms <> [] ->
      (forall l, In l (C.LocsInPirExpr C1) -> In l nms) ->
      ProjectSystem C1 nms = Some Π1 ->
      SystemStep Π1 L Π2 ->
      exists R C2 Π2', ProjectSystem C2 nms = Some Π2'
                       /\ C.PirStep R [] C1 C2
                       /\ LessNondetSystem Π2' Π2
                       /\ RedexToSystemLabel R = L.
  Proof using.
    intros C1 nms Π1 L Π2 H H0 H1 H2; destruct L.
    eapply SysIotaSoundness; eauto.
    eapply SyncSoundness; eauto.
    eapply CommSoundness; eauto.
    eapply ChoiceSoundness; eauto.
  Qed.

  (* Lowering Steps *)
  (* 
     In order to extend soundness out to multiple steps, we need to argue about lowering
     steps along less-nondeterminism. 

     First, we prove that matched synchronization redices can be lowered, but you only get
     almost-matched redices. However, if those redices are matched, you get a less-
     nondetermisitic result.

     Then, we use that to prove that we can always lower a step, if the starting position
     of the lower step is the projection of a Pirouette program. This comes from the fact
     (proved earlier) that the projection of Pirouette programs always preserves
     matched labels.
   *)
  Lemma LowerAlmostMatchedRedexNondet : forall p d q R1 R2 R E11 E12 E21,
      MatchedSyncRedices p d q R1 R2 R ->
      CtrlExprStep' E11 R2 E12 ->
      LessNondet E21 E11 ->
      exists R2' E22, AlmostMatchedSyncRedices p q R1 R2' /\
                      CtrlExprStep' E21 R2' E22 /\
                      (MatchedSyncRedices p d q R1 R2' R -> LessNondet E22 E12).
  Proof using.
    intros p d q R1 R2 R E11 E12 E21 amsr step;
      revert p d q R1 R E21 amsr; induction step;
        intros p d q R1 R' E21 amsr lnd; inversion amsr; subst; inversion lnd; subst.
    all: try (eexists; eexists; split; [|split];
              eauto with CtrlExpr; intro m; inversion m; fail).
    - exists (AllowChoiceLRedex l); exists E1; split; [|split]; auto with CtrlExpr.
    - exists (AllowChoiceLRedex l); exists E0; split; [|split]; auto with CtrlExpr.
    - exists (AllowChoiceLRedex l); exists E0; split; [|split]; auto with CtrlExpr.
      intro matched; inversion matched.
    - destruct (IHstep p d q R0 R3 E11 H4 H2) as [R2' [E22 [amsr' [step' imp]]]].
      exists (ArgRedex R2'); exists (LetRet E22 E12); split; [|split]; auto with CtrlExpr.
      intro msr; inversion msr; subst; constructor; auto. 
    - destruct (IHstep p d q R0 R3 E1 H4 H1) as [R2' [E22 [amsr' [step' imp]]]].
      exists (FunRedex R2'); exists (AppLocal E22 e); split; [|split]; auto with CtrlExpr.
      intro msr; inversion msr; subst; constructor; auto.
    - destruct (IHstep p d q R0 R3 E11 H4 H2) as [R2' [E22 [amsr' [step' imp]]]].
      exists (FunRedex R2'); exists (AppGlobal E22 E12); split; [|split]; auto with CtrlExpr.
      intro msr; inversion msr; subst; constructor; auto.
    - destruct (IHstep p d q R0 R3 E12 H4 H3) as [R2' [E22 [amsr' [step' imp]]]].
      exists (ArgRedex R2'); exists (AppGlobal E11 E22); split; [|split]; auto with CtrlExpr.
      intro msr; inversion msr; subst; constructor; auto.
  Qed.      
  
  Theorem LowerCompiledSystemStep : forall C nms Π11 L Π12 Π21,
      SystemStep Π11 L Π12 ->
      LessNondetSystem Π21 Π11 ->
      ProjectSystem C nms = Some Π21 ->
      exists Π22, LessNondetSystem Π22 Π12 /\ SystemStep Π21 L Π22.
  Proof using.
    intros C nms Π11 L Π12 Π21 step; revert C nms Π21; induction step;
      intros C nms Π21 lnds eq.
    - destruct (proj2 lnds p E1 H) as [E21 [mt21 lnd]].
      assert (~ IsAllowChoiceLabel L)
        by (clear H0; induction L; inversion H1; intro iacl;
            inversion iacl; subst; apply IHL; auto).
      destruct (LowerStepNondet E1 L E2 E21 H0 lnd H4) as [E22 [lnd' step']].
      exists (LM.add p E22 Π21); split; auto;
        [|eapply IotaStep; eauto;
          [apply LM.add_1| intros q E neq; split; intro mt];
          [apply LM.add_2; auto| apply LM.add_3 in mt; auto]].
      unfold LessNondetSystem; split.
      -- intros l El2 mtl2; destruct (L.eq_dec l p); subst.
         pose proof (LMF.MapsToUnique mtl2 (LM.add_1 Π21 p E22)); subst.
         exists E2; split; auto.
         apply LM.add_3 in mtl2; auto.
         apply lnds in mtl2; destruct mtl2 as [El1 [mtl1 lnd'']].
         apply H3 in mtl1; auto. exists El1; split; auto.
      -- intros l El2 mtl2; destruct (L.eq_dec l p); subst.
         pose proof (LMF.MapsToUnique mtl2 H2); subst.
         exists E22; split;auto; apply LM.add_1.
         apply H3 in mtl2; auto. apply lnds in mtl2; destruct mtl2 as [E1' [mtl1 lnd'']].
         exists E1'; split; auto. apply LM.add_2; auto.
    - refine (let l :=  (AddFromMap2 (AddFromMap1 (LM.elements Π1) Π21) Π2) in _).
      assert (forall p E1 E2 E3, In (p, E1, E2, E3) l <->
                                 LM.MapsTo p E1 Π1 /\
                                 LM.MapsTo p E2 Π21 /\
                                 LM.MapsTo p E3 Π2)
        by (pose proof (InAddFromMap2 (AddFromMap1 (LM.elements Π1) Π21) Π2);
            pose proof (InAddFromMap1 (LM.elements Π1) Π21);
            intros p E1 E2 E3; split; [intro i; split; [|split] | intros [mt1 [mt2 mt3]]];
            [ apply H2 in i; destruct i; apply H3 in H4; destruct H4;
              apply LM.elements_2; auto
            |  apply H2 in i; destruct i; apply H3 in H4; apply H4
            |  apply H2 in i; apply i
            | apply LM.elements_1 in mt1;
              pose proof (proj2 (H3 p E1 E2) ltac:(split; auto));
              apply H2; split ; auto]).
      assert (forall p E1 E2 E3, In (p, E1, E2, E3) l -> LessNondet E2 E1 /\ CtrlExprStep E1 L E3).
      intros p E1 E2 E3 i; apply H2 in i; destruct i as [mt1 [mt2 mt3]]; split.
      apply lnds in mt1; destruct mt1 as [E2' [mt21 lnd]].
      pose proof (LMF.MapsToUnique mt21 mt2); subst; auto.
      apply (H0 p); auto.
      assert (NoRepeats3 l); unfold l. apply AddFromMap2NoRepeats.
      apply AddFromMap1NoRepeats. pose proof (LM.elements_3w Π1).
      revert H4. generalize (LM.elements Π1). clear Π1 L H H0 H1 Π21 lnds l H2 H3 Π2 eq.
      intro l; induction l; intro nd; cbn; auto.
      destruct a as [p E]; cbn in *.
      inversion nd; subst. split; [apply IHl; auto|].
      intros q E' i eq; apply H1; subst. apply InA_altdef; apply Exists_exists.
      exists (q, E'); split; cbn; auto. unfold LM.eq_key; cbn; auto.
      assert (~ IsAllowChoiceLabel L) by
          (clear H0 H3; induction L; inversion H; intro iacl;
           inversion iacl; subst; apply IHL; auto).      
      destruct (LowerListOfSteps l L H5 H3 H4) as [ℓ [nd [nr eqv]]].
      exists (OutputListToSystem ℓ); split; [unfold LessNondetSystem; split|
                                             eapply SyncIotaStep; eauto].
      -- intros p E mt. apply OutputUniqueToSystem in mt; auto.
         destruct (nd p E mt) as [E1 [E2 [E3 [i [lnd step]]]]].
         exists E3; split; auto. eapply H2; eauto.
      -- intros p E2 mt2.
         destruct (proj2 (H1 p) ltac:(exists E2; apply mt2)) as [E1 mt1].
         destruct (proj2 lnds p E1 mt1) as [E3 [mt3 lnd]].
         pose proof (proj2 (H2 p E1 E3 E2) ltac:(split; [|split]; auto)).
         destruct (eqv p E1 E3 E2 H6) as [E i].
         pose proof (proj1 (OutputUniqueToSystem ℓ nr p E) i).
         destruct (nd p E i) as [E1' [E2' [E3' [i' [lnd' step]]]]].
         destruct (NoRepeats3Map l H4 _ _ _ _ _ _ _ H6 i') as [eq1 [eq2 eq3]]; subst.
         exists E; split; auto.
      -- intros p E21 E22 mt21 mt22.
         apply OutputUniqueToSystem in mt22; auto.
         destruct (nd p E22 mt22) as [E1 [E22' [E2 [i [lnd step]]]]].
         destruct (proj1 (H2 p E1 E22' E2) i) as [mt1 [mt21' mt2]].
         pose proof (LMF.MapsToUnique mt21' mt21); subst. auto.
      -- intros p; split; [intros [E mt21] | intros [E mt22]].
         --- destruct (proj1 lnds p E mt21) as [E1 [mt1 lnd]].
             destruct (proj1 (H1 p) ltac:(exists E1; apply mt1)) as [E2 mt2].
             pose proof (proj2 (H2 p E1 E E2) ltac:(split; [|split]; auto)).
             destruct (eqv p E1 E E2 H6) as [E' i].
             apply OutputUniqueToSystem in i; auto.
             exists E'; auto.
         --- apply OutputUniqueToSystem in mt22; auto.
             destruct (nd p E mt22) as [E1 [E21 [E2 [i [lnd step]]]]].
             destruct (proj1 (H2 p E1 E21 E2) i) as [mt1 [mt21 mt2]].
             exists E21; auto.
    - assert (~ IsAllowChoiceLabel L1 /\ ~ IsAllowChoiceLabel L2).
      clear Π1 pE1 pE2 qE1 qE2 H1 H2 H3 H4 H5 H6 H7 C nms Π21 eq lnds;
        induction H0. 1,2: split; intro H1; inversion H1; subst.
      1,2: specialize (IHCommLabelPair H); destruct IHCommLabelPair as [IH1 IH2];
        split; intro H1; inversion H1; subst; auto.
      (* end assert *)
      destruct H8.
      destruct (proj2 lnds p pE1 H1) as [Ep21 [mtp21 lndp1]].
      destruct (proj2 lnds q qE1 H2) as [Eq21 [mtq21 lndq1]].
      destruct (LowerStepNondet pE1 L1 pE2 Ep21 H3 lndp1 H8) as [Ep22 [mtp22 lndp2]].
      destruct (LowerStepNondet qE1 L2 qE2 Eq21 H4 lndq1 H9) as [Eq22 [mtq22 lndq2]].
      exists (LM.add p Ep22 (LM.add q Eq22 Π21)); split; auto with CtrlExpr;
        [unfold LessNondetSystem; split |eapply CommStep; eauto].
      -- intros l E1 mt; destruct (L.eq_dec l p); [subst|destruct (L.eq_dec l q); subst].
         pose proof (LMF.MapsToUnique mt (LM.add_1 (LM.add q Eq22 Π21) p Ep22)); subst.
         exists pE2; split; auto.
         1,2: apply LM.add_3 in mt; auto.
         pose proof (LMF.MapsToUnique mt (LM.add_1 Π21 q Eq22)); subst.
         exists qE2; split; auto.
         apply LM.add_3 in mt; auto.
         destruct (proj1 lnds l E1 mt) as [E1' [mt1' lnd1']]. apply H7 in mt1'; auto.
         exists E1'; split; auto.
      -- intros l E2 mt2; destruct (L.eq_dec l p); [subst|destruct (L.eq_dec l q); subst].
         exists Ep22; split; auto. apply LM.add_1.
         pose proof (LMF.MapsToUnique mt2 H5); subst; auto.
         exists Eq22; split; auto. apply LM.add_2; auto; apply LM.add_1.
         pose proof (LMF.MapsToUnique mt2 H6); subst; auto.
         apply H7 in mt2; auto. destruct (proj2 lnds l E2 mt2) as [E1 [mt1 lnd1]].
         exists E1; split; auto with CtrlExpr. apply LM.add_2; auto; apply LM.add_2; auto.
      -- apply LM.add_1.
      -- apply LM.add_2; auto; apply LM.add_1.
      -- intros r E neq1 neq2; split; intro mt.
         apply LM.add_2; auto; apply LM.add_2; auto.
         do 2 apply LM.add_3 in mt; auto.
    - assert (~ IsAllowChoiceLabel L1)
        by (clear Π1 Π2 pE1 pE2 qE1 qE2 H1 H2 H3 H4 H5 H6 H7 C nms Π21 lnds eq;
            induction H0; intros H1; inversion H1; subst; apply IHChoiceLabelPair; auto).
      destruct (proj2 lnds p pE1 H1) as [Ep21 [mtp21 lndp1]].
      destruct (proj2 lnds q qE1 H2) as [Eq21 [mtq21 lndq1]].
      destruct (LowerStepNondet pE1 L1 pE2 Ep21 H3 lndp1 H8) as [Ep22 [mtp22 lndp2]].
      assert (exists Eq22 : CtrlExpr, CtrlExprStep Eq21 L2 Eq22 /\ LessNondet Eq22 qE2).
      apply CtrlExprStepToPrime in lndp2; destruct lndp2 as [R1 [eqR1 step'1]].
      apply CtrlExprStepToPrime in H4; destruct H4 as [R2 [eqR2 step'2]].
      destruct (ChoiceLabelPairToRedices p LR q L1 L2 R1 R2 H H0 eqR1 eqR2) as [R mtchd].
      destruct (LowerAlmostMatchedRedexNondet p LR q R1 R2 R _ _ _ mtchd step'2 lndq1)
        as [R2' [E22 [amsr [step'3 imp]]]].
      destruct (proj1 (ProjectSystemLookup C nms Π21 eq p Ep21) mtp21) as [_ projp].
      destruct (proj1 (ProjectSystemLookup C nms Π21 eq q Eq21) mtq21) as [_ projq].
      destruct (AllowChoiceProject C p q Ep21 R1 Ep22 Eq21 R2' E22
                                   projp projq step'1 step'3 amsr)
        as [d [R' mtchd']].
      destruct (MatchedSyncRedicesUnique1 p LR q d R1 R2 R2' R R' mtchd mtchd')
        as [eq1 [eq2 eq3]]; subst.
      apply imp in mtchd'.
      exists E22; split; auto. apply CtrlExprStep'ToNormal; auto.
      (* end assert *)
      destruct H9 as [Eq22 [step22 lnd22]].
      exists (LM.add p Ep22 (LM.add q Eq22 Π21)); split; auto with CtrlExpr;
        [unfold LessNondetSystem; split |eapply ChoiceStep; eauto].
      -- intros l E1 mt; destruct (L.eq_dec l p); [subst|destruct (L.eq_dec l q); subst].
         pose proof (LMF.MapsToUnique mt (LM.add_1 (LM.add q Eq22 Π21) p Ep22)); subst.
         exists pE2; split; auto.
         1,2: apply LM.add_3 in mt; auto.
         pose proof (LMF.MapsToUnique mt (LM.add_1 Π21 q Eq22)); subst.
         exists qE2; split; auto.
         apply LM.add_3 in mt; auto.
         destruct (proj1 lnds l E1 mt) as [E1' [mt1' lnd1']]. apply H7 in mt1'; auto.
         exists E1'; split; auto.
      -- intros l E2 mt2; destruct (L.eq_dec l p); [subst|destruct (L.eq_dec l q); subst].
         exists Ep22; split; auto. apply LM.add_1.
         pose proof (LMF.MapsToUnique mt2 H5); subst; auto.
         exists Eq22; split; auto. apply LM.add_2; auto; apply LM.add_1.
         pose proof (LMF.MapsToUnique mt2 H6); subst; auto.
         apply H7 in mt2; auto. destruct (proj2 lnds l E2 mt2) as [E1 [mt1 lnd1]].
         exists E1; split; auto with CtrlExpr. apply LM.add_2; auto; apply LM.add_2; auto.
      -- apply LM.add_1.
      -- apply LM.add_2; auto; apply LM.add_1.
      -- intros r E neq1 neq2; split; intro mt.
         apply LM.add_2; auto; apply LM.add_2; auto.
         do 2 apply LM.add_3 in mt; auto.
  Qed.

  (* MULTI-STEP SOUNDNESS *)
  
  Theorem StepsSoundness : forall Π1 Ls Π2 C1 nms Π1',
      nms <> [] ->
      (forall l, In l (C.LocsInPirExpr C1) -> In l nms) ->
      ProjectSystem C1 nms = Some Π1' ->
      LessNondetSystem Π1' Π1 ->
      SystemSteps Π1 Ls Π2 ->
      exists Rs C2 Π2', ProjectSystem C2 nms = Some Π2'
                        /\ C.PirSteps Rs [] C1 C2
                        /\ LessNondetSystem Π2' Π2
                        /\ RedicesToSystemLabels Rs = Ls.
  Proof using.
    intros Π1 Ls; revert Π1; induction Ls as [|L Ls];
      intros Π1 Π2 C1 nms Π1' nempty tn eq lnds steps.
    - inversion steps; subst. exists []; exists C1; exists Π1'; cbn; split; [|split;[|split]]; auto.
      constructor. transitivity Π1; auto; apply EqLessNondet; auto.
    - inversion steps; subst; clear steps; rename Π2 into Π3; rename C2 into Π2;
        rename H2 into step; rename H4 into steps.
      destruct (LowerCompiledSystemStep C1 nms Π1 L Π2 Π1' step lnds eq)
        as [Π2' [lnds' step']].
      destruct (Soundness C1 nms Π1' L Π2' nempty tn eq step')
        as [R [C2 [Π2'' [eq2 [step'' [lnds'' eqR]]]]]].
      assert (LessNondetSystem Π2'' Π2) as lnds2 by (transitivity Π2'; auto).
      assert (forall l, In l (C.LocsInPirExpr C2) -> In l nms).
      intros l i; apply C.LocsInPirExprAfterStep with (l := l) in step''; auto.
      destruct (IHLs Π2 Π3 C2 nms Π2'' nempty H eq2 lnds2 steps)
        as [Rs [C3 [Π3' [eq3 [steps' [lnds3 eqRs]]]]]].
      exists (R :: Rs); exists C3; exists Π3';split; [|split; [|split]]; auto.
      econstructor; eauto. cbn; rewrite eqR; rewrite eqRs; reflexivity.
  Qed.
  
  (* DEADLOCK-FREEDOM-BY-DESIGN *)
  
  (* 
     N.B.: Everything up until here works without typing, but Coq's modules being generative
     force me to put everything in one big module. 
   *)

  Lemma ValuesToValueSystem : forall V nms Π,
      C.PirExprVal V ->
      ProjectSystem V nms = Some Π ->
      ValueSystem Π.
  Proof using.
    intros V nms Π val eq.
    unfold ValueSystem. intros l E mt.
    destruct (proj1 (ProjectSystemLookup V nms Π eq l E) mt).
    apply EPPValue in H0; auto.
  Qed.

  Theorem DeadlockFreedomByDesign :
    forall (C : C.PirExpr) (τ : C.PirTyp) Π (nms : list Loc) Γ Δ,
      nms <> [] ->
      (forall l, In l (C.LocsInPirExpr C) -> In l nms) ->
      C.PirExprClosed C ->
      (C.ctyping Γ Δ C τ) ->
      ProjectSystem C nms = Some Π ->
      DeadlockFree Π.
  Proof using.
    intros C τ Π nms Γ Δ nempty tn C_closed typing C_comp.
    unfold DeadlockFree; intros Ls Π' steps.
    destruct (StepsSoundness Π Ls Π' C nms Π nempty tn C_comp ltac:(reflexivity) steps)
      as [Rs [C2 [Π2' [eq2 [csteps [lnds eqRs]]]]]].
    pose proof (C.StepsPreservation Γ Δ C τ typing Rs [] C2 csteps).
    pose proof (C.PirExprClosedAfterSteps Rs [] C C2 C_closed csteps).
    pose proof (C.Progress C2 Γ Δ τ H0 H).
    destruct H1 as [C2_val | H1].
    - left. pose proof (ValuesToValueSystem C2 nms Π2' C2_val eq2).
      apply LessNondetValSystem in lnds; auto. eapply EqValSystem; eauto.
    - destruct H1 as [R [C' cstep']].
      assert (forall l, C.InvolvedWithRedex R l -> In l nms).
      intros l inv. pose proof (C.InvolvedWithLocsInPirExpr R [] C2 C' cstep' l inv).
      eapply C.LocsInPirExprAfterSteps with (C1 := C) in H1; eauto.
      pose proof (FullCompleteness R [] C2 C' Π2' nms eq2 cstep' H1)
        as [Π3 [Π3' [eq3 [step3 lnds3]]]].
      right. exists (RedexToSystemLabel R).
      destruct (LiftSystemNondetStep Π2' (RedexToSystemLabel R) Π3' Π' step3 lnds)
        as [Π22 [lnds' step4]].
      exists Π22; auto.
  Qed.    

End EndPointProjection.
