Require Export Locations LocationMap.

Require Import Coq.Structures.Orders Coq.Structures.Equalities.
Require Import Coq.Classes.RelationClasses.
Require Import Coq.Lists.SetoidList.
Require Import Coq.FSets.FMapInterface.
Require Import Coq.Program.Wf.

(*
  Implements LocationMap via a function with a (tracked) finite domain.
 *)
Module FunLMap (L : Locations) <: (LocationMap L).

  Record funlmap (elt : Set) : Set :=
    {
    the_fun : L.t -> option elt; (* the underlying funcion *)
    dom : list L.t; (* we track the (finite) domain of the function *)
    dom_spec : forall l, In l dom -> the_fun l <> None
    }.
  Definition t := funlmap.
  Arguments the_fun {elt}.
  Arguments dom {elt}.
  Arguments dom_spec {elt}.

  Section Definitions.
    Set Implicit Arguments.
  Variable elt elt' elt'' : Set.
  Definition empty : t elt.
    refine({| the_fun := fun _ => None;
              dom := nil;
              dom_spec := _
           |}).
    intros l H; inversion H.
  Defined.

  Definition is_empty : t elt -> bool := fun m => match dom m with
                                           | nil => true
                                           | cons _ _ => false
                                           end.

  Definition add (l : L.t) (e : elt) (m : t elt) : t elt.
    refine ({|
               the_fun := fun l' =>
                            if L.eq_dec l l'
                            then Some e
                            else the_fun m l';
               dom := l :: (dom m);
               dom_spec := _
             |}).
    intros l' H. destruct H.
    destruct (L.eq_dec l l') as [_|n]; [|destruct (n H)]. intro H'; inversion H'.
    destruct (L.eq_dec l l') as [eq|n]; subst; [intro H'; inversion H'|].
    apply (dom_spec m); auto.
  Defined.

  (*
    We define this boolean in function for locations as a convenience. We then use it
    to define find for maps.
   *)
  Fixpoint inb (l : L.t) (ℓ : list L.t) : bool :=
    match ℓ with
    | nil => false
    | l' :: ℓ => if L.eq_dec l l'
               then true
               else inb l ℓ
    end.

  Lemma inb_spec : forall l ℓ, inb l ℓ = true <-> In l ℓ.
  Proof using.
    intros l ℓ; split; [intro eq | intro i]; induction ℓ as [|l' ℓ]; cbn in *; auto.
    inversion eq.
    destruct (L.eq_dec l l') as [eq_l_l' | neq]; [left; symmetry; auto| right; apply IHℓ; auto].
    destruct i as [eq | i].
    destruct (L.eq_dec l l') as [_|neq]; [|destruct (neq (eq_sym eq))]; auto.
    destruct (L.eq_dec l l') as [eq | neq]; auto.
  Qed.

  Definition find (l : L.t) (m : t elt) : option elt :=
    if inb l (dom m) then the_fun m l else None.

  (*
    We start by focusing on removing a location from a list. We prove several results about
    this list operation before using it to define the operation on maps.
   *)
  Fixpoint remove_location (ℓ : list L.t) (l : L.t) : list L.t :=
    match ℓ with
    | nil => nil
    | l' :: ℓ => if L.eq_dec l l'
               then remove_location ℓ l
               else l' :: remove_location ℓ l
    end.

  Lemma remove_location_lift_In : forall ℓ l l', In l' (remove_location ℓ l) -> In l' ℓ.
  Proof using.
    intro ℓ; induction ℓ as [|l'' ℓ]; intros l l'; cbn; auto; intro i.
    destruct (L.eq_dec l l'') as [eq|neq]; subst.
    right; eapply IHℓ; eauto.
    destruct i; [left; auto|right; eapply IHℓ; eauto].
  Qed.

  Lemma remove_location_In_or_l : forall ℓ l l', In l ℓ -> In l (remove_location ℓ l') \/ l = l'.
  Proof using.
    intro ℓ; induction ℓ as [|l'' ℓ]; intros l l'; cbn; auto; intro i.
    destruct (L.eq_dec l' l''); destruct i; subst; auto.
    left; left; reflexivity.
    pose proof (IHℓ l l' H) as H0; destruct H0; auto.
    left; right; auto.
  Qed.

  Lemma l_not_in_remove_l : forall ℓ l, ~In l (remove_location ℓ l).
  Proof using.
    intro ℓ; induction ℓ as [|l' ℓ]; intros l i; auto; cbn in i.
    destruct (L.eq_dec l l'); subst. destruct (IHℓ l' i).
    destruct i as [eq | i]; [destruct (n (eq_sym eq))| destruct (IHℓ l i)].
  Qed.

  Lemma remove_l_not_l : forall ℓ l l', l<> l' -> In l ℓ -> In l (remove_location ℓ l').
  Proof using.
    intro ℓ; induction ℓ as [|l'' ℓ]; intros l l' neq i; cbn in *; auto.
    destruct (L.eq_dec l' l'') as [e |n]; subst.
    destruct i as [eq | i]; [subst; destruct (neq eq_refl)|]. apply IHℓ; auto.
    destruct i as [eq | i]; [subst; left; reflexivity|right; apply IHℓ; auto].
  Qed.


  (*
    To remove a location, we both remove it from the domain of our function and we ensure
    that the underlying function returns None. We use refine here so that we can use tactics
    to prove dom_spec still holds. This will be a common pattern throughout this module.
   *)
  Definition remove (l : L.t) (m : t elt) : t elt.
    refine ({|
               the_fun := fun l' => if L.eq_dec l l'
                                 then None
                                 else the_fun m l';
               dom := remove_location (dom m) l;
               dom_spec := _
             |}).
    intros l' H.
    destruct (L.eq_dec l l') as [eq|neq]; subst.
    apply l_not_in_remove_l in H; destruct H.
    apply remove_location_lift_In in H. apply (dom_spec m); auto.
  Defined.
  
  Definition mem (l : L.t) (m : t elt) : bool := inb l (dom m).

  Program Fixpoint elements' (f : L.t -> option elt) (ls : list L.t) :
    option (list (L.t * elt)) :=
    match ls with
    | nil => Some nil
    | l :: ls => match f l, elements' f ls with
               | Some e, Some els' => Some ((l, e) :: els')
               | _, _ => None
               end
    end.

  Lemma dom_elements' : forall f ls,
      (forall l, In l ls -> f l <> None) -> elements' f ls <> None.
  Proof using.
    intros f ls; induction ls; intro dom; cbn; [discriminate|].
    destruct (f a) eqn:eq.
    destruct (elements' f ls) eqn:eq'; [discriminate|].
    apply IHls. intros l i. apply dom. right; auto.
    exfalso; apply (dom a ltac:(left; reflexivity)); auto.
  Qed.    

  Lemma In_elements' : forall f ls l e els',
      elements' f ls = Some els' ->
      List.In (l,e) els' <-> In l ls /\ f l = Some e.
  Proof using.
    intros f ls; induction ls as [|a ls]; intros l e els' eq_els'.
    all: split; [intro i | intros [i eq]].
    - cbn in eq_els'; inversion eq_els'; subst; inversion i.
    - inversion i.
    - cbn in eq_els'. destruct (f a) eqn:eqa. destruct (elements' f ls) eqn:eqls.
      inversion eq_els'; subst; clear eq_els'. destruct i.
      inversion H; subst; clear H. split; [left|]; auto.
      destruct (proj1 (IHls l e l0 eq_refl) H) as [i eq]; split; [right|]; auto.
      inversion eq_els'. inversion eq_els'.
    - destruct i as [H| i]; subst.
      -- cbn in eq_els'. destruct (f l) eqn:eql. destruct (elements' f ls) eqn:eqls.
         all: try discriminate. inversion eq_els'; subst; clear eq_els'.
         inversion eq; subst; clear eq. left; reflexivity.
      -- cbn in eq_els'. destruct (f a) eqn:eql. destruct (elements' f ls) eqn:eqls.
         all: try discriminate. inversion eq_els'; subst; clear eq_els'.
         right. apply (IHls l e l0 eq_refl); split; auto.
  Qed.

  Fixpoint RemoveDups (ls : list L.t) : list L.t :=
    match ls with
    | nil => nil
    | l :: ls => if List.In_dec L.eq_dec l ls
               then RemoveDups ls
               else l :: RemoveDups ls
    end.

  Lemma InRemoveDups : forall l ls, In l ls <-> In l (RemoveDups ls).
  Proof using.
    intros l ls; revert l; induction ls as [| l' ls]; intro l; cbn; split; intro i; auto.
    destruct i as [eq | i]; subst.
    destruct (in_dec L.eq_dec l ls). apply IHls; auto. left; auto.
    destruct (in_dec L.eq_dec l' ls). apply IHls; auto. right; apply IHls; auto.
    destruct (in_dec L.eq_dec l' ls). right; apply IHls; auto.
    destruct i as [eq | i]; subst. left; auto. right; apply IHls; auto.
  Qed.

  Lemma RemoveDupsNoDup : forall ls, NoDup (RemoveDups ls).
  Proof using.
    intros ls; induction ls as [| l ls]; cbn. constructor.
    destruct (in_dec L.eq_dec l ls); auto. constructor; auto. rewrite <- InRemoveDups; auto.
  Qed.
                         
  Definition elements (m : t elt) : list (L.t * elt).
    destruct (elements' (the_fun m) (RemoveDups (dom m))) as [els|] eqn:eq; [exact els|].
    apply dom_elements' in eq; [destruct eq|].
    intro l; rewrite <- InRemoveDups; apply (dom_spec m).
  Defined.

  Definition cardinal (m : t elt) : nat := length (dom m).

  Definition map (f : elt -> elt') (m : t elt) : t elt'.
    refine ({|
               the_fun := fun l => match the_fun m l with
                                | Some e => Some (f e)
                                | None => None
                                end;
               dom := dom m;
               dom_spec := _
             |}).
    intros l i. destruct (the_fun m l) eqn:eq; [intro H; inversion H|].
    apply (dom_spec m l) in eq; auto.
  Defined.

  Definition mapi (f : L.t -> elt -> elt') (m : t elt) : t elt'.
    refine ({|
               the_fun := fun l => match the_fun m l with
                                | Some e => Some (f l e)
                                | None => None
                                end;
               dom := dom m;
               dom_spec := _
             |}).
    intros l i. destruct (the_fun m l) eqn:eq; [intro H; inversion H|].
    apply (dom_spec m l) in eq; auto.
  Defined.

  Definition map2 (f : option elt -> option elt' -> option elt'') (m1 : t elt) (m2 : t elt') : t elt''.
    refine (let g := fun l => match the_fun m1 l, the_fun m2 l with
                                | None, None => None
                                | _, _ => f (the_fun m1 l) (the_fun m2 l)
                           end
            in {|
               the_fun := g;
               dom := (fix h (ls : list L.t) : list L.t :=
                         match ls with
                         | nil => nil
                         | l :: ls => match g l with
                                    | Some _ => l :: h ls
                                    | None => h ls
                                    end
                         end) (dom m1 ++ dom m2);
               dom_spec := _
             |}).
    intros l i.
    induction (dom m1 ++ dom m2). inversion i.
    destruct (g a) eqn:eq.
    destruct i; subst. rewrite eq; intro eq'; inversion eq'.
    1,2: apply IHl0; auto.
  Defined.

  Fixpoint subsetlistb (ls1 ls2: list L.t) : bool :=
    match ls1 with
    | nil => true
    | l :: ls1' => if inb l ls2
                 then subsetlistb ls1' ls2
                 else false
    end.

  Lemma subsetlistb_spec : forall ls1 ls2 : list L.t, subsetlistb ls1 ls2 = true <-> forall l, In l ls1 -> In l ls2.
  Proof using.
    intros ls1; induction ls1 as [|l ls1']. all: intro ls2; split; [intro eq | intros sub];
                                              auto.
    intros l i; inversion i.
    intros l' i; destruct i; subst; cbn in *.
    destruct (inb l' ls2) eqn:eq';[apply inb_spec in eq'; auto|inversion eq].
    destruct (inb l ls2) eqn:eq'; [|inversion eq];
      rewrite (IHls1' ls2) in eq; apply eq; auto.
    cbn in *.
    destruct (inb l ls2) eqn:eq.
    rewrite IHls1'; intros l' i; apply sub; right; auto.
    pose proof (proj2 (inb_spec l ls2) (sub l ltac:(left; reflexivity))).
    transitivity (inb l ls2); auto.
  Qed.

  Definition equal (eql : elt -> elt -> bool) (m1 m2 : t elt) : bool.
    destruct (subsetlistb (dom m1) (dom m2)) eqn:sub1; [|exact false].
    destruct (subsetlistb (dom m2) (dom m1)) eqn:sub2; [|exact false].
    assert (forall l, In l (dom m1) -> the_fun m2 l <> None)
      by (intros l H; apply (dom_spec m2);
          apply subsetlistb_spec with (ls1 := dom m1); auto).
    refine ((fix f (ls : list L.t) (dom1 : forall l, In l ls -> the_fun m1 l <> None) (dom2 : forall l, In l ls -> the_fun m2 l <> None) : bool :=
               match ls as ls' return ls = ls' -> bool with
               | nil => fun _ => true
               | l :: ls => fun eq => (match the_fun m1 l as oe1 return the_fun m1 l = oe1 -> bool with
                                 | Some e1 => fun eq1 =>
                                   (match the_fun m2 l as oe2 return the_fun m2 l = oe2 -> bool with
                                   | Some e2 => fun eq2 => eql e1 e2 && f ls _ _
                                   | None => fun eq2 => _
                                   end) eq_refl
                                 | None => fun eq1 => _
                                 end) eq_refl
               end eq_refl) (dom m1) (dom_spec m1) H); subst.
    intros l0 H0; apply dom1; right; auto.
    intros l0 H0; apply dom2; right; auto.
    apply dom2 in eq2; [destruct eq2| left; reflexivity].
    apply dom1 in eq1; [destruct eq1|left; reflexivity].
  Defined.

  End Definitions.

  Definition MapsTo (elt : Set) (l : L.t) (e : elt) (m : t elt) : Prop := In l (dom m) /\ the_fun m l = Some e.
  Definition In (elt : Set) (k : L.t) (m : t elt) : Prop := exists e : elt, MapsTo k e m.
  Definition Empty (elt : Set) m := forall (a : L.t) (e : elt), ~ MapsTo a e m.
  Definition eq_key (elt : Set) (p p' : L.t * elt) := fst p = fst p'.
  Definition eq_key_elt (elt : Set) (p p' : L.t * elt) := fst p = fst p' /\ snd p = snd p'.

  Theorem mem_1 : forall (elt : Set) (m : t elt) (x : L.t), In x m -> mem x m = true.
    intros elt m x H. unfold mem. unfold In in H. unfold MapsTo in H.
    destruct H as [e [i eq]]. apply inb_spec; auto.
  Qed.
    
  Theorem mem_2 : forall (elt : Set) (m : t elt) (x : L.t), mem x m = true -> In x m.
  Proof using.
    intros elt m x H.
    unfold mem in H; apply inb_spec in H; auto.
    unfold In; unfold MapsTo.
    destruct (the_fun m x) eqn:eq; [|destruct (dom_spec m x H eq)].
    exists e; split; auto.
  Qed.

  Theorem empty_1 : forall (elt : Set), Empty (empty elt).
  Proof using.
    unfold Empty; unfold MapsTo; intros elt a e; cbn; intro H; destruct H; auto.
  Qed.

  Theorem is_empty_1 : forall (elt : Set) (m : t elt), Empty m -> is_empty m = true.
  Proof using.
    intros elt m H; unfold Empty in H. destruct m; cbn in *.
    induction dom0; cbn in *; auto.
    destruct (the_fun0 a) eqn:eq; [|destruct (dom_spec0 a ltac:(left;reflexivity) eq)].
    exfalso; apply (H a e); unfold MapsTo; cbn; split.
    left; reflexivity. exact eq.
  Qed.

  Theorem is_empty_2 : forall (elt : Set) (m : t elt), is_empty m = true -> Empty m.
  Proof using.
    unfold is_empty; unfold Empty; unfold MapsTo; intros elt m H e0 l.
    pose proof (dom_spec m); destruct (dom m).
    - intro H'; destruct H' as [i _]; inversion i.
    - inversion H.
  Qed.

  Theorem add_1 : forall (elt : Set) (m : t elt) (x : L.t) (e : elt), MapsTo x e (add x e m).
  Proof using.
    intros elt m x e; unfold MapsTo; unfold add; cbn; split.
    left; reflexivity. destruct (L.eq_dec x x) as [_|n]; [reflexivity|destruct (n eq_refl)].
  Qed.
    
  Theorem add_2 : forall (elt : Set) (m : t elt) (x y : L.t) (e e' : elt),
      x <> y -> MapsTo y e m -> MapsTo y e (add x e' m).
  Proof using.
    intros elt m x y e e' n mt.
    unfold MapsTo; destruct mt as [i eq]; unfold add; cbn; split; auto.
    destruct (L.eq_dec x y) as [eqxy|_]; [destruct (n eqxy)|]; auto.
  Qed.
    
  Theorem add_3 : forall (elt : Set) (m : t elt) (x y : L.t) (e e' : elt),
      x <> y -> MapsTo y e (add x e' m) -> MapsTo y e m.
  Proof using.
    intros elt m x y e e'; unfold MapsTo; unfold add; cbn; intros neq mt; destruct mt as [[H | i] eq];
      [destruct (neq H)|].
    destruct (L.eq_dec x y) as [H|_]; [destruct (neq H)|].
    split; auto.
  Qed.

  Theorem remove_1 : forall (elt : Set) (m : t elt) (x : L.t), ~ In x (remove x m).
  Proof using.
    intros elt m x; unfold In. unfold MapsTo; unfold remove; cbn.
    intro H; destruct H as [e'' [i eq]]; destruct (L.eq_dec x x) as [_|n];
      [inversion eq | destruct (n eq_refl)].
  Qed.

  Theorem remove_2 : forall (elt : Set) (m : t elt) (x y : L.t) (e : elt),
      x <> y -> MapsTo y e m -> MapsTo y e (remove x m).
  Proof using.
    intros elt m x y e;
      unfold MapsTo; unfold remove; cbn; intros neq mt; destruct mt as [i eq]; split.
    apply remove_l_not_l; auto.
    destruct (L.eq_dec x y) as [H|_]; [destruct (neq H)|]; auto.
  Qed.

  Theorem remove_3 : forall (elt : Set) (m : t elt) (x y : L.t) (e : elt),
      x <> y  -> MapsTo y e (remove x m) -> MapsTo y e m.
  Proof using.
    intros elt m x y e;
    unfold MapsTo; unfold remove; cbn; intros neq mt; destruct mt as [i eq]; split.
    apply remove_location_lift_In in i; auto.
    destruct (L.eq_dec x y); [inversion eq| auto].
  Qed.

  Theorem find_1 : forall (elt : Set) (m : t elt) (x : L.t) (e : elt),
      MapsTo x e m -> find x m = Some e.
  Proof using.
    intros elt m x e;
    unfold MapsTo; unfold find; cbn; intro mt; destruct mt as [i eq]; auto.
    rewrite <- inb_spec in i; rewrite i; auto.
  Qed.

  Theorem find_2 : forall (elt : Set) (m : t elt) (x : L.t) (e : elt),
      find x m = Some e -> MapsTo x e m.
  Proof using.
    intros elt m x e;
    unfold find; unfold MapsTo; split.
    destruct (inb x (dom m)) eqn:eq; [apply inb_spec; auto | inversion H].
    destruct (inb x (dom m)) eqn:eq; [ auto | inversion H].
  Qed.

  Theorem elements_1 : forall (elt : Set) (m : t elt) (x : L.t) (e : elt),
      MapsTo x e m -> List.In (x, e) (elements m).
  Proof using.
    intros elt m x e mt. unfold MapsTo in mt; destruct mt as [i eq].
    unfold elements.
    generalize (fun eq0 => match  dom_elements' (the_fun m) (RemoveDups (dom m))
             (fun l : L.t =>
              subrelation_proper Morphisms_Prop.iff_iff_iff_impl_morphism tt
                (subrelation_respectful (subrelation_refl iff)
                   (subrelation_respectful (subrelation_refl iff)
                      iff_flip_impl_subrelation)) (List.In l (RemoveDups (dom m)))
                (List.In l (dom m)) (symmetry (InRemoveDups l (dom m)))
                (the_fun m l <> None) (the_fun m l <> None)
                (reflexive_proper_proxy iff_Reflexive (the_fun m l <> None))
                (dom_spec m l)) eq0 return (list (L.t * elt)) with end).
    generalize (@eq_refl _ (elements' (the_fun m)(RemoveDups (dom m)))) at 0.
    generalize (elements' (the_fun m) (RemoveDups (dom m))) at 2 3 4 5 6 7.
    intros o eq'' l. destruct o.  
    rewrite In_elements' with (f := the_fun m) (ls := RemoveDups (dom m)); auto.
    split; auto. rewrite <- InRemoveDups; auto.
    exfalso; apply dom_elements' in eq''; auto.
    intros l0 H. rewrite <- InRemoveDups in H; apply (dom_spec m); auto.
  Qed.    

  Theorem elements_2 : forall (elt : Set) (m : t elt) (x : L.t) (e : elt),
      List.In (x, e) (elements m) -> MapsTo x e m.
  Proof using.
    intros elt m x e. unfold MapsTo; unfold elements.
    generalize (fun eq0 => match            dom_elements' (the_fun m) (RemoveDups (dom m))
             (fun l : L.t =>
              subrelation_proper Morphisms_Prop.iff_iff_iff_impl_morphism tt
                (subrelation_respectful (subrelation_refl iff)
                   (subrelation_respectful (subrelation_refl iff)
                      iff_flip_impl_subrelation)) (List.In l (RemoveDups (dom m)))
                (List.In l (dom m)) (symmetry (InRemoveDups l (dom m)))
                (the_fun m l <> None) (the_fun m l <> None)
                (reflexive_proper_proxy iff_Reflexive (the_fun m l <> None))
                (dom_spec m l)) eq0 return (list (L.t * elt)) with end). generalize (@eq_refl _ (elements' (the_fun m) (RemoveDups (dom m)))) at 0.
    generalize (elements' (the_fun m) (RemoveDups (dom m))) at 2 3 4 5 6 7.
    intros o eq l; destruct o.
    rewrite In_elements' with (f := the_fun m) (ls := RemoveDups (dom m)); auto.
    intro i; destruct i; split; auto. apply InRemoveDups; auto.
    exfalso; apply dom_elements' in eq; auto. intro l0; rewrite <- InRemoveDups; apply dom_spec.
  Qed.

  Theorem elements'_3w : forall (elt : Set) f ls ls',
      NoDup ls ->
      elements' f ls = Some ls' ->
      NoDupA (@eq_key elt) ls'.
  Proof using.
    intros elt f ls; induction ls as [|l ls]; intros ls' nd eq; cbn in *.
    inversion eq; subst; clear eq. constructor.
    destruct (f l) eqn:eql; destruct (elements' f ls) eqn:eqls; try discriminate.
    inversion eq; clear eq; subst. constructor.
    all: inversion nd; subst. 2: apply IHls; auto.
    intro i. apply InA_altdef in i. apply Exists_exists in i.
    destruct i as [[l' e'] [i ek]].
    pose proof (proj1 (In_elements' f ls l' e' eqls) i) as H; destruct H.
    unfold eq_key in ek; cbn in ek; subst. apply H1; auto.
  Qed.
    
  Theorem elements_3w : forall (elt : Set) (m : t elt),
      NoDupA (@eq_key elt) (elements m).
  Proof using.
    intros elt m; unfold elements.
    generalize (fun eq => match dom_elements' (the_fun m) (RemoveDups (dom m))
                                           (fun l : L.t =>
                                              subrelation_proper Morphisms_Prop.iff_iff_iff_impl_morphism tt
                                                                 (subrelation_respectful (subrelation_refl iff)
                                                                                         (subrelation_respectful (subrelation_refl iff)
                                                                                                                 iff_flip_impl_subrelation)) (List.In l (RemoveDups (dom m)))
                                                                 (List.In l (dom m)) (symmetry (InRemoveDups l (dom m)))
                                                                 (the_fun m l <> None) (the_fun m l <> None)
                                                                 (reflexive_proper_proxy iff_Reflexive (the_fun m l <> None))
                                                                 (dom_spec m l)) eq return (list (L.t * elt))

                       with end).
    generalize (@eq_refl _ (elements' (the_fun m) (RemoveDups (dom m)))) at 0.
    generalize (elements' (the_fun m) (RemoveDups (dom m))) at 2 3 4 5 6 7.
    intros o eq l; destruct o; [|exfalso; apply dom_elements' in eq; auto].
    apply elements'_3w with (f := the_fun m) (ls := RemoveDups (dom m)); auto.
    apply RemoveDupsNoDup.
    intro l0; rewrite <- InRemoveDups; apply (dom_spec m).
  Qed.

  Definition Equal (elt : Set) (m m' : t elt) := forall y, find y m = find y m'.
  Definition Equiv (elt : Set) (eq_elt : elt -> elt -> Prop) m m' :=
    (forall k, In k m <-> In k m') /\ (forall k e e', MapsTo k e m -> MapsTo k e' m' -> eq_elt e e').
  Definition Equivb (elt : Set) (cmp : elt -> elt -> bool) := Equiv (fun e1 e2 => cmp e1 e2 = true).

  Arguments empty {elt}.
  Arguments MapsTo {elt}.
  Arguments In {elt}.
  Arguments mapi {elt elt'}.
  Arguments map2 {elt elt' elt''}.
  Arguments find {elt}.

End FunLMap.
