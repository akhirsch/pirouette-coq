Require Export LocalLang.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Lists.List.

(*
  Call-by-value lambda calculus as a local language. We use module subtyping so that we can
  export more than is exported by the LocalLang type.
 *)

Module LambdaCalc <: LocalLang.

  (*
    We have types which are simple types with base types booleans and nats. We introduce
    types here so that we can have  intensional typing for functions: lambdas have the form
    λ x : τ. e
    In this module, we ignore the τ.

    Equality is easily decidable for these types.
   *)
  Inductive SimpleType : Set :=
    boolType : SimpleType
  | natType : SimpleType
  | ArrowT : SimpleType -> SimpleType -> SimpleType.

  Definition SimpleTypeEqDec : forall tau sigma : SimpleType, {tau = sigma} + {tau <> sigma}.
    decide equality.
  Qed.

  (*
    Our λ calculus contains standard λ-calculus constructs along with true, false, zero, 
    successor, fixpoints and if-then-else. As promised, our abstraction syntax contains a 
    type for the bound variable.
   *)
  Inductive LC :=
  | var  : nat -> LC
  | ttLC : LC
  | ffLC : LC
  | zero : LC
  | succ : LC -> LC
  | fixP : LC -> LC
  | ite  : LC -> LC -> LC -> LC
  | app  : LC -> LC -> LC
  | abs  : SimpleType -> LC -> LC.

  (* 
     Due to a Coq quirk, we have to give this sort of definition whenever we want to use an
     inductive type for something required by a module type.
   *)
  Definition Expr := LC.

  
  Definition ExprEqDec : forall e1 e2 : Expr, {e1 = e2} + {e1 <> e2}.
  Proof.
    decide equality. apply Nat.eq_dec. apply SimpleTypeEqDec.
  Qed.
  Definition ExprVar : nat -> Expr := var.


  (*
    A λ-calculus program is closed above n if it does not contain free variables above n.
    When moving across an abstraction boundary, we add one to n to account for the fact
    that we must count back to another abstraction barrier before getting to free 
    variables.
   *)
  Fixpoint ExprClosedAbove (n : nat) (e : LC) : Prop :=
    match e with
    | var m => if m <? n then True else False
    | ttLC => True
    | ffLC => True
    | zero => True
    | succ e' => ExprClosedAbove n e'
    | fixP e' => ExprClosedAbove n e'
    | ite b e1 e2 => ExprClosedAbove n b /\ ExprClosedAbove n e1 /\ ExprClosedAbove n e2
    | app e1 e2 => ExprClosedAbove n e1 /\ ExprClosedAbove n e2                                
    | abs τ e' => ExprClosedAbove (S n) e'
    end.

  (* 
     Annoyingly, Coq requires that we repeat definitions and lemmas from the module type
     into modules of that type.
   *)
  Definition ExprClosed := ExprClosedAbove 0.

  (*
    Values are as standard in CBV λ calculus, _except that functions must be closed
    in order to be values_.
   *)
  Inductive LCVal : LC -> Prop :=
  | ttVal  : LCVal ttLC
  | ffVal  : LCVal ffLC
  | zeroVal : LCVal zero
  | succVal : forall e : Expr, LCVal e -> LCVal (succ e)
  | AbsVal : forall (τ : SimpleType) (e : LC), ExprClosedAbove 1 e -> LCVal (abs τ e).
  Definition ExprVal := LCVal.

  Lemma ExprValuesClosed : forall v : Expr, ExprVal v -> ExprClosed v.
  Proof.
    intros v val_v; induction val_v; unfold ExprClosed; simpl; auto.
  Qed.

  Definition ExprUpRename : (nat -> nat) -> nat -> nat :=
    fun ξ n => match n with
            | 0 => 0
            | S n' => S (ξ n')
            end.

  (*
    We define renaming separately from substitution. This makes proving several properties
    easier. This especiallly applies to the fusion properties.
   *)
  Fixpoint ExprRename (e : Expr) (ξ : nat -> nat) : Expr :=
    match e with
    | var n => var (ξ n)
    | ttLC => ttLC
    | ffLC => ffLC
    | zero => zero
    | succ e' => succ (ExprRename e' ξ)
    | fixP e' => fixP (ExprRename e' ξ)
    | ite b e1 e2 => ite (ExprRename b ξ) (ExprRename e1 ξ) (ExprRename e2 ξ)
    | app e1 e2 => app (ExprRename e1 ξ) (ExprRename e2 ξ)
    | abs τ e => abs τ (ExprRename e (ExprUpRename ξ))
    end.
  Notation "e ⟨e| ξ ⟩" := (ExprRename e ξ) (at level 29).

  Lemma ExprRenameExt : forall (e : Expr) (ξ1 ξ2 : nat -> nat),
      (forall n, ξ1 n = ξ2 n) -> e ⟨e| ξ1⟩ = e ⟨e| ξ2⟩.
  Proof.
    intro e; induction e; intros ξ1 ξ2 ext_eq; simpl; auto.
    1,2: rewrite IHe with (ξ2 := ξ2); auto.
    1,2: rewrite IHe1 with (ξ2 := ξ2); auto; rewrite IHe2 with (ξ2 := ξ2); auto.
    - rewrite IHe3 with (ξ2 := ξ2); auto.
    - rewrite IHe with (ξ2 := ExprUpRename ξ2); auto.
      intro n; unfold ExprUpRename; destruct n; auto.
  Qed.
  
  Definition ExprUpSubst : (nat -> Expr) -> nat -> Expr :=
    fun σ n => match n with
            | 0 => var 0
            | S n' => σ n' ⟨e| S ⟩
            end.
  Fixpoint ExprSubst (e : Expr) (σ : nat -> Expr) : Expr :=
    match e with
    | var n => σ n
    | ttLC => ttLC
    | ffLC => ffLC
    | zero => zero
    | succ e' => succ (ExprSubst e' σ)
    | fixP e' => fixP (ExprSubst e' σ)
    | ite b e1 e2 => ite (ExprSubst b σ) (ExprSubst e1 σ) (ExprSubst e2 σ)
    | app e1 e2 => app (ExprSubst e1 σ) (ExprSubst e2 σ)
    | abs τ e => abs τ (ExprSubst e (ExprUpSubst σ))
    end.
  Notation "e [e| σ ]" := (ExprSubst e σ) (at level 29).

  Lemma ExprSubstExt : forall (e : Expr) (σ1 σ2 : nat -> Expr),
      (forall n, σ1 n = σ2 n)
      -> e [e| σ1] = e [e| σ2].
  Proof.
    intro e; induction e; intros σ1 σ2 ext_eq; simpl; auto.
    1,2: rewrite IHe with (σ2 := σ2); auto.
    1,2: rewrite IHe1 with (σ2 := σ2); auto; rewrite IHe2 with (σ2 := σ2); auto.
    - rewrite IHe3 with (σ2 := σ2); auto.
    - rewrite IHe with (σ2 := ExprUpSubst σ2); auto.
      intro n; unfold ExprUpSubst; destruct n; simpl; auto.
      rewrite ext_eq; reflexivity.
  Qed.
  
  Lemma ExprRenameSpec : forall (e : Expr) (ξ : nat -> nat),
      e ⟨e| ξ⟩ = e [e| fun n => ExprVar (ξ n)].
  Proof.
    intros e; induction e; intro ξ; simpl; auto.
    1,2: rewrite IHe; reflexivity.
    1,2: rewrite IHe1; rewrite IHe2; try (rewrite IHe3); reflexivity.
    rewrite IHe. unfold ExprUpSubst. unfold ExprUpRename.
    erewrite ExprSubstExt; [reflexivity |].
    intro n; destruct n; simpl; auto.
  Qed.

  (*
    ExprSubstVar is true judgmentally, so we only need to provide eq_refl.
    
    We need to repeat Lemmas in modules, just like definitions. We thus just
    copy-and-paste the definition of ExprRenameVar into this file.
   *)
  Definition ExprSubstVar : forall n σ, (ExprVar n) [e| σ] = σ n := fun n σ => eq_refl.
  Lemma ExprRenameVar : forall n ξ, (ExprVar n) ⟨e| ξ ⟩ = ExprVar (ξ n).
  Proof.
    intros n ξ.
    rewrite ExprRenameSpec. rewrite ExprSubstVar. reflexivity.
  Qed.
  
  Lemma ExprRenameFusion : forall (e : Expr) (ξ1 ξ2 : nat -> nat),
      (e ⟨e| ξ1⟩) ⟨e| ξ2⟩ = e ⟨e| fun n => ξ2 (ξ1 n)⟩.
  Proof.
    intro e; induction e; intros ξ1 ξ2; simpl; auto.
    1,2,5: rewrite IHe; try (reflexivity).
    2,3: rewrite IHe1; rewrite IHe2; try (rewrite IHe3); reflexivity.
    rewrite ExprRenameExt with (ξ2 := ExprUpRename (fun n => ξ2 (ξ1 n))); auto.
    intro n; unfold ExprUpRename; destruct n; simpl; auto.
  Qed.

  Lemma ExprRenameSubstFusion : forall e ξ σ,
      (e ⟨e| ξ⟩) [e|σ] = e [e| fun n => σ (ξ n)].
  Proof using.
    intro e; induction e; intros ξ σ; cbn; auto.
    all: try (rewrite IHe; auto). 1,2: rewrite IHe1; rewrite IHe2; auto.
    rewrite IHe3; reflexivity.
    rewrite ExprSubstExt with (σ2 := ExprUpSubst (fun n => σ (ξ n))); auto.
    intro n; unfold ExprUpSubst; unfold ExprUpRename; destruct n; cbn; auto.
  Qed.

  Definition ExprIdSubst : nat -> Expr := fun n => ExprVar n.

  Lemma ExprIdentitySubstSpec : forall (e : Expr), e [e| ExprIdSubst] = e.
  Proof.
    intro e; induction e; simpl; auto.
    3,4: rewrite IHe1; rewrite IHe2; try (rewrite IHe3); reflexivity.
    1,2: rewrite IHe; auto.
    rewrite <- IHe at 2. erewrite ExprSubstExt; [reflexivity |].
    intro n; unfold ExprUpSubst; unfold ExprIdSubst; destruct n; auto.
  Qed.

  Definition ExprIdRenaming : nat -> nat := fun n => n.
  Lemma ExprIdRenamingSpec : forall (e : Expr), e ⟨e| ExprIdRenaming ⟩ = e.
  Proof.
    intro e; induction e; simpl; auto.
    3,4: rewrite IHe1; rewrite IHe2; try (rewrite IHe3); reflexivity.
    1,2: rewrite IHe; reflexivity.
    rewrite <- IHe at 2. erewrite ExprRenameExt; [reflexivity |].
    intro n; unfold ExprUpRename; unfold ExprIdRenaming; destruct n; auto.
  Qed.
    
  Lemma ExprUpSubstId : forall n, ExprIdSubst n = (ExprUpSubst ExprIdSubst) n.
  Proof.
    intros n; unfold ExprUpSubst; destruct n.
    - unfold ExprIdSubst; reflexivity.
    - unfold ExprIdSubst. rewrite ExprRenameVar. reflexivity.
  Qed.
  Lemma ExprUpRenamingId : forall n, ExprIdRenaming n = ExprUpRename ExprIdRenaming n.
  Proof.
    intros n; unfold ExprUpRename; destruct n; unfold ExprIdRenaming; reflexivity.
  Qed.

  (* 
     As with inductive types, so with constructors. Thus, due to this quirk of Coq, we need
     to provide a definition whenever we want to provide a constructor as the definition
     of a parameter of the module type.
   *)
  Definition tt := ttLC.
  Definition ff := ffLC.
  Lemma ttValue : ExprVal tt. Proof. constructor. Qed.
  Lemma ffValue : ExprVal ff. Proof. constructor. Qed.

  (*
    This substitution represents replacing the first free variable with e, and then
    moving every other variable "down one" to make up for that.
   *)
  Definition LCStepSubst : Expr -> (nat -> Expr) :=
    fun e n => match n with
            | 0 => e
            | S n' => ExprVar n'
            end.
  
  Inductive LCStep : Expr -> Expr -> Prop :=
  | succStep : forall (e1 e2 : Expr),
      LCStep e1 e2
      -> LCStep (succ e1) (succ e2)
  | iteEStep : forall (b1 b2 e1 e2 : Expr),
      LCStep b1 b2
      -> LCStep (ite b1 e1 e2) (ite b2 e1 e2)
  | iteTTStep : forall (e1 e2 : Expr),
      LCStep (ite tt e1 e2) e1
  | iteFFStep : forall (e1 e2 : Expr),
      LCStep (ite ff e1 e2) e2
  | fixPStep : forall (e : Expr),
      LCStep (fixP e) (app e (fixP e))
  | appAbsStep : forall (τ : SimpleType) (e1 e2 : Expr),
      ExprVal e2
      -> LCStep (app (abs τ e1) e2) (ExprSubst e1 (LCStepSubst e2))
  | appFStep : forall e1 e1' e2 : Expr,
      LCStep e1 e1'
      -> LCStep (app e1 e2) (app e1' e2)
  | appArgStep : forall e1 e2 e2' : Expr,
      LCStep e2 e2'
      -> LCStep (app e1 e2) (app e1 e2').
  Global Hint Constructors LCStep : LC.
  Definition ExprStep := LCStep.

  Theorem ExprClosedAboveSubst : forall (e : Expr) (σ : nat -> Expr) (n : nat),
      ExprClosedAbove n e -> (forall m, m < n -> σ m = ExprVar m) -> e [e|σ] = e.
  Proof using.
    intros e; induction e; intros σ m closed_b static_above; cbn in *; auto.
    - destruct m;
        [destruct closed_b | destruct (n <=? m) eqn:eq;
                             [clear closed_b |destruct closed_b]].
      apply Nat.leb_le in eq. rewrite static_above; auto. apply Lt.le_lt_n_Sm; auto.
    - erewrite IHe; eauto.
    - erewrite IHe; eauto.
    - destruct closed_b as [H1 [H2 H3]];
        erewrite IHe1; eauto; erewrite IHe2; eauto; erewrite IHe3; eauto.
    - destruct closed_b as [H1 H2];
        erewrite IHe1; eauto; erewrite IHe2; eauto.
    - erewrite IHe; eauto. unfold ExprUpSubst.
      intros k lt; destruct k; auto. apply Lt.lt_S_n in lt.
      rewrite static_above; auto.
  Qed.

  Lemma ExprClosedSubst : forall (e : Expr) (σ : nat -> Expr), ExprClosed e -> e [e|σ] = e.
  Proof using.
    intros e σ closed; unfold ExprClosed in closed; apply ExprClosedAboveSubst with (n := 0);
      auto.
    intros m lt; inversion lt.
  Qed.

  Theorem ExprClosedAboveMono : forall m n e, m < n -> ExprClosedAbove m e -> ExprClosedAbove n e.
  Proof using.
    intros m n e; revert m n; induction e; intros k m l H; cbn in *; auto.
    destruct k. destruct H. destruct (n <=? k) eqn:eq. 
    destruct m. inversion l. destruct (n <=? m) eqn:eq'; auto.
    apply Nat.leb_le in eq. apply Nat.leb_gt in eq'. apply Lt.lt_S_n in l.
    pose proof (Lt.lt_le_trans m n k eq' eq).  apply Nat.lt_asymm in H0; auto.
    destruct H.
    all: try (eapply IHe; eauto).
    all: repeat match goal with
                | [ H : _ /\ _ |- _ ] => destruct H
                | [|- _ /\ _ ] => split
                end; eauto.
    apply Lt.lt_le_S in l. apply Lt.le_lt_or_eq in l; destruct l; subst; eauto.
  Qed.

  Theorem NoExprStepFromVal : forall v, ExprVal v -> forall e, ~ ExprStep v e.
  Proof using.
    intros v; induction v; intros val_v e step; inversion val_v; subst;
      inversion step; subst.
    apply (IHv H0 e2 H1).
  Qed.

  Theorem boolSeperation : tt <> ff. discriminate. Qed.

  Lemma ExprRenameClosedAbove : forall e n ξ m,
      (forall k, k < n -> ξ k < m) ->
      ExprClosedAbove n e ->
      ExprClosedAbove m (e ⟨e| ξ⟩).
  Proof using.
    intros e; induction e; cbn; try rename n into x; intros n ξ m ξ_bdd clsd_e; auto.
    all: repeat match goal with
                | [ H : _ /\ _ |- _ ] => destruct H
                | [ |- _ /\ _ ] => split
                end; auto;
      try (eapply IHe; eauto; fail);
      try (eapply IHe1; eauto; fail);
      try (eapply IHe2; eauto; fail);
      try (eapply IHe3; eauto; fail).
    - destruct n. destruct clsd_e.
      destruct (x <=? n) eqn:eq. apply Nat.leb_le in eq.
      assert (x < S n) by (apply Lt.le_lt_n_Sm; auto).
      destruct m. apply ξ_bdd in H. inversion H.
      apply ξ_bdd in H. apply Lt.lt_n_Sm_le in H.
      apply Nat.leb_le in H. rewrite H; auto.
      destruct clsd_e.
    - apply IHe with (n := S n); auto.
      intros k k_lt_Sn; unfold ExprUpRename. destruct k. apply Nat.lt_0_succ.
      apply Lt.lt_n_S. apply Lt.lt_S_n in k_lt_Sn. apply ξ_bdd; auto.
  Qed.

  Lemma ExprSubstClosedAbove : forall e n σ m,
      (forall k, k < n -> ExprClosedAbove m (σ k)) ->
      ExprClosedAbove n e ->
      ExprClosedAbove m (e [e| σ]).
  Proof using.
    intros e; induction e; cbn; try rename n into x; intros n σ m σ_clsd e_clsd; auto;
            repeat match goal with
             | [ H : _ /\ _ |- _ ] => destruct H
             | [ |- _ /\ _ ] => split
                   end; auto.
    all: try (eapply IHe; eauto; fail);
      try (eapply IHe1; eauto; fail);
      try (eapply IHe2; eauto; fail);
      try (eapply IHe3; eauto; fail).
    - destruct n. destruct e_clsd. destruct (x <=? n) eqn:eq; [|destruct e_clsd].
      apply Nat.leb_le in eq. assert (x < S n) by(apply Lt.le_lt_n_Sm; auto).
      apply σ_clsd in H; auto.
    - apply IHe with (n := S n); auto.
      intros k k_lt_Sn; unfold ExprUpSubst; destruct k; cbn; auto.
      apply ExprRenameClosedAbove with (n := m).
      intros l lt_l_m; apply Lt.lt_n_S; auto.
      apply σ_clsd; apply Lt.lt_S_n; auto.
  Qed.

  Theorem ExprClosedAfterStep : forall  e1 e2 n,
      ExprStep e1 e2 -> ExprClosedAbove n e1 -> ExprClosedAbove n e2.
  Proof using.
    intros e1 e2 n step; revert n; induction step; cbn; intros n clsd; auto;
      repeat match goal with
             | [ H : _ /\ _ |- _ ] => destruct H
             | [ |- _ /\ _ ] => split
             end; auto.
    apply ExprSubstClosedAbove with (n := S n); auto.
    intros k lt_k_Sn; unfold LCStepSubst. destruct k; auto; cbn.
    destruct n. apply Lt.lt_S_n in lt_k_Sn; inversion lt_k_Sn.
    apply Lt.lt_S_n in lt_k_Sn.
    apply Lt.lt_n_Sm_le in lt_k_Sn. apply Nat.leb_le in lt_k_Sn; rewrite lt_k_Sn; auto.
  Qed.

  Theorem ExprVarClosed : forall n m, ExprClosedAbove n (ExprVar m) <-> m < n.
    intros n m; cbn; split; intro H; destruct n; auto. destruct H.
    destruct (m <=? n) eqn:eq. apply Nat.leb_le in eq; unfold lt; apply Le.le_n_S; auto.
    destruct H.
    inversion H. destruct (m <=? n) eqn:eq; auto.
    apply Nat.leb_gt in eq. destruct m; [inversion eq|].
    apply Lt.lt_S_n in H. apply Lt.lt_n_Sm_le in eq. apply Lt.le_lt_or_eq in eq.
    destruct eq. assert (n < n) by (transitivity m; auto). apply Lt.lt_irrefl in H1; auto.
    subst; apply Lt.lt_irrefl in H; auto.
  Qed.
  
End LambdaCalc.
