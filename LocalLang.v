(* 
   Module type for the local language.

   We can instantiate this module type with any  number of languages, as we can see in the
   examples.
*)

Module Type LocalLang.

  (*
    The local language is an expression-based language. We assume that expressions have 
    decidable syntactic equality. This is a pretty weak assumption for syntax.
   *)
  Parameter Expr : Set.
  Parameter ExprEqDec : forall e1 e2 : Expr, {e1 = e2} + {e1 <> e2}.

  (* 
     Since we use de Bruijn indices, we represent variables as natural numbers. We don't
     require any sort of binders in  the local language, so these don't necessarily count 
     to the relevant binder. However, in our examples they do. 
   *)
  Parameter ExprVar : nat -> Expr.

  (* 
     An expression is closed above `n` if variables above `n` do not appear free in the
     expression. Clearly, if an expression is closed above `0`, then it contains no free
     variables.

     The property of being closed above is monotonic. Moreover, variables are always closed
     above  anything greater than themselves.
   *)
  Parameter ExprClosedAbove : nat -> Expr -> Prop.
  Definition ExprClosed := ExprClosedAbove 0.
  Parameter ExprClosedAboveMono : forall m n e, m < n -> ExprClosedAbove m e -> ExprClosedAbove n e.
  Parameter ExprVarClosed : forall n m, ExprClosedAbove n (ExprVar m) <-> m < n.

  (* 
     Values are a subset of expressions (here represented as a property of expressions. 
     These are always closed. This restriction is necessary for communication to typecheck
     in Pirouette.
   *)
  Parameter ExprVal : Expr -> Prop.
  Parameter ExprValuesClosed : forall v : Expr, ExprVal v -> ExprClosed v.
  
  (*
    Local languages need to define a(n infinite) substitution operator. This replaces all
    variables in the expression with new subexpressions. We also consider a renaming 
    operation, which allows us to substitute variables more easily with other variables. 
    This can be defined via the "specification" equation below, but it is often better to
    define it seperately.    
   *)
  Parameter ExprSubst : Expr -> (nat -> Expr) -> Expr.
  Parameter ExprRename : Expr -> (nat -> nat) -> Expr.
  Notation "e [e| sigma ]" := (ExprSubst e sigma) (at level 29).
  Notation "e ⟨e| xi ⟩" := (ExprRename e xi) (at level 29).
  Parameter ExprRenameSpec : forall (e : Expr) (ξ : nat -> nat),
      e ⟨e| ξ⟩ = e [e| fun n => ExprVar (ξ n)].

  (* 
     Substitution must correctly replace a variable. This allows us to show that renaming
     must do the same thing.
   *)
  Parameter ExprSubstVar : forall n σ, (ExprVar n ) [e| σ] = σ n.
  Lemma ExprRenameVar : forall n ξ, (ExprVar n) ⟨e| ξ ⟩ = ExprVar (ξ n).
  Proof.
    intros n ξ.
    rewrite ExprRenameSpec. rewrite ExprSubstVar. reflexivity.
  Qed.


  (* 
     Renaming in an expression twice should behave compositionally. For technical reasons, 
     we need this to prove an important property of Pirouette.
   *)
  Parameter ExprRenameFusion : forall (e : Expr) (ξ1 ξ2 : nat -> nat),
      (e ⟨e| ξ1⟩) ⟨e| ξ2⟩ = e ⟨e| fun n => ξ2 (ξ1 n)⟩.

  (* 
     Substitution must be _extensional_ in the sense that it does not rely on the
     implementation of the substitution functions. This allows us to show that renaming is
     also extensional.
   *)
  Parameter ExprSubstExt : forall (e : Expr) (σ1 σ2 : nat -> Expr),
      (forall n, σ1 n = σ2 n) -> e [e| σ1] = e [e| σ2].
  Lemma ExprRenameExt :forall (e : Expr) (ξ1 ξ2 : nat -> nat),
      (forall n, ξ1 n = ξ2 n) -> e ⟨e| ξ1 ⟩= e ⟨e| ξ2⟩.
  Proof using.
    intros e ξ1 ξ2 H.
    repeat rewrite ExprRenameSpec. apply ExprSubstExt.
    intro n; rewrite H; auto.
  Qed.

  (*
    We define identity substitution and renamings. We require that the substitution behave 
    as an  identity. From that, we can show that the renaming  also behaves as an identity.
   *)
  Definition ExprIdSubst : nat -> Expr := fun n => ExprVar n.
  Parameter ExprIdentitySubstSpec : forall (e : Expr), e [e| ExprIdSubst] = e.
  Definition ExprIdRenaming : nat -> nat := fun n => n.
  Lemma ExprIdRenamingSpec : forall (e : Expr), e ⟨e| ExprIdRenaming ⟩ = e.
  Proof using.
    intros e; rewrite ExprRenameSpec; unfold ExprIdRenaming; cbn.
    apply ExprIdentitySubstSpec.
  Qed.
  
  (* 
     Substitution with de Bruijn indices usually relies on `up` constructions on
     substitutions. These are used when going past a constructor to ensure that counting is
     done correctly.

     Since we don't define substitution explicitly here, nor do we have binders, these 
     aren't used here. However, we define them for convenience's sake. We also prove that
     they do not affect the identity substitution and renaming.
   *)
  Definition ExprUpSubst : (nat -> Expr) -> nat -> Expr :=
    fun σ n =>
      match n with
      | 0 => ExprVar 0
      | S n => (σ n) ⟨e| S ⟩
      end.

  Definition ExprUpRename : (nat -> nat) -> nat -> nat :=
    fun ξ n =>
      match n with
      | 0 => 0
      | S n => S (ξ n)
      end.

  
  Lemma ExprUpSubstId : forall n, ExprIdSubst n = (ExprUpSubst ExprIdSubst) n.
  Proof.
    intros n; unfold ExprUpSubst; destruct n.
    - unfold ExprIdSubst; reflexivity.
    - unfold ExprIdSubst. rewrite ExprRenameVar. reflexivity.
  Qed.
  Lemma ExprUpRenamingId : forall n, ExprIdRenaming n = ExprUpRename ExprIdRenaming n.
  Proof.
    intros n; unfold ExprUpRename; destruct n; unfold ExprIdRenaming; reflexivity.
  Qed.

  
  Parameter ExprClosedAboveSubst : forall (e : Expr) (σ : nat -> Expr) (n : nat),
      ExprClosedAbove n e -> (forall m, m < n -> σ m = ExprVar m) -> e [e|σ] = e.
  Lemma ExprClosedSubst : forall (e : Expr) (σ : nat -> Expr), ExprClosed e -> e [e|σ] = e.
  Proof using.
    intros e σ closed; unfold ExprClosed in closed; apply ExprClosedAboveSubst with (n := 0);
      auto.
    intros m lt; inversion lt.
  Qed.

  (*
    We require separate boolean expressions `true` and `false`. These must be separate
    expressions, and both be values.
   *)
  Parameter tt ff : Expr.
  Parameter ttValue : ExprVal tt.
  Parameter ffValue : ExprVal ff.
  Parameter boolSeperation : tt <> ff.
  
  (*
    ExprStep is the small-step semantics for the local language. We require that values not 
    be able to take a step, which reflects the fact that we are using _small_-step
    semantics.
   *)
  Parameter ExprStep : Expr -> Expr -> Prop.
  Parameter NoExprStepFromVal : forall v, ExprVal v -> forall e, ~ ExprStep v e.


  (* 
     Substitution, renaming, and stepping should not change the fact that expressions are
     closed above some level.
   *)
  Parameter ExprRenameClosedAbove : forall e n ξ m,
      (forall k, k < n -> ξ k < m) ->
      ExprClosedAbove n e ->
      ExprClosedAbove m (e ⟨e| ξ⟩).
  Parameter ExprSubstClosedAbove : forall e n σ m,
      (forall k, k < n -> ExprClosedAbove m (σ k)) ->
      ExprClosedAbove n e ->
      ExprClosedAbove m (e [e| σ]).
  Parameter ExprClosedAfterStep : forall  e1 e2 n,
      ExprStep e1 e2 -> ExprClosedAbove n e1 -> ExprClosedAbove n e2.

End LocalLang.

