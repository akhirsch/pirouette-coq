Require Export Locations.

Require Import Coq.Structures.Orders Coq.Structures.Equalities.
Require Import Coq.Classes.RelationClasses.
Require Import Coq.Lists.SetoidList.
Require Import Coq.FSets.FMapInterface.
Require Import Coq.Program.Wf.

(* Finite maps keyed by locations. *)
Module Type LocationMap (L : Locations).

  Parameter t : Set -> Set.
  Set Implicit Arguments.
  Section Types.
    Variable elt : Set.

    (* Map operations *)
    Parameter empty : t elt.
    Parameter is_empty : t elt -> bool.
    Parameter add : L.t -> elt -> t elt -> t elt.
    Parameter find : L.t -> t elt -> option elt.
    Parameter remove : L.t -> t elt -> t elt.
    Parameter mem : L.t -> t elt -> bool.
    Parameter elements : t elt -> list (L.t * elt).

    Variable elt' elt'' : Set.

    (* Specifications of the map operations. *)
    Section spec.

      Variable m m' m'' : t elt. (* Maps *)
      Variable x y z : L.t. (* Locations/Keys *)
      Variable e e' : elt. (* Elements *)

      Parameter MapsTo : L.t -> elt -> t elt -> Prop.
      Definition In (k : L.t) (m : t elt) : Prop := exists e : elt, MapsTo k e m.
      Definition Empty m := forall (a : L.t) (e : elt), ~ MapsTo a e m.
      Definition eq_key (p p' : L.t * elt) := fst p = fst p'.

      Parameter mem_1 : In x m -> mem x m = true.
      Parameter mem_2 : mem x m = true -> In x m.

      Parameter empty_1 : Empty empty.

      Parameter is_empty_1 : Empty m -> is_empty m = true.
      Parameter is_empty_2 : is_empty m = true -> Empty m.

      Parameter add_1 : MapsTo x e (add x e m).
      Parameter add_2 : x <> y -> MapsTo y e m -> MapsTo y e (add x e' m).
      Parameter add_3 : x <> y -> MapsTo y e (add x e' m) -> MapsTo y e m.

      Parameter remove_1 : ~ In x (remove x m).
      Parameter remove_2 : x <> y -> MapsTo y e m -> MapsTo y e (remove x m).
      Parameter remove_3 : x <> y  -> MapsTo y e (remove x m) -> MapsTo y e m.

      Parameter find_1 : MapsTo x e m -> find x m = Some e.
      Parameter find_2 : find x m = Some e -> MapsTo x e m.

      Parameter elements_1 : MapsTo x e m -> List.In (x, e) (elements m).
      Parameter elements_2 : List.In (x, e) (elements m) -> MapsTo x e m.
      Parameter elements_3w : NoDupA eq_key (elements m).


      Definition Equal m m' := forall y, find y m = find y m'.

      Variable cmp : elt -> elt -> bool.

    End spec.
  End Types.
  Arguments empty {elt}.

  Global Hint Immediate mem_2 is_empty_2 add_3 remove_3 find_2 : lmap.
  Global Hint Resolve mem_1 is_empty_1 add_1 add_2 remove_1 remove_2 find_1 : lmap.
End LocationMap.

(*
  Things provable about any LocationMap. These are in another module so that they do not 
  need to be repeated in every LocationMap module.
 *)
Module LocationMapFacts (L : Locations) (LM : LocationMap L).

  (* 
     Locations do not mapt to two different elements.
   *)
  Lemma MapsToUnique : forall {elt : Set} {m : LM.t elt} {l : L.t} {e1 e2 : elt}, LM.MapsTo l e1 m -> LM.MapsTo l e2 m -> e1 = e2.
  Proof using.
    intros elt m l e1 e2 mt1 mt2; apply LM.find_1 in mt1; apply LM.find_1 in mt2; rewrite mt1 in mt2; inversion mt2; reflexivity.
  Qed.
  
  Lemma MapsTo2In : forall {elt : Set} {m : LM.t elt} {l : L.t} {e : elt}, LM.MapsTo l e m -> LM.In l m.
  Proof using.
    intros elt m l e mt; exists e; exact mt.
  Qed.

  (* 
     `find l m` returns `None` if and only if `l` is not bound in `m`. 
   *)
  Lemma find_None_1 : forall {elt : Set} {m : LM.t elt} {l : L.t}, LM.find l m = None -> forall (e : elt), ~ LM.MapsTo l e m.
  Proof using.
    intros elt m l eq e mt; apply LM.find_1 in mt; rewrite mt in eq; inversion eq.
  Qed.
  Lemma find_None_2 : forall {elt : Set} {m : LM.t elt} {l : L.t}, ~ LM.In l m -> LM.find l m = None.
  Proof using.
    intros elt m l H. destruct (LM.find l m) eqn:eq; auto.
    apply LM.find_2 in eq. destruct (H (ex_intro _ e eq)).
  Qed.

  (*
   `find_add_1` and `find_add_2` tell us how find behaves on a map of the form `add l e m`. 
   *)
  Lemma find_add_1 : forall {elt : Set} (m : LM.t elt) (l : L.t) (e : elt), LM.find l (LM.add l e m) = Some e.
  Proof using.
    intros elt m l e. apply LM.find_1. apply LM.add_1.
  Qed.
  Lemma find_add_2 : forall {elt : Set} (m : LM.t elt) (l l' : L.t) (e : elt), l <> l' -> LM.find l (LM.add l' e m) = LM.find l m.
  Proof using.
    intros elt m l l' e H. destruct (LM.find l (LM.add l' e m)) eqn:eq. apply LM.find_2 in eq. apply LM.add_3 in eq; auto. apply LM.find_1 in eq; auto.
    destruct (LM.find l m) eqn:eq'; auto. apply LM.find_2 in eq'. apply (LM.add_2 e (fun e => H (eq_sym e))) in eq'. apply LM.find_1 in eq'.
    transitivity (LM.find l (LM.add l' e m)); auto.
  Qed.

  (*
    find_remove_1 and find_remove_2 tell us how find behaves on a map of the form 
    `remove l m`.
   *)
  Lemma find_remove_1 : forall {elt : Set} (m : LM.t elt) (l : L.t), LM.find l (LM.remove l m) = None.
  Proof using.
    intros elt m l; destruct (LM.find l (LM.remove l m)) eqn:eq; [|reflexivity].
    apply LM.find_2 in eq. destruct (LM.remove_1 (ex_intro _ e eq)).
  Qed.
  Lemma find_remove_2 : forall {elt : Set} (m : LM.t elt) (l l' : L.t), l <> l' -> LM.find l (LM.remove l' m) = LM.find l m.
  Proof using.
    intros elt m l l' neq; destruct (LM.find l (LM.remove l' m)) eqn:eq.
    apply LM.find_2 in eq; apply LM.remove_3 in eq; auto; apply LM.find_1 in eq; auto.
    destruct (LM.find l m) eqn:eq'; [|reflexivity]. apply LM.find_2 in eq'. apply LM.remove_2 with (x := l') in eq'; auto.
    apply LM.find_1 in eq'; rewrite eq' in eq; inversion eq.
  Qed.

  (*
    Equal is a equivalence relation.
   *)
  Instance lm_Equal_refl : forall {elt : Set}, Reflexive (@LM.Equal elt).
  Proof using.
    intros elt. unfold Reflexive; intro m; unfold LM.Equal; intro y; reflexivity.
  Qed.
  Instance lm_Equal_sym : forall {elt : Set}, Symmetric (@LM.Equal elt).
  Proof using.
    intros elt; unfold Symmetric; intros m1 m2; unfold LM.Equal; intros H l; symmetry; apply H.
  Qed.
  Instance lm_Equal_trans : forall {elt : Set}, Transitive (@LM.Equal elt).
  Proof using.
    intros elt; unfold Transitive; intros m1 m2 m3; unfold LM.Equal; intros H1 H2 l;
      transitivity (LM.find l m2); auto.
  Qed.

  
End LocationMapFacts.

  

