(*
  A module describing the properties of locations in 
  Pirouette. Locations are simply any Set with a
  decidable equality relation.
 *)
Module Type Locations.

  Parameter t : Set.

  Parameter eq_dec : forall x y : t, {x = y} + {x <> y}.

End Locations.


