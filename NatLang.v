Require Export LocalLang.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Lists.List.


(*
  A simple language with natural numbers and booleans. This language has no control flow
  or evaluation at all. It is inspired by the local language in the Core Choreography
  Calculus.
 *)
Module NatLang <: LocalLang.

  Inductive NL :=
  | var : nat -> NL
  | Z : NL
  | Succ : NL -> NL
  | ttrue : NL
  | ffalse : NL.
  Global Hint Constructors NL : NL.
  Definition Expr := NL.
  Definition ExprEqDec : forall e1 e2 : Expr, {e1 = e2} + {e1 <> e2}.
    decide equality; apply Nat.eq_dec.
  Qed.
  Definition ExprVar : nat -> Expr := var.

  Inductive NLVal : NL -> Prop :=
    ZVal : NLVal Z
  | SuccVal : forall v, NLVal v -> NLVal (Succ v)
  | TTrueVal : NLVal ttrue
  | FFalseVal : NLVal ffalse.
  Global Hint Constructors NLVal : NL.
  Definition ExprVal : Expr -> Prop := NLVal.

  (*
    Being closed is a much-simpler property here than in the λ calculus, since we do not
    have any binders to worry about.
   *)
  Fixpoint ExprClosedAbove (n : nat) (e : Expr) : Prop :=
    match e with
    | var x => x < n
    | Z => True
    | Succ e => ExprClosedAbove n e
    | ttrue => True
    | ffalse => True
    end.
  Definition ExprClosed := ExprClosedAbove 0.

  Theorem ExprValuesClosed : forall v : Expr, ExprVal v -> ExprClosed v.
  Proof using. intros v val_v; induction val_v; cbn; auto. Qed.

  Theorem ExprClosedAboveMono : forall m n e, m < n -> ExprClosedAbove m e -> ExprClosedAbove n e.
  Proof using.
    intros m n e; revert m n; induction e; try rename n into x;
      intros m n m_lt_n cb; cbn; auto.
    - cbn in cb; transitivity m; auto.
    - cbn in cb; apply IHe with (m := m); auto.
  Qed.
               
  Theorem ExprVarClosed : forall n m, ExprClosedAbove n (ExprVar m) <-> m < n.
  Proof using. intros n m; cbn; reflexivity. Qed.

  (*
    Since we have no binders, we substitution and renaming are very simple.
   *)
  Fixpoint ExprSubst (e : Expr) (σ : nat -> Expr) : Expr :=
    match e with
    | var x => σ x
    | Z => Z
    | Succ e => Succ (ExprSubst e σ)
    | ttrue => ttrue
    | ffalse => ffalse
    end.
  Fixpoint ExprRename (e : Expr) (ξ : nat -> nat) : Expr :=
    match e with
    | var x => var (ξ x)                    
    | Z => Z
    | Succ e => Succ (ExprRename e ξ)
    | ttrue => ttrue
    | ffalse => ffalse
    end.
  Notation "e [e| sigma ]" := (ExprSubst e sigma) (at level 29).
  Notation "e ⟨e| xi ⟩" := (ExprRename e xi) (at level 29).

  Theorem ExprRenameSpec : forall (e : Expr) (ξ : nat -> nat),
      e ⟨e| ξ⟩ = e [e| fun n => ExprVar (ξ n)].
  Proof using. intro e; induction e; intro ξ; cbn; try rewrite IHe; reflexivity. Qed.

  Theorem ExprSubstVar : forall n σ, (ExprVar n ) [e| σ] = σ n.
  Proof using. intros n σ; cbn; reflexivity. Qed.

  Lemma ExprRenameVar : forall n ξ, (ExprVar n) ⟨e| ξ ⟩ = ExprVar (ξ n).
  Proof.
    intros n ξ.
    rewrite ExprRenameSpec. rewrite ExprSubstVar. reflexivity.
  Qed.

  Theorem ExprRenameFusion : forall (e : Expr) (ξ1 ξ2 : nat -> nat),
      (e ⟨e| ξ1⟩) ⟨e| ξ2⟩ = e ⟨e| fun n => ξ2 (ξ1 n)⟩.
  Proof using. intro e; induction e; intros ξ1 ξ2; cbn; try rewrite IHe; reflexivity. Qed.
    
  Definition ExprUpSubst : (nat -> Expr) -> nat -> Expr :=
    fun σ n =>
      match n with
      | 0 => ExprVar 0
      | S n => (σ n) ⟨e| S ⟩
      end.

  Definition ExprUpRename : (nat -> nat) -> nat -> nat :=
    fun ξ n =>
      match n with
      | 0 => 0
      | S n => S (ξ n)
      end.

  Theorem ExprSubstExt : forall (e : Expr) (σ1 σ2 : nat -> Expr),
      (forall n, σ1 n = σ2 n) -> e [e| σ1] = e [e| σ2].
  Proof using. intros e σ1 σ2 eq; induction e; cbn; auto. apply f_equal; auto. Qed.

  Theorem ExprRenameExt :forall (e : Expr) (ξ1 ξ2 : nat -> nat),
      (forall n, ξ1 n = ξ2 n) -> e ⟨e| ξ1 ⟩= e ⟨e| ξ2⟩.
  Proof using. intros e ξ1 ξ2 eq; induction e; cbn; auto. apply f_equal; auto. Qed.
  
  Definition ExprIdSubst : nat -> Expr := fun n => ExprVar n.
  Theorem ExprIdentitySubstSpec : forall (e : Expr), e [e| ExprIdSubst] = e.
  Proof using. intro e; induction e; cbn; auto; rewrite IHe; reflexivity. Qed.
  
  Definition ExprIdRenaming : nat -> nat := fun n => n.
  Theorem ExprIdRenamingSpec : forall (e : Expr), e ⟨e| ExprIdRenaming ⟩ = e.
  Proof using. intro e; induction e; cbn; auto; rewrite IHe; reflexivity. Qed.
    
  Lemma ExprUpSubstId : forall n, ExprIdSubst n = (ExprUpSubst ExprIdSubst) n.
  Proof.
    intros n; unfold ExprUpSubst; destruct n.
    - unfold ExprIdSubst; reflexivity.
    - unfold ExprIdSubst. rewrite ExprRenameVar. reflexivity.
  Qed.
  
  Lemma ExprUpRenamingId : forall n, ExprIdRenaming n = ExprUpRename ExprIdRenaming n.
  Proof.
    intros n; unfold ExprUpRename; destruct n; unfold ExprIdRenaming; reflexivity.
  Qed.

  Theorem ExprClosedAboveSubst : forall (e : Expr) (σ : nat -> Expr) (n : nat),
      ExprClosedAbove n e -> (forall m, m < n -> σ m = ExprVar m) -> e [e|σ] = e.
  Proof using.
    intro e; induction e; cbn; intros σ x cb σ_bdd; auto; apply f_equal; eapply IHe; eauto.
  Qed.

  Lemma ExprClosedSubst : forall (e : Expr) (σ : nat -> Expr), ExprClosed e -> e [e|σ] = e.
  Proof using.
    intros e σ closed; unfold ExprClosed in closed; apply ExprClosedAboveSubst with (n := 0);
      auto.
    intros m lt; inversion lt.
  Qed.

  Definition tt : Expr := ttrue. Definition ff : Expr := ffalse.
  Theorem ttValue : ExprVal tt. Proof using. unfold tt; unfold ExprVal; auto with NL. Qed.
  Theorem ffValue : ExprVal ff. Proof using. unfold ff; unfold ExprVal; auto with NL. Qed.
  Theorem boolSeperation : tt <> ff. Proof using. unfold tt; unfold ff; discriminate. Qed.

  Inductive NLStep : Expr -> Expr -> Prop :=.
  Definition ExprStep : Expr -> Expr -> Prop := NLStep.
  Theorem NoExprStepFromVal : forall v, ExprVal v -> forall e, ~ ExprStep v e.
  Proof using. intros v _ e step; destruct step. Qed.

  Theorem ExprRenameClosedAbove : forall e n ξ m,
      (forall k, k < n -> ξ k < m) ->
      ExprClosedAbove n e ->
      ExprClosedAbove m (e ⟨e| ξ⟩).
  Proof using.
    intros e; induction e; intros x ξ m ξ_bdd cb; cbn in *; auto. eapply IHe; eauto.
  Qed.

  Theorem ExprSubstClosedAbove : forall e n σ m,
      (forall k, k < n -> ExprClosedAbove m (σ k)) ->
      ExprClosedAbove n e ->
      ExprClosedAbove m (e [e| σ]).
  Proof using.
    intros e; induction e; intros x σ m σ_bdd cb; cbn in *; auto. eapply IHe; eauto.
  Qed.

  Theorem ExprClosedAfterStep : forall  e1 e2 n,
      ExprStep e1 e2 -> ExprClosedAbove n e1 -> ExprClosedAbove n e2.
  Proof using. intros e1 e2 n step; destruct step. Qed.

End NatLang.

