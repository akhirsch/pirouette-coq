Require Export LocalLang.
Require Export Locations.
Require Export SyncLabels.

Require Import Coq.Arith.Arith.
Require Import Coq.Program.Wf.
Require Import Coq.Logic.JMeq.
Require Import Coq.Classes.RelationClasses.
Require Import Coq.Lists.List.
Require Import Sorted Orders Coq.Sorting.Mergesort Permutation.
Require Import Program.Tactics.
Require Import Coq.Structures.Orders.
Require Import Coq.Bool.Bool.
Require Import Psatz.
Require Import Coq.Program.Equality.

Module Pirouette (Import E : LocalLang) (L : Locations) (Import SL : SyncLabels).
  
  Definition Loc : Set := L.t.
  Import ListNotations.

  Inductive PirExpr : Set :=
    Done (ℓ : Loc) (e : Expr) : PirExpr
  | Var (X : nat) : PirExpr (* Note these are _choreography_ variables *)
  | Send (ℓ1 : Loc) (e : Expr) (ℓ2 : Loc) (C : PirExpr) : PirExpr
  | If (ℓ : Loc) (e : Expr) (C1 : PirExpr) (C2 : PirExpr) : PirExpr
  | Sync (ℓ1 : Loc) (d : SyncLabel) (ℓ2 : Loc) (C : PirExpr) : PirExpr (* Note that we don't list the variable explicitly, since we use de Bruijn *)
  | DefLocal (ℓ : Loc) (C1 : PirExpr) (C2 : PirExpr) : PirExpr
  | FunLocal (ℓ : Loc) (C : PirExpr) : PirExpr
  | FunGlobal (C : PirExpr) : PirExpr 
  | AppLocal (ℓ : Loc) (C : PirExpr) (e : Expr) : PirExpr (* The location is necessary bookkeeping, we expect C to evaluate to fun_l f(l.x). C' *)
  | AppGlobal (C1 C2 : PirExpr) : PirExpr.
  Global Hint Constructors PirExpr : PirExpr.
  Notation "'Ret' p ⟪ e ⟫" := (Done p e) (at level 25).
  Notation "p ⟪ e ⟫ → q 'fby' C" := (Send p e q C) (at level 25).
  Notation "'Cond' p ⦃ e ⦄ 'Then' C1 'Else' C2" := (If p e C1 C2) (at level 25).
  Notation "p ⟨ d ⟩ → q 'fby' C" := (Sync p d q C) (at level 25).
  Notation "'LetLocal' p '⟪new⟫' ← C1 'fby' C2" := (DefLocal p C1 C2) (at level 25).
  
  Definition PirExprEqDec : forall C1 C2 :PirExpr, {C1 = C2} + {C1 <> C2}.
  Proof using.
    decide equality.
    all: try (apply ExprEqDec).
    all: try (apply L.eq_dec).
    apply Nat.eq_dec.
    apply SyncLabelEqDec.
  Defined.

  Inductive PirExprVal : PirExpr -> Prop :=
  | ReturnVal : forall (l : Loc) (v : Expr),
      ExprVal v -> PirExprVal (Done l v)
  | FunLocalVal : forall l C, PirExprVal (FunLocal l C)
  | FunGlobalVal : forall C, PirExprVal (FunGlobal C).
  
  (* RENAMING *)

  (* 
     Renaming in de Bruijn styles replaces variables with other variables. It can be 
     defined as a form of substitution, but we use renaming in the definition of 
     substitution, so it is easier to define it separately.
     
     Renamings are simply functions `ξ` of type `Loc → Nat → Nat`. We can think of `ξ` as 
     telling us that we should change `ℓ`'s variable `n` to the variable `ξ n`, still owned
     by `ℓ`.

     We define renaming both expression variables and choreography variables for Pirouette
     expressions. These operations
     1. Respect the identity renaming
     2. Are extensional
     3. Enjoy fusion
   *)

  (* 
     We use `PirExprUpLocalRename ξ ℓ` to denote `ξ` when we have gone past an additional
     binder for a variable owned by `ℓ`. This is a new local renaming, and so has type
     `Loc → Nat → Nat`.
   *)
  Definition PirExprUpLocalRename : (Loc -> nat -> nat) -> Loc -> Loc -> nat -> nat :=
    fun ξ l1 l2 => if L.eq_dec l1 l2
                then ExprUpRename (ξ l2)
                else ξ l2.
                       
  Fixpoint PirExprLocalRename (C : PirExpr) (ξ : Loc -> nat -> nat) : PirExpr :=
    match C with
    | Done ℓ e => Done ℓ (e ⟨e| (ξ ℓ)⟩)
    | Var n => Var n
    | Send ℓ1 e ℓ2 C =>
      Send ℓ1 (e ⟨e| (ξ ℓ1)⟩) ℓ2 (PirExprLocalRename C (PirExprUpLocalRename ξ ℓ2))
    (* The bound variable is owned by `ℓ2` *)
    | If ℓ e C1 C2 => If ℓ (e ⟨e| (ξ ℓ)⟩) (PirExprLocalRename C1 ξ) (PirExprLocalRename C2 ξ)
    | Sync ℓ1 d ℓ2 C1 => Sync ℓ1 d ℓ2 (PirExprLocalRename C1 ξ)
    | DefLocal ℓ C1 C2 =>
      DefLocal ℓ (PirExprLocalRename C1 ξ) (PirExprLocalRename C2 (PirExprUpLocalRename ξ ℓ))
    | FunLocal ℓ C => FunLocal ℓ (PirExprLocalRename C (PirExprUpLocalRename ξ ℓ))
    | FunGlobal C => FunGlobal (PirExprLocalRename C ξ)
    | AppLocal ℓ C e => AppLocal ℓ (PirExprLocalRename C ξ) (e ⟨e| ξ ℓ⟩)
    | AppGlobal C1 C2 => AppGlobal (PirExprLocalRename C1 ξ) (PirExprLocalRename C2 ξ)
    end.
  Notation "C ⟨local| x ⟩" := (PirExprLocalRename C x) (at level 20).

  Definition PirExprLocalIdRenaming : Loc -> nat -> nat := fun _ n => n.
  
  Lemma PirExprUpIdExprRename : forall ℓ1 ℓ2 n,
      PirExprUpLocalRename PirExprLocalIdRenaming ℓ1 ℓ2 n = PirExprLocalIdRenaming ℓ1 n.
  Proof using.
    intros ℓ1 ℓ2 n; destruct n; unfold PirExprUpLocalRename; unfold PirExprLocalIdRenaming.
    all: destruct (L.eq_dec ℓ1 ℓ2) as [_|neq]; simpl; reflexivity.
  Qed.

  Lemma PirExprUpLocalRenameExt : forall (ξ1 ξ2 : Loc -> nat -> nat),
      (forall (ℓ : Loc) (n : nat), ξ1 ℓ n = ξ2 ℓ n) ->
      forall (ℓ1 ℓ2 : Loc) (n : nat), PirExprUpLocalRename ξ1 ℓ1 ℓ2 n = PirExprUpLocalRename ξ2 ℓ1 ℓ2 n.
  Proof using.
    intros ξ1 ξ2 eq ℓ1 ℓ2 n; unfold PirExprUpLocalRename; unfold ExprUpRename.
    destruct (L.eq_dec ℓ1 ℓ2); auto; destruct n; auto.
  Qed.

  Lemma PirExprLocalRenameExtensional : forall (ξ1 ξ2 : Loc -> nat -> nat),
      (forall (ℓ : Loc) (n : nat), ξ1 ℓ n = ξ2 ℓ n) ->
      forall (C : PirExpr), C ⟨local| ξ1 ⟩ = C ⟨local| ξ2⟩.
  Proof using.
    intros ξ1 ξ2 eqv C; revert ξ1 ξ2 eqv; induction C; intros ξ1 ξ2 eqv; cbn; auto.
    all: try (rewrite ExprRenameExt with (ξ2 := ξ2 ℓ)); auto.
    all: try (rewrite IHC with (ξ2 := ξ2); auto; fail).
    all: try (rewrite IHC1 with (ξ2 := ξ2); [rewrite IHC2 with (ξ2 := ξ2)|]; auto; fail).
    (* Special cases: we bind a local variable. *)
    - (* Send *)
      rewrite ExprRenameExt with (ξ2 := ξ2 ℓ1); auto;
       rewrite IHC with (ξ2 := PirExprUpLocalRename ξ2 ℓ2); auto.
      apply PirExprUpLocalRenameExt; auto.
    - (* LetLocal *)
      rewrite IHC1 with (ξ2 := ξ2); [rewrite IHC2 with (ξ2 := PirExprUpLocalRename ξ2 ℓ)|];
        auto; apply PirExprUpLocalRenameExt; auto.
    - (* FunLocal *)
      rewrite IHC with (ξ2 := PirExprUpLocalRename ξ2 ℓ); auto.
      apply PirExprUpLocalRenameExt; auto.
  Qed.
  
  Lemma PirExprLocalRenameFusion : forall (C : PirExpr) (ξ1 ξ2 : Loc -> nat -> nat), (C ⟨local| ξ1⟩)⟨local| ξ2⟩ = C ⟨local| fun ℓ n => ξ2 ℓ (ξ1 ℓ n)⟩.
  Proof using.
    intros C; induction C; intros ξ1 ξ2; cbn; auto.
    all: try (rewrite ExprRenameFusion; auto).
    all: try (rewrite IHC; auto; fail).
    all: try (rewrite IHC1; auto; rewrite IHC2; auto; fail).
    (* Special cases: we bind a local variable *)
    - (* Send *)
      rewrite IHC.
      rewrite PirExprLocalRenameExtensional
        with (ξ1 := fun ℓ n => PirExprUpLocalRename ξ2 ℓ2 ℓ (PirExprUpLocalRename ξ1 ℓ2 ℓ n))
             (ξ2 := PirExprUpLocalRename (fun ℓ n => ξ2 ℓ (ξ1 ℓ n)) ℓ2); auto.
      unfold PirExprUpLocalRename; unfold ExprUpRename;
        intros ℓ n; destruct (L.eq_dec ℓ2 ℓ); destruct n; auto.
    - (* LetLocal *)
      rewrite IHC1; rewrite IHC2; auto.
      rewrite PirExprLocalRenameExtensional
        with (ξ1 := fun ℓ' n => PirExprUpLocalRename ξ2 ℓ ℓ' (PirExprUpLocalRename ξ1 ℓ ℓ' n))
             (ξ2 := PirExprUpLocalRename (fun ℓ' n => ξ2 ℓ' (ξ1 ℓ' n)) ℓ); auto.
      unfold PirExprUpLocalRename; unfold ExprUpRename;
        intros ℓ' n; destruct (L.eq_dec ℓ ℓ'); destruct n; auto.
    - (* FunLocal *)
      rewrite IHC.
      rewrite PirExprLocalRenameExtensional
        with (ξ1 := fun ℓ' n => PirExprUpLocalRename ξ2 ℓ ℓ' (PirExprUpLocalRename ξ1 ℓ ℓ' n))
             (ξ2 := PirExprUpLocalRename (fun ℓ' n => ξ2 ℓ' (ξ1 ℓ' n)) ℓ); auto.
      unfold PirExprUpLocalRename; unfold ExprUpRename;
        intros ℓ' n; destruct (L.eq_dec ℓ ℓ'); destruct n; auto.
  Qed.

  Lemma PirExprLocalIdRenamingSpec : forall C, C ⟨local| PirExprLocalIdRenaming⟩ = C.
  Proof using.
    intros C; induction C; cbn; auto.
    all: try (rewrite IHC; auto).
    all: try (rewrite ExprIdRenamingSpec; auto).
    all: try (rewrite IHC1; rewrite IHC2; auto; fail).
    - (* Send *)
      rewrite PirExprLocalRenameExtensional with (ξ2 := PirExprLocalIdRenaming);
        [rewrite IHC; auto|].
      apply PirExprUpIdExprRename.
    - (* LetLocal *)
      rewrite IHC1.
      rewrite PirExprLocalRenameExtensional with (ξ2 := PirExprLocalIdRenaming);
        [rewrite IHC2| apply PirExprUpIdExprRename]; auto.
    - (* FunLocal *)
      rewrite PirExprLocalRenameExtensional with (ξ2 := PirExprLocalIdRenaming);
        [rewrite IHC|]; auto.
      apply PirExprUpIdExprRename.
  Qed.

  Definition PirExprUpRename : (nat -> nat) -> nat -> nat :=
    fun f n => match n with
            | 0 => 0
            | S n => S (f n)
            end.
  Fixpoint PirExprRename (C : PirExpr) (ξ : nat -> nat) :=
    match C with
    | Done ℓ e => Done ℓ e
    | Var n => Var (ξ n)
    | Send ℓ1 e ℓ2 C => Send ℓ1 e ℓ2 (PirExprRename C ξ)
    | If ℓ e C1 C2 => If ℓ e (PirExprRename C1 ξ) (PirExprRename C2 ξ)
    | Sync ℓ1 d ℓ2 C => Sync ℓ1 d ℓ2 (PirExprRename C ξ)
    | DefLocal ℓ C1 C2 => DefLocal ℓ (PirExprRename C1 ξ) (PirExprRename C2 ξ)
    | FunLocal ℓ C => FunLocal ℓ (PirExprRename C (PirExprUpRename ξ))
    | FunGlobal C => FunGlobal (PirExprRename C (PirExprUpRename (PirExprUpRename ξ))) (* We need two up, because we're defining two global variables *)
    | AppLocal ℓ C e => AppLocal ℓ (PirExprRename C ξ) e
    | AppGlobal C1 C2 => AppGlobal (PirExprRename C1 ξ) (PirExprRename C2 ξ)
    end.
  Notation "C ⟨global| x ⟩" := (PirExprRename C x) (at level 20).

  Definition PirExprIdRenaming := fun n : nat => n.

  Lemma PirExprUpIdRename : forall n, PirExprUpRename PirExprIdRenaming n = PirExprIdRenaming n.
  Proof using.
    intro n; destruct n; cbn; unfold PirExprIdRenaming; auto.
  Qed.
  Lemma PirExprUpRenameExt : forall ξ1 ξ2, (forall n, ξ1 n = ξ2 n) -> forall n, PirExprUpRename ξ1 n = PirExprUpRename ξ2 n.
  Proof using.
    intros ξ1 ξ2 eqv n; destruct n; cbn; auto.
  Qed.

  Lemma PirExprRenameExt : forall C ξ1 ξ2, (forall n, ξ1 n = ξ2 n) -> C ⟨global| ξ1⟩ = C ⟨global| ξ2⟩.
  Proof using.
    intros C; induction C; intros ξ1 ξ2 eqv; cbn; auto.
    all: try (rewrite IHC with (ξ2 := ξ2); auto; fail).
    all: try (rewrite IHC1 with (ξ2 := ξ2); [rewrite IHC2 with (ξ2 := ξ2)|]; auto; fail).
    (* Special cases: we bind a choreography variable *)
    - (* FunLocal *)
      rewrite IHC with (ξ2 := PirExprUpRename ξ2); [| apply PirExprUpRenameExt]; auto.
    - (*FunGlobal *)
      rewrite IHC with (ξ2 := PirExprUpRename (PirExprUpRename ξ2)); auto.
      repeat apply PirExprUpRenameExt; auto.
  Qed.    
  
  Lemma PirExprIdRenamingSpec : forall C, C ⟨global| PirExprIdRenaming ⟩ = C.
  Proof using.
    intro C; induction C; cbn; auto.
    all: try (rewrite IHC); auto.
    all: try (rewrite IHC1; rewrite IHC2; auto).
    all: rewrite PirExprRenameExt with (ξ2 := PirExprIdRenaming); [rewrite IHC; reflexivity|].
    all: intro n; destruct n as [|n]; cbn; auto; destruct n; cbn; auto.
  Qed.

  Lemma PirExprRenameFusion : forall C ξ1 ξ2, (C ⟨global| ξ1⟩) ⟨global|ξ2⟩ = C ⟨global| fun n => ξ2 (ξ1 n)⟩.
  Proof using.
    intro C; induction C; intros ξ1 ξ2; cbn; auto.
    all: try (rewrite IHC; auto).
    all: try (rewrite IHC1; rewrite IHC2; auto).
    rewrite PirExprRenameExt with (ξ2 := PirExprUpRename (fun n => ξ2 (ξ1 n))).
    3: rewrite PirExprRenameExt with (ξ2 := PirExprUpRename (PirExprUpRename (fun n => ξ2 (ξ1 n)))).
    1,3: reflexivity.
    all: intro n; unfold PirExprUpRename; destruct n; auto; destruct n; auto.
  Qed.

  (* SUBSTITUTION *)

  (*
    We define both substitution of both local-language and choreography variables. This is
    infinite substitution---every variable is given something to substitute it with. We can
    use this to simulate finite substitution.

    Substitutions are functions `σ` of the form `Loc → Nat → Expr` or `Nat → PirExpr`. The 
    first is a local substititution, which tells us to replace `ℓ`'s variable `n` with the
    local-language expression `σ ℓ n`. The second is a global substitution, and it tells
    us to replace the choreography variable `n` with the Pirouette expression `σ n`.

    Both forms of substitution:
    1. Treat the identity correctly
    2. Are extensional
   *)


  (*
    We use `PirExprUpLocalSubst σ ℓ` to denote `σ` when we have gone past an additional binder
    for `ℓ`. It is still a local substitution, and thus has type `Loc → Nat → Expr`.
   *)
  Definition PirExprUpLocalSubst : (Loc -> nat -> Expr) -> Loc -> Loc -> nat -> Expr :=
    fun σ ℓ1 ℓ2 n =>
      if L.eq_dec ℓ1 ℓ2 then
        match n with
        | 0 => ExprVar 0
        | S n => (σ ℓ2 n) ⟨e| S ⟩ (* We have to account for free variables in `σ ℓ2 n` *)
                         (* Note that if `σ ℓ2 n` is `m`, then the result is `S m`. *)
        end
      else σ ℓ2 n.
  
  (* Definition SendUpPirExprLocalSubst (σk : nat -> PirExpr) (ℓ : Loc) : nat -> PirExpr := *)
  (*   fun n => (σk n)⟨local| fun ℓ' m => if L.eq_dec ℓ ℓ' then S m else m⟩. *)

  Fixpoint PirExprLocalSubst (C : PirExpr) (σ : Loc -> nat -> Expr) : PirExpr :=
    match C with
    | Done ℓ e => Done ℓ (e [e| (σ ℓ)])
    | Var n => Var n
    | Send ℓ1 e ℓ2 C => Send ℓ1 (e [e| (σ ℓ1)]) ℓ2 (PirExprLocalSubst C (PirExprUpLocalSubst σ ℓ2))
    | If ℓ e C1 C2 => If ℓ (e [e| (σ ℓ)]) (PirExprLocalSubst C1 σ) (PirExprLocalSubst C2 σ)
    | Sync ℓ1 d ℓ2 C => Sync ℓ1 d ℓ2 (PirExprLocalSubst C σ)
    | DefLocal ℓ C1 C2 =>
      DefLocal ℓ (PirExprLocalSubst C1 σ) (PirExprLocalSubst C2 (PirExprUpLocalSubst σ ℓ))
    | FunLocal ℓ C => FunLocal ℓ (PirExprLocalSubst C (PirExprUpLocalSubst σ ℓ))
    | FunGlobal C => FunGlobal (PirExprLocalSubst C σ)
    | AppLocal ℓ C e => AppLocal ℓ (PirExprLocalSubst C σ) (e [e| (σ ℓ)])
    | AppGlobal C1 C2 => AppGlobal (PirExprLocalSubst C1 σ) (PirExprLocalSubst C2 σ)
    end.
  Notation "C [local| s ]" := (PirExprLocalSubst C s) (at level 20).

  Lemma PirExprUpLocalSubstExt : forall (σ1 σ2 : Loc -> nat -> Expr),
      (forall (ℓ : Loc) (n : nat), σ1 ℓ n = σ2 ℓ n) ->
      forall (ℓ1 ℓ2 : Loc) (n : nat), PirExprUpLocalSubst σ1 ℓ1 ℓ2 n = PirExprUpLocalSubst σ2 ℓ1 ℓ2 n.
  Proof using.
    intros σ1 σ2 eq ℓ1 ℓ2 n; unfold PirExprUpLocalSubst; destruct n; auto;
      repeat (rewrite eq); reflexivity.
  Qed.

  Lemma PirExprLocalSubstExt : forall (σ1 σ2 : Loc -> nat -> Expr),
      (forall (ℓ : Loc) (n : nat), σ1 ℓ n = σ2 ℓ n) ->
      forall C, C [local| σ1] = C [local| σ2].
  Proof using.
    intros σ1 σ2 eqv C; revert σ1 σ2 eqv; induction C; intros σ1 σ2 eqv; cbn.
    all: try (rewrite ExprSubstExt with (σ2 := σ2 ℓ)); auto.
    all: try (rewrite IHC with (σ2 := σ2); auto; fail).
    all: try (rewrite IHC1 with (σ2 := σ2); [rewrite IHC2 with (σ2 := σ2)|]; auto; fail).
    (* Special Cases: we bind a local variable*)
    - (* Send *)
      rewrite ExprSubstExt with (σ2 := σ2 ℓ1); auto;
        rewrite IHC with (σ2 := PirExprUpLocalSubst σ2 ℓ2); auto;
          apply PirExprUpLocalSubstExt; auto.
    - (* LetLocal *)
      rewrite IHC1 with (σ2 := σ2);
        [rewrite IHC2 with (σ2 := PirExprUpLocalSubst σ2 ℓ)|];
        auto; apply PirExprUpLocalSubstExt; auto.
    - (* FunLocal *)
      erewrite IHC; eauto; apply PirExprUpLocalSubstExt; auto.
  Qed.
  
  Definition PirExprLocalSubstId : Loc -> nat -> Expr := fun _ n => ExprVar n.

  (*
    For any location (i.e., for the *fibre* above that location, in categorical terms), 
    the local identity substitution is the identity substitution for the local language.
   *)
  Lemma PirExprLocalSubstIdFibre : forall (ℓ : Loc),
      PirExprLocalSubstId ℓ = ExprIdSubst.
  Proof using.
    intros ℓ; unfold PirExprLocalSubstId; unfold ExprIdSubst; reflexivity.
  Qed.

  Lemma PirExprUpLocalSubstId : forall ℓ1 ℓ2 n, PirExprLocalSubstId ℓ1 n
                                 = PirExprUpLocalSubst PirExprLocalSubstId ℓ2 ℓ1 n.
  Proof using.
    intros ℓ1 ℓ2 n; unfold PirExprUpLocalSubst; unfold PirExprLocalSubstId;
      destruct (L.eq_dec ℓ2 ℓ1); destruct n; auto; rewrite ExprRenameVar; reflexivity.
  Qed.
  
  Lemma PirExprLocalSubstIdSpec : forall (C : PirExpr), C [local| PirExprLocalSubstId] = C.
  Proof using.
    intro C; induction C; unfold PirExprLocalSubstId; cbn; auto.
    all: try (rewrite PirExprLocalSubstIdFibre).
    all: try (rewrite ExprIdentitySubstSpec); auto.
    all: fold PirExprLocalSubstId; try (rewrite IHC; auto; fail);
      try (rewrite IHC1; rewrite IHC2; auto; fail).
    - (* Send *)
      rewrite PirExprLocalSubstExt with (σ2 := PirExprLocalSubstId); [rewrite IHC|]; auto.
      intros ℓ n; unfold PirExprUpLocalSubst; unfold PirExprLocalSubstId; cbn.
      destruct (L.eq_dec ℓ2 ℓ); cbn; destruct n; auto; rewrite ExprRenameVar; reflexivity.
    - (* LetLocal *)
      rewrite IHC1.
      rewrite PirExprLocalSubstExt with (σ2 := PirExprLocalSubstId); [rewrite IHC2; reflexivity|].
      intros ℓ' n; symmetry; apply PirExprUpLocalSubstId.
    - (* FunLocal *)
      rewrite PirExprLocalSubstExt with (σ2 := PirExprLocalSubstId); [rewrite IHC; reflexivity|].
      intros ℓ' n; symmetry; apply PirExprUpLocalSubstId.
  Qed.
      
  Definition PirExprUpSubst : (nat -> PirExpr) -> nat -> PirExpr :=
    fun f  n => match n with
             | 0 => Var 0
             | S n => (f n) ⟨global| S⟩
             end.

  Definition PirExprUpSubstAllLocal : (nat -> PirExpr) -> Loc -> nat -> PirExpr :=
    fun f l n => (f n) ⟨local|fun l' m => if L.eq_dec l l' then S m else m⟩.

  Fixpoint PirExprSubst (C : PirExpr) (σ : nat -> PirExpr) :=
    match C with
    | Done l e => Done l e
    | Var n => σ n
    | Send l1 e l2 C => Send l1 e l2 (PirExprSubst C (PirExprUpSubstAllLocal σ l2))
    | If l e C1 C2 => If l e (PirExprSubst C1 σ) (PirExprSubst C2 σ)
    | Sync l1 d l2 C => Sync l1 d l2 (PirExprSubst C σ)
    | DefLocal l C1 C2 => DefLocal l (PirExprSubst C1 σ) (PirExprSubst C2 (PirExprUpSubstAllLocal σ l))
    | FunLocal l C => FunLocal l (PirExprSubst C (PirExprUpSubst(PirExprUpSubstAllLocal σ l)))
    | FunGlobal C => FunGlobal (PirExprSubst C (PirExprUpSubst (PirExprUpSubst σ)))
    | AppLocal l C e => AppLocal l (PirExprSubst C σ) e
    | AppGlobal C1 C2 => AppGlobal (PirExprSubst C1 σ) (PirExprSubst C2 σ)
    end.
  Notation "C [global| s ]" := (PirExprSubst C s) (at level 20).
  Definition PirExprIdSubst := fun n => Var n.

  Lemma PirExprUpSubstExt : forall σ1 σ2 : nat -> PirExpr,
      (forall n, σ1 n = σ2 n) -> forall n, PirExprUpSubst σ1 n = PirExprUpSubst σ2 n.
  Proof using.
    intros σ1 σ2 H n; unfold PirExprUpSubst; destruct n; cbn; auto. rewrite H; reflexivity.
  Qed.

  Lemma PirExprUpSubstAllLocalExt : forall σ1 σ2,
      (forall n, σ1 n = σ2 n) ->
      forall l n, PirExprUpSubstAllLocal σ1 l n = PirExprUpSubstAllLocal σ2 l n.
  Proof using.
    intros σ1 σ2 eqv l n; unfold PirExprUpSubstAllLocal; rewrite eqv; reflexivity.
  Qed.

  Lemma PirExprSubstExt : forall (C : PirExpr) (σ1 σ2 : nat -> PirExpr),
      (forall n, σ1 n = σ2 n) ->
      C[global|σ1] = C[global|σ2].
  Proof using.
    intro C; induction C; intros σ1 σ2 eqv; cbn; auto.
    all: try (erewrite IHC; eauto).
    all: try (erewrite IHC1; [erewrite IHC2|]; eauto).
    3,4: repeat apply PirExprUpSubstExt; auto.
    all: apply PirExprUpSubstAllLocalExt; auto.
  Qed.

  Lemma PirExprSubstUpId : forall n, PirExprUpSubst PirExprIdSubst n = PirExprIdSubst n.
  Proof using.
    intro n; unfold PirExprUpSubst; unfold PirExprIdSubst; destruct n; auto.
  Qed.

  Lemma PirExprUpSubstAllLocalId : forall l n,
      PirExprUpSubstAllLocal PirExprIdSubst l n = PirExprIdSubst n.
  Proof using.
    intro n; unfold PirExprUpSubstAllLocal; unfold PirExprIdSubst; cbn; reflexivity.
  Qed.
    
  Lemma PirExprSubstIdSpec : forall C, C[global|PirExprIdSubst] = C.
  Proof using.
    intro C; induction C; cbn; auto.
    all: try (rewrite IHC; auto).
    all: try (rewrite IHC1); try (rewrite IHC2); auto.
    1,3,4: erewrite PirExprSubstExt; [rewrite IHC; reflexivity|].
    4: erewrite PirExprSubstExt; [rewrite IHC2; reflexivity|].
    all: intro n; destruct n; cbn; auto; destruct n; auto.
  Qed.      

  (* RENAMING SPECS *)

  (* Renaming is a special type of substitution, and we prove the two agree below. *)
  
  Theorem PirExprLocalRenameSpec : forall (C : PirExpr) (ξ : Loc -> nat -> nat),
      C ⟨local| ξ ⟩ = C[local| (fun ℓ n => ExprVar (ξ ℓ n))].
  Proof using.
    intro C; induction C; intros ξ; cbn.
    all: repeat (rewrite ExprRenameSpec).
    all: try (repeat (rewrite IHC)).
    all: try (rewrite IHC1; rewrite IHC2).
    all: try reflexivity.
    - (* Send *)
      rewrite PirExprLocalSubstExt with (σ2 := PirExprUpLocalSubst (fun ℓ n => ExprVar (ξ ℓ n)) ℓ2);
        [reflexivity|].
      intros ℓ n; unfold PirExprUpLocalSubst; unfold PirExprUpLocalRename;
        destruct (L.eq_dec ℓ2 ℓ); destruct n; auto.
      unfold ExprUpRename; symmetry; apply ExprRenameVar. 
    - (* LetLocal *)
      rewrite PirExprLocalSubstExt
        with (σ1 := fun ℓ' n => ExprVar (PirExprUpLocalRename ξ ℓ ℓ' n ))
             (σ2 := PirExprUpLocalSubst (fun ℓ' n => ExprVar (ξ ℓ' n)) ℓ); [reflexivity|].
      intros ℓ' n; unfold PirExprUpLocalSubst; unfold PirExprUpLocalRename;
        destruct (L.eq_dec ℓ ℓ'); destruct n; auto.
      unfold ExprUpRename; rewrite ExprRenameVar; reflexivity.
    - (* FunLocal *)
      rewrite PirExprLocalSubstExt
        with (σ2 := PirExprUpLocalSubst (fun ℓ' n => ExprVar (ξ ℓ' n)) ℓ); auto.
      intros ℓ' n; unfold PirExprUpLocalSubst; unfold PirExprUpLocalRename;
        destruct (L.eq_dec ℓ ℓ'); destruct n; auto; unfold ExprUpRename;
          rewrite ExprRenameVar; auto.
  Qed.
  
  Lemma PirExprRenameSpec : forall C ξ, C ⟨global| ξ⟩ = C [global| fun n => Var (ξ n)].
  Proof using.
    intros C; induction C; intros ξ; cbn; auto.
    all: try (rewrite IHC; auto).
    all: try (rewrite IHC1; rewrite IHC2; auto; fail).
    all: erewrite PirExprSubstExt; [reflexivity|].
    all: intro n; destruct n; cbn; auto; destruct n; auto.
  Qed.

  (* CLOSURE (ABOVE) *)
  
  (* 
     A program is closed above `n` if it does not contain any variables greater than `n`.
     In the case of local variables, a program is closed above `n` for `l` if it does not 
     contain any `l`-owned variables greater than `n`. A program is closed if it is closed
     above `0`.

     We define local- and choreography- variable closedness (above) separately as functions.
     We then define an inductive proposition that combines choreography-variable closedness
     with local-variable closedness for every location.
   *)
  
  Fixpoint PirExprLVarClosedAbove (l : Loc) (n : nat) (C: PirExpr) : Prop :=
    match C with
    | Done l' e => if L.eq_dec l l'
                  then ExprClosedAbove n e
                  else True
    | Var x => True
    | Send l1 e l2 C => (if L.eq_dec l l1
                       then ExprClosedAbove n e
                       else True)
                       /\ if L.eq_dec l l2
                         then PirExprLVarClosedAbove l (S n) C
                         else PirExprLVarClosedAbove l n C
    | If l' e C1 C2 => (if L.eq_dec l l'
                      then ExprClosedAbove n e
                       else True)
                      /\ PirExprLVarClosedAbove l n C1
                      /\ PirExprLVarClosedAbove l n C2
    | Sync l1 d l2 C2 => PirExprLVarClosedAbove l n C2
    | DefLocal l' C1 C2 => PirExprLVarClosedAbove l n C1
                          /\ if L.eq_dec l l'
                            then PirExprLVarClosedAbove l (S n) C2
                            else PirExprLVarClosedAbove l n C2
    | FunLocal l' C => if L.eq_dec l l'
                      then PirExprLVarClosedAbove l (S n) C
                      else PirExprLVarClosedAbove l n C
    | FunGlobal C => PirExprLVarClosedAbove l n C
    | AppLocal l' C e => (if L.eq_dec l l'
                         then ExprClosedAbove n e
                         else True)
                        /\ PirExprLVarClosedAbove l n C
    | AppGlobal C1 C2 => PirExprLVarClosedAbove l n C1 /\ PirExprLVarClosedAbove l n C2
    end.

  Fixpoint PirExprCVarClosedAbove (n : nat) (C : PirExpr) : Prop :=
    match C with
    | Done l e => True
    | Var m => m < n
    | Send l1 e l2 C => PirExprCVarClosedAbove n C
    | If l e C1 C2 => PirExprCVarClosedAbove n C1 /\ PirExprCVarClosedAbove n C2
    | Sync l1 d l2 C => PirExprCVarClosedAbove n C
    | DefLocal l C1 C2 => PirExprCVarClosedAbove n C1 /\ PirExprCVarClosedAbove n C2
    | FunLocal l C => PirExprCVarClosedAbove (S n) C
    | FunGlobal C => PirExprCVarClosedAbove (S (S n)) C
    | AppLocal l C e => PirExprCVarClosedAbove n C
    | AppGlobal C1 C2 => PirExprCVarClosedAbove n C1 /\ PirExprCVarClosedAbove n C2
    end.
  
  Inductive PirExprClosedAbove : (Loc -> nat) -> nat -> PirExpr -> Prop :=
  | DoneClosedAbove : forall n m p e, ExprClosedAbove (n p) e -> PirExprClosedAbove n m (Done p e)
  | VarClosedAbove : forall n m k, lt k m -> PirExprClosedAbove n m (Var k)
  | SendClosedAbove : forall p e q C n m,
      ExprClosedAbove (n p) e
      -> PirExprClosedAbove (fun r => if L.eq_dec q r then S (n q) else n r) m C 
      -> PirExprClosedAbove n m (Send p e q C)
  | SyncClosedAbove : forall n m p d q C,
      PirExprClosedAbove n m C -> PirExprClosedAbove n m (Sync p d q C)
  | IfClosedAbove : forall n m p e C1 C2,
      ExprClosedAbove (n p) e -> PirExprClosedAbove n m C1 -> PirExprClosedAbove n m C2
      -> PirExprClosedAbove n m (If p e C1 C2)
  | DefLocalClosedAbove : forall n m l C1 C2,
      PirExprClosedAbove n m C1 ->
      PirExprClosedAbove (fun l' => if L.eq_dec l l' then S (n l) else n l') m C2 ->
      PirExprClosedAbove n m (DefLocal l C1 C2)
  | FunLocalClosedAbove : forall n m l C,
      PirExprClosedAbove (fun l' => if L.eq_dec l l' then S (n l) else n l') (S m) C ->
      PirExprClosedAbove n m (FunLocal l C)
  | FunGlobalClosedAbove : forall n m C,
      PirExprClosedAbove n (S (S m)) C ->
      PirExprClosedAbove n m (FunGlobal C)
  | AppLocalClosedAbove : forall n m l C e,
      PirExprClosedAbove n m C -> ExprClosedAbove (n l) e ->
      PirExprClosedAbove n m (AppLocal l C e)
  | AppGlobalClosedAbove : forall n m C1 C2,
      PirExprClosedAbove n m C1 -> PirExprClosedAbove n m C2 ->
      PirExprClosedAbove n m (AppGlobal C1 C2).

  (* 
     PirExprClosedAbove is the conjunction of closedness for choreography variables and
     closedness for local variables for every location.
   *)
  Lemma PirExprClosedAbove_Spec : forall (C : PirExpr) (f : Loc -> nat) (n : nat),
      PirExprClosedAbove f n C
      <-> PirExprCVarClosedAbove n C /\ forall l, PirExprLVarClosedAbove l (f l) C.
  Proof using.
    intro C; induction C; intros f m.
    all: split; [intro cb; split;[|intro l1] | intros [ccb ceb]]; cbn in *; auto.
    (*
      This starts a pattern used often in the rest of the development: most of the goals
      are solved by a big `match goal`. 
     *)
    all: repeat (match goal with
                 | [|- True ] => exact I
                 | [H : ?P |- ?P ] => exact H
                 | [ H : PirExprClosedAbove ?f ?m ?C |- _ ] =>
                   (* This tests if `C` is (partially) known *)
                   (* the `fresh` call fails unless `C` is a coq variable *)
                   tryif (let test := fresh "eq_" C in idtac) 
                   then fail (* `C` is a coq variable, we don't want to use `inversion` *)
                   else inversion H; subst; clear H (* `C` is partially known *)
                 | [ H : context[L.eq_dec ?a ?b] |- _ ] => destruct (L.eq_dec a b); subst
                 | [ |- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
                 | [ |- _ /\ _ ] => split
                 | [ H : _ /\ _ |- _ ] => destruct H
                 | [ H : forall l, PirExprLVarClosedAbove l (?f l) ?C
                              |- PirExprLVarClosedAbove ?l (?f ?l) ?C ] => exact (H l)
                 | [ |- PirExprClosedAbove ?f ?m ?C ] =>
                   (* same pattern here as above *)
                   tryif (let test := fresh "eq_" C in idtac)
                   then fail
                   else econstructor; eauto
                 | [ IHC : forall f n, PirExprClosedAbove f n ?C
                                  <-> PirExprCVarClosedAbove n ?C /\
                                    forall l, PirExprLVarClosedAbove l (f l) ?C,
                       H: PirExprClosedAbove ?f ?n ?C |- _ ] =>
                   lazymatch goal with (* Check if we've already derived the facts we can *)
                   | [_ : PirExprCVarClosedAbove n C,
                          _ : forall l, PirExprLVarClosedAbove l _ C |- _ ] =>
                     (* We've derived both facts, do nothing *)
                     fail
                   | _ =>
                     (* We haven't derived at least one of the two, 
                        put them in the context *)
                     let H1 := fresh in
                         let H2 := fresh in
                         pose proof ((proj1 (IHC f n)) H) as [H1 H2]
                   end
                 end);
                 repeat (match goal with
                         | [ H : forall l, PirExprLVarClosedAbove l (if L.eq_dec ?l' l
                                                             then _
                                                             else _) ?C |- _ ] =>
                           pose proof (H l');
                           let n := fresh "n" in
                           destruct (L.eq_dec l' l') as [_ | n]; [|destruct (n eq_refl)];
                           assert (forall l, l' <> l -> PirExprLVarClosedAbove l (f l) C)
                             by (let l := fresh in
                                 let neq := fresh in
                                 intros l neq; specialize (H l);
                                 destruct (L.eq_dec l' l) as [eq | _]; [destruct (neq eq)|];
                                 auto); clear H
                         end; cbn; auto).
    (* The remaining goals we solve by hand. They're not very interesting here. *)
    - pose proof (ceb ℓ); destruct (L.eq_dec ℓ ℓ) as [_ |n];[|destruct (n eq_refl)]; auto.
    - specialize (ceb ℓ1); destruct (L.eq_dec ℓ1 ℓ1) as [_|n];[|destruct (n eq_refl)]; auto.
      destruct ceb; auto.
    - apply IHC; split; auto; intro l''; specialize (ceb l''); destruct ceb;
        destruct (L.eq_dec ℓ2 l''); subst; auto.
      destruct (L.eq_dec l'' l'') as [_ | n]; [| destruct (n eq_refl)]; auto.
      destruct (L.eq_dec l'' ℓ2) as [eq |_ ]; [destruct (n (eq_sym eq))|]; auto.
    - specialize (ceb ℓ); destruct ceb; destruct (L.eq_dec ℓ ℓ) as [_ | n];
        [|destruct (n eq_refl)]; auto.
    - apply IHC1; split; auto. intro l''; specialize (ceb l'');
                                 destruct ceb as [ceb1 [ceb2 ceb3]]; auto.
    - apply IHC2; split; auto. intro l''; specialize (ceb l'');
                                 destruct ceb as [ceb1 [ceb2 ceb3]]; auto.
    - apply IHC; split; auto.
    - apply IHC1; split; auto.
      intro l''; specialize (ceb l''); destruct ceb; auto.
    - apply IHC2; split; auto.
      intro l''; specialize (ceb l''); destruct ceb;
        destruct (L.eq_dec ℓ l''); destruct (L.eq_dec l'' ℓ); subst;
          try match goal with
              | [ H : ?l <> ?l |- _ ] => destruct (H eq_refl)
              end; auto.
    - apply IHC; split; auto; intro l'; specialize (ceb l').
      destruct (L.eq_dec l' ℓ); destruct (L.eq_dec ℓ l'); subst;
        try match goal with
            | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
            end; auto.
    - apply IHC; split; auto.
    - apply IHC; split; auto; intro l'; specialize (ceb l'); destruct ceb;
        destruct (L.eq_dec l' ℓ); subst; auto.
    - specialize (ceb ℓ); destruct (L.eq_dec ℓ ℓ) as [_|neq];[|destruct (neq eq_refl)];
        destruct ceb; auto.
    - apply IHC1; split; auto. intro l; apply ceb; auto.
    - apply IHC2; split; auto. intro l; apply ceb; auto.
  Qed.

  Definition PirExprLVarClosed (C : PirExpr) := forall l, PirExprLVarClosedAbove l 0 C.
  Definition PirExprCVarClosed := PirExprCVarClosedAbove 0.
  Definition PirExprClosed := PirExprClosedAbove (fun _ => 0) 0.
  Corollary PirExprClosedSpec : forall C, PirExprClosed C <->  PirExprCVarClosed C /\ PirExprLVarClosed C.
  Proof using.
    unfold PirExprLVarClosed; unfold PirExprCVarClosed; unfold PirExprClosed.
    intro C; apply PirExprClosedAbove_Spec.
  Qed.

  Lemma PirExprLVarClosedAbove_Subst : forall (C : PirExpr) (σ : Loc -> nat -> Expr) (f : Loc -> nat),
      (forall l, PirExprLVarClosedAbove l (f l) C) -> (forall l n, n < f l -> σ l n = ExprVar n) ->
      C [local|σ] = C.
  Proof using.
    intro C; induction C; intros σ f closed_b static_above; cbn in *; auto.
    - specialize (closed_b ℓ). destruct (L.eq_dec ℓ ℓ) as [_ | n]; [|destruct (n eq_refl)].
      rewrite ExprClosedAboveSubst with (n := f ℓ); auto.
    - rewrite (IHC (PirExprUpLocalSubst σ ℓ2) (fun l => if L.eq_dec l ℓ2
                                                 then S (f l)
                                                 else f l)).
      rewrite ExprClosedAboveSubst with (n := f ℓ1); auto.
      -- specialize (closed_b ℓ1); destruct (L.eq_dec ℓ1 ℓ1) as [_ | n];
           [|destruct (n eq_refl)]; destruct closed_b; auto.
      -- intro l''; specialize (closed_b l''); destruct closed_b.
         destruct (L.eq_dec l'' ℓ2); subst; auto.
      -- intros l0 n H; specialize (closed_b l0); destruct closed_b;
           destruct (L.eq_dec l0 ℓ1); subst; unfold PirExprUpLocalSubst.
         destruct (L.eq_dec ℓ1 ℓ2); destruct (L.eq_dec ℓ2 ℓ1); subst;
           try match goal with
               | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
               end. destruct n; auto. apply lt_S_n in H. rewrite static_above; auto.
         rewrite ExprRenameVar; auto.
         apply static_above; auto.
         destruct (L.eq_dec ℓ2 l0); destruct (L.eq_dec l0 ℓ2); subst;
           try match goal with
               | [H : ?a <> ?a |- _ ] => destruct (H eq_refl)
               end.
         destruct n; auto. apply lt_S_n in H; rewrite static_above; auto.
         rewrite ExprRenameVar; auto.
         apply static_above; auto.
    - rewrite (IHC1  σ f); auto.
      rewrite (IHC2 σ f); auto.
      2,3: intro l'; specialize (closed_b l'); destruct closed_b as [H1 [H2 H3]]; auto.
      rewrite ExprClosedAboveSubst with (n := f ℓ); auto.
      specialize (closed_b ℓ); destruct closed_b as [H1 H2];
        destruct (L.eq_dec ℓ ℓ) as [_ |n]; [|destruct (n eq_refl)]; auto.
    - rewrite (IHC σ f); auto.
    - rewrite (IHC1 σ f); auto.
      2: intro l0; specialize (closed_b l0); destruct closed_b; auto.
      rewrite (IHC2 (PirExprUpLocalSubst σ ℓ) (fun l' => if L.eq_dec ℓ l'
                                                  then S (f l')
                                                  else f l')); auto.
      all: intro l0; 
        destruct (closed_b l0) as [H1 H2];
        destruct (L.eq_dec ℓ l0); destruct (L.eq_dec l0 ℓ); subst;
          try match goal with
              | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
              end; auto.
      all: intros m lt; unfold PirExprUpLocalSubst.
      destruct (L.eq_dec ℓ ℓ) as [_ |n]; [|destruct (n eq_refl)]. destruct m; auto.
      apply lt_S_n in lt. rewrite static_above; auto. apply ExprRenameVar.
      destruct (L.eq_dec ℓ l0) as [eq|_]; [destruct (n eq)|]. apply static_above; auto.
    - rewrite IHC with (f := fun l' => if L.eq_dec l' ℓ then S (f l') else f l'); auto.
      intro l'; specialize (closed_b l'); destruct (L.eq_dec l' ℓ); auto.
      intros l' m; unfold PirExprUpLocalSubst;
        destruct (L.eq_dec ℓ l'); destruct (L.eq_dec l' ℓ); subst;
        try match goal with
            | [H : ?a <> ?a |- _] => destruct (H eq_refl)
            | [H : ?a = ?a |- _ ] => clear H
            end; intro m_lt_f; auto.
      destruct m; auto. apply lt_S_n in m_lt_f. rewrite static_above; auto.
      apply ExprRenameVar.
    - rewrite IHC with (f := f); auto.
    - rewrite ExprClosedAboveSubst with (n := f ℓ).
      rewrite IHC with (f := f); auto.
      intro l'; apply closed_b.
      specialize (closed_b ℓ); destruct closed_b;
        destruct (L.eq_dec ℓ ℓ) as [_|neq];[|destruct (neq eq_refl)]; auto.
      specialize (static_above ℓ); auto.
    - rewrite IHC1 with (f := f); auto. rewrite IHC2 with (f := f); auto.
      all: intro l; apply closed_b; auto.
  Qed.
      
  Corollary PirExprLVarClosed_Subst : forall (C : PirExpr) (σ : Loc -> nat -> Expr),
      PirExprLVarClosed C -> C[local|σ] = C.
  Proof using.
    intros C σ; unfold PirExprLVarClosed; intro closed;
      apply PirExprLVarClosedAbove_Subst with (f := fun _ => 0); auto.
    intros l n H. inversion H.
  Qed.

  Corollary PirExprClosed_ExprSubst : forall (C : PirExpr) (σ : Loc -> nat -> Expr),
      PirExprClosed C -> C [local|σ] = C.
  Proof using.
    intros C σ H; apply PirExprClosedSpec in H; destruct H; apply PirExprLVarClosed_Subst; auto.
  Qed.

  Lemma PirExprCVarClosedAbove_Subst : forall (C : PirExpr) (σ : nat -> PirExpr) (n : nat),
      PirExprCVarClosedAbove n C -> (forall m, m < n -> σ m = Var m) ->
      C [global| σ] = C.
  Proof using.
    intros C; induction C; intros σ m closed_b static_above; cbn in *; auto.
    - rewrite (IHC (PirExprUpSubstAllLocal σ ℓ2) m); auto.
      unfold PirExprUpSubstAllLocal.
      intros k lt; rewrite static_above; auto.
    - destruct closed_b; rewrite (IHC1 σ m); auto; rewrite (IHC2 σ m); auto.
    - rewrite (IHC σ m); auto.
    - destruct closed_b; rewrite (IHC1 σ m); auto.
      rewrite (IHC2 (PirExprUpSubstAllLocal σ ℓ) m); auto.
      unfold PirExprUpSubstAllLocal; intros k lt; rewrite static_above; auto.
    - rewrite IHC with (n := S m); auto.
      intros n n_lt_Sm; unfold PirExprUpSubst; unfold PirExprUpSubstAllLocal; destruct n; auto.
      apply lt_S_n in n_lt_Sm; specialize (static_above n n_lt_Sm); rewrite static_above.
      cbn; auto.
    - rewrite IHC with (n := S (S m)); auto.
      intros n n_lt_SSm; unfold PirExprUpSubst; destruct n; auto; destruct n; auto.
      rewrite static_above; cbn; auto.
      apply lt_S_n in n_lt_SSm; apply lt_S_n in n_lt_SSm; auto.
    - rewrite IHC with (n := m); auto.
    - destruct closed_b; rewrite IHC1 with (n := m); auto; rewrite IHC2 with (n := m); auto.
  Qed.

  Corollary PirExprCVarClosed_Subst : forall (C : PirExpr) (σ : nat -> PirExpr),
      PirExprCVarClosed C -> C [global| σ] = C.
  Proof using.
    intros C σ H; unfold PirExprCVarClosed in H;
      apply PirExprCVarClosedAbove_Subst with (n := 0); auto.
    intros m H0; inversion H0.
  Qed.

  Corollary PirExprClosed_PirExprSubst : forall (C : PirExpr) (σ : nat -> PirExpr),
      PirExprClosed C -> C [global| σ] = C.
  Proof using.
    intros C σ H; apply PirExprClosedSpec in H; destruct H ;apply PirExprCVarClosed_Subst; auto.
  Qed.

  Lemma PirExprLVarClosedAboveMono : forall (C : PirExpr) (n m : nat) (l : Loc),
      n < m -> PirExprLVarClosedAbove l n C -> PirExprLVarClosedAbove l m C.
  Proof using.
    intro C; induction C; intros m k p lt closed_n; cbn in *; eauto.
    destruct (L.eq_dec p ℓ); subst; eauto. eapply ExprClosedAboveMono; eauto.
    destruct (L.eq_dec p ℓ1); destruct (L.eq_dec p ℓ2); subst; destruct closed_n; split;
      eauto.
    all: try match goal with
             | [ H : ?a < ?b, H' : ExprClosedAbove ?a ?e |- ExprClosedAbove ?b ?e ] =>
               apply (ExprClosedAboveMono a b e H H')
             end.
    1,2: apply lt_n_S in lt; eauto.
    all: repeat match goal with
                | [ |- True ] => exact I
                | [ H : ?P |- ?P ] => exact H
                | [ H : _ /\ _ |- _ ] => destruct H
                | [ |- _ /\ _ ] => split
                | [ H : context[L.eq_dec ?a ?b] |- _ ] => destruct (L.eq_dec a b); subst
                | [ |- context[L.eq_dec ?a ?b]] => destruct (L.eq_dec a b); subst
                | [ IH :forall n m l, n < m -> PirExprLVarClosedAbove l n ?C -> PirExprLVarClosedAbove l m ?C,
                      H : ?a < ?b, H' : PirExprLVarClosedAbove ?l ?a ?C |- _ ] =>
                  lazymatch goal with
                  | [ _ : PirExprLVarClosedAbove l b C |- _ ] => fail
                  | _ => pose proof (IH a b l H H')
                  end
                | [ H : ?a < ?b, H' : ExprClosedAbove ?a ?e |- ExprClosedAbove ?b ?e ] =>
                  apply (ExprClosedAboveMono a b e H H')
                end.
    all: apply lt_n_S in lt; eauto.
  Qed.

  Lemma PirExprCVarClosedAboveMono : forall (C : PirExpr) (n m : nat),
      n < m -> PirExprCVarClosedAbove n C -> PirExprCVarClosedAbove m C.
  Proof using.
    intros C; induction C; intros m k pf closed_below; cbn in *; eauto.
    transitivity m; auto.
    all: try (destruct closed_below; split; eauto).
    all: apply lt_n_S in pf; eauto.
    apply lt_n_S in pf; eauto.
  Qed.

  Lemma PirExprClosedAboveExt : forall C f g n,
      (forall l, f l = g l) ->
      PirExprClosedAbove f n C -> PirExprClosedAbove g n C.
  Proof using.
    intros C f g n H H0.
    apply PirExprClosedAbove_Spec in H0; destruct H0.
    apply PirExprClosedAbove_Spec; split; auto.
    intro l; rewrite <- H; apply H1.
  Qed.

  (* 
     Renaming and substitution have a bounded affect on closure. In particular,
     they only introduce "reasonable" new variables---i.e., those in the substituted 
     expressions.
   *)
  Lemma PirExprLocalRenameClosedAbove : forall C f g ξ n,
      (forall k l, k < f l -> ξ l k < g l) ->
      PirExprClosedAbove f n C ->
      PirExprClosedAbove g n (C ⟨local| ξ⟩).
  Proof using.
    intros C; induction C; try rename n into x; intros f g ξ n ξ_bdd C_clsd;
      inversion C_clsd; subst; auto; constructor; auto;
        try (eapply ExprRenameClosedAbove; eauto; fail).
    all: try (eapply IHC; eauto; fail);
      try (eapply IHC1; eauto; fail);
      try (eapply IHC2; eauto; fail).
    - eapply IHC; eauto. cbn; intros k l k_lt_fp.
      unfold PirExprUpLocalRename. unfold ExprUpRename. destruct (L.eq_dec ℓ2 l); subst.
      destruct k. apply Nat.lt_0_succ. apply Lt.lt_S_n in k_lt_fp.
      apply Lt.lt_n_S. 1,2: apply ξ_bdd; auto.
    - eapply IHC2; eauto. cbn; intros k l k_lt_fp.
      unfold PirExprUpLocalRename. unfold ExprUpRename. destruct (L.eq_dec ℓ l); subst.
      destruct k. apply Nat.lt_0_succ. apply Lt.lt_S_n in k_lt_fp.
      apply Lt.lt_n_S. 1,2: apply ξ_bdd; auto.
    - eapply IHC; eauto. cbn; intros k l k_lt_fp.
      unfold PirExprUpLocalRename. unfold ExprUpRename. destruct (L.eq_dec ℓ l); subst.
      destruct k. apply Nat.lt_0_succ. apply Lt.lt_S_n in k_lt_fp.
      apply Lt.lt_n_S. 1,2: apply ξ_bdd; auto.
  Qed.
          
  Lemma PirExprLocalSubstClosedAbove : forall C f g σ n,
      (forall k l, k < f l -> ExprClosedAbove (g l) (σ l k)) ->
      PirExprClosedAbove f n C ->
      PirExprClosedAbove g n (C [local| σ]).
  Proof using.
    intro C; induction C; try rename n into x; intros f g σ n σ_clsd C_clsd;
      inversion C_clsd; subst; constructor; auto;
        try (eapply ExprSubstClosedAbove; eauto; fail).
    all: try (eapply IHC; eauto; fail); try (eapply IHC1; eauto; fail);
      try (eapply IHC2; eauto; fail).
    - eapply IHC; eauto.
      intros k l k_lt_fp; cbn in *.
      unfold PirExprUpLocalSubst; destruct (L.eq_dec ℓ2 l); subst.
      destruct k. apply ExprVarClosed. apply Nat.lt_0_succ.
      eapply ExprRenameClosedAbove with (n := g l).
      intros k' lt_k'_fp; apply Lt.lt_n_S; auto.
      1,2: apply σ_clsd; auto; apply Lt.lt_S_n in k_lt_fp; auto.
    - eapply IHC2; eauto; cbn.
      intros k l k_lt_fp. unfold PirExprUpLocalSubst.
      destruct (L.eq_dec ℓ l); subst.
      destruct k. rewrite ExprVarClosed. apply Nat.lt_0_succ.
      eapply ExprRenameClosedAbove with (n := g l).
      intros k' lt_k'_fp; apply Lt.lt_n_S; auto.
      1,2: apply σ_clsd; auto; apply Lt.lt_S_n in k_lt_fp; auto.
    - eapply IHC; eauto; cbn.
      intros k l k_lt_fp. unfold PirExprUpLocalSubst.
      destruct (L.eq_dec ℓ l); subst.
      destruct k. rewrite ExprVarClosed. apply Nat.lt_0_succ.
      eapply ExprRenameClosedAbove with (n := g l).
      intros k' lt_k'_fp; apply Lt.lt_n_S; auto.
      1,2: apply σ_clsd; auto; apply Lt.lt_S_n in k_lt_fp; auto.
  Qed.

  Lemma PirExprClosedAboveRename : forall C f n m ξ,
      PirExprClosedAbove f n C ->
      (forall k, k < n -> ξ k < m) ->
      PirExprClosedAbove f m (C ⟨global|ξ⟩).
  Proof using.
    intros C; induction C; try rename n into x; intros f n m ξ cclosed ξ_bdd;
      inversion cclosed; subst; try (constructor; auto).
    all: try (eapply IHC; eauto; fail); try (eapply IHC1; eauto; fail);
      try (eapply IHC2; eauto; fail).
    - eapply IHC; eauto. unfold PirExprUpRename.
      intros k k_lt_Sn; destruct k. apply Nat.lt_0_succ.
      apply Lt.lt_S_n in k_lt_Sn. apply Lt.lt_n_S. auto.
    - eapply IHC; eauto. unfold PirExprUpRename.
      intros k k_lt_Sn; destruct k. apply Nat.lt_0_succ.
      apply Lt.lt_S_n in k_lt_Sn. apply Lt.lt_n_S. destruct k.
      apply Nat.lt_0_succ. apply Lt.lt_S_n in k_lt_Sn. apply Lt.lt_n_S. auto.
  Qed.
  
  Lemma PirExprClosedAboveSubst : forall C f n m σ,
      PirExprClosedAbove f n C ->
      (forall k, k < n -> PirExprClosedAbove f m (σ k)) ->
      PirExprClosedAbove f m (C [global| σ]).
  Proof using.
    intros C; induction C; try rename n into x; intros f n m σ c_clsd σ_clsd;
      inversion c_clsd; subst; cbn ;auto; try (constructor; auto).
    all: try (eapply IHC; eauto; fail); try (eapply IHC1; eauto; fail);
      try (eapply IHC2; eauto; fail).
    - eapply IHC; eauto. intros k k_lt_n.
      unfold PirExprUpSubstAllLocal. eapply PirExprLocalRenameClosedAbove; eauto.
      intros k' l k_lt_fp; cbn.
      destruct (L.eq_dec ℓ2 l); subst; auto. apply Lt.lt_n_S; auto.
    - eapply IHC2; eauto. intros k k_lt_n.
      unfold PirExprUpSubstAllLocal. eapply PirExprLocalRenameClosedAbove; eauto.
      intros k' l k_lt_fp; cbn.
      destruct (L.eq_dec ℓ l); subst; auto. apply Lt.lt_n_S; auto.
    - unfold PirExprUpSubst; unfold PirExprUpSubstAllLocal.
      eapply IHC; eauto.
      intros k lt_k_Sn.
      destruct k. constructor. apply Nat.lt_0_succ.
      apply PirExprClosedAboveRename with (n := m).
      apply Lt.lt_S_n in lt_k_Sn.
      apply PirExprLocalRenameClosedAbove with (f := f); auto.
      intros k' l k'_lt_fp. destruct (L.eq_dec ℓ l); subst; auto.
      apply lt_n_S; auto.
      intros k0 H; apply lt_n_S; auto.
    - eapply IHC; eauto. unfold PirExprUpSubst.
      intros k lt_k_SSn.
      destruct k; [constructor; apply Nat.lt_0_succ|].
      apply lt_S_n in lt_k_SSn.
      destruct k; [constructor; apply lt_n_S; auto; apply Nat.lt_0_succ|].
      apply PirExprClosedAboveRename with (n := S m).
      apply lt_S_n in lt_k_SSn.
      apply PirExprClosedAboveRename with (n := m); auto.
      1,2: intros k' k'_lt_m; apply lt_n_S; auto.
  Qed.      
  
  (* EQUIVALENCE *)

  (*
    Two Pirouette programs are equivalent if every location's view of the programs are 
    equal.
   *)

  (* 
     You might wonder why we don't have the ability to move over a def. It's because it's
     unsound! If you have l1 ⟪e⟫ → l2 ⟪x⟫ fby (let l3 ⟪y⟫ := C1 fby C2) (in the named form)
     then, you don't know if l2 uses x in C1 or not! 
   *)
  
  Reserved Notation "C1 ≡ C2" (at level 30).
  Inductive equiv : PirExpr -> PirExpr -> Prop :=
    (* Transitivity has to be axiomatized explicitly *)
  | equivTrans : forall C1 C2 C3, C1 ≡ C2 -> C2 ≡ C3 -> C1 ≡ C3
  (* Reflexivity comes from the following two rules + congruence *)
  | DoneRefl : forall l e, Done l e ≡ Done l e
  | VarRefl : forall n, Var n ≡ Var n
  (* The rules labeled `context` make equivalence a congruence *)
  | SendContext : forall l1 e l2 C1 C2,
      C1 ≡ C2 ->
      l1 ⟪e⟫ → l2 fby C1 ≡ l1 ⟪e⟫ → l2 fby C2
  | SyncContext : forall l1 d l2 C1 C2,
      C1 ≡ C2 ->
      l1 ⟨d⟩ → l2 fby C1 ≡ l1 ⟨d⟩ → l2 fby C2                  
  | IfContext : forall l e C11 C12 C21 C22,
      C11 ≡ C21 -> C12 ≡ C22 ->
      Cond l ⦃ e ⦄ Then C11 Else C12 ≡ Cond l⦃e⦄ Then C21 Else C22
  | DefLocalContext : forall l C11 C12 C21 C22,
      C11 ≡ C21 -> C12 ≡ C22 ->
      LetLocal l ⟪new⟫ ← C11 fby C12 ≡ LetLocal l ⟪new⟫ ← C21 fby C22
  | FunLocalContext : forall l C1 C2, (* Equivalence does work under lambda *)
      C1 ≡ C2 ->
      FunLocal l C1 ≡ FunLocal l C2
  | FunGlobalContext : forall C1 C2,
      C1 ≡ C2 ->
      FunGlobal C1 ≡ FunGlobal C2
  | AppLocalContext : forall C1 C2 l e,
      C1 ≡ C2 ->
      AppLocal l C1 e ≡ AppLocal l C2 e
  | AppGlobalContext : forall C11 C12 C21 C22,
      C11 ≡ C21 -> C12 ≡ C22 ->
      AppGlobal C11 C12 ≡ AppGlobal C21 C22
  (* All that's left is to define when you can swap two instructions. Note that most of 
     these are presented twice, once in each direction. This allows us to prove symmetry
     without assuming it. *)
  | SwapSendSend : forall l1 e l2 l3 e' l4 C,
      l1 <> l3 -> l1 <> l4 -> l2 <> l3 -> l2 <> l4 ->
      l1 ⟪e⟫ → l2 fby (l3 ⟪e'⟫ → l4 fby C) ≡ l3 ⟪e'⟫ → l4 fby (l1 ⟪e⟫ → l2 fby C)
  | SwapSendSync : forall l1 e l2 l3 d l4 C,
      l1 <> l3 -> l1 <> l4 -> l2 <> l3 -> l2 <> l4 ->
      l1 ⟪e⟫ → l2 fby (l3 ⟨d⟩ → l4 fby C) ≡ l3 ⟨d⟩ → l4 fby (l1 ⟪e⟫ → l2 fby C)
  | SwapSyncSend : forall l1 d l2 l3 e l4 C,
      l1 <> l3 -> l1 <> l4 -> l2 <> l3 -> l2 <> l4 ->
      l1 ⟨d⟩ → l2 fby (l3 ⟪e⟫ → l4 fby C) ≡ l3 ⟪e⟫ → l4 fby (l1 ⟨d⟩ → l2 fby C)
  | SwapSendIf : forall l1 e l2 l3 e' C1 C2,
      l1 <> l3 -> l2 <> l3 ->
      l1 ⟪e⟫ → l2 fby (Cond l3 ⦃e'⦄ Then C1 Else C2) ≡ Cond l3 ⦃e'⦄ Then (l1 ⟪e⟫ → l2 fby C1) Else (l1 ⟪e⟫ → l2 fby C2)
  | SwapIfSend : forall l1 e l2 e' l3 C1 C2,
      l1 <> l2 -> l1 <> l3 ->
      Cond l1 ⦃e⦄ Then (l2 ⟪e'⟫ → l3 fby C1) Else (l2 ⟪e'⟫ → l3 fby C2) ≡ l2 ⟪e'⟫ → l3 fby (Cond l1 ⦃e⦄ Then C1 Else C2)
  | SwapSyncSync : forall l1 d l2 l3 d' l4 C,
      l1 <> l3 -> l1 <> l4 -> l2 <> l3 -> l2 <> l4 ->
      l1 ⟨d⟩ → l2 fby (l3 ⟨d'⟩ → l4 fby C) ≡ l3 ⟨d'⟩ → l4 fby (l1 ⟨d⟩ → l2 fby C)
  | SwapSyncIf : forall l1 d l2 l3 e C1 C2,
      l1 <> l3 -> l2 <> l3 ->
      l1 ⟨d⟩ → l2 fby (Cond l3 ⦃e⦄ Then C1 Else C2) ≡ Cond l3 ⦃e⦄ Then (l1 ⟨d⟩ → l2 fby C1) Else (l1 ⟨d⟩ → l2 fby C2)
  | SwapIfSync : forall l1 e l2 d l3 C1 C2,
      l1 <> l2 -> l1 <> l3 ->
      Cond l1 ⦃e⦄ Then (l2 ⟨d⟩ → l3 fby C1) Else (l2 ⟨d⟩ → l3 fby C2) ≡ l2 ⟨d⟩ → l3 fby (Cond l1 ⦃e⦄ Then C1 Else C2)
  | SwapIfIf : forall l1 e l2 e' C1 C2 C3 C4,
      l1 <> l2 ->
      Cond l1 ⦃e⦄ Then (Cond l2 ⦃e'⦄ Then C1 Else C2) Else (Cond l2 ⦃e'⦄ Then C3 Else C4) ≡ Cond l2 ⦃e'⦄ Then (Cond l1 ⦃e⦄ Then C1 Else C3) Else (Cond l1 ⦃e⦄ Then C2 Else C4)
  where "C1 ≡ C2" := (equiv C1 C2).
  Global Hint Constructors equiv : PirExpr.

  (* Reflexivity *)
  Fixpoint equivRefl (C : PirExpr) : C ≡ C :=
    match C with
    | Done l e => DoneRefl l e
    | Var n => VarRefl n
    | Send l1 e l2 C => SendContext l1 e l2 C C (equivRefl C)
    | If l e C1 C2 => IfContext l e C1 C2 C1 C2 (equivRefl C1) (equivRefl C2)
    | Sync l1 d l2 C => SyncContext l1 d l2 C C (equivRefl C)
    | DefLocal l C1 C2 => DefLocalContext l C1 C2 C1 C2 (equivRefl C1) (equivRefl C2)
    | FunLocal l C => FunLocalContext l C C (equivRefl C)
    | FunGlobal C => FunGlobalContext C C (equivRefl C)
    | AppLocal l C e => AppLocalContext C C l e (equivRefl C)
    | AppGlobal C1 C2 => AppGlobalContext C1 C2 C1 C2 (equivRefl C1) (equivRefl C2)
    end.
  Global Hint Resolve equivRefl : PirExpr.
  (* Declare it so that we can use the `reflexivity` tactic *)
  Instance : Reflexive equiv := equivRefl. 

  (* Symmetry*)
  Theorem equivSym : forall C1 C2 : PirExpr, C1 ≡ C2 -> C2 ≡ C1.
  Proof using.
    intros C1 C2 equiv; induction equiv; eauto with PirExpr.
  Qed.
  Global Hint Resolve equivSym : PirExpr.
  Instance : Symmetric equiv := equivSym. (* Allow the `symmetry` tactic. *)

  (* Declare transitivity *)
  Instance : Transitive equiv := equivTrans. (* Allow the `transitivity` tactic *)
  
  (* Smart constructors for ≡ *)
  (*
    The following mix swaps with context rules, usig transitivity. This gives a nice set
    of rules to build proofs with, even as the rules above are nicer to examine.
   *)
  Lemma SmartSwapSendSend : forall (l1 l2 l3 l4 : Loc) (C1 C2 : PirExpr) (e1 e2 : Expr),
      l1 <> l3 -> l2 <> l3 -> l1 <> l4 -> l2 <> l4 ->
      C1 ≡ C2 ->
      Send l1 e1 l2 (Send l3 e2 l4 C1) ≡ Send l3 e2 l4 (Send l1 e1 l2 C2).
  Proof using.
    intros l1 l2 l3 l4 C1 C2 e1 e2 neq_p_r neq_q_r neq_p_s neq_q_s eqv;
      etransitivity; [apply SwapSendSend|]; auto with PirExpr.
  Qed.
  Lemma SmartSwapSendIf : forall l1 e1 l2 e2 l3 C1 C1' C2 C2',
      l1 <> l2 -> l1 <> l3 ->
      C1 ≡ C1' -> C2 ≡ C2' ->
      Send l2 e2 l3 (If l1 e1 C1 C2) ≡ If l1 e1 (Send l2 e2 l3 C1') (Send l2 e2 l3 C2').
  Proof using.
    intros n l1 e1 l2 e2 l3 C1 C1' C2 C2' neq1 neq2 equiv1; etransitivity;
      [apply SwapSendIf |]; eauto with PirExpr.
  Qed.
  Lemma SmartSwapIfSend : forall l1 e1 l2 e2 l3 C1 C1' C2 C2',
      l1 <> l2 -> l1 <> l3 ->
      C1 ≡ C1' -> C2 ≡ C2' ->
      If l1 e1 (Send l2 e2 l3 C1) (Send l2 e2 l3 C2) ≡ Send l2 e2 l3 (If l1 e1 C1' C2').
  Proof using.
    intros n l1 e1 l2 e2 l3 C1 C1' C2 C2' neq1 neq2 equiv1; etransitivity;
      [apply SwapIfSend|]; eauto with PirExpr.
  Qed.
  Lemma SmartSwapSendSync : forall (l1 l2 l3 l4 : Loc) (e : Expr) (s : SyncLabel)
                              (C C' : PirExpr),
      l1 <> l3 -> l1 <> l4 -> l2 <> l3 -> l2 <> l4 -> C ≡ C' ->
      Send l1 e l2 (Sync l3 s l4 C) ≡ Sync l3 s l4 (Send l1 e l2 C').
  Proof using.
    intros l1 l2 l3 l4 e s C C' neq1 neq2 neq3 neq4 equiv; etransitivity;
      [apply SwapSendSync|]; eauto with PirExpr.
  Qed.
  Lemma SmartSwapSyncSend : forall (l1 l2 l3 l4 : Loc) (e : Expr) (s : SyncLabel)
                              (C C' : PirExpr),
      l1 <> l3 -> l1 <> l4 -> l2 <> l3 -> l2 <> l4 -> C ≡ C' ->
      Sync l1 s l2 (Send l3 e l4 C) ≡ Send l3 e l4 (Sync l1 s l2 C').
  Proof using.
    intros p q r s e d C C' neq1 neq2 neq3 neq4 equiv; etransitivity;
      [apply SwapSyncSend|]; eauto with PirExpr.
  Qed.
  Lemma SmartSwapIfIf : forall l1 e1 l2 e2 C11 C11' C12 C12' C21 C21' C22 C22',
      l1 <> l2 ->
      C11 ≡ C11' -> C12 ≡ C12' -> C21 ≡ C21' -> C22 ≡ C22' ->
      If l1 e1 (If l2 e2 C11 C12) (If l2 e2 C21 C22) ≡
         If l2 e2 (If l1 e1 C11' C21') (If l1 e1 C12' C22').
  Proof using.
    intros n l1 e1 m l2 e2 C11 C11' C12 C12' C21 C21' C22 C22' neq equiv1; etransitivity;
      [apply SwapIfIf|]; eauto with PirExpr.
  Qed.
  Lemma SmartSwapIfSync : forall (l1 l2 l3 : Loc) (e : Expr) (s : SyncLabel)
                            (C1 C1' C2 C2' : PirExpr),
      l1 <> l2 -> l1 <> l3 -> C1 ≡ C1' -> C2 ≡ C2' ->
      If l1 e (Sync l2 s l3 C1) (Sync l2 s l3 C2) ≡ Sync l2 s l3 (If l1 e C1' C2').
  Proof using.
    intros m l1 l2 l3 e s C1 C1' C2 C2' neq1 neq2 equiv1; etransitivity;
      [apply SwapIfSync|]; eauto with PirExpr.
  Qed.
  Lemma SmartSwapSyncIf : forall (l1 l2 l3 : Loc) (e : Expr) (s : SyncLabel)
                            (C1 C1' C2 C2' : PirExpr),
      l1 <> l3 -> l2 <> l3 -> C1 ≡ C1' -> C2 ≡ C2' ->
      Sync l1 s l2 (If l3 e C1 C2) ≡ If l3 e (Sync l1 s l2 C1') (Sync l1 s l2 C2').
  Proof using.
    intros m p q r e d C1 C1' C2 C2' neq1 neq2 equiv1; etransitivity;
      [apply SwapSyncIf|]; eauto with PirExpr.
  Qed.
  Lemma SmartSwapSyncSync : forall (l1 l2 l3 l4 : Loc) (s1 s2 : SyncLabel) (C C' : PirExpr),
      l1 <> l3 -> l1 <> l4 -> l2 <> l3 -> l2 <> l4 -> C ≡ C' ->
      Sync l1 s1 l2 (Sync l3 s2 l4 C) ≡ Sync l3 s2 l4 (Sync l1 s1 l2 C').
  Proof using.
    intros l1 l2 l3 l4 s1 s2 C1 C1' neq1 neq2 neq3 neq4 equiv; etransitivity;
      [apply SwapSyncSync|]; auto with PirExpr.
  Qed.
  Global Hint Resolve SmartSwapSendSend SmartSwapSendIf SmartSwapIfSend SmartSwapIfIf
         SmartSwapSendSync SmartSwapSyncSend SmartSwapIfSync SmartSwapSyncSync : PirExpr.

  (* Properties of equivalence *)

  (*
    Values are only equivalent to values.
   *)
  Lemma PirExprValStableEquiv : forall C1 C2,
      PirExprVal C1 -> C1 ≡ C2 -> PirExprVal C2.
  Proof using.
    intros C1 C2 val1 eqv; revert val1; induction eqv; intro val1; inversion val1;
      subst; auto.
    all: inversion val1; subst; constructor; auto; eapply PirExprClosedAboveEquiv; eauto.
  Qed.
  
  (*
    Local renaming and subsitution do not affect equivalence, as long as you do the 
    same renamings/subsitutions on each side.
   *)
  Lemma EquivStableLocalRename : forall (ξ : Loc -> nat -> nat) (C1 C2 : PirExpr),
      C1 ≡ C2 -> C1 ⟨local| ξ ⟩ ≡ C2 ⟨local| ξ⟩.
  Proof using.
    intros ξ C1 C2 eqv; revert ξ; induction eqv; intros ξ; cbn; auto; try reflexivity.
    etransitivity; eauto. (* Transitivity case is easy to solve *)
    all: eauto with PirExpr. (* This takes care of most of the cases, but takes a second. *)
    - (* Swap Send Send*)
      unfold PirExprUpLocalRename at 1 4.
      destruct (L.eq_dec l2 l3) as [eq|_]; [destruct (H1 eq)|].
      destruct (L.eq_dec l4 l1) as [eq|_]; [destruct (H0 (eq_sym eq))|].
      apply SmartSwapSendSend; auto.
      erewrite PirExprLocalRenameExtensional; [reflexivity|].
      intros l n; unfold PirExprUpLocalRename.
      destruct (L.eq_dec l4 l); destruct (L.eq_dec l2 l); subst;
        try match goal with | [H : ?a <> ?a |- _ ] => destruct (H eq_refl) end; auto.
    - (* Swap Send If *)
      etransitivity; [apply SwapSendIf|]; auto.
      unfold PirExprUpLocalRename at 1; destruct (L.eq_dec l2 l3) as [eq|_];
        [destruct (H0 eq)|]; reflexivity.
    - (* Swap If Send *)
      etransitivity; [apply SwapIfSend|]; auto.
      unfold PirExprUpLocalRename at 3.
      destruct (L.eq_dec l3 l1) as [eq|_]; [destruct (H0 (eq_sym eq))|]; reflexivity.
  Qed.

  Lemma EquivStableLocalSubst : forall(σ : Loc -> nat -> Expr) (C1 C2 : PirExpr),
      C1 ≡ C2 -> C1 [local| σ] ≡ C2 [local| σ].
  Proof using.
    intros σ C1 C2 eqv; revert σ; induction eqv; intros σ; cbn; auto; try reflexivity.
    etransitivity; eauto.
    all: eauto with PirExpr.
    - (* Swap Send Send *)
      unfold PirExprUpLocalSubst at 1 4; destruct (L.eq_dec l2 l3);
        destruct (L.eq_dec l4 l1); subst;
          try match goal with [H : ?a <> ?a |- _ ] => destruct (H eq_refl) end; auto.
      etransitivity; [apply SwapSendSend|]; auto;
        erewrite PirExprLocalSubstExt; [reflexivity|].
      intros l n1; unfold PirExprUpLocalSubst.
      destruct (L.eq_dec l4 l); destruct (L.eq_dec l2 l); subst;
        try match goal with [H : ?a <> ?a |- _] => destruct (H eq_refl) end; auto.
    - (* Swap Send If *)
      etransitivity; [apply SwapSendIf|]; auto.
      unfold PirExprUpLocalSubst at 1; destruct (L.eq_dec l2 l3) as [eq|_];
        [destruct (H0 eq)|]; reflexivity.
    - (* Swap If Send *)
      etransitivity; [apply SwapIfSend|]; auto.
      unfold PirExprUpLocalSubst at 3; destruct (L.eq_dec l3 l1) as [eq|_];
        [destruct (H0 (eq_sym eq))|]; reflexivity.
  Qed.

  (* 
     Similarly, doing the same global renaming/substitution on both sides of an equivalence
     does not change the equivalence.
   *)
  Lemma EquivStableRename : forall (ξ : nat -> nat) (C1 C2 : PirExpr),
      C1 ≡ C2 -> C1 ⟨global| ξ⟩ ≡ C2 ⟨global| ξ ⟩.
  Proof using.
    intros ξ C1 C2 eqv; revert ξ; induction eqv; intro ξ; cbn; auto with PirExpr.
    transitivity (C2 ⟨global| ξ⟩); auto.
  Qed.

  Lemma EquivStableSubst : forall (σ : nat -> PirExpr) (C1 C2 : PirExpr),
      C1 ≡ C2 -> C1 [global| σ ] ≡ C2 [global| σ].
  Proof using.
    intros σ C1 C2 eqv; revert σ; induction eqv; intro σ; cbn; auto with PirExpr.
    transitivity (C2 [global| σ]); auto.
    apply SmartSwapSendSend; auto. erewrite PirExprSubstExt; [reflexivity|].
    intro n; unfold PirExprUpSubstAllLocal.
    repeat rewrite PirExprLocalRenameFusion. apply PirExprLocalRenameExtensional.
    intros l' m; destruct (L.eq_dec l2 l'); destruct (L.eq_dec l4 l'); subst;
      try match goal with
          | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
          end; reflexivity.
  Qed.

  (* 
     It turns out that we often need to preserver equivalences with weaker conditions.
     In particular, we should only need to use the fact that every Pirouette program in the
     substitions are equivalent.
   *)
  Lemma WeakSubstUpExt : forall (σ1 σ2 : nat -> PirExpr),
      (forall n, σ1 n ≡ σ2 n) ->
      forall n, PirExprUpSubst σ1 n ≡ PirExprUpSubst σ2 n.
  Proof using.
    intros σ1 σ2 H n; destruct n; unfold PirExprUpSubst; cbn; auto with PirExpr.
    apply EquivStableRename; auto.
  Qed.
  Global Hint Resolve EquivStableLocalRename EquivStableLocalSubst EquivStableRename
         EquivStableSubst WeakSubstUpExt : PirExpr.

  Lemma WeakSubstUpForExprExt : forall (σ1 σ2 : nat -> PirExpr),
      (forall n, σ1 n ≡ σ2 n) ->
      forall l n, PirExprUpSubstAllLocal σ1 l n ≡ PirExprUpSubstAllLocal σ2 l n.
  Proof using.
    intros σ1 σ2 eqv l n.
    unfold PirExprUpSubstAllLocal. apply EquivStableLocalRename; auto.
  Qed.
  Global Hint Resolve WeakSubstUpForExprExt : PirExpr.

  (* 
     This applies when we substitute equivalent things into a single choreography. We use
     it to prove the more-general theorem shortly. However, this gives us the ability to
     induct over Pirouette programs, as opposed to equivalences on them.
   *)
  Lemma WeakSubstExt' : forall (σ1 σ2 : nat -> PirExpr) (C :PirExpr),
      (forall n, σ1 n ≡ σ2 n) -> C [global| σ1] ≡ C[global|σ2].
  Proof using.
    intros σ1 σ2 C; revert σ1 σ2; induction C; intros σ1 σ2 eqv; cbn; auto with PirExpr.
  Qed.
  Global Hint Resolve WeakSubstExt' : PirExpr.
  
  Lemma WeakSubstExt : forall (σ1 σ2 : nat -> PirExpr) (C1 C2 : PirExpr),
      C1 ≡ C2 -> (forall n, σ1 n ≡ σ2 n) ->
      C1 [global| σ1] ≡ C2 [global| σ2].
  Proof using.
    intros σ1 σ2 C1 C2 eqv; revert σ1 σ2; induction eqv; intros σ1 σ2 eqvσ; cbn;
      auto with PirExpr.
    - transitivity (C2 [global|σ2]); auto with PirExpr.
    - apply SmartSwapSendSend; auto.
      apply WeakSubstExt'.
      (* At this point, we're done reasoning aobut C1 and C2. *)
      intro n. unfold PirExprUpSubstAllLocal. repeat rewrite PirExprLocalRenameFusion.
      transitivity (σ2 n ⟨local| fun l n0 => if L.eq_dec l4 l
                                          then S (if L.eq_dec l2 l then S n0 else n0)
                                          else if L.eq_dec l2 l then S n0 else n0⟩).
      apply EquivStableLocalRename; auto.
      erewrite PirExprLocalRenameExtensional; [reflexivity|].
      intros l n0; cbn. destruct (L.eq_dec l4 l); destruct (L.eq_dec l2 l); auto.
  Qed.
  Global Hint Resolve WeakSubstExt : PirExpr.

  (*
    A few inversion lemmas, useful for values: if a Pirouette program is equivalent
    to a value, it must itself be the same type of value.
   *)
  Lemma RetEquivInversion : forall l e C, Done l e ≡ C -> C = Done l e.
  Proof using.
    intros l e C eqv; dependent induction eqv; auto.
  Qed.

  Lemma FunLocalEquivInversion : forall l C1 C2,
      FunLocal l C1 ≡ C2 -> exists C1', C2 = FunLocal l C1' /\ C1 ≡ C1'.
  Proof using.
    intros l C1 C2 eqv; dependent induction eqv; auto.
    specialize (IHeqv1 l C1 eq_refl). destruct IHeqv1 as [C1' [eq eqv']]; subst.
    specialize (IHeqv2 l C1' eq_refl). destruct IHeqv2 as [C1'' [eq eqv'']]; subst.
    exists C1''; split; auto. transitivity C1'; auto.
    exists C2; split; auto with PirExpr.
  Qed.

  Lemma FunGlobalEquivInversion :
    forall C1 C2, FunGlobal C1 ≡ C2 -> exists C1', C2 = FunGlobal C1' /\ C1 ≡ C1'.
  Proof using.
    intros C1 C2 eqv; dependent induction eqv; auto.
    specialize (IHeqv1 C1 eq_refl); destruct IHeqv1 as [C1' [eq eqv1']]; subst.
    specialize (IHeqv2 C1' eq_refl); destruct IHeqv2 as [C1'' [eq eqv2']]; subst.
    exists C1''; split; eauto with PirExpr.
    exists C2; split; auto with PirExpr.
  Qed.

  (* Equivalent programs have the same open variables. *)
  Lemma PirExprClosedAboveEquiv : forall C1 C2,
      C1 ≡ C2 -> forall f n, PirExprClosedAbove f n C1 -> PirExprClosedAbove f n C2.
  Proof using.
    intros C1 C2 equiv; induction equiv; intros f m closed_b;
      try (inversion closed_b; subst; econstructor; eauto with PirExpr; fail);
      repeat match goal with
             | [ H : ?P |- ?P ] => exact H
             | [ H : ?a <> ?a |- _] => destruct (H eq_refl)
             | [ H :PirExprClosedAbove ?f ?m ?C |- _ ] =>
               tryif let H := fresh "H_" C in idtac
               then fail
               else inversion H; subst; clear H
             | [ H : context[L.eq_dec ?a ?b] |- _ ] => destruct (L.eq_dec a b); subst
             end; try (repeat econstructor; eauto; fail).
    - (* Swap Send Send *)
      repeat econstructor; eauto.
      destruct (L.eq_dec l4 l1) as [eq|_]; [destruct (H0 (eq_sym eq))|]; auto.
      destruct (L.eq_dec l4 l2) as [eq|_]; [destruct (H2 (eq_sym eq))|].
      eapply PirExprClosedAboveExt; eauto.
      intro l; cbn; destruct (L.eq_dec l4 l); destruct (L.eq_dec l2 l); subst; auto.
    - (* Swap Send If *)
      repeat econstructor; eauto.
      destruct (L.eq_dec l3 l1) as [eq|_]; [destruct (H0 (eq_sym eq))|]; auto.
  Qed.

  (* LOCATIONS IN PIROUETTE PROGRAMS *)

  (*
    Collects the set of location names found in a Pirouette program as a list.
   *)
  Fixpoint LocsInPirExpr (C : PirExpr) : list Loc :=
    match C with
    | Done l _ => l :: nil
    | Var _ => nil
    | Send l1 _ l2 C' => l1 :: l2 :: (LocsInPirExpr C')
    | If l1 _ C1 C2 => l1 :: (LocsInPirExpr C1) ++ (LocsInPirExpr C2)
    | Sync l1 s l2 C => l1 :: l2 :: LocsInPirExpr C
    | DefLocal l C1 C2 => l :: (LocsInPirExpr C1) ++ (LocsInPirExpr C2)
    | FunLocal l C => l :: LocsInPirExpr C
    | FunGlobal C => LocsInPirExpr C
    | AppLocal l C e => l :: LocsInPirExpr C
    | AppGlobal C1 C2 => (LocsInPirExpr C1) ++ (LocsInPirExpr C2)
    end.

  (* 
     Equivalent programs have the same locations in them.
   *)
  Lemma LocsInPirExprInvariant : forall C1 C2 : PirExpr,
      C1 ≡ C2 -> forall l : Loc, In l (LocsInPirExpr C1) <-> In l (LocsInPirExpr C2).
  Proof using.
    intros C1 C2 equiv; induction equiv; intros l'; split; intros i; cbn in *;
      repeat match goal with
             | [ H: _ \/ _ |- _ ] => destruct H
             | [ H : In _ (_ ++ _) |- _ ] => apply in_app_or in H; cbn in H
             | [ |- context[In _ (_ ++ _)] ] => rewrite in_app_iff; cbn
             | [ IH : forall l, In l (LocsInPirExpr ?C1) <-> In l (LocsInPirExpr ?C2),
                   H : In ?l (LocsInPirExpr ?C1) |- _ ] =>
               lazymatch goal with
               | [_ : In l (LocsInPirExpr C2) |-_ ] => fail
               | _ => pose proof (proj1 (IH l) H)
               end
             | [ IH : forall l, In l (LocsInPirExpr ?C1) <-> In l (LocsInPirExpr ?C2),
                   H : In ?l (LocsInPirExpr ?C2) |- _ ] =>
               lazymatch goal with
               | [_ : In l (LocsInPirExpr C1) |-_ ] => fail
               | _ => pose proof (proj2 (IH l) H)
               end
             end; eauto.
    (* 
       Eauto leaves a bunch of ors here, so go right until auto can take care of it.
     *)
    all: repeat (right; auto). 
  Qed.

  Lemma PirExprClosedAboveLocsInPirExpr : forall C f g n,
      PirExprClosedAbove f n C ->
      (forall l, In l (LocsInPirExpr C) -> f l = g l) ->
      PirExprClosedAbove g n C.
  Proof using.
    intro C; induction C; intros f g m clsd eq;
      inversion clsd; subst; constructor; auto; cbn in *;
        repeat match goal with
               | [ H : forall l', ?l = l' \/ ?P -> f l' = g l' |- _ ] =>
                 assert (f l = g l) by apply (H l ltac:(left; reflexivity));
                   let l'' := fresh "l" in
                   let H' := fresh in
                   assert (forall l', P -> f l' = g l')
                     by (intros l'' H'; apply (H l'' ltac:(right; exact H')));
                     clear H
               | [ H: forall l, False -> _ |- _ ] => clear H
               | [ H : ExprClosedAbove (?f ?l) ?e, H' : ?f ?l = ?g ?l'
                   |- ExprClosedAbove (?g ?l') ?e] => rewrite <- H'; exact H
               | [ H : forall l', In l' (?a ++ ?b) -> ?P |- _ ] =>
                 let H' := fresh in
                 let l' := fresh "l" in
                 let H1 := fresh in 
                 assert (forall l', In l' a -> P) as H1
                     by (intros l' H'; apply H; apply in_or_app; left; exact H');
                   assert (forall l', In l' b -> P)
                   by (intros l' H'; apply H; apply in_or_app; right; exact H');
                   clear H
               | [IH : forall f g n, PirExprClosedAbove f n ?C ->
                                     (forall l, In l (LocsInPirExpr ?C) -> f l = g l) ->
                                     PirExprClosedAbove g n ?C,
                    H1 : PirExprClosedAbove ?f ?n ?C,
                    H2 : forall l, In l (LocsInPirExpr ?C) -> ?f l = ?g l
                              |- PirExprClosedAbove ?g ?n ?C] =>
                 apply (IH f g n H1 H2)
               end; auto.
    - apply IHC with (f := fun r => if L.eq_dec ℓ2 r then S (f ℓ2) else f r); auto.
      intros l'' i; destruct (L.eq_dec ℓ2 l''); subst; auto.
    - apply IHC2 with (f := fun r => if L.eq_dec ℓ r then S (f ℓ) else f r); auto.
      intros l'' i; destruct (L.eq_dec ℓ l''); subst; auto. 
    - apply IHC with (f := fun r => if L.eq_dec ℓ r then S (f ℓ) else f r); auto.
      intros l'' i; destruct (L.eq_dec ℓ l''); subst; auto. 
  Qed.

  (* OPERATIONAL SEMANTICS VIA A LABELED-TRANSITION SYSTEM *)

  (* 
     Redices are the labels for the labeled-transition system semantics of Pirouette.
   *)
  Inductive Redex : Set :=
  | RDone : Loc -> Expr -> Expr -> Redex
  | RIfE : Loc -> Expr -> Expr -> Redex
  | RIfTT : Loc -> Redex
  | RIfFF : Loc -> Redex
  | RSendE : Loc -> Expr -> Expr -> Loc -> Redex
  | RSendV : Loc -> Expr -> Loc -> Redex
  | RSync : Loc -> SyncLabel -> Loc -> Redex
  | RDefLocal : Loc -> Expr -> Redex
  | RAppLocalE : Loc -> Expr -> Expr -> Redex
  | RAppLocal : Loc -> Expr -> Redex
  | RAppGlobal : Redex
  | RFun : Redex -> Redex
  | RArg : Redex -> Redex.
  Global Hint Constructors Redex : PirExpr.

  (* 
     Substitute a single local variable owned by l with the local expression `v`. We intend
     `v` to be a value, hence the name. This is used for sending and local functions.
   *)
  Definition ValueSubst (l : Loc) (v : Expr) : Loc -> nat -> Expr :=
    fun l' n =>
      if L.eq_dec l l'
      then match n with
           | 0 => v
           | S m => ExprVar m
           end
      else ExprVar n.

  (*
    Substutitons for functions take care of recursion. For global functions, we 
    simultaneously substitute the argument of the function.
   *)
  Definition AppLocalSubst (l : Loc) (C : PirExpr) : nat -> PirExpr :=
    fun n => match n with
          | 0 => FunLocal l C
          | S m => Var m
          end.

  Definition AppGlobalSubst (C1 C2 : PirExpr) : nat -> PirExpr :=
    fun n => match n with
          | 0 => C2
          | 1 => FunGlobal C1
          | S (S m) => Var m
          end.

  Lemma UpValueSubst : forall (l1 l2 : Loc) (v : Expr),
      l1 <> l2 ->
      forall l3 n, PirExprUpLocalSubst (ValueSubst l1 v) l2 l3 n = ValueSubst l1 v l3 n.
  Proof using.
    intros l1 l2 v H l3 n.
    unfold PirExprUpLocalSubst; unfold ValueSubst.
    destruct (L.eq_dec l2 l3);
      destruct (L.eq_dec l1 l3); auto.
    exfalso; apply H; transitivity l3; auto.
    destruct n; auto. rewrite ExprRenameVar; auto.
  Qed.

  (* 
     The labeled-transition system itself. Here, the list `B` is the set of _blocked_ 
     locations, which are not allowed to participate in the step. Note how `B` grows
     as we go under constructors.
   *)
  Inductive PirStep : Redex -> list Loc -> PirExpr -> PirExpr -> Prop :=
  | CDoneEStep : forall (l : Loc) (B : list Loc) (e1 e2 : Expr),
      ~ In l B ->
      ExprStep e1 e2 ->
      PirStep (RDone l e1 e2) B (Done l e1) (Done l e2)
  | CSendEStep : forall (l1 l2 : Loc) (B : list Loc),
      ~ In l1 B -> l1 <> l2 ->
      forall (e1 e2 : Expr) (C : PirExpr),
        ExprStep e1 e2 -> PirStep (RSendE l1 e1 e2 l2) B (Send l1 e1 l2 C) (Send l1 e2 l2 C)
  | CSendIStep : forall (l1 l2 : Loc) (e : Expr) (C1 C2 : PirExpr) (B : list Loc) (R : Redex),
      PirStep R (l1 :: l2 :: B) C1 C2 ->
      PirStep R B (Send l1 e l2 C1) (Send l1 e l2 C2)
  | CSendVStep : forall (l1 l2 : Loc) (v : Expr) (C : PirExpr) (B : list Loc),
      ~ In l1 B -> ~ In l2 B -> l1 <> l2 ->
      ExprVal v ->
      PirStep (RSendV l1 v l2) B (Send l1 v l2 C) (C [local| ValueSubst l2 v])
  | CIfEStep : forall (l1 : Loc) (e1 e2 : Expr) (C1 C2 : PirExpr) (B : list Loc),
      ~ In l1 B ->
      ExprStep e1 e2 ->
      PirStep (RIfE l1 e1 e2) B (If l1 e1 C1 C2) (If l1 e2 C1 C2)
  | CIfIStep : forall (l1 : Loc) (e : Expr) (C1 C2 C3 C4 : PirExpr) (B : list Loc) (R : Redex),
      PirStep R (l1 :: B) C1 C3 ->
      PirStep R (l1 :: B) C2 C4 ->
      PirStep R B (If l1 e C1 C2) (If l1 e C3 C4)
  | CIfTrueStep : forall (l1 : Loc) (C1 C2 : PirExpr) (B : list Loc),
      ~ In l1 B ->
      PirStep (RIfTT l1) B (If l1 tt C1 C2) C1
  | CIfFalseStep : forall (l1 : Loc) (C1 C2 : PirExpr) (B : list Loc),
      ~ In l1 B ->
      PirStep (RIfFF l1) B (If l1 ff C1 C2) C2
  | CDefLocalIStep : forall R l C1 C1' C2 B,
      PirStep R B C1 C1' ->
      PirStep (RArg R) B (DefLocal l C1 C2) (DefLocal l C1' C2)
  | CDefLocalStep : forall (l : Loc) (v : Expr) (C2 : PirExpr),
      ExprVal v ->
      PirStep (RDefLocal l v) nil (DefLocal l (Done l v) C2) (C2 [local| ValueSubst l v])
  | CAppLocalFunStep : forall (l : Loc) (C1 C2 : PirExpr) (e : Expr) R B,
      PirStep R B C1 C2 ->
      PirStep (RFun R) B (AppLocal l C1 e) (AppLocal l C2 e)
  | CAppLocalArgStep : forall (l : Loc) (C : PirExpr) (e1 e2 : Expr) B,
      ~ In l B ->
      ExprStep e1 e2 ->
      PirStep (RAppLocalE l e1 e2) B (AppLocal l C e1) (AppLocal l C e2)
  | CAppLocalStep : forall (l : Loc) (C : PirExpr) (v : Expr),
      ExprVal v ->
      PirStep (RAppLocal l v) [] (AppLocal l (FunLocal l C) v)
                        (C [local| ValueSubst l v] [global| AppLocalSubst l C])
  | CAppGlobalFunStep : forall (C1 C1' C2 : PirExpr) R B,
      PirStep R B C1 C1' ->
      PirStep (RFun R) B (AppGlobal C1 C2) (AppGlobal C1' C2)
  | CAppGlobalArgStep : forall (C1 C2 C2' : PirExpr) R B,
      PirStep R B C2 C2' ->
      PirStep (RArg R) B (AppGlobal C1 C2) (AppGlobal C1 C2')
  | CAppGlobalStep : forall (C1 C2 : PirExpr),
      PirExprVal C2 ->
      PirStep RAppGlobal [] (AppGlobal (FunGlobal C1) C2)
                        (C1 [global| AppGlobalSubst C1 C2])
  | CSyncStep : forall (l1 l2 : Loc) (d : SyncLabel) (C : PirExpr) (B : list Loc),
      ~ In l1 B -> ~ In l2 B -> l1 <> l2 ->
      PirStep (RSync l1 d l2) B (Sync l1 d l2 C) C
  | CSyncIStep : forall (l1 l2 : Loc) (d : SyncLabel) (C1 C2 : PirExpr) (B : list Loc) (R : Redex),
      PirStep R (l1 :: l2 :: B) C1 C2 ->
      PirStep R B (Sync l1 d l2 C1) (Sync l1 d l2 C2).
  Global Hint Constructors PirStep : PirExpr.

  (* Multiple steps, formalized as the reflexive-transitive closure. *)
  Inductive PirSteps : list Redex -> list Loc -> PirExpr -> PirExpr -> Prop :=
  | NoPirSteps : forall B C, PirSteps [] B C C
  | AddPirStep : forall R Rs B C1 C2 C3,
      PirStep R B C1 C2 -> PirSteps Rs B C2 C3 -> PirSteps (R :: Rs) B C1 C3.
  Global Hint Constructors PirSteps : PirExpr.
  
  (* If no more locations are blocked, you can take the same steps. *)
  Lemma PirStepSub : forall R B1 C1 C2,
      PirStep R B1 C1 C2 -> forall B2, (forall q, In q B2 -> In q B1) -> PirStep R B2 C1 C2.
  Proof using.
    intros R B1 C1 C2 step; induction step; intros B2 sub.
    all: repeat (match goal with
                 | [ H : ~ In ?l ?B, H' : forall l', In l' ?B2 -> In l' ?B |- _ ] =>
                   lazymatch goal with
                   | [ _ : ~ In l B2 |- _ ] => fail
                   | _ =>
                     let ni := fresh "ni" in
                     assert (~ In l B2) as ni
                         by (intro i; apply H' in i; apply H in i; destruct i)
                   end
                 | [s : forall l, In l ?B2 -> In l ?B1, IH : forall B2, (forall l, In l B2 -> In l ?B1) -> _
                                                             |- _ ] => specialize (IH B2 s)
                 | [s : forall l, In l ?B2 -> In l ?B1,
                      IH : forall B2, (forall l, In l B2 -> In l (?a :: ?B1)) -> _ |- _ ] =>
                   let sub := fresh "sub" in
                   assert (forall l, In l (a :: B2) -> In l (a :: B1)) as sub
                       by (intros l i; destruct i as [eq | i];
                           [left; auto | right; apply s; auto]);
                   specialize (IH (a :: B2) sub)
                 | [s : forall l, In l ?B2 -> In l ?B1,
                      IH : forall B2, (forall l, In l B2 -> In l (?a :: ?b :: ?B1)) -> _ |- _ ] =>
                   let sub := fresh "sub" in
                   assert (forall l, In l (a :: b :: B2) -> In l (a :: b :: B1)) as sub
                       by (intros l i; destruct i as [eq | i];
                           [left; auto | right; destruct i as [eq | i];
                                         [left; auto | right; apply s; auto]]);
                   specialize (IH (a :: b :: B2) sub)
                 end; auto with PirExpr).
    - (* Reducing a send. Note that B2 must be empty. *)
      destruct B2; auto with PirExpr.
      specialize (sub l0 ltac:(left; reflexivity)); inversion sub.
    - (* Reducing an AppLocal. *)
      destruct B2; auto with PirExpr.
      specialize (sub l0 ltac:(left; reflexivity)); inversion sub.
    - (* Reducing an AppGlobal *)
      destruct B2; auto with PirExpr.
      specialize (sub l ltac:(left; reflexivity)); inversion sub.
  Qed.

  (* 
     We treat `B` as a set: the order or number of times a location is in `B` doesn't
     matter.
   *)
  Corollary PirStepRearrange : forall R B1 C1 C2,
      PirStep R B1 C1 C2 -> forall B2, (forall q, In q B1 <-> In q B2) -> PirStep R B2 C1 C2.
  Proof using.
    intros R B1 C1 C2 H B2 H0; apply PirStepSub with (B1 := B1); auto.
    intros l H1; apply H0; auto.
  Qed.

  (*
    If we ever reduce with a send redex, then the two locations in the redex must be
    unequal. Similarly with sync.
   *)
  Lemma PirStepSendVDistinguish : forall l1 v l2 B C1 C2,
      PirStep (RSendV l1 v l2) B C1 C2 ->
      l1 <> l2.
  Proof using.
    intros l1 v l2 B C1 C2 step; dependent induction step;
      repeat match goal with
             | [ IH : forall a b c, RSendV ?l1 ?v ?l2 = RSendV a b c -> _ |- _ ] =>
               specialize (IH l1 v l2 eq_refl)
             end; auto.
  Qed.

  Lemma PirStepSyncDistinguish : forall l1 s l2 B C1 C2,
      PirStep (RSync l1 s l2) B C1 C2 ->
      l1 <> l2.
  Proof using.
    intros l1 s l2 B C1 C2 step; dependent induction step;
      repeat match goal with
             | [ IH : forall a b c, RSync ?l1 ?s ?l2 = RSync a b c -> _ |- _ ] =>
               specialize (IH l1 s l2 eq_refl)
             end; auto.
  Qed.

  (* INVOLVED WITH REDEX *)
  (*
    Redices can tell us what locations must not be blocked in order for a reduction to
    take place. Most importantly, we can use this to rule out certain reductions entirely.
   *)

  Fixpoint InvolvedWithRedex (R : Redex) (l : L.t) : Prop :=
    match R with
    | RDone l' e1 e2 => l = l'
    | RIfE l' e1 e2 => l = l'
    | RIfTT l' => l = l'
    | RIfFF l' => l = l'
    | RSendE l1 e1 e2 l2 => l = l1
    | RSendV l1 v l2 => l = l1 \/ l = l2
    | RSync l1 d l2 => l = l1 \/ l = l2
    | RDefLocal l' e => l = l'
    | RAppLocalE l' e1 e2 => l = l'
    | RAppLocal l' e => l = l'
    | RAppGlobal => False
    | RFun R => InvolvedWithRedex R l
    | RArg R => InvolvedWithRedex R l
    end.

  (* 
     We can be sure that certain redices---those that synchronize with everyone---take
     place only when the block list is completely empty.
   *)
  Lemma RDefLocalNoBlockers : forall l e B C1 C2,
      PirStep (RDefLocal l e) B C1 C2 ->
      B = [].
  Proof using.
    intros l e B C1 C2 step; dependent induction step;
      repeat match goal with
             | [H: _ :: _ = [] |- _ ] => inversion H
             | [IH : forall a b, RDefLocal ?l ?e = RDefLocal a b -> _ |- _ ] =>
               specialize (IH l e eq_refl)
             end; auto.
  Qed.

  Lemma RAppLocalNoBlockers : forall l e B C1 C2,
      PirStep (RAppLocal l e) B C1 C2 ->
      B = [].
  Proof using.
    intros l e B C1 C2 step; dependent induction step;
      repeat match goal with
             | [H: _ :: _ = [] |- _ ] => inversion H
             | [IH : forall a b, RAppLocal ?l ?e = RAppLocal a b -> _ |- _ ] =>
               specialize (IH l e eq_refl)
             end; auto.
  Qed.

  Lemma RAppGlobalNoBlockers : forall B C1 C2,
      PirStep RAppGlobal B C1 C2 ->
      B = [].
  Proof using.
    intros B C1 C2 step; dependent induction step;
      repeat match goal with
             | [H: _ :: _ = [] |- _ ] => inversion H
             | [IH : ?a = ?a -> _ |- _ ] =>
               specialize (IH eq_refl)
             end; auto.
  Qed.
  
  (*
    We can rule out any step with a redex that involves a blocked location.
   *)
  Lemma NoPirStepInList : forall l B R,
      In l B ->
      InvolvedWithRedex R l ->
      forall C1 C2, ~PirStep R B C1 C2.
  Proof using.
    intros l B R H H0 C1 C2 step; induction step; cbn in H0;
    match goal with
    | [ i : In ?l ?B, n : ~ In ?l' ?B, e : ?l = ?l' |- False ] =>
      apply n; rewrite <- e; exact i
    | [ i : In ?l ?B, n : ~ In ?l' ?B, e : ?l' = ?l |- False ] =>
      apply n; rewrite e; exact i
    | [ H : _ \/ _ |- _ ] => destruct H; subst
    | [ H1 : ?P, H2 : ~ ?P |- _ ] => destruct (H2 H1)
    | _ => idtac
    end.
    all: try (apply IHstep; auto; right; right; auto; fail).
    all: try (apply IHstep1; auto; right; auto; fail).
    all: try match goal with
             | [H1 : ?P, H2 : ~?P |- _] => destruct (H2 H1)
             end.
    all: inversion H.
  Qed.

  (*
    Local Substititons do not change the locations in a Pirouette program.
   *)
  Lemma LocsInPirExprLocalSubst : forall C σ,
      LocsInPirExpr C = LocsInPirExpr (C [local| σ]).
  Proof using.
    intro C; induction C; intros σ; cbn; auto.
    all: try (erewrite IHC; eauto; fail).
    all: rewrite IHC1 with (σ := σ).
    1,3: rewrite IHC2 with (σ := σ); reflexivity.
    rewrite IHC2 with (σ := PirExprUpLocalSubst σ ℓ); reflexivity.
  Qed.
  
  (* 
     Renaming choreography variables does not change the locations in a Pirouette program.
   *)
  Lemma LocsInPirExprRenaming : forall C ξ,
      LocsInPirExpr (C ⟨global| ξ⟩) = LocsInPirExpr C.
  Proof using.
    intro C; induction C; intros ξ; cbn; auto.
    all: try (erewrite IHC; eauto; fail).
    all: rewrite IHC1 with (ξ := ξ); rewrite IHC2 with (ξ := ξ); auto.
  Qed.

  (*
    Substituting a variable for a Pirouette program can only add the locations in the
    Pirouette programs being substituted in.
   *)
  Lemma LocsInPirExprSubst : forall C σ l,
      In l (LocsInPirExpr (C [global| σ])) ->
      In l (LocsInPirExpr C) \/ exists n, In l (LocsInPirExpr (σ n)).
  Proof using.
    intro C; induction C; cbn; intros σ l i; auto;
      repeat match goal with
             | [ i : _ \/ _ |- _ ] => destruct i; auto
             | [ i : In _ (_ ++ _) |- _ ] => apply in_app_or in i
             | [ IH: forall σ l, In l (LocsInPirExpr (?C [global|σ])) ->
                            In l (LocsInPirExpr ?C) \/ (exists n, In l (LocsInPirExpr (σ n))),
                   H : In ?l (LocsInPirExpr (?C [global|?σ])) |- _ ] =>
               lazymatch goal with
               | [_ : In l (LocsInPirExpr C) |- _ ] => fail
               | [_ : In l (LocsInPirExpr (σ _)) |- _ ] => fail
               | _ => let H' := fresh in destruct (IH σ l H) as [H'|H'];
                                          [| let n := fresh "n" in destruct H' as [n H']];
                                          auto
               end
             | [ H : In ?p (LocsInPirExpr (?σ ?n)) |-
                 _ \/ exists m, In ?p (LocsInPirExpr (?σ m))] =>  right; exists n; auto
             end.
    all: try (left; right; apply in_or_app; auto; fail).
    - right. unfold PirExprUpSubstAllLocal in H0. exists n0.
      rewrite PirExprLocalRenameSpec in H0. rewrite <- LocsInPirExprLocalSubst in H0; auto.
    - right. unfold PirExprUpSubstAllLocal in H0. exists n0.
      rewrite PirExprLocalRenameSpec in H0. rewrite <- LocsInPirExprLocalSubst in H0; auto.
    - right. unfold PirExprUpSubstAllLocal in H0.
      unfold PirExprUpSubst in H0; destruct n0. cbn in H0; destruct H0.
      rewrite LocsInPirExprRenaming in H0.
      rewrite PirExprLocalRenameSpec in H0; rewrite <- LocsInPirExprLocalSubst in H0.
      exists n0; auto.
    - unfold PirExprUpSubst in H. destruct n0; [cbn in H; destruct H|].
      destruct n0; [cbn in H; destruct H|].
      repeat rewrite LocsInPirExprRenaming in H. right; exists n0; auto.
    - left; apply in_or_app; auto.
    - left; apply in_or_app; auto.
  Qed.

  (*
    Importantly, taking a step cannot add locations to a Pirouette programs. However, it can
    remove them. To see this, consider the following Pirouette program:
    l1.4 -> l2.x; l2.(x + 3)
    Taking a step reduces to l2.(4 + 3), which does not contain l1.
   *)
  Lemma LocsInPirExprAfterStep : forall R B C1 C2 l,
      PirStep R B C1 C2 ->
      In l (LocsInPirExpr C2) ->
      In l (LocsInPirExpr C1).
  Proof using.
    intros R B C1 C2 l step; revert l; induction step;
      cbn; intros p i; auto;
        repeat match goal with
               | [ i : _ \/ _ |- _ ] => destruct i; auto
               | [ i : In _ (_ ++ _) |- _ ] => apply in_app_or in i
               | [ IH : forall l, In l (LocsInPirExpr ?C2) -> In l (LocsInPirExpr ?C1),
                     i : In ?l (LocsInPirExpr ?C2) |- _ ] =>
                 lazymatch goal with
                 | [ _ : In l (LocsInPirExpr C1) |- _ ] => fail
                 | _ => pose proof (IH l i); auto
                 end
               end.
    rewrite <- LocsInPirExprLocalSubst in i; auto.
    1-6: right; apply in_or_app; auto.
    3-7: apply in_or_app; auto.
    - rewrite <- LocsInPirExprLocalSubst in i; auto.
    - apply LocsInPirExprSubst in i; destruct i.
      rewrite <- LocsInPirExprLocalSubst in H0; auto.
      destruct H0 as [n i]; unfold AppLocalSubst in i.
      destruct n; [| cbn in i; destruct i]. cbn in i; auto.
    - apply LocsInPirExprSubst in i; destruct i; auto.
      destruct H0 as [n i]; unfold AppGlobalSubst in i.
      destruct n; auto. destruct n; auto. cbn in i; destruct i.
  Qed.

  Lemma LocsInPirExprAfterSteps : forall Rs B C1 C2 l,
      PirSteps Rs B C1 C2 ->
      In l (LocsInPirExpr C2) ->
      In l (LocsInPirExpr C1).
  Proof using.
    intros Rs; induction Rs as [|R Rs]; intros B C1 C2 l steps i;
      inversion steps; subst; auto.
    apply IHRs with (l := l) in H5; auto.
    apply LocsInPirExprAfterStep with (l := l) in H1; auto.
  Qed.

  (*
    Taking steps does not introduce new free variables.
   *)
  Lemma PirExprClosedAboveAfterStep : forall R B C1 C2 f n,
      PirStep R B C1 C2 ->
      PirExprClosedAbove f n C1 ->
      PirExprClosedAbove f n C2.
  Proof using.
    intros R B C1 C2 f n step; revert f n; induction step; intros f n clsd;
      inversion clsd; subst; auto; try (constructor; auto);
        try (eapply ExprClosedAfterStep; eauto; fail);
        try (eapply IHC; eauto; fail); try (eapply IHC1; eauto; fail);
          try (eapply IHC2; eauto; fail).
    - eapply PirExprLocalSubstClosedAbove; eauto; cbn; intros k l k_lt_fp.
      unfold ValueSubst. destruct (L.eq_dec l2 l); subst.
      destruct k.
      destruct (f l). apply ExprValuesClosed; auto.
      eapply ExprClosedAboveMono; [|apply ExprValuesClosed; auto]. apply Nat.lt_0_succ.
      apply lt_S_n in k_lt_fp. all: apply ExprVarClosed; auto.
    - eapply PirExprLocalSubstClosedAbove; eauto; cbn; intros k l' k_lt_fp.
      unfold ValueSubst. destruct (L.eq_dec l l'); subst.
      destruct k.
      destruct (f l'). apply ExprValuesClosed; auto.
      eapply ExprClosedAboveMono; [|apply ExprValuesClosed; auto]. apply Nat.lt_0_succ.
      apply lt_S_n in k_lt_fp. all: apply ExprVarClosed; auto.
    - inversion H4; subst. apply PirExprClosedAboveSubst with (n := S n); eauto.
      eapply PirExprLocalSubstClosedAbove; eauto; cbn.
      intros k l' k_lt_p. unfold ValueSubst. destruct (L.eq_dec l l'); subst.
      destruct k; auto. apply lt_S_n in k_lt_p. 1,2: apply ExprVarClosed; auto.
      intros k k_lt_Sn. unfold AppLocalSubst. destruct k; auto.
      apply lt_S_n in k_lt_Sn; constructor; auto.
    - inversion H4; subst. eapply PirExprClosedAboveSubst with (n := S (S n)); auto.
      intros k k_lt_SSn. unfold AppGlobalSubst. destruct k; auto.
      destruct k; auto. do 2 apply lt_S_n in k_lt_SSn; constructor; auto.
  Qed.

  Lemma PirExprClosedAfterStep : forall R B C1 C2,
      PirStep R B C1 C2 ->
      PirExprClosed C1 ->
      PirExprClosed C2.
  Proof using.
    intros R B C1 C2 H H0. unfold PirExprClosed in *.
    eapply PirExprClosedAboveAfterStep; eauto.
  Qed.

  Lemma PirExprClosedAfterSteps : forall Rs B C1 C2,
      PirExprClosed C1 ->
      PirSteps Rs B C1 C2 ->
      PirExprClosed C2.
  Proof using.
    intros Rs; induction Rs as [|R Rs]; intros B C1 C2 closed1 steps;
      inversion steps; subst; auto.
    apply PirExprClosedAfterStep in H1; auto. eapply IHRs in H5; eauto.
  Qed.

  (*
    Locations involved in a reduction must be in the program being reduced (though not 
    necessarily the program after reduction).
   *)
  Lemma InvolvedWithLocsInPirExpr : forall R B C1 C2,
      PirStep R B C1 C2 ->
      forall l, InvolvedWithRedex R l -> In l (LocsInPirExpr C1).
  Proof using.
    intros R B C1; revert R B; induction C1; intros R B C' step p inv;
      inversion step; subst; cbn in *; auto;
        repeat match goal with
               | [ H : False |- _ ] => destruct H
               | [ H : _ \/ _ |- _] => destruct H; subst
               | [ |- ?a = ?a \/ _ ] => left; reflexivity
               | [ |- _ \/ ?a = ?a \/ _ ] => right; left; reflexivity
               end; auto.
    - right; right; eapply IHC1; eauto.
    - right; apply in_or_app; left; eapply IHC1_1; eauto.
    - right; right; eapply IHC1; eauto.
    - right; apply in_or_app; left; eapply IHC1_1; eauto.
    - right; eapply IHC1; eauto.
    - apply in_or_app; left; eapply IHC1_1; eauto.
    - apply in_or_app; right; eapply IHC1_2; eauto.
  Qed.


  (* REDEX OF *)
  
  (*
    A slightly-different notion of what locations are actively involved in a redex.
    Whereas InvolvedWithRedex tells us which locations cannot be blocked in order to use
    a redex, RedexOf tells us which locations might have their projected programs change
    with this redex.
   *)
  Inductive RedexOf : Loc -> Redex -> Prop :=
  | RODone : forall l e1 e2, RedexOf l (RDone l e1 e2)
  | ROIfE : forall l e1 e2, RedexOf l (RIfE l e1 e2)
  | ROIfTT : forall l, RedexOf l (RIfTT l)
  | ROIfFF : forall l, RedexOf l (RIfFF l)
  | ROSendE : forall l1 e1 e2 l2, RedexOf l1 (RSendE l1 e1 e2 l2)
  | ROSendV1 : forall l1 v l2, RedexOf l1 (RSendV l1 v l2)
  | ROSendV2 : forall l1 v l2, RedexOf l2 (RSendV l1 v l2)
  | ROSync1 : forall l1 s l2, RedexOf l1 (RSync l1 s l2)
  | ROSync2 : forall l1 s l2, RedexOf l2 (RSync l1 s l2)
  | ROLDef : forall l e, RedexOf l (RDefLocal l e)
  | ROArg : forall l R, RedexOf l R -> RedexOf l (RArg R)
  | ROFun : forall l R, RedexOf l R -> RedexOf l (RFun R).
  Global Hint Constructors RedexOf: PirExpr.


  (* Any location which R is a redex of cannot be blocked *)
  Lemma UNoIStepInList : forall l B R,
      In l B ->
      RedexOf l R ->
      forall C1 C2, ~PirStep R B C1 C2.
  Proof using.
    intros l B R H H0 C1 C2 step; induction step; inversion H0; subst;
    match goal with
    | [ i : In ?l ?B, n : ~ In ?l ?B |- False ] =>
      destruct (n i)
    | _ => idtac
    end.
    all: try (apply IHstep; auto; right; right; auto; fail).
    all: try (apply IHstep1; auto; right; auto; fail).
    all: inversion H.
  Qed.

  (*
    As a corollary of the above, we can rule out a large number of reductions.
   *)
  Corollary UNoDoneIStepInList : forall l B,
      In l B ->
      forall e1 e2 C1 C2, ~PirStep (RDone l e1 e2) B C1 C2.
  Proof using.
    intros l B H e1 e2 C1 C2; apply UNoIStepInList with (l := l); auto; apply RODone.
  Qed.

  Corollary UNoSendEIStepInList1 : forall l1 B,
      In l1 B ->
      forall e1 e2 C1 C2 l2, ~PirStep (RSendE l1 e1 e2 l2) B C1 C2.
  Proof using.
    intros l1 B H l2 e1 e2 C1 C2; apply UNoIStepInList with (l := l1); auto; apply ROSendE.
  Qed.

  Corollary UNoSendVIStepInList1 : forall l1 B,
      In l1 B ->
      forall v l2 C1 C2, ~PirStep (RSendV l1 v l2) B C1 C2.
  Proof using.
    intros l1 B H v l2 C1 C2; apply UNoIStepInList with (l := l1); auto; apply ROSendV1.
  Qed.

  Corollary UNoSendVIStepInList2 : forall B l2,
      In l2 B ->
      forall l1 v C1 C2, ~PirStep (RSendV l1 v l2) B C1 C2.
  Proof using.
    intros B l2 H l1 v C1 C2; apply UNoIStepInList with (l := l2); auto; apply ROSendV2.
  Qed.

  Corollary UNoIfEIStepInList : forall l B,
      In l B ->
      forall e1 e2 C1 C2, ~PirStep (RIfE l e1 e2) B C1 C2.
  Proof using.
   intros l B H e1 e2 C1 C2; apply UNoIStepInList with (l := l); auto; apply ROIfE.
  Qed.

  Corollary UNoIfTTStepInList : forall l B,
      In l B ->
      forall C1 C2, ~PirStep (RIfTT l) B C1 C2.
  Proof using.
   intros l B H C1 C2; apply UNoIStepInList with (l := l); auto; apply ROIfTT.
  Qed.

  Corollary UNoIfFFStepInList : forall l B,
      In l B ->
      forall C1 C2, ~PirStep (RIfFF l) B C1 C2.
  Proof using.
   intros l B H C1 C2; apply UNoIStepInList with (l := l); auto; apply ROIfFF.
  Qed.
  
  Corollary UNoSyncStepInList1 : forall l1 B,
      In l1 B ->
      forall s l2 C1 C2, ~PirStep (RSync l1 s l2) B C1 C2.
  Proof using.
   intros l1 B H l2 C1 C2; apply UNoIStepInList with (l := l1); auto; constructor.
  Qed. 

  Corollary UNoSyncStepInList2 : forall B l2,
      In l2 B ->
      forall l1 s C1 C2, ~PirStep (RSync l1 s l2) B C1 C2.
  Proof using.
   intros B l2 H l1 C1 C2; apply UNoIStepInList with (l := l2); auto; constructor.
  Qed.

  Corollary UNoLDefStepInList : forall B l,
      In l B ->
      forall e C1 C2, ~PirStep (RDefLocal l e) B C1 C2.
  Proof using.
   intros B l H e C1 C2; apply UNoIStepInList with (l := l); auto; constructor.
  Qed.

  Global Hint Resolve PirStepRearrange UNoDoneIStepInList : PirExpr.
  Global Hint Resolve UNoSendEIStepInList1 UNoSendVIStepInList1
         UNoSendVIStepInList2 : PirExpr.
  Global Hint Resolve UNoIfEIStepInList UNoIfTTStepInList UNoIfFFStepInList: PirExpr.
  Global Hint Resolve UNoSyncStepInList1 UNoSyncStepInList2 UNoLDefStepInList : PirExpr.

  (* SIMULATION *)

  (* 
     A quick pair of tactics to help determine prove that something is or is not in a list.
   *)
  Ltac InList := repeat match goal with
                       | [ H : ?P |- ?P ] => exact H
                       | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
                       | [ |- In ?a (?a :: _) ] => left; reflexivity
                       | [ i : In ?a (_ :: _) |- _ ] => destruct i; subst
                       | [ |- In ?a (_ :: _) ] => right
                       end.
  Ltac NotInList :=
    repeat match goal with
           | [ |- ~ In ?a nil ] => let H := fresh in intro H; inversion H
           | [ nin : ~ In ?a ?l |- ~ In ?a ?l ] => exact nin
           | [ H : ~ In ?a (?b :: ?l) |- _ ] =>
             match goal with
             | [ _ : a <> b, _ : ~ In a l |- _ ] => fail 1
             | _ => assert (a <> b) by (let eq := fresh in intro eq; apply H; left; auto);
                   assert(~ In a l) by (let i := fresh in intro i; apply H; right; auto)
             end
           | [ neq : ?a <> ?b |- ~ In ?a (?b :: ?l) ] =>
             let H := fresh in
             intro H; destruct H as [eq | i]; [exfalso; apply neq; auto | revert i; fold (not (In a l))]
           | [i : ~ In ?p ?B1, ext : forall q, In q ?B1 <-> In q ?B2 |- ~ In ?p ?B2 ] =>
             let H := fresh in intro H; rewrite <- ext in H; apply i; exact H
           end.


  (*
    Equivalence is well-behaved with respect to the operational semantics.
    In particular, it behaves as a simulation.

    N.B.: This takes a while to run. In particular, one step takes many minutes.
   *)
  Theorem EquivSimulation : forall C1 C2, C1 ≡ C2 -> forall R B C1',
        PirStep R B C1 C1' -> exists C2', PirStep R B C2 C2' /\ C1' ≡ C2'.
  Proof using.
    intros C1 C2 equiv; induction equiv; intros R B Cstep step;
      eauto with PirExpr; repeat match goal with
                              | [ H : PirStep ?R ?B ?C1 ?C1' |- _ ] =>
                                tryif (let x := fresh "fail_" C1 in idtac)
                                then fail
                                else inversion H; subst; eauto with PirExpr; clear H
                              end.
    all: try (eexists; split; eauto with PirExpr; fail).
    all: repeat match goal with
                | [IH : forall R B C', PirStep R B ?C C' ->
                                  exists C2', PirStep R B ?C2 C2' /\ C' ≡ C2',
                     H : PirStep ?R ?B ?C ?C' |- _ ] =>
                  lazymatch goal with
                  | [ _ : PirStep R B C2 ?C2', _ : C' ≡ ?C2' |- _] => fail
                  | _ => let H' := fresh in
                        let C2 := fresh "C" in
                        let step := fresh "step" in
                        let eqv := fresh "eqv" in
                        pose proof (IH R B C' H) as H'; destruct H' as [C2 [step eqv]]
                  end
                end; eauto with PirExpr.
    apply RetEquivInversion in equiv1; subst; eexists; split; eauto with PirExpr.
    (* These two commands, starting with `all:`, take quite a while. *)
    all: try (exfalso; eapply UNoIStepInList; eauto; eauto with PirExpr; InList; fail).
    all: try (eexists; split; [repeat econstructor |]; eauto with PirExpr; try NotInList;
              auto;
              try (eapply PirStepRearrange; eauto with PirExpr; intros l; split; unfold In;
                   intro i; tauto);
              fail).
    - apply FunLocalEquivInversion in equiv0; destruct equiv0 as [C' [eq eqv]]; subst.
      exists (C' [local|ValueSubst l e] [global|AppLocalSubst l C'] ); split; auto with PirExpr.
      apply WeakSubstExt; [apply EquivStableLocalSubst|]; auto.
      intro n; destruct n; cbn; auto with PirExpr.
    - apply FunGlobalEquivInversion in equiv1; destruct equiv1 as [C1' [eq equiv1]]; subst.
      exists (C1' [global| AppGlobalSubst C1' C22]); split; auto with PirExpr.
      apply CAppGlobalStep; auto.
      eapply PirExprValStableEquiv; eauto.
      apply WeakSubstExt; auto. intro n; destruct n; cbn; auto with PirExpr.
      destruct n; auto with PirExpr.
    - exists (l1 ⟪e⟫ → l2 fby C [local|ValueSubst l4 e']); split; eauto with PirExpr.
      pose proof (CSendVStep l3 l4 e' (l1 ⟪e⟫ → l2 fby C) B ltac:(NotInList) ltac:(NotInList) H13 H14) as H3; cbn in H3.
      unfold ValueSubst in H3 at 1; destruct (L.eq_dec l4 l1) as [eq|_]; [destruct (H0 (eq_sym eq))|].
      fold ExprIdSubst in H3; rewrite ExprIdentitySubstSpec in H3.
      rewrite PirExprLocalSubstExt with (σ2 := ValueSubst l4 e') in H3; auto.
      intros l n; unfold PirExprUpLocalSubst. unfold ValueSubst.
      destruct (L.eq_dec l2 l) as [eq1 | neq1]; destruct(L.eq_dec l4 l) as [eq2 |neq2];
        subst; try match goal with
                   | [H : ?a <> ?a |- _ ] => destruct (H eq_refl)
                   end.
      destruct n; cbn; auto; apply ExprRenameVar.
      all: reflexivity.
    - exists (l3 ⟪e'⟫ → l4 fby (C [local|ValueSubst l2 e])); split; auto with PirExpr.
      apply CSendIStep. apply CSendVStep; auto; NotInList.
      cbn; unfold ValueSubst at 1.
        destruct (L.eq_dec l2 l3) as [eq |_]; [destruct (H1 eq)|].
      fold ExprIdSubst. rewrite ExprIdentitySubstSpec.
      rewrite PirExprLocalSubstExt with (σ2 := ValueSubst l2 e); auto with PirExpr.
      intros l n; unfold PirExprUpLocalSubst; unfold ValueSubst.
      destruct (L.eq_dec l4 l); destruct (L.eq_dec l2 l); subst;
        try (match goal with | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl) end).
      1,2: destruct n; cbn; auto. rewrite ExprRenameVar; reflexivity. reflexivity.
    - exists (Cond l3 ⦃ e' ⦄ Then C1 [local|ValueSubst l2 e] Else C2 [local|ValueSubst l2 e]);
        split; auto with PirExpr.
      apply CIfIStep; apply CSendVStep; auto; NotInList.
      cbn; unfold ValueSubst at 1;
        destruct (L.eq_dec l2 l3) as [eq|_]; [destruct (H0 eq)|].
      fold ExprIdSubst; rewrite ExprIdentitySubstSpec; reflexivity.
    - exists ((Cond l1 ⦃ e ⦄ Then C1 Else C2) [local|ValueSubst l3 e']); split; auto with PirExpr.
      apply CSendVStep; try NotInList; auto.
      cbn; unfold ValueSubst at 3; destruct (L.eq_dec l3 l1) as [eq | _];
        [destruct (H0 (eq_sym eq))|].
      fold ExprIdSubst; rewrite ExprIdentitySubstSpec; reflexivity.
  Qed.
  
End Pirouette.
