Require Import Pirouette.
Require Import TypedPirouette.

Require Import Coq.Arith.Arith.
Require Import Coq.Program.Wf.
Require Import Coq.Logic.JMeq.
Require Import Coq.Classes.RelationClasses.
Require Import Coq.Lists.List.
Require Import Sorted Orders Coq.Sorting.Mergesort Permutation.
Require Import Program.Tactics.
Require Import Coq.Structures.Orders.
Require Import Coq.Bool.Bool.
Require Import Psatz.
Require Import Coq.Program.Equality.

(*
  This module explores a semantics based on equivalence. Our key result is the equivalence
  with a restricted form of the labeled-transition system that forms the full operational
  semantics of Pirouette.
 *)

Module RestrictedSemantics (Import LL : LocalLang) (Import TLL : TypedLocalLang LL) (L : Locations) (Import SL : SyncLabels).
   (* 
      Note that most of our results don't rely on typing, but the module system forces us to
      include it from the beginning
    *)
  Include (TypedPirouette L LL TLL SL).
  Include ListNotations.

  (*
    The restricted version of the labeled-transition system.
   *)
  Inductive RPirStep : Redex -> list Loc -> PirExpr -> PirExpr -> Prop :=
  | DoneEStep : forall (l : Loc) (e1 e2 : Expr),
      ExprStep e1 e2 -> RPirStep (RDone l e1 e2) nil (Done l e1) (Done l e2)
  | SendEStep : forall (l1 l2 : Loc) (B : list Loc),
      ~ In l1 B -> ~ In l2 B -> l1 <> l2 ->
      forall (e1 e2 : Expr) (C : PirExpr),
        ExprStep e1 e2 -> RPirStep (RSendE l1 e1 e2 l2) B (Send l1 e1 l2 C) (Send l1 e2 l2 C)
  | SendIStep : forall (l1 l2 : Loc) (e : Expr) (C1 C2 : PirExpr) (B : list Loc) (R : Redex),
      RPirStep R (l1 :: l2 :: B) C1 C2 ->
      RPirStep R B (Send l1 e l2 C1) (Send l1 e l2 C2)
  | SendVStep : forall (l1 l2 : Loc) (v : Expr) (C : PirExpr) (B : list Loc),
      ~ In l1 B -> ~ In l2 B -> l1 <> l2 ->
      ExprVal v ->
      RPirStep (RSendV l1 v l2) B (Send l1 v l2 C) (C [local| ValueSubst l2 v])
  | IfEStep : forall (l1 : Loc) (e1 e2 : Expr) (C1 C2 : PirExpr) (B : list Loc),
      ~ In l1 B ->
      ExprStep e1 e2 ->
      RPirStep (RIfE l1 e1 e2) B (If l1 e1 C1 C2) (If l1 e2 C1 C2)
  | IfIStep : forall (l1 : Loc) (e : Expr) (C1 C2 C3 C4 : PirExpr) (B : list Loc) (R : Redex),
      RPirStep R (l1 :: B) C1 C3 ->
      RPirStep R (l1 :: B) C2 C4 ->
      RPirStep R B (If l1 e C1 C2) (If l1 e C3 C4)
  | IfTrueStep : forall (l1 : Loc) (C1 C2 : PirExpr) (B : list Loc),
      ~ In l1 B ->
      RPirStep (RIfTT l1) B (If l1 tt C1 C2) C1
  | IfFalseStep : forall (l1 : Loc) (C1 C2 : PirExpr) (B : list Loc),
      ~ In l1 B ->
      RPirStep (RIfFF l1) B (If l1 ff C1 C2) C2
  | DefLocalIStep : forall R l C1 C1' C2,
      RPirStep R [] C1 C1' ->
      RPirStep (RArg R) [] (DefLocal l C1 C2) (DefLocal l C1' C2)
  | DefLocalStep : forall (l : Loc) (v : Expr) (C2 : PirExpr),
      ExprVal v ->
      RPirStep (RDefLocal l v) nil (DefLocal l (Done l v) C2) (C2 [local| ValueSubst l v])
  | AppLocalFunStep : forall l C1 C2 e R,
      RPirStep R [] C1 C2 ->
      RPirStep (RFun R) [] (AppLocal l C1 e) (AppLocal l C2 e)
  | AppLocalExprStep : forall l C e1 e2,
      ExprStep e1 e2 ->
      RPirStep (RAppLocalE l e1 e2) [] (AppLocal l C e1) (AppLocal l C e2)
  | AppLocalValStep : forall l C v,
      ExprVal v ->
      RPirStep (RAppLocal l v) []
                (AppLocal l (FunLocal l C) v)
                (C [local| ValueSubst l v] [global| AppLocalSubst l C])
  | AppGlobalFunStep : forall C11 C12 C2 R,
      RPirStep R [] C11 C12 ->
      RPirStep (RFun R) [] (AppGlobal C11 C2) (AppGlobal C12 C2)
  | AppGlobalArgStep : forall C1 C21 C22 R,
      RPirStep R [] C21 C22 ->
      RPirStep (RArg R) [] (AppGlobal C1 C21) (AppGlobal C1 C22)
  | AppGlobalValStep : forall C1 C2,
      PirExprVal C2 ->
      RPirStep RAppGlobal []
                (AppGlobal (FunGlobal C1) C2)
                (C1 [global| AppGlobalSubst C1 C2])
  | SyncStep : forall (l1 l2 : Loc) (d : SyncLabel) (C : PirExpr) (B : list Loc),
      ~ In l1 B -> ~ In l2 B -> l1 <> l2 ->
      RPirStep (RSync l1 d l2) B (Sync l1 d l2 C) C
  | SyncIStep : forall (l1 l2 : Loc) (d : SyncLabel) (C1 C2 : PirExpr) (B : list Loc)
                  (R : Redex),
      RPirStep R (l1 :: l2 :: B) C1 C2 ->
      RPirStep R B (Sync l1 d l2 C1) (Sync l1 d l2 C2).
  Global Hint Constructors RPirStep : PirExpr.

  (*
    Restricted steps really are steps.
   *)
  Theorem LiftRestrictedPirStepToUnrestricted : forall C1 C2 B R,
      RPirStep B R C1 C2 ->
      PirStep B R C1 C2.
  Proof using.
    intros C1 C2 B R step; induction step; auto with PirExpr.
    
  Qed.
  
  (* 
     Rearrangement for this restricted semantics. This shows that we treat the blocked set 
     `B` as a set.
   *)
  Lemma RStepRearrange : forall R B1 C1 C2,
      RPirStep R B1 C1 C2 -> forall B2, (forall q, In q B1 <-> In q B2) -> RPirStep R B2 C1 C2.
  Proof using.
    intros R B1 C1 C2 step; induction step; intros B2 ext.
    all: try match goal with (* The only list equivalent to the empty list is empty. *)
         | [H : forall q, In q [] <-> In q ?B |- _ ] =>
           let H' := fresh in
           assert (B = []) as H'
               by (let x := fresh in
                   destruct B as [| x B];
                   [ reflexivity
                   | pose proof ((proj2 (H x)) ltac:(left; reflexivity)) as H';
                     inversion H']);
             subst; clear H
         | [H : forall q, In q ?B <-> In q [] |- _ ] =>
           let H' := fresh in
           assert (B = []) as H'
               by (let x := fresh in
                   destruct B as [| x B];
                   [ reflexivity
                   | pose proof ((proj1 (H x)) ltac:(left; reflexivity)) as H';
                     inversion H']);
             subst; clear H
         end.
    all: try (constructor; try NotInList; auto; fail).
    - apply SendIStep; auto; apply IHstep; intro q; split; intro i;
        repeat (destruct i as [eq | i]; [left; exact eq | right]; auto); apply ext; auto.
    - apply IfIStep; [apply IHstep1 | apply IHstep2]; intro q; split; intro i;
        repeat (destruct i as [eq | i]; [left | right]; auto); apply ext; auto.
    - apply SyncIStep; auto; apply IHstep; intro q; split; intro i;
        repeat (destruct i as [eq | i]; [left; exact eq | right]; auto); apply ext; auto.
  Qed.

  (* REDEX OF *)
  (*
    We know that locations listed in a redex cannot appear in the block set of a step that
    is labeled with that redex. We use this to rule out cases in several important theorems.
   *)
  
  Inductive RestrictedRedexOf : Loc -> Redex -> Prop :=
  | RRODone : forall l e1 e2, RestrictedRedexOf l (RDone l e1 e2)
  | RROIfE : forall l e1 e2, RestrictedRedexOf l (RIfE l e1 e2)
  | RROIfTT : forall l, RestrictedRedexOf l (RIfTT l)
  | RROIfFF : forall l, RestrictedRedexOf l (RIfFF l)
  | RROSendE1 : forall l1 e1 e2 l2, RestrictedRedexOf l1 (RSendE l1 e1 e2 l2)
  | RROSendE2 : forall l1 e1 e2 l2, RestrictedRedexOf l2 (RSendE l1 e1 e2 l2)
  | RROSendV1 : forall l1 v l2, RestrictedRedexOf l1 (RSendV l1 v l2)
  | RROSendV2 : forall l1 v l2, RestrictedRedexOf l2 (RSendV l1 v l2)
  | RROSync1 : forall l1 s l2, RestrictedRedexOf l1 (RSync l1 s l2)
  | RROSync2 : forall l1 s l2, RestrictedRedexOf l2 (RSync l1 s l2)
  | RROLDef : forall l e, RestrictedRedexOf l (RDefLocal l e)
  | RRArg : forall l R, RestrictedRedexOf l R -> RestrictedRedexOf l (RArg R)
  | RRFun : forall l R, RestrictedRedexOf l R -> RestrictedRedexOf l (RFun R).
  Global Hint Constructors RestrictedRedexOf: PirExpr.

  Lemma NoIStepInList : forall p B R,
      In p B ->
      RestrictedRedexOf p R ->
      forall C1 C2, ~RPirStep R B C1 C2.
  Proof using.
    intros p B R H H0 C1 C2 step; induction step; inversion H0;
    match goal with
    | [ i : In ?p ?B, n : ~ In ?p' ?B, e : ?p = ?p' |- False ] =>
      apply n; rewrite <- e; exact i
    | [ i : In ?p ?B, n : ~ In ?p' ?B, e : ?p' = ?p |- False ] =>
      apply n; rewrite e; exact i
    | _ => idtac
    end.
    all: try (apply IHstep; auto; right; right; auto; fail).
    all: try (apply IHstep1; auto; right; auto; fail).
    all: inversion H.
  Qed.

  Corollary NoDoneIStepInList : forall p B,
      In p B ->
      forall e1 e2 C1 C2, ~RPirStep (RDone p e1 e2) B C1 C2.
  Proof using.
    intros p B H e1 e2 C1 C2; apply NoIStepInList with (p := p); auto; apply RRODone.
  Qed.
  Corollary NoSendEIStepInList1 : forall p B,
      In p B ->
      forall e1 e2 C1 C2 q, ~RPirStep (RSendE p e1 e2 q) B C1 C2.
  Proof using.
    intros p B H q e1 e2 C1 C2; apply NoIStepInList with (p := p); auto; apply RROSendE1.
  Qed.
  Corollary NoSendEIStepInList2 : forall B q,
      In q B ->
      forall p e1 e2 C1 C2, ~RPirStep (RSendE p e1 e2 q) B C1 C2.
  Proof using.
    intros B q H p e1 e2 C1 C2; apply NoIStepInList with (p := q); auto; apply RROSendE2.
  Qed.
  Corollary NoSendVIStepInList1 : forall p B,
      In p B ->
      forall v q C1 C2, ~RPirStep (RSendV p v q) B C1 C2.
  Proof using.
    intros p B H v q C1 C2; apply NoIStepInList with (p := p); auto; apply RROSendV1.
  Qed.
  Corollary NoSendVIStepInList2 : forall B q,
      In q B ->
      forall p v C1 C2, ~RPirStep (RSendV p v q) B C1 C2.
  Proof using.
    intros B q H p v C1 C2; apply NoIStepInList with (p := q); auto; apply RROSendV2.
  Qed.
  Corollary NoIfEIStepInList : forall p B,
      In p B ->
      forall e1 e2 C1 C2, ~RPirStep (RIfE p e1 e2) B C1 C2.
  Proof using.
   intros p B H e1 e2 C1 C2; apply NoIStepInList with (p := p); auto; apply RROIfE.
  Qed.
  Corollary NoIfTTStepInList : forall p B,
      In p B ->
      forall C1 C2, ~RPirStep (RIfTT p) B C1 C2.
  Proof using.
   intros p B H C1 C2; apply NoIStepInList with (p := p); auto; apply RROIfTT.
  Qed.
  Corollary NoIfFFStepInList : forall p B,
      In p B ->
      forall C1 C2, ~RPirStep (RIfFF p) B C1 C2.
  Proof using.
   intros p B H C1 C2; apply NoIStepInList with (p := p); auto; apply RROIfFF.
  Qed.
  Corollary NoSyncStepInList1 : forall p B,
      In p B ->
      forall d q C1 C2, ~RPirStep (RSync p d q) B C1 C2.
  Proof using.
   intros p B H q C1 C2; apply NoIStepInList with (p := p); auto; constructor.
  Qed. 
  Corollary NoSyncStepInList2 : forall B q,
      In q B ->
      forall p d C1 C2, ~RPirStep (RSync p d q) B C1 C2.
  Proof using.
   intros B q H p C1 C2; apply NoIStepInList with (p := q); auto; constructor.
  Qed.
  Corollary NoLDefStepInList : forall B p,
      In p B ->
      forall e C1 C2, ~RPirStep (RDefLocal p e) B C1 C2.
  Proof using.
   intros B p H e C1 C2; apply NoIStepInList with (p := p); auto; constructor.
  Qed.

  Global Hint Resolve RStepRearrange NoDoneIStepInList
         NoSendEIStepInList1 NoSendVIStepInList1 NoSendEIStepInList2
         NoSendVIStepInList2 NoIfEIStepInList NoIfTTStepInList NoIfFFStepInList
         NoSyncStepInList1 NoSyncStepInList2 NoLDefStepInList : PirExpr.

  (* EQUIVALENCE AS A SIMULATION *)

  (*
    Two quick lemmas about lists that will make things easier.
   *)
  Lemma ConsNotIn : forall {A : Type} (a b : A) (l : list A),
      a <> b -> ~ In a l -> ~ In a (b :: l).
  Proof using.
    intros A a b l H H0 H1.
    destruct H1; auto.
  Qed.
  Lemma NotInCons : forall {A : Type} (a b : A) (l : list A),
      ~ In a (b :: l) -> a <> b /\ ~ In a l.
  Proof using.
    intros A a b l H; split; intro H0; apply H; [left | right]; auto.
  Qed.
  Global Hint Resolve ConsNotIn NotInCons : PirExpr.

  (*
    Equivalence plays nicely with restricted steps. In particular, equivalence acts as a
    simulation for restricted steps.
   *)
  Theorem RestrictedEquivSimulation : forall C1 C2, C1 ≡ C2 -> forall R B C1',
        RPirStep R B C1 C1' -> exists C2', RPirStep R B C2 C2' /\ C1' ≡ C2'.
  Proof using.
    intros C1 C2 equiv; induction equiv; intros R B Cstep step;
      eauto with PirExpr; repeat match goal with
                              | [ H : RPirStep ?R ?B ?C1 ?C1' |- _ ] =>
                                tryif (let x := fresh "fail_" C1 in idtac)
                                then fail
                                else inversion H; subst; eauto with PirExpr; clear H
                                 end.
    all: try (eexists; split; eauto with PirExpr; fail).
    all: repeat match goal with
                | [IH : forall R B C', RPirStep R B ?C C' ->
                                  exists C2', RPirStep R B ?C2 C2' /\ C' ≡ C2',
                     H : RPirStep ?R ?B ?C ?C' |- _ ] =>
                  lazymatch goal with
                  | [ _ : RPirStep R B C2 ?C2', _ : C' ≡ ?C2' |- _] => fail
                  | _ => let H' := fresh in
                        let C2 := fresh "C" in
                        let step := fresh "step" in
                        let eqv := fresh "eqv" in
                        pose proof (IH R B C' H) as H'; destruct H' as [C2 [step eqv]]
                  end
                end; eauto with PirExpr.
    apply RetEquivInversion in equiv1; subst; eexists; split; eauto with PirExpr.
    all: try (exfalso; eapply NoIStepInList; eauto; eauto with PirExpr; InList; fail).
    (* This next step takes a long time. Go get yourself a cup of coffee. :) *)
    all: try (eexists; split; [repeat econstructor |]; eauto with PirExpr; try NotInList;
              auto; try (eapply RStepRearrange; eauto with PirExpr;
                         intros ℓ; split; unfold In; intro i; tauto); fail).
    - apply FunLocalEquivInversion in equiv0; destruct equiv0 as [C' [eq eqv]]; subst.
      exists (C' [local|ValueSubst l e] [global|AppLocalSubst l C'] ); split; auto with PirExpr.
      apply WeakSubstExt; [apply EquivStableLocalSubst|]; auto.
      intro n; destruct n; cbn; auto with PirExpr.
    - apply FunGlobalEquivInversion in equiv1; destruct equiv1 as [C1' [eq equiv1]]; subst.
      exists (C1' [global| AppGlobalSubst C1' C22]); split; auto with PirExpr.
      apply AppGlobalValStep; auto.
      eapply PirExprValStableEquiv; eauto.
      apply WeakSubstExt; auto. intro n; destruct n; cbn; auto with PirExpr.
      destruct n; auto with PirExpr.
    - exists (l1 ⟪e⟫ → l2 fby C [local|ValueSubst l4 e']); split; eauto with PirExpr.
      pose proof (SendVStep l3 l4 e' (l1 ⟪e⟫ → l2 fby C) B
                            ltac:(NotInList) ltac:(NotInList) H13 H14) as H3;
        cbn in H3.
      unfold ValueSubst in H3 at 1; destruct (L.eq_dec l4 l1) as [eq|_];
        [destruct (H0 (eq_sym eq))|].
      fold ExprIdSubst in H3; rewrite ExprIdentitySubstSpec in H3.
      rewrite PirExprLocalSubstExt with (σ2 := ValueSubst l4 e') in H3; auto.
      intros ℓ n; unfold PirExprUpLocalSubst. unfold ValueSubst.
      destruct (L.eq_dec l2 ℓ) as [eq1 | neq1]; destruct(L.eq_dec l4 ℓ) as [eq2 |neq2];
        subst; try match goal with
                   | [H : ?a <> ?a |- _ ] => destruct (H eq_refl)
                   end.
      destruct n; cbn; auto; apply ExprRenameVar.
      all: reflexivity.
    - exists (l3 ⟪e'⟫ → l4 fby (C [local|ValueSubst l2 e])); split; auto with PirExpr.
      cbn; unfold ValueSubst at 1;
        destruct (L.eq_dec l2 l3) as [eq |_]; [destruct (H1 eq)|].
      fold ExprIdSubst; rewrite ExprIdentitySubstSpec.
      rewrite PirExprLocalSubstExt with (σ2 := ValueSubst l2 e); auto with PirExpr.
      intros ℓ n; unfold PirExprUpLocalSubst; unfold ValueSubst.
      destruct (L.eq_dec l4 ℓ); destruct (L.eq_dec l2 ℓ); subst;
        try (match goal with | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl) end).
      1,2: destruct n; cbn; auto. rewrite ExprRenameVar; reflexivity. reflexivity.
    - exists (Cond l3 ⦃ e' ⦄ Then C1 [local|ValueSubst l2 e] Else C2 [local|ValueSubst l2 e]);
        split; auto with PirExpr.
      cbn; unfold ValueSubst at 1;
        destruct (L.eq_dec l2 l3) as [eq|_]; [destruct (H0 eq)|].
      fold ExprIdSubst; rewrite ExprIdentitySubstSpec; reflexivity.
    - exists ((Cond l1 ⦃ e ⦄ Then C1 Else C2) [local|ValueSubst l3 e']);
        split; auto with PirExpr.
      apply SendVStep; try NotInList; auto.
      cbn; unfold ValueSubst at 3; destruct (L.eq_dec l3 l1) as [eq | _];
        [destruct (H0 (eq_sym eq))|].
      fold ExprIdSubst; rewrite ExprIdentitySubstSpec; reflexivity.
  Qed.

  (* EQUIVLANCE-BASED OPERATIONAL SEMANTICS *)
  
  (*
    The following defines the equivalence-based semantics. We refer to it as "standard" 
    steps because most of the previous works on choreographies use equivalence-based 
    semantics.
   *)
  Inductive StdPirStep : PirExpr -> PirExpr -> Prop :=
  | StdDoneEStep : forall (l : Loc) (e1 e2 : Expr),
      ExprStep e1 e2
      -> StdPirStep (Done l e1) (Done l e2)
  | StdSendEStep : forall (l1 l2 : Loc) (e1 e2 : Expr) (C : PirExpr),
      ExprStep e1 e2
      -> l1 <> l2
      -> StdPirStep (Send l1 e1 l2 C) (Send l1 e2 l2 C)
  | StdSendVStep : forall (l1 l2 : Loc) (v : Expr) (C : PirExpr),
      ExprVal v
      -> l1 <> l2
      -> StdPirStep (Send l1 v l2 C) (C [local| ValueSubst l2 v])
  | StdIfEStep : forall (l : Loc) (e1 e2 : Expr) (C1 C2 : PirExpr),
      ExprStep e1 e2
      -> StdPirStep (If l e1 C1 C2) (If l e2 C1 C2)
  | StdIfTrueStep : forall (l : Loc) (C1 C2 : PirExpr),
      StdPirStep (If l tt C1 C2) C1
  | StdIfFalseStep : forall (l : Loc) (C1 C2 : PirExpr),
      StdPirStep (If l ff C1 C2) C2
  | StdSyncStep : forall (l1 l2 : Loc) (d : SyncLabel) (C : PirExpr),
      l1 <> l2 ->
      StdPirStep (Sync l1 d l2 C) C
  | StdDefLocalIStep : forall (l : Loc) (C1 C1' C2 : PirExpr),
      StdPirStep C1 C1' -> StdPirStep (DefLocal l C1 C2) (DefLocal l C1' C2)
  | StdDefLocalStep : forall (l : Loc) (v : Expr) (C : PirExpr),
      ExprVal v ->
      StdPirStep (DefLocal l (Done l v) C) (C [local|ValueSubst l v])
  | StdAppLocalFunStep : forall l (C1 C2 : PirExpr) e,
      StdPirStep C1 C2 ->
      StdPirStep (AppLocal l C1 e) (AppLocal l C2 e)
  | StdAppLocalArgStep : forall l C e1 e2,
      ExprStep e1 e2 ->
      StdPirStep (AppLocal l C e1) (AppLocal l C e2)
  | StdAppLocalStep : forall l C v,
      ExprVal v ->
      StdPirStep (AppLocal l (FunLocal l C) v)
                 (C [local|ValueSubst l v] [global|AppLocalSubst l C])
  | StdAppGlobalFunStep : forall (C1 C1' C2 : PirExpr),
      StdPirStep C1 C1'
      -> StdPirStep (AppGlobal C1 C2) (AppGlobal C1' C2)
  | StdAppGlobalArgStep : forall (C1 C2 C2' : PirExpr),
      StdPirStep C2 C2'
      -> StdPirStep (AppGlobal C1 C2) (AppGlobal C1 C2')
  | StdAppGlobalStep : forall (C1 C2 : PirExpr),
      PirExprVal C2 ->
      StdPirStep (AppGlobal (FunGlobal C1) C2) (C1 [global|AppGlobalSubst C1 C2])
  | StdEquivStep : forall (C1 C1' C2 C2' : PirExpr),
      C1 ≡ C1'
      -> StdPirStep C1' C2'
      -> C2 ≡ C2'
      -> StdPirStep C1 C2.
  Global Hint Constructors StdPirStep : PirExpr.

  (*
    Equivalence simulation means that we can easily take a restricted step instead of a
    standard step. However, we only get something equivalent to the output as a result.
   *)
  Theorem StdToRStep : forall (C1 C2 : PirExpr),
      StdPirStep C1 C2
      -> exists R C2', C2 ≡ C2' /\ RPirStep R nil C1 C2'.
  Proof using.
    intros C1 C2 stdstep; induction stdstep.
    all:try ( eexists; eexists; split; [reflexivity | constructor; auto]; fail).
    - destruct IHstdstep as [R [C2' [equiv step]]].
      exists (RArg R); exists (LetLocal l ⟪new⟫ ← C2' fby C2); split; auto with PirExpr.
    - destruct IHstdstep as [R [C2' [equiv step]]].
      exists (RFun R); exists (AppLocal l C2' e); split; auto with PirExpr.
    - destruct IHstdstep as [R [C2' [equiv step]]].
      exists (RFun R); exists (AppGlobal C2' C2); split; auto with PirExpr.
    - destruct IHstdstep as [R [C2'' [equiv step]]].
      exists (RArg R); exists (AppGlobal C1 C2''); split; auto with PirExpr.
    - destruct IHstdstep as [R [C2'' [equiv step]]].
      destruct (RestrictedEquivSimulation C1' C1 ltac:(auto with PirExpr) R [] C2'' step)
        as [C2''' [step' equiv']].
      exists R; exists C2'''; split; auto with PirExpr.
      transitivity C2'; auto; transitivity C2''; auto.
  Qed.

  (* RESTRICTED STEPS TO STANDARD *)

  (*
    We have one direction of the equivalence, now we just need to get the other direction.

    Proof sketch: we show that you can always use equivalence to put a redex "on top"
    of a term which is being reduced. This is actually the part that needs the restriction
    of the labeled-transition system. Then, we show that a restricted step with the redex
    "on top"  corresponds to a standard step. This gives us a way to get a standard step
    from any restricted labeled-transition system step.

    We start by defining what it means for a redex to be "on top" of a term.
   *)  
  Inductive RedexOnTop : Redex -> PirExpr -> Prop :=
  | DoneOnTop : forall l e1 e2, RedexOnTop (RDone l e1 e2) (Done l e1)
  | SendEOnTop : forall l1 e1 e2 l2 C, RedexOnTop (RSendE l1 e1 e2 l2) (Send l1 e1 l2 C)
  | SendVOnTop : forall l1 v l2 C, RedexOnTop (RSendV l1 v l2) (Send l1 v l2 C)
  | IfEOnTop : forall l e1 e2 C1 C2, RedexOnTop (RIfE l e1 e2) (If l e1 C1 C2)
  | IfTTOnTop : forall l C1 C2, RedexOnTop (RIfTT l) (If l tt C1 C2)
  | IfFFOnTop : forall l C1 C2, RedexOnTop (RIfFF l) (If l ff C1 C2)
  | SyncOnTop : forall l1 s l2 C, RedexOnTop (RSync l1 s l2) (Sync l1 s l2 C)
  | IDefLocalOntop : forall l C1 C2 R, RedexOnTop R C1 -> RedexOnTop (RArg R) (DefLocal l C1 C2)
  | DefLocalOnTop : forall l e C, RedexOnTop (RDefLocal l e) (DefLocal l (Done l e) C)
  | AppLocalEOnTop : forall l e1 e2 C, RedexOnTop (RAppLocalE l e1 e2) (AppLocal l C e1)
  | AppLocalOnTop : forall l C e, RedexOnTop (RAppLocal l e) (AppLocal l (FunLocal l C) e)
  | AppGlobalOnTop : forall C1 C2,
      PirExprVal C2 -> RedexOnTop RAppGlobal (AppGlobal (FunGlobal C1) C2)
  | LocalFunOnTop : forall R l C e, RedexOnTop R C -> RedexOnTop (RFun R) (AppLocal l C e)
  | GlobalFunOnTop : forall R C1 C2, RedexOnTop R C1 -> RedexOnTop (RFun R) (AppGlobal C1 C2)
  | GlobalArgOnTop : forall R C1 C2, RedexOnTop R C2 -> RedexOnTop (RArg R) (AppGlobal C1 C2).
  Global Hint Constructors RedexOnTop : PirExpr.

  (* Ltac to clean up assumptions of the form `~ In a (b :: c)`. *)
  Ltac ListClean :=
    repeat match goal with
           | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
           | [ H1 : ?P, H2 : ~?P |- _ ] => destruct (H2 H1)
           | [ H : ~ In ?a (?b :: ?c) |- _ ] =>
             assert (b <> a) by (let eq:= fresh in intro eq; apply H; left; exact eq);
             assert (~ In a c) by (let i := fresh in intro i; apply H; right; exact i);
             clear H
           end.

  (*
    Now we show that a term can always be rearranged such that a redex is it can step by
    is on top.
   *)
  Lemma RStepOnTop : forall (R : Redex) (B : list Loc) (C1 C2 : PirExpr),
      RPirStep R B C1 C2 ->
      exists C1' C2', C1 ≡ C1' /\ C2 ≡ C2' /\ RPirStep R B C1' C2' /\ RedexOnTop R C1'.
  Proof using.
    intros R B C1 C2 step; induction step.
    all: try(eexists; eexists; split; [|split; [|split]]; eauto with PirExpr; fail).
    all: repeat match goal with
                | [ IH : exists C1' C2', ?C1 ≡ C1' /\ ?C2 ≡ C2' /\ RPirStep ?R ?B C1' C2'
                                    /\ RedexOnTop ?R C1' |- _ ] =>
                  let C1' := fresh "C" in
                  let C2' := fresh "C" in
                  let eqv1 := fresh "eqv" in
                  let eqv2 := fresh "eqv" in
                  let step := fresh "step" in
                  let ontop := fresh "ontop" in
                  destruct IH as [C1' [C2' [eqv1 [eqv2 [step ontop]]]]]
                end.
    - induction ontop.
      all: inversion step0; subst; ListClean.
      all: try (exfalso; eapply NoIStepInList; eauto; eauto with PirExpr; InList; fail).
      all: eexists; eexists; split; [|split;[|split]]; [| | | eauto with PirExpr];
        try match goal with
            | [ |- RPirStep _ _ _ _ ] => auto with PirExpr
            end;
        try match goal with
            | [ H : ?C1 ≡ ?C2 |- Send ?l1 ?e ?l2 ?C1 ≡ _ ] =>
              transitivity (Send l1 e l2 C2); auto with PirExpr
            end.
      cbn; unfold ValueSubst at 2.
      destruct (L.eq_dec l3 l1) as [eq|_];[destruct (H (eq_sym eq))|].
      rewrite ExprIdentitySubstSpec. apply SendContext.
      erewrite PirExprLocalSubstExt ; [reflexivity|].
      intros l n. unfold PirExprUpLocalSubst; unfold ValueSubst.
      destruct (L.eq_dec l3 l); destruct (L.eq_dec l2 l); subst;
        try match goal with
            | [H : ?a <> ?a |- _ ] => destruct (H eq_refl)
            end; auto.
      destruct n; auto; symmetry; apply ExprRenameVar.
    - induction ontop.
      all: inversion step; subst; ListClean.
      all: inversion step0; subst; ListClean.
      all: repeat match goal with
                  | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
                  | [ H : ?P, H' : ~?P |- _ ] => destruct (H' H)
                  | [ H : RedexOnTop ?R ?C |- _ ] =>
                    tryif (let H := fresh "_" C in idtac)
                    then fail
                    else inversion H; subst; clear H
                  end.
      all: repeat match goal with
                  | [ H : RPirStep ?R ?B _ _  |- _ ]=>
                    eapply NoIStepInList in H; [destruct H| |]; eauto with PirExpr;
                      InList; fail
                  end.
      all: eexists; eexists; split; [|split;[|split]]; [| | | eauto with PirExpr];
        try match goal with
            | [ |- RPirStep _ _ _ _ ] => eauto with PirExpr
            end;
      try match goal with
          | [ H : ?C1 ≡ ?C1', H' : ?C2 ≡ ?C2' |- If ?l1 ?e ?C1 ?C2 ≡ _ ] =>
            transitivity (If l1 e C1' C2'); auto with PirExpr
          end.
      cbn. unfold ValueSubst at 3.
      destruct (L.eq_dec l2 l1) as [eq|_];[destruct (H3 (eq_sym eq))|].
      rewrite ExprIdentitySubstSpec. reflexivity.
    - exists (DefLocal l C C2); exists (DefLocal l C0 C2); split; [|split;[|split]];
        auto with PirExpr.
    - exists (AppLocal l C e); exists (AppLocal l C0 e); split; [|split;[|split]]; auto with PirExpr.
    - exists (AppGlobal C C2); exists (AppGlobal C0 C2); split; [|split;[|split]]; auto with PirExpr.
    - exists (AppGlobal C1 C); exists (AppGlobal C1 C0);
        split; [|split;[|split]]; auto with PirExpr.
    - induction ontop.
      all: inversion step0; subst; ListClean.
      all: try (exfalso; eapply NoIStepInList; eauto; eauto with PirExpr; InList; fail).
      all: eexists; eexists; split; [|split;[|split]]; [| | | eauto with PirExpr];
      
      try match goal with
          | [ |- RPirStep _ _ _ _ ] => auto with PirExpr
          end;
      try match goal with
          | [ H : ?C1 ≡ ?C2 |- Sync ?l1 ?e ?l2 ?C1 ≡ _ ] =>
            transitivity (Sync l1 e l2 C2); auto with PirExpr
          end.
  Qed.

  (* Any restricted step with the redex on top is a standard step. *)
  Lemma RStepOnTopToStd : forall (C1 C2 : PirExpr) (R : Redex) (B : list Loc),
      RedexOnTop R C1 ->
      RPirStep R B C1 C2 ->
      StdPirStep C1 C2.
  Proof using.
    intros C1 C2 R B ontop rstep; induction rstep; inversion ontop; subst;
      eauto with PirExpr.
    all: try (inversion rstep; fail).
    - apply NoSendEIStepInList1 in rstep; [destruct rstep| left; reflexivity].
    - apply NoSendVIStepInList1 in rstep; [destruct rstep | left; reflexivity].
    - apply NoIfEIStepInList in rstep1; [destruct rstep1 | left; reflexivity].
    - apply NoIfTTStepInList in rstep1; [destruct rstep1 | left; reflexivity].
    - apply NoIfFFStepInList in rstep1; [destruct rstep1 | left; reflexivity].
    - apply NoSyncStepInList1 in rstep; [destruct rstep | left; reflexivity].
  Qed.
  
  (* Putting it all together. *)
  Theorem RStepToStd : forall (C1 C2 : PirExpr) (R : Redex) (B : list Loc),
      RPirStep R B C1 C2 -> StdPirStep C1 C2.
  Proof using.
    intros C1 C2 R B rstep.
    apply RStepOnTop in rstep;
      destruct rstep as [C1' H]; destruct H as [C2' H];
      destruct H as [equiv1 H]; destruct H as [equiv2 H]; destruct H as [rstep ontop].
    apply StdEquivStep with (C1' := C1') (C2' := C2'); auto.
    apply RStepOnTopToStd with (R := R) (B := B); auto.
  Qed.

  (* RELATIVE TYPE SAFETY FOR RESTRICTED STEPS *)

  (*
    We now show that restricted steps enjoy (relative) type safety. Preservation is easy
    because we can just appeal to the lifting. Progress is harder, because we have to
    show that the resulting step is still a restricted step.
   *)
  Corollary RestrictedRelativePreservation :
    (forall (Γ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
        Γ ⊢e e ::: τ
        -> forall e' : Expr, ExprStep e e'
                       -> Γ ⊢e e' ::: τ) ->
    forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (C : PirExpr) (τ : PirTyp),
      Γ;; Δ ⊢c C ::: τ -> forall (R : Redex) (B : list Loc) (C': PirExpr),
        RPirStep R B C C' -> Γ;; Δ ⊢c C' ::: τ.
  Proof.
    intros H Γ Δ C τ H0 R B C' H1; apply LiftRestrictedPirStepToUnrestricted in H1.
    eapply RelativePreservation in H1; eauto.
  Qed.

  Theorem RestrictedRelativeProgress :
    (forall (e : Expr) (τ : ExprTyp) (Γ : nat -> ExprTyp),
        Γ ⊢e e ::: τ -> ExprClosed e ->
        ExprVal e \/ exists e' : Expr, ExprStep e e') ->
    (forall (v : Expr) (Γ : nat -> ExprTyp),
        Γ ⊢e v ::: bool -> ExprVal v -> {v = tt} + {v = ff}) ->
    forall (C : PirExpr) (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (τ : PirTyp),
      PirExprClosed C -> Γ;; Δ ⊢c C ::: τ ->
      PirExprVal C \/ exists R C', RPirStep R nil C C'.
  Proof.
    intros ExprProgress BoolInversion C Γ Δ τ cc typ; revert cc; induction typ; intro cc;
      inversion cc; subst.
    2: inversion H2.
    2-5,8,9: right.
    8,9: left; constructor; auto.
    - destruct (ExprProgress e τ (Γ ℓ) ltac:(assumption) ltac:(assumption));
        [left; constructor; auto | right].
      destruct H0 as [e' step]; exists (RDone ℓ e e'); exists (Done ℓ e'); auto with PirExpr.
    - destruct (ExprProgress e τ (Γ ℓ₁) ltac:(assumption) ltac:(assumption))
        as [eval | [e' step]];
        eexists; eexists; eauto with PirExpr.
    - destruct (ExprProgress e bool (Γ ℓ) ltac:(assumption) ltac:(assumption))
        as [eval | [e' step]];
        [destruct (BoolInversion e (Γ ℓ) ltac:(assumption) eval); subst|].
      all: eexists; eexists; eauto with PirExpr.
    - eexists; eexists; eauto with PirExpr.
    - destruct (IHtyp1 ltac:(assumption)) as [cval | [R [C' step]]].
      inversion cval; subst; inversion typ1; subst.
      all: eexists; eexists; eauto with PirExpr.
    - fold PirExprClosed in H4; specialize (IHtyp H4); destruct IHtyp.
      inversion H0; subst; inversion typ; subst.
      destruct (ExprProgress _ _ _ H H6); [| destruct H1 as [e' step]];
        eexists; eexists; eauto with PirExpr.
      destruct H0 as [R [C' step]]; eexists; eexists; eauto with PirExpr.
    - specialize (IHtyp1 H3); specialize (IHtyp2 H4).
      destruct IHtyp1 as [IHtyp1 | [R [C' step]]]. 2: eexists; eexists; eauto with PirExpr.
      destruct IHtyp2 as [IHtyp2 | [R [C' step]]].
      all: inversion IHtyp1; subst; inversion typ1; subst.
      all: eexists; eexists; eauto with PirExpr.
  Qed.
  
End RestrictedSemantics.
