Require Export LocalLang.
Require Export TypedLocalLang.

Module Type SoundlyTypedLocalLang (Import LL : LocalLang) (Import TLL : TypedLocalLang LL).

  (* 
     A soundly-typed local language does not allow boolean values other than true and false.
     This is important in order to know that if expressions in Pirouette don't go wrong. If
     we didn't have this, we could have a boolean expression which can't reduce, yet is
     neither true nor false.     
   *)
  Parameter BoolInversion : forall (Γ : nat -> ExprTyp) (v : Expr),
      Γ ⊢e v ::: bool ->
      ExprVal v ->
      {v = tt} + {v = ff}.

  (*
    Progress and preservation for the local language is standard.
   *)  
  Parameter ExprPreservation : forall (Γ : nat -> ExprTyp) (e1 e2 : Expr) (τ : ExprTyp),
      Γ ⊢e e1 ::: τ -> ExprStep e1 e2 -> Γ ⊢e e2 ::: τ.
  Parameter ExprProgress : forall (Γ : nat -> ExprTyp) (e1 : Expr) (τ : ExprTyp),
      Γ ⊢e e1 ::: τ -> ExprClosed e1 -> ExprVal e1 \/ exists e2, ExprStep e1 e2.
End SoundlyTypedLocalLang.
