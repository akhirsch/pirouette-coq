Require Export LocalLang.
Require Export TypedLocalLang.
Require Export SoundlyTypedLocalLang.
Require Export NatLang.
Require Export TypedNatLang.

Module SoundlyTypedNatLang <: (SoundlyTypedLocalLang NatLang TypedNatLang).
  Include TypedNatLang.

  Theorem BoolInversion : forall (Γ : nat -> ExprTyp) (v : Expr),
      Γ ⊢e v ::: bool ->
      ExprVal v ->
      {v = tt} + {v = ff}.
  Proof using.
    intros Γ v typ val_v; destruct v; unfold tt; unfold ff; try (left;reflexivity);
      try (right; reflexivity).
    all: exfalso; inversion typ; subst. inversion val_v.
  Qed.
  
  Theorem ExprPreservation : forall (Γ : nat -> ExprTyp) (e1 e2 : Expr) (τ : ExprTyp),
      Γ ⊢e e1 ::: τ -> ExprStep e1 e2 -> Γ ⊢e e2 ::: τ.
  Proof using.
    intros Γ e1 e2 τ typ step; destruct step.
  Qed.

  Theorem ExprProgress : forall (Γ : nat -> ExprTyp) (e1 : Expr) (τ : ExprTyp),
      Γ ⊢e e1 ::: τ -> ExprClosed e1 -> ExprVal e1 \/ exists e2, ExprStep e1 e2.
  Proof using.
    intros Γ e1 τ typ; induction typ; cbn; intro clsd;
      try (left; unfold ExprVal; auto with NL).
    inversion clsd. destruct IHtyp; auto; try (constructor; auto).
    destruct H as [e2 step]; destruct step.
  Qed.

End SoundlyTypedNatLang.
