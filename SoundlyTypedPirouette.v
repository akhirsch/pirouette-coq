Require Export Pirouette.
Require Export TypedPirouette.
Require Export LocalLang.
Require Export TypedLocalLang.
Require Export SoundlyTypedLocalLang.

Module SoundlyTypedPirouette (Import LL : LocalLang) (Import TLL : TypedLocalLang LL)
       (Import STLL : SoundlyTypedLocalLang LL TLL)
       (L : Locations) (Import SL : SyncLabels).
  Include (TypedPirouette L LL TLL SL).

    Theorem Preservation:
      forall (Γ : L.t -> nat -> ExprTyp) (Δ : nat -> PirTyp) (C : PirExpr) (τ : PirTyp),
        Γ;; Δ ⊢c C ::: τ -> forall (R : Redex) (B : list L.t) (C': PirExpr),
          PirStep R B C C' -> Γ;; Δ ⊢c C' ::: τ.
    Proof.
      apply RelativePreservation. intros Γ e τ H e' H0.
      apply ExprPreservation with (e1 := e); auto.
    Qed.

    Theorem StepsPreservation :
      forall (Γ : L.t -> nat -> ExprTyp) (Δ : nat -> PirTyp) (C : PirExpr) (τ : PirTyp),
        Γ;; Δ ⊢c C ::: τ -> forall (Rs : list Redex) (B : list L.t) (C': PirExpr),
          PirSteps Rs B C C' -> Γ;; Δ ⊢c C' ::: τ.
    Proof.
      apply RelativeStepsPreservation. intros Γ e τ H e' H0.
      apply ExprPreservation with (e1 := e); auto.
    Qed.

    Theorem Progress :
    forall (C : PirExpr) (Γ : L.t -> nat -> ExprTyp) (Δ : nat -> PirTyp) (τ : PirTyp),
      PirExprClosed C -> Γ;; Δ ⊢c C ::: τ ->
      PirExprVal C \/ exists R C', PirStep R nil C C'.
    Proof.
      apply RelativeProgress; auto.
      intros e τ Γ H H0; eapply ExprProgress; eauto.
      intros v Γ H H0; eapply BoolInversion; eauto.
    Qed.

End SoundlyTypedPirouette.
