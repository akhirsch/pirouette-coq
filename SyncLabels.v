(*
  A module defining synchronization labels. They are a choice of left or right. They have
  decidable equality. By having them in their own module, we can share them between 
  Pirouette and the control language.
*)
Module Type SyncLabels.
  Inductive SyncLabel := Left | Right.
  Definition SyncLabelEqDec : forall d1 d2 : SyncLabel, {d1 = d2} + {d1 <> d2}.
    decide equality.
  Defined.
  
End SyncLabels.
