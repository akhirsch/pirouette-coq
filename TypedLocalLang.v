Require Export LocalLang.

(*
  Assumptions for a type system for local languages.

  Again, this is given as a module type. See STLC.v and TypedNatLang.v for instantiations.
 *)

Module Type TypedLocalLang (Import E : LocalLang).
  
  (*
    Types are a Set with decidable equality.
   *)
  Parameter ExprTyp : Set.
  Parameter ExprTypEqDec : forall tau sigma : ExprTyp, {tau = sigma} + {tau <> sigma}.

  (*
    We require a typing judgment of the form Γ ⊢ e : τ. We represent a context Γ as a 
    function from natural numbers (which represent variables) to types. This judgment is a
    proposition (probably given as an inductive type, but we do not bake that in).
   *)
  Parameter ExprTyping : (nat -> ExprTyp) -> Expr -> ExprTyp -> Prop.
  Notation "Gamma ⊢e e ::: tau" := (ExprTyping Gamma e tau) (at level 30).

  (* 
     Variables are given the type they are assigned in the context.
   *)
  Parameter ExprVarTyping : forall (Γ : nat -> ExprTyp) (n : nat),
      Γ ⊢e (ExprVar n) ::: (Γ n).

  (*
    Typing is extensional in the since that it does not look at the internal definition of
    its context; it only cares about what type the context associates with what variable.
   *)
  Parameter ExprTypingExt : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
      (forall n, Γ n = Δ n) ->
      Γ ⊢e e ::: τ ->
      Δ ⊢e e ::: τ.

  (*
    We require that each expression have a unique type within a  given context. This is
    probably the most burdensome requirement of this module type. However, it is necessary
    for equivalence of choreographies to be type preserving.
   *)
  Parameter ExprTypingUnique : forall (Γ : nat -> ExprTyp) (e : Expr) (τ σ : ExprTyp),
      Γ ⊢e e ::: τ ->
      Γ ⊢e e ::: σ ->
      τ = σ.

  (*
    The type system should not take the values assigned by the context to variables not
    free in the expression. Thus, if an expression e is closed above n, the values of the 
    context above n should not matter.

    This is enough to allow us to prove that the context does not matter at all when typing
    closed programs. Since we insist that all values are closed, it does not matter when
    typing values.
   *)
  Parameter ExprClosedAboveTyping : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp) (n : nat),
      ExprClosedAbove n e -> (forall m, m < n -> Γ m = Δ m) -> Γ ⊢e e ::: τ -> Δ ⊢e e ::: τ.
  Lemma ExprClosedTyping : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
      ExprClosed e -> Γ ⊢e e ::: τ -> Δ ⊢e e ::: τ.
  Proof.
    intros Γ Δ e τ H H0. unfold ExprClosed in H.
    apply ExprClosedAboveTyping with (Γ := Γ) (n := 0); auto.
    intros m H1. inversion H1.
  Qed.
  Lemma ExprValueTyping : forall (Γ Δ : nat -> ExprTyp) (v : Expr) (τ : ExprTyp),
      ExprVal v -> Γ ⊢e v ::: τ -> Δ ⊢e v ::: τ.
  Proof.
    intros Γ Δ v τ H H0. apply ExprClosedTyping with (Γ := Γ); auto.
    apply ExprValuesClosed; auto.
  Qed.
  
  (*
    We represent weakening by allowing renaming, changing the  context to match. This
    represents exchange as well as the classical definition of weakening. To see why this
    represents weakening, consider an expression e which is closed above n, but which does
    contain the variable n. If we type e in some context Γ, we can think of the "real" 
    finite context as Γ up through n. Then imagine renaming with the function
    ξ i = if i = n + 1 then 0 else i + 1
    Clearly, the resulting expression is only closed above n + 1. Thus, the new real
    context contains some variables not in the original context.
   *)
  Parameter ExprWeakening : forall (Γ Δ : nat -> ExprTyp) (ξ : nat -> nat) (e : Expr) (τ : ExprTyp),
      (forall n, Γ n = Δ (ξ n)) ->
      Γ ⊢e e ::: τ ->
      Δ ⊢e e ⟨e| ξ⟩ ::: τ.

  (*
    We require a type of booleans, and require that true and false are of boolean type.
   *)
  Parameter bool : ExprTyp.
  Parameter TrueTyping : forall (Γ : nat -> ExprTyp),
      Γ ⊢e tt ::: bool.
  Parameter FalseTyping : forall (Γ : nat -> ExprTyp),
      Γ ⊢e ff ::: bool.

  (*
    Substitutions replace every variable with a new expression. These expressions can
    themselves contain variables. If Γ is a context, and σ assigns every variable x an 
    expression that (under a new context Δ) has the type Γ gives x, then we say that σ
    changes Γ to Δ, which we write Γ ⊢ σ ⊣ Δ. 
   *)
  Definition ExprSubstTyping : (nat -> ExprTyp) -> (nat -> Expr) -> (nat -> ExprTyp) -> Prop :=
    fun Γ σ Δ => forall n : nat, Δ ⊢e (σ n) ::: (Γ n).
  Notation "Gamma ⊢es sigma ⊣ Delta"  := (ExprSubstTyping Gamma sigma Delta) (at level 30).

  (* 
     We then require that, if σ changes Γ to Δ and e hastype τ under Γ, then e[e| σ] has
     type τ under Δ.
   *)
  Parameter ExprSubstType :
    forall (Γ Δ : nat -> ExprTyp) (σ : nat -> Expr) (e : Expr) (τ : ExprTyp),
      Γ ⊢es σ ⊣ Δ -> Γ ⊢e e ::: τ -> Δ ⊢e e [e| σ ] ::: τ.

  (*
    Clearly, the identity substitution changes Γ to itself.
   *)
  Lemma ExprIdSubstTyping : forall (Γ : nat -> ExprTyp), Γ ⊢es ExprIdSubst ⊣ Γ.
  Proof.
    unfold ExprSubstTyping; intros Γ n; unfold ExprIdSubst; apply ExprVarTyping.
  Qed.                                                   
  
End TypedLocalLang.
