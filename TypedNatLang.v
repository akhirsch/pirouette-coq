Require Export LocalLang.
Require Export TypedLocalLang.
Require Export NatLang.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Lists.List.

(*
  A very simple type system for a very simple language.
*)
Module TypedNatLang <: (TypedLocalLang NatLang).
  Include NatLang.

  Inductive NatLangType := N | B.
  Definition ExprTyp := NatLangType.
  Definition ExprTypEqDec : forall tau sigma : ExprTyp, {tau = sigma} + {tau <> sigma}.
    decide equality.
  Defined.

  Reserved Notation "Γ ⊢e e ::: τ" (at level 30).
  Inductive NLTyping : (nat -> ExprTyp) -> Expr -> ExprTyp -> Prop :=
  | VarTyping : forall Γ x, Γ ⊢e (var x) ::: (Γ x) (* Requires that variables are in scope *)
  | ZTyping : forall Γ, Γ ⊢e Z ::: N
  | STyping : forall Γ e, Γ ⊢e e ::: N -> Γ ⊢e (Succ e) ::: N
  | ttrueTyping : forall Γ, Γ ⊢e ttrue ::: B
  | ffalseTyping : forall Γ, Γ ⊢e ffalse ::: B
  where "Γ ⊢e e ::: τ" := (NLTyping Γ e τ).
  Global Hint Constructors NLTyping : NL.
  Definition ExprTyping : (nat -> ExprTyp) -> Expr -> ExprTyp -> Prop := NLTyping.

  Theorem ExprVarTyping : forall (Γ : nat -> ExprTyp) (n : nat),
      Γ ⊢e (ExprVar n) ::: (Γ n).
  Proof using. intros Γ n; unfold ExprVar; constructor. Qed.
  
  Theorem ExprTypingExt : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
      (forall n, Γ n = Δ n) ->
      Γ ⊢e e ::: τ ->
      Δ ⊢e e ::: τ.
  Proof using.
    intros Γ Δ e τ eq typ; revert Δ eq; induction typ; intros Δ eq; auto with NL.
    rewrite eq; constructor.
  Qed.

  Theorem ExprTypingUnique : forall (Γ : nat -> ExprTyp) (e : Expr) (τ σ : ExprTyp),
      Γ ⊢e e ::: τ ->
      Γ ⊢e e ::: σ ->
      τ = σ.
  Proof using.
    intros Γ e τ σ typ1; revert σ; induction typ1; intros σ typ2; inversion typ2; subst;
      reflexivity.
  Qed.

  Theorem ExprWeakening : forall (Γ Δ : nat -> ExprTyp) (ξ : nat -> nat) (e : Expr) (τ : ExprTyp),
      (forall n, Γ n = Δ (ξ n)) ->
      Γ ⊢e e ::: τ ->
      Δ ⊢e e ⟨e| ξ⟩ ::: τ.
  Proof using.
    intros Γ Δ ξ e τ eq typ; revert Δ ξ eq; induction typ; intros Δ ξ eq; cbn in *;
      auto with NL.
    rewrite eq; auto with NL.
  Qed.

  Theorem ExprClosedAboveTyping : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp) (n : nat),
      ExprClosedAbove n e -> (forall m, m < n -> Γ m = Δ m) -> Γ ⊢e e ::: τ -> Δ ⊢e e ::: τ.
  Proof using.
    intros Γ Δ e; revert Γ Δ; induction e; intros Γ Δ τ x cb bdd ty; inversion ty;
      subst; auto with NL.
    - rewrite bdd; auto with NL. - constructor; eapply IHe; eauto.
  Qed.

  Lemma ExprClosedTyping : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
      ExprClosed e -> Γ ⊢e e ::: τ -> Δ ⊢e e ::: τ.
  Proof.
    intros Γ Δ e τ H H0. unfold ExprClosed in H.
    apply ExprClosedAboveTyping with (Γ := Γ) (n := 0); auto.
    intros m H1. inversion H1.
  Qed.

  Lemma ExprValueTyping : forall (Γ Δ : nat -> ExprTyp) (v : Expr) (τ : ExprTyp),
      ExprVal v -> Γ ⊢e v ::: τ -> Δ ⊢e v ::: τ.
  Proof.
    intros Γ Δ v τ H H0. apply ExprClosedTyping with (Γ := Γ); auto.
    apply ExprValuesClosed; auto.
  Qed.
  
  Definition bool : ExprTyp := B.
  Theorem TrueTyping : forall (Γ : nat -> ExprTyp), Γ ⊢e tt ::: bool.
  Proof using. intro Γ; constructor. Qed.
  
  Theorem FalseTyping : forall (Γ : nat -> ExprTyp), Γ ⊢e ff ::: bool.
  Proof using. intro Γ; constructor. Qed.

  Definition ExprSubstTyping : (nat -> ExprTyp) -> (nat -> Expr) -> (nat -> ExprTyp) -> Prop :=
    fun Γ σ Δ => forall n : nat, Δ ⊢e (σ n) ::: (Γ n).
  Notation "Gamma ⊢es sigma ⊣ Delta"  := (ExprSubstTyping Gamma sigma Delta) (at level 30).
  Theorem ExprSubstType :
    forall (Γ Δ : nat -> ExprTyp) (sigma : nat -> Expr) (e : Expr) (τ : ExprTyp),
      Γ ⊢es sigma ⊣ Δ -> Γ ⊢e e ::: τ -> Δ ⊢e e [e| sigma ] ::: τ.
  Proof using.
    intros Γ Δ σ e τ σ_ty e_ty; revert Δ σ σ_ty; induction e_ty; intros Δ σ σ_ty; cbn;
      try (constructor; auto); apply σ_ty.
  Qed.

  Lemma ExprIdSubstTyping : forall (Γ : nat -> ExprTyp), Γ ⊢es ExprIdSubst ⊣ Γ.
  Proof.
    unfold ExprSubstTyping. intros Γ n. unfold ExprIdSubst. apply ExprVarTyping.
  Qed.                                                   
  
End TypedNatLang.
