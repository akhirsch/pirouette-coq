Require Export LocalLang.
Require Export TypedLocalLang.
Require Export Pirouette.

Module TypedPirouette (Import L : Locations) (Import LL : LocalLang)
       (Import TLL : TypedLocalLang LL) (Import SL : SyncLabels).
  
  Include (Pirouette LL L SL).

  Inductive PirTyp : Set :=
  | LocatedType : Loc -> ExprTyp -> PirTyp
  | LocalFun : Loc -> ExprTyp -> PirTyp -> PirTyp
  | PirExprFun : PirTyp -> PirTyp -> PirTyp.

  Reserved Notation "G ;; D ⊢c C ::: tau" (at level 30).
  Inductive ctyping : (Loc -> nat -> ExprTyp) -> (nat -> PirTyp) -> PirExpr -> PirTyp -> Prop :=
  | TDone : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
              (ℓ : Loc) (e : Expr) (τ : ExprTyp),
       (Γ ℓ) ⊢e e ::: τ ->
  (* ------------------------------ *)
      Γ ;; Δ ⊢c Done ℓ e ::: (LocatedType ℓ τ)
  | TVar : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (n : nat),
  (* ----------------------------- *)
      Γ ;; Δ ⊢c Var n ::: (Δ n)
  | TSend : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
              (ℓ₁ : Loc) (e : Expr) (τ : ExprTyp) (ℓ₂ : Loc) (C : PirExpr) (σ : PirTyp),
      (Γ ℓ₁) ⊢e e ::: τ ->
      (fun ℓ₃ n => if L.eq_dec ℓ₂ ℓ₃
               then match n with
                    | 0 => τ
                    | S n => Γ ℓ₃ n
                    end
               else Γ ℓ₃ n);; Δ ⊢c C ::: σ ->
      ℓ₁ <> ℓ₂ ->
  (* --------------------------------------- *)
      Γ ;; Δ ⊢c (Send ℓ₁ e ℓ₂ C) ::: σ
  | TIf : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
            (ℓ : Loc) (e : Expr) (C1 C2 : PirExpr) (τ : PirTyp),
      (Γ ℓ) ⊢e e ::: bool ->
      Γ ;; Δ ⊢c C1 ::: τ ->
      Γ ;; Δ ⊢c C2 ::: τ ->
  (* --------------------------------- *)
      Γ;; Δ ⊢c If ℓ e C1 C2 ::: τ
  | TSync : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
              (ℓ₁ : Loc) (s : SyncLabel) (ℓ₂ : Loc) (C : PirExpr) (τ : PirTyp),
      Γ ;; Δ ⊢c C ::: τ ->
      ℓ₁ <> ℓ₂ ->
  (* ---------------------------------- *)
      Γ ;; Δ ⊢c Sync ℓ₁ s ℓ₂ C ::: τ
  | TDefLocal : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
                  (ℓ : Loc) (C1 C2 : PirExpr) (τ : ExprTyp) (σ : PirTyp),
      Γ ;; Δ ⊢c C1 ::: (LocatedType ℓ τ) ->
      (fun ℓ' n =>
         if L.eq_dec ℓ ℓ'
         then match n with
              | 0 => τ
              | S n => Γ ℓ' n
              end
         else Γ ℓ' n);; Δ ⊢c C2 ::: σ ->
  (* ------------------------------------*)
      Γ ;; Δ ⊢c DefLocal ℓ C1 C2 ::: σ
  | TFunLocal : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) 
                 (C : PirExpr) (ℓ : Loc) (τ : ExprTyp) (σ : PirTyp),
      (fun ℓ' n =>
         if L.eq_dec ℓ' ℓ
         then match n with
              | 0 => τ
              | S n => Γ ℓ' n
              end
         else Γ ℓ' n);; (fun n => match n with
                              | 0 => LocalFun ℓ τ σ
                              | S n => Δ n
                              end) ⊢c C ::: σ ->
      Γ;; Δ ⊢c (FunLocal ℓ C) ::: LocalFun ℓ τ σ
  | TFunGlobal : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
                 (C : PirExpr) (τ σ : PirTyp),
      Γ;; (fun n => match n with
                 | 0 => τ
                 | 1 => PirExprFun τ σ
                 | S (S n) => Δ n
                 end) ⊢c C ::: σ
      -> Γ;; Δ ⊢c FunGlobal C ::: PirExprFun τ σ
  | TAppLocal : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
                 (C : PirExpr) (ℓ : Loc) (e : Expr) (τ : ExprTyp) (σ : PirTyp),
      (Γ ℓ) ⊢e e ::: τ ->
      Γ;; Δ ⊢c C ::: LocalFun ℓ τ σ ->
      Γ;; Δ ⊢c AppLocal ℓ C e ::: σ
  | TAppGlobal : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
                   (C1 C2 : PirExpr) (τ σ : PirTyp),
      Γ;; Δ ⊢c C1 ::: PirExprFun τ σ ->
      Γ;; Δ ⊢c C2 ::: τ ->
      Γ;; Δ ⊢c AppGlobal C1 C2 ::: σ
  where "Gamma ;; Delta ⊢c C ::: tau" := (ctyping Gamma Delta C tau).
  Global Hint Constructors ctyping : PirExpr.
  
  (*
    WEAKENING
   *)

  (*
    We represent weakening by allowing renaming, changing the  context to match. This
    represents exchange as well as the classical definition of weakening. To see why this
    represents weakening, consider an expression e which is closed above n, but which does
    contain the variable n. If we type e in some context Γ, we can think of the "real" 
    finite context as Γ up through n. Then imagine renaming with the function
    ξ i = if i = n + 1 then 0 else i + 1
    Clearly, the resulting expression is only closed above n + 1. Thus, the new real
    context contains some variables not in the original context.

    We define weakening for both local and Pirouette contexts.
   *)
  Theorem TypeLocalWeakening : forall (Γ1 Γ2 : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp)
                               (ξ : Loc -> nat -> nat),
      (forall ℓ n, Γ1 ℓ n = Γ2 ℓ (ξ ℓ n)) ->
      forall (τ : PirTyp) (C : PirExpr),
        Γ1;; Δ ⊢c C ::: τ ->
        Γ2;; Δ ⊢c C⟨local| ξ⟩ ::: τ.
  Proof using.
    intros Γ1 Γ2 Δ ξ eqv τ C typing; revert Γ2 ξ eqv; induction typing; intros Γ2 ξ eqv;
      cbn; repeat match goal with
                  | [ H : ?Γ ?ℓ ⊢e ?e ::: ?t, eqv : forall ℓ n, ?Γ ℓ n = ?Γ2 ℓ (?ξ ℓ n) |- _] =>
                    lazymatch goal with
                    | [_ : Γ2 ℓ ⊢e e ⟨e| ξ ℓ ⟩ ::: t |- _ ] => fail
                    | _ => pose proof (ExprWeakening (Γ ℓ) (Γ2 ℓ) (ξ ℓ) e t (eqv ℓ) H)
                    end
                  end; auto with PirExpr.
    - apply TSend with (τ := τ); auto; apply IHtyping.
      intros ℓ n; unfold PirExprUpLocalRename; unfold ExprUpRename;
        destruct (L.eq_dec ℓ₂ ℓ); subst; destruct n; auto.
    - apply TDefLocal with (τ := τ); auto; apply IHtyping2.
      intros ℓ' n; unfold PirExprUpLocalRename; unfold ExprUpRename;
        destruct (L.eq_dec ℓ ℓ'); destruct n; auto.
    - apply TFunLocal; auto. apply IHtyping.
      intros ℓ' n; unfold PirExprUpLocalRename; unfold ExprUpRename.
      destruct (L.eq_dec ℓ' ℓ); destruct (L.eq_dec ℓ ℓ'); subst;
        try match goal with
            | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
            end; destruct n; auto.
    - apply TAppLocal with (τ := τ); auto.
    - apply TAppGlobal with (τ := τ); auto.
  Qed.
      
  Theorem TypePirExprWeakening : forall (Γ : Loc -> nat -> ExprTyp) (Δ1 Δ2 : nat -> PirTyp)
                                (ξ : nat -> nat),
      (forall n, Δ1 n = Δ2 (ξ n)) ->
      forall (τ : PirTyp) (C : PirExpr),
        Γ ;; Δ1 ⊢c C ::: τ ->
        Γ ;; Δ2 ⊢c C ⟨global| ξ⟩ ::: τ.
  Proof using.
    intros Γ Δ1 Δ2 ξ eqv τ C typing; revert Δ2 ξ eqv; induction typing;
      intros Δ2 ξ eqv; cbn; eauto with PirExpr.
    - rewrite eqv; apply TVar.
    - apply TFunLocal; apply IHtyping.
      unfold PirExprUpRename; destruct n; auto.
    - apply TFunGlobal; apply IHtyping.
      unfold PirExprUpRename; repeat (destruct n; auto).
  Qed.

  (* EXTENSIONALITY *)

  (*
    We only care about what variables map to which types, not how the context calculates 
    this.
   *)
  Theorem PirTypingExt : forall (Γ1 Γ2 : Loc -> nat -> ExprTyp) (Δ1 Δ2 : nat -> PirTyp)
                            (C : PirExpr) (τ : PirTyp),
      (forall (ℓ : Loc) (n : nat), Γ1 ℓ n = Γ2 ℓ n) ->
      (forall n : nat, Δ1 n = Δ2 n) ->
      Γ1;; Δ1 ⊢c C ::: τ -> Γ2;; Δ2 ⊢c C ::: τ.
  Proof.
    intros Γ1 Γ2 Δ1 Δ2 C τ eqΓ eqΔ typing; revert Γ2 Δ2 eqΓ eqΔ; induction typing;
      intros Γ2 Δ2 eqΓ eqΔ;
      repeat match goal with
             | [ H : ?Γ ?ℓ ⊢e ?e ::: ?t, eqv : forall ℓ n, ?Γ ℓ n = ?Γ2 ℓ n |- _ ] =>
               lazymatch goal with
               | [_ : Γ2 ℓ ⊢e e ::: t |- _ ] => fail
               | _ => pose proof (ExprTypingExt (Γ ℓ) (Γ2 ℓ) e t (eqv ℓ) H)
               end
             end; auto with PirExpr.
    - rewrite eqΔ; apply TVar; auto with PirExpr.
    - apply TSend with (τ := τ); auto with PirExpr; apply IHtyping; auto.
      intros ℓ n; destruct (L.eq_dec ℓ₂ ℓ); destruct n; auto.
    - apply TDefLocal with (τ := τ); auto with PirExpr; apply IHtyping2; auto.
      intros ℓ' n; destruct (L.eq_dec ℓ ℓ'); destruct n; auto.
    - apply TFunLocal; apply IHtyping; auto.
      intros ℓ' n; destruct (L.eq_dec ℓ' ℓ); destruct n; auto.
      intro n; destruct n; auto.
    - apply TFunGlobal; apply IHtyping; auto.
      destruct n; auto; destruct n; auto.
    - apply TAppLocal with (τ := τ); auto.
    - apply TAppGlobal with (τ := τ); auto.
  Qed.

  (*
    Equivalence of Pirouette programs preserves types.
   *)
  Theorem EquivTyping : forall (C1 C2 : PirExpr),
      C1 ≡ C2 ->
      forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (τ : PirTyp),
        Γ ;; Δ ⊢c C1 ::: τ -> Γ ;; Δ ⊢c C2 ::: τ.
  Proof using.
    intros C1 C2 equiv; induction equiv; intros Γ Δ τ typ; auto with PirExpr;
      repeat match goal with
             | [ H : ?Γ ;; ?Δ ⊢c ?C ::: ?t |- _ ] =>
               tryif let x := fresh "_" C in idtac
               then fail
               else inversion H; subst; clear H
             end; auto with PirExpr.
    all: repeat match goal with
                | [IH : forall G D t, G ;; D ⊢c ?C1 ::: t  -> G ;; D ⊢c ?C2 ::: t,
                     H : ?Γ ;; ?Δ ⊢c ?C1 ::: ?τ |- _ ] =>
                  lazymatch goal with
                  | [_ : Γ ;; Δ ⊢c C2 ::: τ |- _ ] => fail
                  | _ => pose proof (IH Γ Δ τ H)
                  end
                end; eauto with PirExpr.
    - apply TSend with (τ := τ1); auto.
      destruct (L.eq_dec l2 l3) as [eq | _ ]; [destruct (H1 eq)|]; auto.
      apply TSend with (τ := τ0); auto.
      destruct (L.eq_dec l4 l1) as [eq|_]; [destruct (H0 (eq_sym eq))|]; auto.
      eapply PirTypingExt; [| |exact H14]; auto.
      intros ℓ n. cbn. destruct (L.eq_dec l4 ℓ); destruct (L.eq_dec l2 ℓ); subst;
        try match goal with [H : ?a <> ?a |- _] => destruct (H eq_refl) end;
        auto; destruct n; auto.
    - apply TIf. destruct (L.eq_dec l2 l3) as [eq|_]; [destruct (H0 eq)|auto].
      all: apply TSend with (τ := τ0); auto.
    - apply TSend with (τ := τ0); auto.
      apply TIf; auto.
      destruct (L.eq_dec l3 l1) as [eq | _ ]; [destruct (H0 (eq_sym eq))| auto].
      rewrite (ExprTypingUnique (Γ l2) e' τ0 τ1 H11 H10); auto.
  Qed.      
  
  (*
    Substitutions replace every variable with a new expression. These expressions can
    themselves contain variables. If Γ is a context, and σ assigns every variable x an 
    expression that (under a new context Δ) has the type Γ gives x, then we say that σ
    changes Γ to Δ, which we write Γ ⊢ σ ⊣ Δ. 
   *)
  Definition PirExprSubstTyping Γ Δ1 σ Δ2 :=
    (forall (n : nat), Γ;; Δ2 ⊢c (σ n) ::: (Δ1 n)).

  Theorem PirExprLocalSubstTypingSpec : forall Γ1 σ Γ2 Δ C τ,
      (forall ℓ, Γ1 ℓ ⊢es σ ℓ ⊣ Γ2 ℓ) -> Γ1 ;; Δ ⊢c C ::: τ -> Γ2 ;; Δ ⊢c C [local| σ] ::: τ.
  Proof using.
    intros Γ1 σ Γ2 Δ C τ eqv typing; revert σ Γ2 eqv; induction typing; intros σ' Γ2 eqv;
      cbn; repeat match goal with
                  | [ H : ?Γ ?ℓ ⊢e ?e ::: ?t, eqv : forall ℓ, ?Γ ℓ ⊢es ?σ' ℓ ⊣ ?Γ2 ℓ |- _] =>
                    lazymatch goal with
                    | [ _ : Γ2 ℓ ⊢e e [e|σ' ℓ] ::: t |- _] => fail
                    | _ => pose proof (ExprSubstType (Γ ℓ) (Γ2 ℓ) (σ' ℓ) e t (eqv ℓ) H)
                    end
                  end; eauto with PirExpr.
    - apply TSend with (τ := τ); auto; apply IHtyping.
      intro ℓ; unfold ExprSubstTyping; intro n; unfold PirExprUpLocalSubst;
        destruct (L.eq_dec ℓ₂ ℓ); destruct n; subst; auto.
      apply ExprVarTyping. apply ExprWeakening with (Γ := Γ2 ℓ); auto.
      all: apply eqv.
    - apply TDefLocal with (τ := τ); auto; apply IHtyping2; auto.
      intro ℓ'; unfold PirExprUpLocalSubst; intro n;
        destruct (L.eq_dec ℓ ℓ'); destruct n; auto with PirExpr.
      apply ExprVarTyping. apply ExprWeakening with (Γ := Γ2 ℓ'); auto.
      all: apply eqv.
    - apply TFunLocal; apply IHtyping; auto.
      intro ℓ'; unfold PirExprUpLocalSubst; cbn. unfold ExprSubstTyping. intro n; cbn.
      destruct (L.eq_dec ℓ' ℓ); destruct (L.eq_dec ℓ ℓ'); subst;
        try match goal with
            | [ H : ?a <> ?a |- _] => destruct (H eq_refl)
            end; auto.
      destruct n; auto.
      apply ExprVarTyping. apply ExprWeakening with (Γ := Γ2 ℓ); auto.
      apply eqv. apply eqv.
  Qed.      
         
  (* Lemma TypingLocalUp :forall Γ Δ C τ, *)
  (*     Γ ;; Δ ⊢c C ::: τ  -> *)
  (*     forall p σ, (fun r n => if L.eq_dec r p *)
  (*                     then match n with *)
  (*                          | 0 => σ *)
  (*                          | S n => Γ r n *)
  (*                          end *)
  (*                     else Γ r n) ;; Δ ⊢c C ⟨local| fun r n => if L.eq_dec r p *)
  (*                                                           then S n *)
  (*                                                           else n ⟩ ::: τ. *)
  (* Proof using. *)
  (*   intros Γ Δ C τ H q σ. *)
  (*   apply TypeLocalWeakening with (Γ1 := Γ); auto. *)
  (*   intros p0 n; destruct (L.eq_dec q p0); subst. *)
  (*   destruct (L.eq_dec p0 p0) as [_ | neq]; [|destruct (neq eq_refl)]; auto. *)
  (*   destruct (L.eq_dec p0 q) as [eq | _ ]; [destruct (n0 (eq_sym eq))|] ;auto. *)
  (* Qed. *)
  
  Theorem PirExprSubstTypingSpec : forall Γ Δ1 σ Δ2,
      (PirExprSubstTyping Γ Δ1 σ Δ2) ->
      forall (C : PirExpr) (τ : PirTyp),
        (Γ;; Δ1 ⊢c C ::: τ) -> (Γ;; Δ2 ⊢c (C [global| σ]) ::: τ).
  Proof.
    intros Γ Δ1 σ Δ2 styping C τ typing; revert σ Δ2 styping; induction typing;
      try rename σ into ρ; intros σ Δ2 styping; cbn; auto with PirExpr.
    - apply TSend with (τ := τ); auto. apply IHtyping.
      unfold PirExprSubstTyping. intro n. unfold PirExprSubstTyping in styping.
      unfold PirExprUpSubstAllLocal. apply TypeLocalWeakening with (Γ1 := Γ); auto.
      intros ℓ n0; destruct (L.eq_dec ℓ₂ ℓ); subst; auto.
    - apply TDefLocal with (τ := τ). apply IHtyping1; auto.
      unfold PirExprUpSubstAllLocal. apply IHtyping2. unfold PirExprSubstTyping.
      intro n; apply TypeLocalWeakening with (Γ1 := Γ); auto.
      intros ℓ' n0; destruct (L.eq_dec ℓ ℓ'); auto.
    - apply TFunLocal; apply IHtyping. unfold PirExprSubstTyping in *; intro n.
      unfold PirExprUpSubst; unfold PirExprUpSubstAllLocal; destruct n; cbn; auto.
      apply TVar.
      apply TypePirExprWeakening with (Δ1 := Δ2); auto.
      apply TypeLocalWeakening with (Γ1 := Γ); auto.
      intros ℓ' m; destruct (L.eq_dec ℓ ℓ'); destruct (L.eq_dec ℓ' ℓ); subst;
        try match goal with
            | [ H : ?a <> ?a |- _ ] => destruct (H eq_refl)
            end; auto.
    - apply TFunGlobal; apply IHtyping; repeat unfold PirExprUpSubst.
      unfold PirExprSubstTyping in *.
      intro n.
      destruct n; [apply TVar|].
      destruct n; [apply TVar|].
      apply TypePirExprWeakening with (Δ1 := fun m => match m with
                                               | 0 => PirExprFun τ ρ
                                               | S m => Δ2 m
                                               end); auto.
      apply TypePirExprWeakening with (Δ1 := Δ2); auto.
    - apply TAppLocal with (τ := τ); auto.
    - apply TAppGlobal with (τ := τ); auto.
  Qed.

  (* CLOSED PROGRAMS *)

  (*
    The type system does not care about the types assigned to variables _not_ in a
    Pirouette program.
   *)
  Lemma PirExprClosedAboveTyping : forall (Γ1 Γ2 : Loc -> nat -> ExprTyp)
                                  (Δ1 Δ2 : nat -> PirTyp)
                                  (C : PirExpr) (τ : PirTyp)
                                  (n : Loc -> nat) (m : nat),
      PirExprClosedAbove n m C ->
      (forall (ℓ : Loc) (k : nat), k < (n ℓ) -> Γ1 ℓ k = Γ2 ℓ k) ->
      (forall k : nat, k < m -> Δ1 k = Δ2 k) ->
      Γ1;; Δ1 ⊢c C ::: τ -> Γ2;; Δ2 ⊢c C ::: τ.
  Proof.
    intros Γ1 Γ2 Δ1 Δ2 C τ n m cb eqΓ eqΔ typ; revert Γ2 Δ2 n m cb eqΓ eqΔ;
      induction typ; try rename n into n'; intros Γ2 Δ2 n m cb eqΓ eqΔ;
        inversion cb; subst.
    - apply TDone; eapply ExprClosedAboveTyping; [| apply eqΓ|]; eauto.
    - rewrite eqΔ; auto; apply TVar.
    - eapply TSend; eauto.
      eapply ExprClosedAboveTyping; [| | exact H]; eauto.
      eapply IHtyp; eauto. cbn in *; intros ℓ k l.
      destruct (L.eq_dec ℓ₂ ℓ); subst; auto. destruct k; auto. apply eqΓ.
      apply Lt.lt_S_n in l; auto.
    - apply TIf. eapply ExprClosedAboveTyping; [| apply eqΓ|]; eauto.
      eapply IHtyp1; eauto. eapply IHtyp2; eauto.
    - apply TSync; auto; eapply IHtyp; eauto.
    - eapply TDefLocal. eapply IHtyp1; eauto.
      eapply IHtyp2; eauto. cbn in *; intros ℓ' k l.
      destruct (L.eq_dec ℓ ℓ'); subst; auto. destruct k; auto.
      apply eqΓ; apply Lt.lt_S_n in l; auto.
    - eapply TFunLocal; eauto; eapply IHtyp; eauto.
      intros ℓ' k k_lt; cbn in *.
      2: intros k k_lt.
      destruct (L.eq_dec ℓ' ℓ); destruct (L.eq_dec ℓ ℓ'); subst;
        try match goal with
            | [H : ?a <> ?a |- _ ] => destruct (H eq_refl)
            end; auto.
      all: destruct k; auto.
      all: apply Lt.lt_S_n in k_lt.
      apply eqΓ; auto. apply eqΔ; auto.
    - eapply TFunGlobal; eauto; eapply IHtyp; eauto.
      intros k k_lt; destruct k; auto; destruct k; auto.
      repeat apply Lt.lt_S_n in k_lt. apply eqΔ; auto.
    - eapply TAppLocal; eauto. eapply ExprClosedAboveTyping; [| apply eqΓ|]; eauto.
    - eapply TAppGlobal; eauto.       
  Qed.


  Lemma PirExprClosedTyping : forall (Γ1 Γ2 : Loc -> nat -> ExprTyp) (Δ1 Δ2 : nat -> PirTyp)
                             (C : PirExpr) (τ : PirTyp),
      PirExprClosed C -> Γ1;; Δ1 ⊢c C ::: τ -> Γ2;; Δ2 ⊢c C ::: τ.
  Proof.
    intros Γ1 Γ2 Δ1 Δ2 C τ H H0.
    unfold PirExprClosed in H.
    apply PirExprClosedAboveTyping with (Γ1 := Γ1) (Δ1 := Δ1) (n := fun _ => 0) (m := 0); auto.
    intros ℓ k H1; inversion H1.
    intros k H1; inversion H1.
  Qed.

  (* Theorem PirExprValueTyping : forall (C : PirExpr) (τ : PirTyp), *)
  (*     PirExprVal C -> *)
  (*      forall Γ1 Γ2 Δ1 Δ2, Γ1;; Δ1 ⊢c C ::: τ -> Γ2;; Δ2 ⊢c C ::: τ. *)
  (* Proof. *)
  (*   intros C τ H Γ1 Γ2 Δ1 Δ2 H0. *)
  (*   eapply PirExprClosedTyping; eauto. apply PirExprValuesClosed; auto. *)
  (* Qed. *)

  
  (* SPECIFIC SUBSTITUTIONS *)
  
  (*
    We prove that the three specific substitutions that we use in the operational semantics
    of Pirouette have specific types.
   *)
  Lemma ValueSubstTyping: forall (Γ : Loc -> nat -> ExprTyp) (ℓ : Loc) (e : Expr) (τ : ExprTyp),
      (Γ ℓ ⊢e e ::: τ) ->
      forall ℓ₂, ((fun ℓ₃ n => if L.eq_dec ℓ ℓ₃
                     then match n with
                          | 0 => τ
                          | S n => Γ ℓ₃ n
                          end
                     else Γ ℓ₃ n) ℓ₂) ⊢es ValueSubst ℓ e ℓ₂ ⊣ (Γ ℓ₂).
  Proof using.
    intros Γ ℓ e τ tpng ℓ'. unfold ValueSubst.
    unfold ExprSubstTyping. intro n. destruct n; destruct (L.eq_dec ℓ ℓ'); subst; auto.
    all: apply ExprVarTyping.
  Qed.

  Lemma AppLocalSubstTyping : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (ℓ : Loc)
                                (τ : ExprTyp) (σ : PirTyp) (C : PirExpr),
      (fun ℓ' n => if L.eq_dec ℓ' ℓ
                then match n with
                     | 0 => τ
                     | S n => Γ ℓ' n
                     end
                else Γ ℓ' n);; (fun n => match n with
                                      | 0 => LocalFun ℓ τ σ
                                      | S n => Δ n
                                      end) ⊢c C ::: σ ->
      PirExprSubstTyping Γ (fun n => match n with
                               | 0 => LocalFun ℓ τ σ
                               | S n => Δ n
                               end) (AppLocalSubst ℓ C) Δ.
  Proof using.
    intros Γ Δ ℓ τ σ C typing; unfold PirExprSubstTyping; unfold AppLocalSubst;
      intro n; destruct n; [| apply TVar].
    apply TFunLocal; auto.
  Qed.

  Lemma AppGlobalSubstTyping : forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (τ σ : PirTyp)
                                 (C1 C2 : PirExpr),
      Γ;; (fun n => match n with
                | 0 => τ
                | 1 => PirExprFun τ σ
                | S (S n) => Δ n
                end) ⊢c C1 ::: σ ->
      Γ;; Δ ⊢c C2 ::: τ ->
      PirExprSubstTyping Γ (fun n => match n with
                               | 0 => τ
                               | 1 => PirExprFun τ σ
                               | S (S n) => Δ n
                               end) (AppGlobalSubst C1 C2) Δ.
  Proof using.
    intros Γ Δ τ σ C1 C2 typing1 typing2; unfold PirExprSubstTyping; unfold AppGlobalSubst;
      intro n; destruct n; [| destruct n]; auto; [| apply TVar].
    apply TFunGlobal; auto. 
  Qed.
  
  (* RELATIVE PRESERVATION *)
  (*
    If the local language enjoys preservation, then so does Pirouette.
   *)
  Theorem RelativePreservation :
    (forall (Γ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
        Γ ⊢e e ::: τ                                      (* Local Preservation *)
        -> forall e' : Expr, ExprStep e e'
                       -> Γ ⊢e e' ::: τ) ->
    forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (C : PirExpr) (τ : PirTyp),
      Γ;; Δ ⊢c C ::: τ -> forall (R : Redex) (B : list Loc) (C': PirExpr),
        PirStep R B C C' -> Γ;; Δ ⊢c C' ::: τ.   (* Pirouette Preservation *)
  Proof using.
    intros expr_pres Γ Δ C τ typing R B C' step; revert Γ Δ τ typing; induction step;
      intros Γ Δ τ typing; inversion typing; subst;
        repeat (match goal with
                | [ expr_typing : ?G ⊢e ?e ::: ?t, expr_step : ExprStep ?e ?e' |- _ ] =>
                  lazymatch goal with
                  | [_ : ?G ⊢e e' ::: t |- _ ] => fail
                  | _ => pose proof (expr_pres G e t expr_typing e' expr_step)
                  end
                end; eauto with PirExpr);
        try (econstructor; eauto with PirExpr; fail);
        auto.
    1,2: eapply PirExprLocalSubstTypingSpec; eauto.
    1,2: intro ℓ; apply ValueSubstTyping.
    2: inversion H6; subst.
    1,2: eapply ExprClosedTyping; eauto; apply ExprValuesClosed; auto.
    - eapply PirExprSubstTypingSpec; eauto; [apply AppLocalSubstTyping|];
        inversion H7; subst; eauto.
      eapply PirExprLocalSubstTypingSpec; eauto. intro ℓ; cbn.
      unfold ValueSubst. unfold ExprSubstTyping; intro n; destruct n; auto.
      all: destruct (L.eq_dec l ℓ); destruct (L.eq_dec ℓ l); subst;
        try match goal with
            | [H : ?a <> ?a |- _ ] => destruct (H eq_refl)
            end; auto.
      all: apply ExprVarTyping.
    - inversion H4; subst; eapply PirExprSubstTypingSpec; eauto.
      apply AppGlobalSubstTyping; auto.
  Qed.    

  Theorem RelativeStepsPreservation :
    (forall (Γ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
        Γ ⊢e e ::: τ
        -> forall e' : Expr, ExprStep e e'
                       -> Γ ⊢e e' ::: τ) ->
    forall (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (C : PirExpr) (τ : PirTyp),
      Γ;; Δ ⊢c C ::: τ -> forall (Rs : list Redex) (B : list Loc) (C': PirExpr),
        PirSteps Rs B C C' -> Γ;; Δ ⊢c C' ::: τ.
  Proof using.
    intros expr_pres Γ Δ C τ typing Rs; revert Γ Δ C τ typing; induction Rs;
      intros Γ Δ C τ typing B C' steps; inversion steps; subst; auto.
    rename a into R; clear steps; rename H1 into step; rename H5 into steps.
    eapply RelativePreservation in step; eauto.
  Qed.

  (* RELATIVE PROGRESS *)
  (*
    If the local language enjoys progress and boolean invertability, 
    then Pirouette also enjoys progress.
   *)

  Corollary RelativeProgress :
    (forall (e : Expr) (τ : ExprTyp) (Γ : nat -> ExprTyp),
        Γ ⊢e e ::: τ -> ExprClosed e ->                (* Local Progress *)
        ExprVal e \/ exists e' : Expr, ExprStep e e') ->
    (forall (v : Expr) (Γ : nat -> ExprTyp),
        Γ ⊢e v ::: bool -> ExprVal v -> {v = tt} + {v = ff}) ->  (* Boolean Inverstability *)
    forall (C : PirExpr) (Γ : Loc -> nat -> ExprTyp) (Δ : nat -> PirTyp) (τ : PirTyp),
      PirExprClosed C -> Γ;; Δ ⊢c C ::: τ ->  (* Pirouette Progress *)
      PirExprVal C \/ exists R C', PirStep R nil C C'.
  Proof using.
    intros ExprProgress BoolInversion C Γ Δ τ cc typ; revert cc; induction typ; intro cc;
      inversion cc; subst.
    2: inversion H2.
    2-5,8,9: right.
    8,9: left; constructor; auto.
    - destruct (ExprProgress e τ (Γ ℓ) ltac:(assumption) ltac:(assumption));
        [left; constructor; auto | right].
      destruct H0 as [e' step]; exists (RDone ℓ e e'); exists (Done ℓ e'); auto with PirExpr.
    - destruct (ExprProgress e τ (Γ ℓ₁) ltac:(assumption) ltac:(assumption))
        as [eval | [e' step]];
        eexists; eexists; eauto with PirExpr.
    - destruct (ExprProgress e bool (Γ ℓ) ltac:(assumption) ltac:(assumption))
        as [eval | [e' step]];
        [destruct (BoolInversion e (Γ ℓ) ltac:(assumption) eval); subst|].
      all: eexists; eexists; eauto with PirExpr.
    - eexists; eexists; eauto with PirExpr.
    - destruct (IHtyp1 ltac:(assumption)) as [cval | [R [C' step]]].
      inversion cval; subst; inversion typ1; subst.
      all: eexists; eexists; eauto with PirExpr.
    - fold PirExprClosed in H4; specialize (IHtyp H4); destruct IHtyp.
      inversion H0; subst; inversion typ; subst.
      destruct (ExprProgress _ _ _ H H6); [| destruct H1 as [e' step]];
        eexists; eexists; eauto with PirExpr.
      destruct H0 as [R [C' step]]; eexists; eexists; eauto with PirExpr.
    - specialize (IHtyp1 H3); specialize (IHtyp2 H4).
      destruct IHtyp1 as [IHtyp1 | [R [C' step]]]. 2: eexists; eexists; eauto with PirExpr.
      destruct IHtyp2 as [IHtyp2 | [R [C' step]]].
      all: inversion IHtyp1; subst; inversion typ1; subst.
      all: eexists; eexists; eauto with PirExpr.
  Qed.
  
End TypedPirouette.

