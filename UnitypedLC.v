Require Export LocalLang.
Require Export TypedLocalLang.
Require Export LambdaCalc.
Require Import Coq.Arith.PeanoNat.
Require Import Coq.Lists.List.

Module UnitypedLC <: (TypedLocalLang LambdaCalc).
  Include LambdaCalc.

  Inductive UniType := Ast.
  Definition ExprTyp := UniType.
  Definition ExprTypEqDec : forall tau sigma : ExprTyp, {tau = sigma} + {tau <> sigma}.
    intros τ σ; destruct τ; destruct σ; left; reflexivity.
  Qed.

  Reserved Notation "Γ ⊢e e ::: τ" (at level 30).
  Inductive Unityping : (nat -> ExprTyp) -> Expr -> ExprTyp -> Prop :=
  | TrivialTyping : forall Γ e, Γ ⊢e e ::: Ast
  where "Γ ⊢e e ::: τ" := (Unityping Γ e τ).
  Global Hint Constructors Unityping : LC.
  Definition ExprTyping := Unityping.

  Theorem ExprVarTyping : forall (Γ : nat -> ExprTyp) (n : nat),
      Γ ⊢e (ExprVar n) ::: (Γ n).
  Proof using. intros Γ n; destruct (Γ n); apply TrivialTyping. Qed.
  
  Theorem ExprTypingExt : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
      (forall n, Γ n = Δ n) ->
      Γ ⊢e e ::: τ ->
      Δ ⊢e e ::: τ.
  Proof using. intros Γ Δ e τ _ _; destruct τ; apply TrivialTyping. Qed.

  Theorem ExprTypingUnique : forall (Γ : nat -> ExprTyp) (e : Expr) (τ σ : ExprTyp),
      Γ ⊢e e ::: τ ->
      Γ ⊢e e ::: σ ->
      τ = σ.
  Proof using. intros _ _ τ σ _ _; destruct τ; destruct σ; reflexivity. Qed.

  Theorem ExprWeakening : forall (Γ Δ : nat -> ExprTyp) (ξ : nat -> nat) (e : Expr) (τ : ExprTyp),
      (forall n, Γ n = Δ (ξ n)) ->
      Γ ⊢e e ::: τ ->
      Δ ⊢e e ⟨e| ξ⟩ ::: τ.
  Proof using. intros _ Δ ξ e τ _ _; destruct τ; auto with LC. Qed.

  Theorem ExprClosedAboveTyping : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp) (n : nat),
      ExprClosedAbove n e -> (forall m, m < n -> Γ m = Δ m) -> Γ ⊢e e ::: τ -> Δ ⊢e e ::: τ.
  Proof using. intros _ Δ e τ _ _ _ _; destruct τ; auto with LC. Qed.
    
  Lemma ExprClosedTyping : forall (Γ Δ : nat -> ExprTyp) (e : Expr) (τ : ExprTyp),
      ExprClosed e -> Γ ⊢e e ::: τ -> Δ ⊢e e ::: τ.
  Proof.
    intros Γ Δ e τ H H0. unfold ExprClosed in H.
    apply ExprClosedAboveTyping with (Γ := Γ) (n := 0); auto.
    intros m H1. inversion H1.
  Qed.

  Lemma ExprValueTyping : forall (Γ Δ : nat -> ExprTyp) (v : Expr) (τ : ExprTyp),
      ExprVal v -> Γ ⊢e v ::: τ -> Δ ⊢e v ::: τ.
  Proof.
    intros Γ Δ v τ H H0. apply ExprClosedTyping with (Γ := Γ); auto.
    apply ExprValuesClosed; auto.
  Qed.
  
  Definition bool : ExprTyp := Ast.
  Theorem TrueTyping : forall (Γ : nat -> ExprTyp),
      Γ ⊢e tt ::: bool.
  Proof using. intro Γ; unfold bool; auto with LC. Qed.
  Theorem FalseTyping : forall (Γ : nat -> ExprTyp),
      Γ ⊢e ff ::: bool.
  Proof using. intro Γ; unfold bool; auto with LC. Qed.

  Definition ExprSubstTyping : (nat -> ExprTyp) -> (nat -> Expr) -> (nat -> ExprTyp) -> Prop :=
    fun Γ σ Δ => forall n : nat, Δ ⊢e (σ n) ::: (Γ n).
  Notation "Gamma ⊢es sigma ⊣ Delta"  := (ExprSubstTyping Gamma sigma Delta) (at level 30).
  Theorem ExprSubstType :
    forall (Γ Δ : nat -> ExprTyp) (sigma : nat -> Expr) (e : Expr) (τ : ExprTyp),
      Γ ⊢es sigma ⊣ Δ -> Γ ⊢e e ::: τ -> Δ ⊢e e [e| sigma ] ::: τ.
  Proof using. intros _ Δ σ e τ _ _; destruct τ; auto with LC. Qed.
                      
  Lemma ExprIdSubstTyping : forall (Γ : nat -> ExprTyp), Γ ⊢es ExprIdSubst ⊣ Γ.
  Proof.
    unfold ExprSubstTyping. intros Γ n. unfold ExprIdSubst. apply ExprVarTyping.
  Qed.                                                   

  Lemma ExprSubstLWeakening : forall (Γ Δ1 Δ2 : nat -> ExprTyp) (σ : nat -> Expr) (ξ : nat -> nat),
      (forall n, Δ1 n = Δ2 (ξ n)) ->
      Γ ⊢es σ ⊣ Δ1 ->
      Γ ⊢es fun n => (σ n) ⟨e|ξ⟩ ⊣ Δ2.
  Proof.
    intros Γ Δ1 Δ2 σ ξ subΔ typing.
    unfold ExprSubstTyping in *; intro n.
    eapply ExprWeakening; eauto.
  Qed.

  Lemma ExprSubstRWeakening : forall (Γ1 Γ2 Δ : nat -> ExprTyp) (σ : nat -> Expr) (ξ : nat -> nat),
      (forall n, Γ1 (ξ n) = Γ2 n) ->
      Γ1 ⊢es σ ⊣ Δ ->
      Γ2 ⊢es fun n => σ (ξ n) ⊣ Δ.
  Proof.
    intros Γ1 Γ2 Δ σ ξ subΓ typing.
    unfold ExprSubstTyping in *; intro n.
    rewrite <- subΓ. apply typing.
  Qed.
  
  Lemma ExprSubstTypeExpand : forall (Γ Δ : nat -> ExprTyp) (σ : nat -> Expr),
      Γ ⊢es σ ⊣ Δ ->
      forall τ : ExprTyp, ExprSubstTyping (fun n => match n with | 0 => τ | S n => Γ n end)
                                     (ExprUpSubst σ)
                                     (fun n => match n with |0 => τ | S n => Δ n end).
  Proof.
    intros Γ Δ σ typing τ.
    unfold ExprUpSubst.
    unfold ExprSubstTyping in *.
    intro m.
    unfold ExprUpSubst. destruct m; simpl.
    apply ExprVarTyping.
    apply ExprWeakening with (Γ := Δ); auto.
  Qed.

End UnitypedLC.
